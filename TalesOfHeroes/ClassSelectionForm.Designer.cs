﻿namespace TalesOfHeroes
{
    partial class ClassSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClassSelectionForm));
            this.button_folkhero = new System.Windows.Forms.Button();
            this.button_entertainer = new System.Windows.Forms.Button();
            this.button_charlatan = new System.Windows.Forms.Button();
            this.button_acolyte = new System.Windows.Forms.Button();
            this.button_noble = new System.Windows.Forms.Button();
            this.button_hermit = new System.Windows.Forms.Button();
            this.button_guildartisan = new System.Windows.Forms.Button();
            this.button_sage = new System.Windows.Forms.Button();
            this.button_outlander = new System.Windows.Forms.Button();
            this.button_criminal = new System.Windows.Forms.Button();
            this.button_sailor = new System.Windows.Forms.Button();
            this.button_soldier = new System.Windows.Forms.Button();
            this.button_urchin = new System.Windows.Forms.Button();
            this.button_submit = new System.Windows.Forms.Button();
            this.button_fighter = new System.Windows.Forms.Button();
            this.pictureBox_fighter = new System.Windows.Forms.PictureBox();
            this.button_monk = new System.Windows.Forms.Button();
            this.button_druid = new System.Windows.Forms.Button();
            this.button_bard = new System.Windows.Forms.Button();
            this.button_barbarian = new System.Windows.Forms.Button();
            this.button_sorcerer = new System.Windows.Forms.Button();
            this.button_ranger = new System.Windows.Forms.Button();
            this.button_paladin = new System.Windows.Forms.Button();
            this.button_rogue = new System.Windows.Forms.Button();
            this.button_cleric = new System.Windows.Forms.Button();
            this.button_warlock = new System.Windows.Forms.Button();
            this.button_wizard = new System.Windows.Forms.Button();
            this.pictureBox_barbarian = new System.Windows.Forms.PictureBox();
            this.pictureBox_paladin = new System.Windows.Forms.PictureBox();
            this.pictureBox_ranger = new System.Windows.Forms.PictureBox();
            this.pictureBox_rogue = new System.Windows.Forms.PictureBox();
            this.textBox_description = new System.Windows.Forms.TextBox();
            this.label_strength = new System.Windows.Forms.Label();
            this.label_agility = new System.Windows.Forms.Label();
            this.label_constitution = new System.Windows.Forms.Label();
            this.label_intellegence = new System.Windows.Forms.Label();
            this.label_wisdom = new System.Windows.Forms.Label();
            this.label_charisma = new System.Windows.Forms.Label();
            this.textBox_agility_mod = new System.Windows.Forms.TextBox();
            this.textBox_constitution_mod = new System.Windows.Forms.TextBox();
            this.textBox_intellegence_mod = new System.Windows.Forms.TextBox();
            this.textBox_wisdom_mod = new System.Windows.Forms.TextBox();
            this.textBox_charisma_mod = new System.Windows.Forms.TextBox();
            this.textBox_strength_mod = new System.Windows.Forms.TextBox();
            this.label_stat = new System.Windows.Forms.Label();
            this.label_modifier = new System.Windows.Forms.Label();
            this.label_hp = new System.Windows.Forms.Label();
            this.comboBox_chosenenemy = new System.Windows.Forms.ComboBox();
            this.label_chosenenemy = new System.Windows.Forms.Label();
            this.label_startingequipment = new System.Windows.Forms.Label();
            this.toolTip_abilities = new System.Windows.Forms.ToolTip(this.components);
            this.label_cantrips = new System.Windows.Forms.Label();
            this.label_classes = new System.Windows.Forms.Label();
            this.label_backgrounds = new System.Windows.Forms.Label();
            this.checkedListBox_cantrips = new System.Windows.Forms.CheckedListBox();
            this.comboBox_strength = new System.Windows.Forms.ComboBox();
            this.comboBox_wisdom = new System.Windows.Forms.ComboBox();
            this.comboBox_intellegence = new System.Windows.Forms.ComboBox();
            this.comboBox_constitution = new System.Windows.Forms.ComboBox();
            this.comboBox_agility = new System.Windows.Forms.ComboBox();
            this.comboBox_charisma = new System.Windows.Forms.ComboBox();
            this.button_help = new System.Windows.Forms.Button();
            this.pictureBox_conceal1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_fighter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_barbarian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_paladin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ranger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_rogue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_conceal1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_folkhero
            // 
            this.button_folkhero.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_folkhero.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_folkhero.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_folkhero.Location = new System.Drawing.Point(745, 244);
            this.button_folkhero.Name = "button_folkhero";
            this.button_folkhero.Size = new System.Drawing.Size(133, 42);
            this.button_folkhero.TabIndex = 23;
            this.button_folkhero.TabStop = false;
            this.button_folkhero.Text = "Folk Hero";
            this.button_folkhero.UseVisualStyleBackColor = true;
            this.button_folkhero.Click += new System.EventHandler(this.Button_folkhero_Click);
            // 
            // button_entertainer
            // 
            this.button_entertainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_entertainer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_entertainer.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_entertainer.Location = new System.Drawing.Point(745, 196);
            this.button_entertainer.Name = "button_entertainer";
            this.button_entertainer.Size = new System.Drawing.Size(133, 42);
            this.button_entertainer.TabIndex = 22;
            this.button_entertainer.TabStop = false;
            this.button_entertainer.Text = "Entertainer";
            this.button_entertainer.UseVisualStyleBackColor = true;
            this.button_entertainer.Click += new System.EventHandler(this.Button_entertainer_Click);
            // 
            // button_charlatan
            // 
            this.button_charlatan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_charlatan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_charlatan.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_charlatan.Location = new System.Drawing.Point(745, 100);
            this.button_charlatan.Name = "button_charlatan";
            this.button_charlatan.Size = new System.Drawing.Size(133, 42);
            this.button_charlatan.TabIndex = 21;
            this.button_charlatan.TabStop = false;
            this.button_charlatan.Text = "Charlatan";
            this.button_charlatan.UseVisualStyleBackColor = true;
            this.button_charlatan.Click += new System.EventHandler(this.Button_charlatan_Click);
            // 
            // button_acolyte
            // 
            this.button_acolyte.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_acolyte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_acolyte.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_acolyte.Location = new System.Drawing.Point(745, 52);
            this.button_acolyte.Name = "button_acolyte";
            this.button_acolyte.Size = new System.Drawing.Size(133, 42);
            this.button_acolyte.TabIndex = 20;
            this.button_acolyte.TabStop = false;
            this.button_acolyte.Text = "Acolyte";
            this.button_acolyte.UseVisualStyleBackColor = true;
            this.button_acolyte.Click += new System.EventHandler(this.Button_acolyte_Click);
            // 
            // button_noble
            // 
            this.button_noble.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_noble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_noble.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_noble.Location = new System.Drawing.Point(877, 148);
            this.button_noble.Name = "button_noble";
            this.button_noble.Size = new System.Drawing.Size(133, 42);
            this.button_noble.TabIndex = 19;
            this.button_noble.TabStop = false;
            this.button_noble.Text = "Noble";
            this.button_noble.UseVisualStyleBackColor = true;
            this.button_noble.Click += new System.EventHandler(this.Button_noble_Click);
            // 
            // button_hermit
            // 
            this.button_hermit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_hermit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_hermit.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_hermit.Location = new System.Drawing.Point(877, 52);
            this.button_hermit.Name = "button_hermit";
            this.button_hermit.Size = new System.Drawing.Size(133, 42);
            this.button_hermit.TabIndex = 18;
            this.button_hermit.TabStop = false;
            this.button_hermit.Text = "Hermit";
            this.button_hermit.UseVisualStyleBackColor = true;
            this.button_hermit.Click += new System.EventHandler(this.Button_hermit_Click);
            // 
            // button_guildartisan
            // 
            this.button_guildartisan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_guildartisan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_guildartisan.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_guildartisan.Location = new System.Drawing.Point(745, 292);
            this.button_guildartisan.Name = "button_guildartisan";
            this.button_guildartisan.Size = new System.Drawing.Size(133, 42);
            this.button_guildartisan.TabIndex = 17;
            this.button_guildartisan.TabStop = false;
            this.button_guildartisan.Text = "Guild Artisan";
            this.button_guildartisan.UseVisualStyleBackColor = true;
            this.button_guildartisan.Click += new System.EventHandler(this.Button_guildartisan_Click);
            // 
            // button_sage
            // 
            this.button_sage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_sage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_sage.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_sage.Location = new System.Drawing.Point(877, 196);
            this.button_sage.Name = "button_sage";
            this.button_sage.Size = new System.Drawing.Size(133, 42);
            this.button_sage.TabIndex = 16;
            this.button_sage.TabStop = false;
            this.button_sage.Text = "Sage";
            this.button_sage.UseVisualStyleBackColor = true;
            this.button_sage.Click += new System.EventHandler(this.Button_sage_Click);
            // 
            // button_outlander
            // 
            this.button_outlander.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_outlander.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_outlander.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_outlander.Location = new System.Drawing.Point(877, 100);
            this.button_outlander.Name = "button_outlander";
            this.button_outlander.Size = new System.Drawing.Size(133, 42);
            this.button_outlander.TabIndex = 15;
            this.button_outlander.TabStop = false;
            this.button_outlander.Text = "Outlander";
            this.button_outlander.UseVisualStyleBackColor = true;
            this.button_outlander.Click += new System.EventHandler(this.Button_outlander_Click);
            // 
            // button_criminal
            // 
            this.button_criminal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_criminal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_criminal.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_criminal.Location = new System.Drawing.Point(745, 148);
            this.button_criminal.Name = "button_criminal";
            this.button_criminal.Size = new System.Drawing.Size(133, 42);
            this.button_criminal.TabIndex = 14;
            this.button_criminal.TabStop = false;
            this.button_criminal.Text = "Criminal";
            this.button_criminal.UseVisualStyleBackColor = true;
            this.button_criminal.Click += new System.EventHandler(this.Button_criminal_Click);
            // 
            // button_sailor
            // 
            this.button_sailor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_sailor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_sailor.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_sailor.Location = new System.Drawing.Point(877, 244);
            this.button_sailor.Name = "button_sailor";
            this.button_sailor.Size = new System.Drawing.Size(133, 42);
            this.button_sailor.TabIndex = 13;
            this.button_sailor.TabStop = false;
            this.button_sailor.Text = "Sailor";
            this.button_sailor.UseVisualStyleBackColor = true;
            this.button_sailor.Click += new System.EventHandler(this.Button_sailor_Click);
            // 
            // button_soldier
            // 
            this.button_soldier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_soldier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_soldier.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_soldier.Location = new System.Drawing.Point(877, 292);
            this.button_soldier.Name = "button_soldier";
            this.button_soldier.Size = new System.Drawing.Size(133, 42);
            this.button_soldier.TabIndex = 12;
            this.button_soldier.TabStop = false;
            this.button_soldier.Text = "Soldier";
            this.button_soldier.UseVisualStyleBackColor = true;
            this.button_soldier.Click += new System.EventHandler(this.Button_soldier_Click);
            // 
            // button_urchin
            // 
            this.button_urchin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_urchin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_urchin.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_urchin.Location = new System.Drawing.Point(810, 340);
            this.button_urchin.Name = "button_urchin";
            this.button_urchin.Size = new System.Drawing.Size(133, 42);
            this.button_urchin.TabIndex = 24;
            this.button_urchin.TabStop = false;
            this.button_urchin.Text = "Urchin";
            this.button_urchin.UseVisualStyleBackColor = true;
            this.button_urchin.Click += new System.EventHandler(this.Button_urchin_Click);
            // 
            // button_submit
            // 
            this.button_submit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_submit.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_submit.Location = new System.Drawing.Point(54, 754);
            this.button_submit.Name = "button_submit";
            this.button_submit.Size = new System.Drawing.Size(133, 42);
            this.button_submit.TabIndex = 25;
            this.button_submit.TabStop = false;
            this.button_submit.Text = "Submit";
            this.button_submit.UseVisualStyleBackColor = true;
            this.button_submit.Click += new System.EventHandler(this.Button_submit_Click);
            // 
            // button_fighter
            // 
            this.button_fighter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_fighter.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_fighter.Location = new System.Drawing.Point(66, 244);
            this.button_fighter.Name = "button_fighter";
            this.button_fighter.Size = new System.Drawing.Size(133, 42);
            this.button_fighter.TabIndex = 16;
            this.button_fighter.TabStop = false;
            this.button_fighter.Text = "Fighter";
            this.button_fighter.UseVisualStyleBackColor = true;
            this.button_fighter.Click += new System.EventHandler(this.Button_fighter_Click);
            // 
            // pictureBox_fighter
            // 
            this.pictureBox_fighter.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_fighter.Image = global::TalesOfHeroes.Properties.Resources.Sword;
            this.pictureBox_fighter.Location = new System.Drawing.Point(19, 241);
            this.pictureBox_fighter.Name = "pictureBox_fighter";
            this.pictureBox_fighter.Size = new System.Drawing.Size(100, 50);
            this.pictureBox_fighter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_fighter.TabIndex = 62;
            this.pictureBox_fighter.TabStop = false;
            // 
            // button_monk
            // 
            this.button_monk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_monk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_monk.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_monk.Location = new System.Drawing.Point(66, 292);
            this.button_monk.Name = "button_monk";
            this.button_monk.Size = new System.Drawing.Size(133, 42);
            this.button_monk.TabIndex = 23;
            this.button_monk.TabStop = false;
            this.button_monk.Text = "Monk";
            this.button_monk.UseVisualStyleBackColor = true;
            this.button_monk.Click += new System.EventHandler(this.Button_monk_Click);
            // 
            // button_druid
            // 
            this.button_druid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_druid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_druid.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_druid.Location = new System.Drawing.Point(66, 196);
            this.button_druid.Name = "button_druid";
            this.button_druid.Size = new System.Drawing.Size(133, 42);
            this.button_druid.TabIndex = 22;
            this.button_druid.TabStop = false;
            this.button_druid.Text = "Druid";
            this.button_druid.UseVisualStyleBackColor = true;
            this.button_druid.Click += new System.EventHandler(this.Button_druid_Click);
            // 
            // button_bard
            // 
            this.button_bard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_bard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_bard.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_bard.Location = new System.Drawing.Point(66, 100);
            this.button_bard.Name = "button_bard";
            this.button_bard.Size = new System.Drawing.Size(133, 42);
            this.button_bard.TabIndex = 21;
            this.button_bard.TabStop = false;
            this.button_bard.Text = "Bard";
            this.button_bard.UseVisualStyleBackColor = true;
            this.button_bard.Click += new System.EventHandler(this.Button_bard_Click);
            // 
            // button_barbarian
            // 
            this.button_barbarian.BackColor = System.Drawing.SystemColors.Window;
            this.button_barbarian.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_barbarian.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_barbarian.Location = new System.Drawing.Point(66, 52);
            this.button_barbarian.Name = "button_barbarian";
            this.button_barbarian.Size = new System.Drawing.Size(133, 42);
            this.button_barbarian.TabIndex = 20;
            this.button_barbarian.TabStop = false;
            this.button_barbarian.Text = "Barbarian";
            this.button_barbarian.UseVisualStyleBackColor = false;
            this.button_barbarian.Click += new System.EventHandler(this.Button_barbarian_Click);
            // 
            // button_sorcerer
            // 
            this.button_sorcerer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_sorcerer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_sorcerer.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_sorcerer.Location = new System.Drawing.Point(198, 196);
            this.button_sorcerer.Name = "button_sorcerer";
            this.button_sorcerer.Size = new System.Drawing.Size(133, 42);
            this.button_sorcerer.TabIndex = 19;
            this.button_sorcerer.TabStop = false;
            this.button_sorcerer.Text = "Sorcerer";
            this.button_sorcerer.UseVisualStyleBackColor = true;
            this.button_sorcerer.Click += new System.EventHandler(this.Button_sorcerer_Click);
            // 
            // button_ranger
            // 
            this.button_ranger.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ranger.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ranger.Location = new System.Drawing.Point(198, 100);
            this.button_ranger.Name = "button_ranger";
            this.button_ranger.Size = new System.Drawing.Size(133, 42);
            this.button_ranger.TabIndex = 18;
            this.button_ranger.TabStop = false;
            this.button_ranger.Text = "Ranger";
            this.button_ranger.UseVisualStyleBackColor = true;
            this.button_ranger.Click += new System.EventHandler(this.Button_ranger_Click);
            // 
            // button_paladin
            // 
            this.button_paladin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_paladin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_paladin.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_paladin.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.button_paladin.Location = new System.Drawing.Point(198, 52);
            this.button_paladin.Name = "button_paladin";
            this.button_paladin.Size = new System.Drawing.Size(133, 42);
            this.button_paladin.TabIndex = 17;
            this.button_paladin.TabStop = false;
            this.button_paladin.Text = "Paladin";
            this.button_paladin.UseVisualStyleBackColor = true;
            this.button_paladin.Click += new System.EventHandler(this.Button_paladin_Click);
            // 
            // button_rogue
            // 
            this.button_rogue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_rogue.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_rogue.Location = new System.Drawing.Point(198, 148);
            this.button_rogue.Name = "button_rogue";
            this.button_rogue.Size = new System.Drawing.Size(133, 42);
            this.button_rogue.TabIndex = 15;
            this.button_rogue.TabStop = false;
            this.button_rogue.Text = "Rogue";
            this.button_rogue.UseVisualStyleBackColor = true;
            this.button_rogue.Click += new System.EventHandler(this.Button_rogue_Click);
            // 
            // button_cleric
            // 
            this.button_cleric.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_cleric.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_cleric.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_cleric.Location = new System.Drawing.Point(66, 148);
            this.button_cleric.Name = "button_cleric";
            this.button_cleric.Size = new System.Drawing.Size(133, 42);
            this.button_cleric.TabIndex = 14;
            this.button_cleric.TabStop = false;
            this.button_cleric.Text = "Cleric";
            this.button_cleric.UseVisualStyleBackColor = true;
            this.button_cleric.Click += new System.EventHandler(this.Button_cleric_Click);
            // 
            // button_warlock
            // 
            this.button_warlock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_warlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_warlock.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_warlock.Location = new System.Drawing.Point(198, 244);
            this.button_warlock.Name = "button_warlock";
            this.button_warlock.Size = new System.Drawing.Size(133, 42);
            this.button_warlock.TabIndex = 13;
            this.button_warlock.TabStop = false;
            this.button_warlock.Text = "Warlock";
            this.button_warlock.UseVisualStyleBackColor = true;
            this.button_warlock.Click += new System.EventHandler(this.Button_warlock_Click);
            // 
            // button_wizard
            // 
            this.button_wizard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_wizard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_wizard.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_wizard.Location = new System.Drawing.Point(198, 292);
            this.button_wizard.Name = "button_wizard";
            this.button_wizard.Size = new System.Drawing.Size(133, 42);
            this.button_wizard.TabIndex = 12;
            this.button_wizard.TabStop = false;
            this.button_wizard.Text = "Wizard";
            this.button_wizard.UseVisualStyleBackColor = true;
            this.button_wizard.Click += new System.EventHandler(this.Button_wizard_Click);
            // 
            // pictureBox_barbarian
            // 
            this.pictureBox_barbarian.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_barbarian.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_barbarian.Image")));
            this.pictureBox_barbarian.Location = new System.Drawing.Point(9, 33);
            this.pictureBox_barbarian.Name = "pictureBox_barbarian";
            this.pictureBox_barbarian.Size = new System.Drawing.Size(100, 50);
            this.pictureBox_barbarian.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_barbarian.TabIndex = 62;
            this.pictureBox_barbarian.TabStop = false;
            // 
            // pictureBox_paladin
            // 
            this.pictureBox_paladin.Image = global::TalesOfHeroes.Properties.Resources.Hammer;
            this.pictureBox_paladin.Location = new System.Drawing.Point(250, 12);
            this.pictureBox_paladin.Name = "pictureBox_paladin";
            this.pictureBox_paladin.Size = new System.Drawing.Size(109, 62);
            this.pictureBox_paladin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_paladin.TabIndex = 62;
            this.pictureBox_paladin.TabStop = false;
            // 
            // pictureBox_ranger
            // 
            this.pictureBox_ranger.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_ranger.Image = global::TalesOfHeroes.Properties.Resources.Arrows;
            this.pictureBox_ranger.Location = new System.Drawing.Point(307, 109);
            this.pictureBox_ranger.Name = "pictureBox_ranger";
            this.pictureBox_ranger.Size = new System.Drawing.Size(70, 22);
            this.pictureBox_ranger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_ranger.TabIndex = 62;
            this.pictureBox_ranger.TabStop = false;
            // 
            // pictureBox_rogue
            // 
            this.pictureBox_rogue.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_rogue.Image = global::TalesOfHeroes.Properties.Resources.Dagger;
            this.pictureBox_rogue.Location = new System.Drawing.Point(295, 152);
            this.pictureBox_rogue.Name = "pictureBox_rogue";
            this.pictureBox_rogue.Size = new System.Drawing.Size(87, 36);
            this.pictureBox_rogue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_rogue.TabIndex = 62;
            this.pictureBox_rogue.TabStop = false;
            // 
            // textBox_description
            // 
            this.textBox_description.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_description.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_description.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_description.Location = new System.Drawing.Point(391, 19);
            this.textBox_description.Multiline = true;
            this.textBox_description.Name = "textBox_description";
            this.textBox_description.ReadOnly = true;
            this.textBox_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_description.Size = new System.Drawing.Size(303, 389);
            this.textBox_description.TabIndex = 28;
            this.textBox_description.TabStop = false;
            this.textBox_description.MouseEnter += new System.EventHandler(this.textBox_description_MouseEnter);
            // 
            // label_strength
            // 
            this.label_strength.AutoSize = true;
            this.label_strength.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_strength.Location = new System.Drawing.Point(163, 443);
            this.label_strength.Name = "label_strength";
            this.label_strength.Size = new System.Drawing.Size(74, 22);
            this.label_strength.TabIndex = 35;
            this.label_strength.Text = "Strength";
            this.toolTip_abilities.SetToolTip(this.label_strength, "Measures: Natural athleticism, bodily power.\r\nIt affects your damage with melee w" +
        "eapons in combat.\r\nImportant for: Barbarian, fighter, paladin.");
            // 
            // label_agility
            // 
            this.label_agility.AutoSize = true;
            this.label_agility.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_agility.Location = new System.Drawing.Point(163, 483);
            this.label_agility.Name = "label_agility";
            this.label_agility.Size = new System.Drawing.Size(59, 22);
            this.label_agility.TabIndex = 36;
            this.label_agility.Text = "Agility";
            this.toolTip_abilities.SetToolTip(this.label_agility, "Measures: Physical agility, reflexes, balance, poise.\r\nIt helps you avoid damage " +
        "in combat, giving bonus\r\nto your AC.\r\nImportant for: Monk, ranger, rogue.");
            // 
            // label_constitution
            // 
            this.label_constitution.AutoSize = true;
            this.label_constitution.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_constitution.Location = new System.Drawing.Point(163, 523);
            this.label_constitution.Name = "label_constitution";
            this.label_constitution.Size = new System.Drawing.Size(105, 22);
            this.label_constitution.TabIndex = 37;
            this.label_constitution.Text = "Constitution";
            this.toolTip_abilities.SetToolTip(this.label_constitution, "Measures: Health, stamina, vital force.\r\nAffects how many Hit Points you gain wit" +
        "h each level.\r\nImportant for: Everyone.");
            // 
            // label_intellegence
            // 
            this.label_intellegence.AutoSize = true;
            this.label_intellegence.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_intellegence.Location = new System.Drawing.Point(163, 563);
            this.label_intellegence.Name = "label_intellegence";
            this.label_intellegence.Size = new System.Drawing.Size(104, 22);
            this.label_intellegence.TabIndex = 38;
            this.label_intellegence.Text = "Intellegence";
            this.toolTip_abilities.SetToolTip(this.label_intellegence, "Measures: Mental acuity, information recall, analytical skill.\r\nImportant for: Wi" +
        "zard.");
            // 
            // label_wisdom
            // 
            this.label_wisdom.AutoSize = true;
            this.label_wisdom.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_wisdom.Location = new System.Drawing.Point(163, 603);
            this.label_wisdom.Name = "label_wisdom";
            this.label_wisdom.Size = new System.Drawing.Size(77, 22);
            this.label_wisdom.TabIndex = 39;
            this.label_wisdom.Text = "Wisdom";
            this.toolTip_abilities.SetToolTip(this.label_wisdom, "Measures: Awareness, intuition, insight.\r\nImportant for: Cleric, druid.");
            // 
            // label_charisma
            // 
            this.label_charisma.AutoSize = true;
            this.label_charisma.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_charisma.Location = new System.Drawing.Point(163, 643);
            this.label_charisma.Name = "label_charisma";
            this.label_charisma.Size = new System.Drawing.Size(83, 22);
            this.label_charisma.TabIndex = 40;
            this.label_charisma.Text = "Charisma";
            this.toolTip_abilities.SetToolTip(this.label_charisma, "Measures: Confidence, eloquence, leadership.\r\nImportant for: Bard, sorcerer, warl" +
        "ock.");
            // 
            // textBox_agility_mod
            // 
            this.textBox_agility_mod.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_agility_mod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_agility_mod.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_agility_mod.Location = new System.Drawing.Point(100, 480);
            this.textBox_agility_mod.Name = "textBox_agility_mod";
            this.textBox_agility_mod.ReadOnly = true;
            this.textBox_agility_mod.Size = new System.Drawing.Size(46, 29);
            this.textBox_agility_mod.TabIndex = 46;
            this.textBox_agility_mod.TabStop = false;
            this.textBox_agility_mod.Text = "-";
            this.textBox_agility_mod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_constitution_mod
            // 
            this.textBox_constitution_mod.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_constitution_mod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_constitution_mod.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_constitution_mod.Location = new System.Drawing.Point(100, 520);
            this.textBox_constitution_mod.Name = "textBox_constitution_mod";
            this.textBox_constitution_mod.ReadOnly = true;
            this.textBox_constitution_mod.Size = new System.Drawing.Size(46, 29);
            this.textBox_constitution_mod.TabIndex = 45;
            this.textBox_constitution_mod.TabStop = false;
            this.textBox_constitution_mod.Text = "-";
            this.textBox_constitution_mod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_intellegence_mod
            // 
            this.textBox_intellegence_mod.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_intellegence_mod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_intellegence_mod.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_intellegence_mod.Location = new System.Drawing.Point(100, 560);
            this.textBox_intellegence_mod.Name = "textBox_intellegence_mod";
            this.textBox_intellegence_mod.ReadOnly = true;
            this.textBox_intellegence_mod.Size = new System.Drawing.Size(46, 29);
            this.textBox_intellegence_mod.TabIndex = 44;
            this.textBox_intellegence_mod.TabStop = false;
            this.textBox_intellegence_mod.Text = "-";
            this.textBox_intellegence_mod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_wisdom_mod
            // 
            this.textBox_wisdom_mod.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_wisdom_mod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_wisdom_mod.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_wisdom_mod.Location = new System.Drawing.Point(100, 600);
            this.textBox_wisdom_mod.Name = "textBox_wisdom_mod";
            this.textBox_wisdom_mod.ReadOnly = true;
            this.textBox_wisdom_mod.Size = new System.Drawing.Size(46, 29);
            this.textBox_wisdom_mod.TabIndex = 43;
            this.textBox_wisdom_mod.TabStop = false;
            this.textBox_wisdom_mod.Text = "-";
            this.textBox_wisdom_mod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_charisma_mod
            // 
            this.textBox_charisma_mod.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_charisma_mod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_charisma_mod.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_charisma_mod.Location = new System.Drawing.Point(100, 640);
            this.textBox_charisma_mod.Name = "textBox_charisma_mod";
            this.textBox_charisma_mod.ReadOnly = true;
            this.textBox_charisma_mod.Size = new System.Drawing.Size(46, 29);
            this.textBox_charisma_mod.TabIndex = 42;
            this.textBox_charisma_mod.TabStop = false;
            this.textBox_charisma_mod.Text = "-";
            this.textBox_charisma_mod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_strength_mod
            // 
            this.textBox_strength_mod.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_strength_mod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_strength_mod.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_strength_mod.Location = new System.Drawing.Point(100, 440);
            this.textBox_strength_mod.Name = "textBox_strength_mod";
            this.textBox_strength_mod.ReadOnly = true;
            this.textBox_strength_mod.Size = new System.Drawing.Size(46, 29);
            this.textBox_strength_mod.TabIndex = 41;
            this.textBox_strength_mod.TabStop = false;
            this.textBox_strength_mod.Text = "-";
            this.textBox_strength_mod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label_stat
            // 
            this.label_stat.AutoSize = true;
            this.label_stat.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_stat.Location = new System.Drawing.Point(51, 414);
            this.label_stat.Name = "label_stat";
            this.label_stat.Size = new System.Drawing.Size(31, 18);
            this.label_stat.TabIndex = 47;
            this.label_stat.Text = "Stat";
            // 
            // label_modifier
            // 
            this.label_modifier.AutoSize = true;
            this.label_modifier.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_modifier.Location = new System.Drawing.Point(92, 414);
            this.label_modifier.Name = "label_modifier";
            this.label_modifier.Size = new System.Drawing.Size(63, 18);
            this.label_modifier.TabIndex = 48;
            this.label_modifier.Text = "Modifier";
            // 
            // label_hp
            // 
            this.label_hp.AutoSize = true;
            this.label_hp.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_hp.Location = new System.Drawing.Point(386, 443);
            this.label_hp.Name = "label_hp";
            this.label_hp.Size = new System.Drawing.Size(156, 22);
            this.label_hp.TabIndex = 56;
            this.label_hp.Text = "Starting Hit Points:";
            // 
            // comboBox_chosenenemy
            // 
            this.comboBox_chosenenemy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_chosenenemy.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_chosenenemy.FormattingEnabled = true;
            this.comboBox_chosenenemy.Items.AddRange(new object[] {
            "Aberrations",
            "Angels",
            "Beasts",
            "Constructs",
            "Demons",
            "Dragons",
            "Elementals",
            "Fey",
            "Giants",
            "Humanoids",
            "Monstrosities",
            "Plants",
            "Undead"});
            this.comboBox_chosenenemy.Location = new System.Drawing.Point(391, 480);
            this.comboBox_chosenenemy.Name = "comboBox_chosenenemy";
            this.comboBox_chosenenemy.Size = new System.Drawing.Size(137, 30);
            this.comboBox_chosenenemy.Sorted = true;
            this.comboBox_chosenenemy.TabIndex = 57;
            this.comboBox_chosenenemy.TabStop = false;
            this.comboBox_chosenenemy.SelectedIndexChanged += new System.EventHandler(this.ComboBox_chosenenemy_SelectedIndexChanged);
            // 
            // label_chosenenemy
            // 
            this.label_chosenenemy.AutoSize = true;
            this.label_chosenenemy.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_chosenenemy.Location = new System.Drawing.Point(534, 483);
            this.label_chosenenemy.Name = "label_chosenenemy";
            this.label_chosenenemy.Size = new System.Drawing.Size(195, 22);
            this.label_chosenenemy.TabIndex = 58;
            this.label_chosenenemy.Text = "Choose a type of enemy";
            // 
            // label_startingequipment
            // 
            this.label_startingequipment.AutoSize = true;
            this.label_startingequipment.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_startingequipment.Location = new System.Drawing.Point(386, 523);
            this.label_startingequipment.Name = "label_startingequipment";
            this.label_startingequipment.Size = new System.Drawing.Size(161, 22);
            this.label_startingequipment.TabIndex = 59;
            this.label_startingequipment.Text = "Starting equipment:";
            // 
            // toolTip_abilities
            // 
            this.toolTip_abilities.AutoPopDelay = 5000;
            this.toolTip_abilities.BackColor = System.Drawing.SystemColors.Window;
            this.toolTip_abilities.InitialDelay = 100;
            this.toolTip_abilities.ReshowDelay = 100;
            // 
            // label_cantrips
            // 
            this.label_cantrips.AutoSize = true;
            this.label_cantrips.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_cantrips.Location = new System.Drawing.Point(841, 442);
            this.label_cantrips.Name = "label_cantrips";
            this.label_cantrips.Size = new System.Drawing.Size(75, 22);
            this.label_cantrips.TabIndex = 61;
            this.label_cantrips.Text = "Cantrips";
            this.toolTip_abilities.SetToolTip(this.label_cantrips, "Measures: Natural athleticism, bodily power.\r\nIt affects your damage with melee w" +
        "eapons in combat.\r\nImportant for: Barbarian, fighter, paladin.");
            // 
            // label_classes
            // 
            this.label_classes.AutoSize = true;
            this.label_classes.Font = new System.Drawing.Font("Baskerville Old Face", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_classes.Location = new System.Drawing.Point(148, 16);
            this.label_classes.Name = "label_classes";
            this.label_classes.Size = new System.Drawing.Size(98, 24);
            this.label_classes.TabIndex = 63;
            this.label_classes.Text = "CLASSES";
            this.toolTip_abilities.SetToolTip(this.label_classes, "Measures: Natural athleticism, bodily power.\r\nIt affects your damage with melee w" +
        "eapons in combat.\r\nImportant for: Barbarian, fighter, paladin.");
            // 
            // label_backgrounds
            // 
            this.label_backgrounds.AutoSize = true;
            this.label_backgrounds.Font = new System.Drawing.Font("Baskerville Old Face", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_backgrounds.Location = new System.Drawing.Point(793, 16);
            this.label_backgrounds.Name = "label_backgrounds";
            this.label_backgrounds.Size = new System.Drawing.Size(174, 24);
            this.label_backgrounds.TabIndex = 64;
            this.label_backgrounds.Text = "BACKGROUNDS";
            this.toolTip_abilities.SetToolTip(this.label_backgrounds, "Measures: Natural athleticism, bodily power.\r\nIt affects your damage with melee w" +
        "eapons in combat.\r\nImportant for: Barbarian, fighter, paladin.");
            // 
            // checkedListBox_cantrips
            // 
            this.checkedListBox_cantrips.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox_cantrips.CheckOnClick = true;
            this.checkedListBox_cantrips.Location = new System.Drawing.Point(813, 480);
            this.checkedListBox_cantrips.Name = "checkedListBox_cantrips";
            this.checkedListBox_cantrips.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.checkedListBox_cantrips.Size = new System.Drawing.Size(172, 300);
            this.checkedListBox_cantrips.Sorted = true;
            this.checkedListBox_cantrips.TabIndex = 60;
            this.checkedListBox_cantrips.TabStop = false;
            this.checkedListBox_cantrips.SelectedIndexChanged += new System.EventHandler(this.checkedListBox_cantrips_Click);
            // 
            // comboBox_strength
            // 
            this.comboBox_strength.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_strength.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_strength.FormattingEnabled = true;
            this.comboBox_strength.Items.AddRange(new object[] {
            "--",
            "8",
            "10",
            "12",
            "13",
            "14",
            "15"});
            this.comboBox_strength.Location = new System.Drawing.Point(48, 440);
            this.comboBox_strength.Name = "comboBox_strength";
            this.comboBox_strength.Size = new System.Drawing.Size(46, 30);
            this.comboBox_strength.TabIndex = 50;
            this.comboBox_strength.TabStop = false;
            this.comboBox_strength.Text = "--";
            this.comboBox_strength.SelectedIndexChanged += new System.EventHandler(this.ComboBox_strength_SelectedIndexChanged);
            // 
            // comboBox_wisdom
            // 
            this.comboBox_wisdom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_wisdom.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_wisdom.FormattingEnabled = true;
            this.comboBox_wisdom.Items.AddRange(new object[] {
            "--",
            "8",
            "10",
            "12",
            "13",
            "14",
            "15"});
            this.comboBox_wisdom.Location = new System.Drawing.Point(48, 600);
            this.comboBox_wisdom.Name = "comboBox_wisdom";
            this.comboBox_wisdom.Size = new System.Drawing.Size(46, 30);
            this.comboBox_wisdom.TabIndex = 54;
            this.comboBox_wisdom.TabStop = false;
            this.comboBox_wisdom.Text = "--";
            this.comboBox_wisdom.SelectedIndexChanged += new System.EventHandler(this.ComboBox_wisdom_SelectedIndexChanged);
            // 
            // comboBox_intellegence
            // 
            this.comboBox_intellegence.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_intellegence.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_intellegence.FormattingEnabled = true;
            this.comboBox_intellegence.Items.AddRange(new object[] {
            "--",
            "8",
            "10",
            "12",
            "13",
            "14",
            "15"});
            this.comboBox_intellegence.Location = new System.Drawing.Point(48, 560);
            this.comboBox_intellegence.Name = "comboBox_intellegence";
            this.comboBox_intellegence.Size = new System.Drawing.Size(46, 30);
            this.comboBox_intellegence.TabIndex = 53;
            this.comboBox_intellegence.TabStop = false;
            this.comboBox_intellegence.Text = "--";
            this.comboBox_intellegence.SelectedIndexChanged += new System.EventHandler(this.ComboBox_intellegence_SelectedIndexChanged);
            // 
            // comboBox_constitution
            // 
            this.comboBox_constitution.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_constitution.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_constitution.FormattingEnabled = true;
            this.comboBox_constitution.Items.AddRange(new object[] {
            "--",
            "8",
            "10",
            "12",
            "13",
            "14",
            "15"});
            this.comboBox_constitution.Location = new System.Drawing.Point(48, 520);
            this.comboBox_constitution.Name = "comboBox_constitution";
            this.comboBox_constitution.Size = new System.Drawing.Size(46, 30);
            this.comboBox_constitution.TabIndex = 52;
            this.comboBox_constitution.TabStop = false;
            this.comboBox_constitution.Text = "--";
            this.comboBox_constitution.SelectedIndexChanged += new System.EventHandler(this.ComboBox_constitution_SelectedIndexChanged);
            // 
            // comboBox_agility
            // 
            this.comboBox_agility.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_agility.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_agility.FormattingEnabled = true;
            this.comboBox_agility.Items.AddRange(new object[] {
            "--",
            "8",
            "10",
            "12",
            "13",
            "14",
            "15"});
            this.comboBox_agility.Location = new System.Drawing.Point(48, 480);
            this.comboBox_agility.Name = "comboBox_agility";
            this.comboBox_agility.Size = new System.Drawing.Size(46, 30);
            this.comboBox_agility.TabIndex = 51;
            this.comboBox_agility.TabStop = false;
            this.comboBox_agility.Text = "--";
            this.comboBox_agility.SelectedIndexChanged += new System.EventHandler(this.ComboBox_agility_SelectedIndexChanged);
            // 
            // comboBox_charisma
            // 
            this.comboBox_charisma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_charisma.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_charisma.FormattingEnabled = true;
            this.comboBox_charisma.Items.AddRange(new object[] {
            "--",
            "8",
            "10",
            "12",
            "13",
            "14",
            "15"});
            this.comboBox_charisma.Location = new System.Drawing.Point(48, 640);
            this.comboBox_charisma.Name = "comboBox_charisma";
            this.comboBox_charisma.Size = new System.Drawing.Size(46, 30);
            this.comboBox_charisma.TabIndex = 55;
            this.comboBox_charisma.TabStop = false;
            this.comboBox_charisma.Text = "--";
            this.comboBox_charisma.SelectedIndexChanged += new System.EventHandler(this.ComboBox_charisma_SelectedIndexChanged);
            // 
            // button_help
            // 
            this.button_help.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_help.Font = new System.Drawing.Font("Baskerville Old Face", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_help.Location = new System.Drawing.Point(1064, 19);
            this.button_help.Name = "button_help";
            this.button_help.Size = new System.Drawing.Size(60, 36);
            this.button_help.TabIndex = 25;
            this.button_help.TabStop = false;
            this.button_help.Text = "Help";
            this.button_help.UseVisualStyleBackColor = true;
            // 
            // pictureBox_conceal1
            // 
            this.pictureBox_conceal1.Location = new System.Drawing.Point(677, 19);
            this.pictureBox_conceal1.Name = "pictureBox_conceal1";
            this.pictureBox_conceal1.Size = new System.Drawing.Size(40, 389);
            this.pictureBox_conceal1.TabIndex = 65;
            this.pictureBox_conceal1.TabStop = false;
            // 
            // ClassSelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1450, 808);
            this.Controls.Add(this.pictureBox_conceal1);
            this.Controls.Add(this.label_backgrounds);
            this.Controls.Add(this.label_classes);
            this.Controls.Add(this.button_urchin);
            this.Controls.Add(this.button_fighter);
            this.Controls.Add(this.button_folkhero);
            this.Controls.Add(this.pictureBox_fighter);
            this.Controls.Add(this.button_entertainer);
            this.Controls.Add(this.button_help);
            this.Controls.Add(this.button_charlatan);
            this.Controls.Add(this.button_monk);
            this.Controls.Add(this.button_acolyte);
            this.Controls.Add(this.label_cantrips);
            this.Controls.Add(this.button_noble);
            this.Controls.Add(this.button_druid);
            this.Controls.Add(this.button_hermit);
            this.Controls.Add(this.checkedListBox_cantrips);
            this.Controls.Add(this.button_guildartisan);
            this.Controls.Add(this.button_bard);
            this.Controls.Add(this.button_sage);
            this.Controls.Add(this.label_startingequipment);
            this.Controls.Add(this.button_outlander);
            this.Controls.Add(this.button_barbarian);
            this.Controls.Add(this.button_criminal);
            this.Controls.Add(this.label_chosenenemy);
            this.Controls.Add(this.button_sailor);
            this.Controls.Add(this.button_sorcerer);
            this.Controls.Add(this.button_soldier);
            this.Controls.Add(this.comboBox_chosenenemy);
            this.Controls.Add(this.button_ranger);
            this.Controls.Add(this.label_hp);
            this.Controls.Add(this.button_paladin);
            this.Controls.Add(this.comboBox_charisma);
            this.Controls.Add(this.button_rogue);
            this.Controls.Add(this.button_cleric);
            this.Controls.Add(this.comboBox_wisdom);
            this.Controls.Add(this.button_warlock);
            this.Controls.Add(this.comboBox_intellegence);
            this.Controls.Add(this.button_wizard);
            this.Controls.Add(this.comboBox_constitution);
            this.Controls.Add(this.pictureBox_barbarian);
            this.Controls.Add(this.comboBox_agility);
            this.Controls.Add(this.pictureBox_paladin);
            this.Controls.Add(this.comboBox_strength);
            this.Controls.Add(this.pictureBox_ranger);
            this.Controls.Add(this.label_modifier);
            this.Controls.Add(this.pictureBox_rogue);
            this.Controls.Add(this.label_stat);
            this.Controls.Add(this.textBox_agility_mod);
            this.Controls.Add(this.textBox_constitution_mod);
            this.Controls.Add(this.textBox_intellegence_mod);
            this.Controls.Add(this.textBox_wisdom_mod);
            this.Controls.Add(this.textBox_charisma_mod);
            this.Controls.Add(this.textBox_strength_mod);
            this.Controls.Add(this.label_charisma);
            this.Controls.Add(this.label_wisdom);
            this.Controls.Add(this.label_intellegence);
            this.Controls.Add(this.label_constitution);
            this.Controls.Add(this.label_agility);
            this.Controls.Add(this.label_strength);
            this.Controls.Add(this.textBox_description);
            this.Controls.Add(this.button_submit);
            this.Name = "ClassSelectionForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ClassSelectionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_fighter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_barbarian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_paladin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ranger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_rogue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_conceal1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_folkhero;
        private System.Windows.Forms.Button button_entertainer;
        private System.Windows.Forms.Button button_charlatan;
        private System.Windows.Forms.Button button_acolyte;
        private System.Windows.Forms.Button button_noble;
        private System.Windows.Forms.Button button_hermit;
        private System.Windows.Forms.Button button_guildartisan;
        private System.Windows.Forms.Button button_sage;
        private System.Windows.Forms.Button button_outlander;
        private System.Windows.Forms.Button button_criminal;
        private System.Windows.Forms.Button button_sailor;
        private System.Windows.Forms.Button button_soldier;
        private System.Windows.Forms.Button button_urchin;
        private System.Windows.Forms.Button button_submit;
        private System.Windows.Forms.Button button_monk;
        private System.Windows.Forms.Button button_druid;
        private System.Windows.Forms.Button button_bard;
        private System.Windows.Forms.Button button_barbarian;
        private System.Windows.Forms.Button button_sorcerer;
        private System.Windows.Forms.Button button_ranger;
        private System.Windows.Forms.Button button_paladin;
        private System.Windows.Forms.Button button_fighter;
        private System.Windows.Forms.Button button_rogue;
        private System.Windows.Forms.Button button_cleric;
        private System.Windows.Forms.Button button_warlock;
        private System.Windows.Forms.Button button_wizard;
        private System.Windows.Forms.TextBox textBox_description;
        private System.Windows.Forms.Label label_strength;
        private System.Windows.Forms.Label label_agility;
        private System.Windows.Forms.Label label_constitution;
        private System.Windows.Forms.Label label_intellegence;
        private System.Windows.Forms.Label label_wisdom;
        private System.Windows.Forms.Label label_charisma;
        private System.Windows.Forms.TextBox textBox_agility_mod;
        private System.Windows.Forms.TextBox textBox_constitution_mod;
        private System.Windows.Forms.TextBox textBox_intellegence_mod;
        private System.Windows.Forms.TextBox textBox_wisdom_mod;
        private System.Windows.Forms.TextBox textBox_charisma_mod;
        private System.Windows.Forms.TextBox textBox_strength_mod;
        private System.Windows.Forms.Label label_stat;
        private System.Windows.Forms.Label label_modifier;
        private System.Windows.Forms.Label label_hp;
        private System.Windows.Forms.ComboBox comboBox_chosenenemy;
        private System.Windows.Forms.Label label_chosenenemy;
        private System.Windows.Forms.Label label_startingequipment;
        private System.Windows.Forms.ToolTip toolTip_abilities;
        private System.Windows.Forms.CheckedListBox checkedListBox_cantrips;
        private System.Windows.Forms.Label label_cantrips;
        private System.Windows.Forms.ComboBox comboBox_strength;
        private System.Windows.Forms.ComboBox comboBox_wisdom;
        private System.Windows.Forms.ComboBox comboBox_intellegence;
        private System.Windows.Forms.ComboBox comboBox_constitution;
        private System.Windows.Forms.ComboBox comboBox_agility;
        private System.Windows.Forms.ComboBox comboBox_charisma;
        private System.Windows.Forms.PictureBox pictureBox_barbarian;
        private System.Windows.Forms.PictureBox pictureBox_fighter;
        private System.Windows.Forms.PictureBox pictureBox_paladin;
        private System.Windows.Forms.PictureBox pictureBox_ranger;
        private System.Windows.Forms.PictureBox pictureBox_rogue;
        private System.Windows.Forms.Button button_help;
        private System.Windows.Forms.Label label_classes;
        private System.Windows.Forms.Label label_backgrounds;
        private System.Windows.Forms.PictureBox pictureBox_conceal1;
    }
}