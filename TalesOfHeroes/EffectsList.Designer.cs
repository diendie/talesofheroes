﻿namespace TalesOfHeroes
{
    partial class EffectsList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgw_effect = new System.Windows.Forms.DataGridView();
            this.effect_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effect_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effect_Behaviour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effect_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effect_Stat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effect_change = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effect_Duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_effect_selection = new System.Windows.Forms.GroupBox();
            this.cb_effect_selection = new System.Windows.Forms.ComboBox();
            this.l_effect_selection = new System.Windows.Forms.Label();
            this.b_list_all = new System.Windows.Forms.Button();
            this.b_list_active = new System.Windows.Forms.Button();
            this.gb_player = new System.Windows.Forms.GroupBox();
            this.l_health = new System.Windows.Forms.Label();
            this.b_create = new System.Windows.Forms.Button();
            this.b_apply_selected = new System.Windows.Forms.Button();
            this.l_exp_rem = new System.Windows.Forms.Label();
            this.l_exp_cur = new System.Windows.Forms.Label();
            this.l_level = new System.Windows.Forms.Label();
            this.b_intup = new System.Windows.Forms.Button();
            this.b_agiup = new System.Windows.Forms.Button();
            this.b_strup = new System.Windows.Forms.Button();
            this.b_intdwn = new System.Windows.Forms.Button();
            this.b_agidwn = new System.Windows.Forms.Button();
            this.b_strdwn = new System.Windows.Forms.Button();
            this.l_int = new System.Windows.Forms.Label();
            this.l_agi = new System.Windows.Forms.Label();
            this.l_str = new System.Windows.Forms.Label();
            this.tb_playerName = new System.Windows.Forms.TextBox();
            this.b_next_turn = new System.Windows.Forms.Button();
            this.gb_Entity = new System.Windows.Forms.GroupBox();
            this.b_entity_LA2 = new System.Windows.Forms.Button();
            this.b_entity_LA1 = new System.Windows.Forms.Button();
            this.b_apply_entity2 = new System.Windows.Forms.Button();
            this.l_entity_health2 = new System.Windows.Forms.Label();
            this.l_entity_name2 = new System.Windows.Forms.Label();
            this.b_apply_entity1 = new System.Windows.Forms.Button();
            this.l_entity_health1 = new System.Windows.Forms.Label();
            this.l_entity_name1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgw_effect)).BeginInit();
            this.gb_effect_selection.SuspendLayout();
            this.gb_player.SuspendLayout();
            this.gb_Entity.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgw_effect
            // 
            this.dgw_effect.AllowUserToAddRows = false;
            this.dgw_effect.AllowUserToDeleteRows = false;
            this.dgw_effect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgw_effect.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.effect_ID,
            this.effect_Name,
            this.effect_Behaviour,
            this.effect_type,
            this.effect_Stat,
            this.effect_change,
            this.effect_Duration});
            this.dgw_effect.Dock = System.Windows.Forms.DockStyle.Right;
            this.dgw_effect.Location = new System.Drawing.Point(221, 0);
            this.dgw_effect.Name = "dgw_effect";
            this.dgw_effect.ReadOnly = true;
            this.dgw_effect.Size = new System.Drawing.Size(759, 515);
            this.dgw_effect.TabIndex = 0;
            // 
            // effect_ID
            // 
            this.effect_ID.HeaderText = "ID";
            this.effect_ID.Name = "effect_ID";
            this.effect_ID.ReadOnly = true;
            this.effect_ID.Width = 43;
            // 
            // effect_Name
            // 
            this.effect_Name.HeaderText = "Name";
            this.effect_Name.Name = "effect_Name";
            this.effect_Name.ReadOnly = true;
            this.effect_Name.Width = 150;
            // 
            // effect_Behaviour
            // 
            this.effect_Behaviour.HeaderText = "Behaviour";
            this.effect_Behaviour.Name = "effect_Behaviour";
            this.effect_Behaviour.ReadOnly = true;
            this.effect_Behaviour.Width = 80;
            // 
            // effect_type
            // 
            this.effect_type.HeaderText = "Type";
            this.effect_type.Name = "effect_type";
            this.effect_type.ReadOnly = true;
            // 
            // effect_Stat
            // 
            this.effect_Stat.HeaderText = "Stat";
            this.effect_Stat.Name = "effect_Stat";
            this.effect_Stat.ReadOnly = true;
            this.effect_Stat.Width = 51;
            // 
            // effect_change
            // 
            this.effect_change.HeaderText = "Changes";
            this.effect_change.Name = "effect_change";
            this.effect_change.ReadOnly = true;
            this.effect_change.Width = 74;
            // 
            // effect_Duration
            // 
            this.effect_Duration.HeaderText = "Duration";
            this.effect_Duration.Name = "effect_Duration";
            this.effect_Duration.ReadOnly = true;
            this.effect_Duration.Width = 72;
            // 
            // gb_effect_selection
            // 
            this.gb_effect_selection.Controls.Add(this.cb_effect_selection);
            this.gb_effect_selection.Controls.Add(this.l_effect_selection);
            this.gb_effect_selection.Location = new System.Drawing.Point(12, 12);
            this.gb_effect_selection.Name = "gb_effect_selection";
            this.gb_effect_selection.Size = new System.Drawing.Size(200, 63);
            this.gb_effect_selection.TabIndex = 1;
            this.gb_effect_selection.TabStop = false;
            this.gb_effect_selection.Text = "Effect selection";
            // 
            // cb_effect_selection
            // 
            this.cb_effect_selection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_effect_selection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_effect_selection.FormattingEnabled = true;
            this.cb_effect_selection.Location = new System.Drawing.Point(9, 32);
            this.cb_effect_selection.Name = "cb_effect_selection";
            this.cb_effect_selection.Size = new System.Drawing.Size(176, 21);
            this.cb_effect_selection.Sorted = true;
            this.cb_effect_selection.TabIndex = 1;
            this.cb_effect_selection.TextChanged += new System.EventHandler(this.cb_effect_selection_TextChanged);
            // 
            // l_effect_selection
            // 
            this.l_effect_selection.AutoSize = true;
            this.l_effect_selection.Location = new System.Drawing.Point(6, 16);
            this.l_effect_selection.Name = "l_effect_selection";
            this.l_effect_selection.Size = new System.Drawing.Size(130, 13);
            this.l_effect_selection.TabIndex = 0;
            this.l_effect_selection.Text = "Select an Effect to display";
            // 
            // b_list_all
            // 
            this.b_list_all.Location = new System.Drawing.Point(12, 81);
            this.b_list_all.Name = "b_list_all";
            this.b_list_all.Size = new System.Drawing.Size(89, 27);
            this.b_list_all.TabIndex = 2;
            this.b_list_all.Text = "List all";
            this.b_list_all.UseVisualStyleBackColor = true;
            this.b_list_all.Click += new System.EventHandler(this.b_list_all_Click);
            // 
            // b_list_active
            // 
            this.b_list_active.Enabled = false;
            this.b_list_active.Location = new System.Drawing.Point(123, 81);
            this.b_list_active.Name = "b_list_active";
            this.b_list_active.Size = new System.Drawing.Size(89, 27);
            this.b_list_active.TabIndex = 3;
            this.b_list_active.Text = "List active";
            this.b_list_active.UseVisualStyleBackColor = true;
            this.b_list_active.Click += new System.EventHandler(this.b_list_active_Click);
            // 
            // gb_player
            // 
            this.gb_player.Controls.Add(this.l_health);
            this.gb_player.Controls.Add(this.b_create);
            this.gb_player.Controls.Add(this.b_apply_selected);
            this.gb_player.Controls.Add(this.l_exp_rem);
            this.gb_player.Controls.Add(this.l_exp_cur);
            this.gb_player.Controls.Add(this.l_level);
            this.gb_player.Controls.Add(this.b_intup);
            this.gb_player.Controls.Add(this.b_agiup);
            this.gb_player.Controls.Add(this.b_strup);
            this.gb_player.Controls.Add(this.b_intdwn);
            this.gb_player.Controls.Add(this.b_agidwn);
            this.gb_player.Controls.Add(this.b_strdwn);
            this.gb_player.Controls.Add(this.l_int);
            this.gb_player.Controls.Add(this.l_agi);
            this.gb_player.Controls.Add(this.l_str);
            this.gb_player.Controls.Add(this.tb_playerName);
            this.gb_player.Location = new System.Drawing.Point(19, 147);
            this.gb_player.Name = "gb_player";
            this.gb_player.Size = new System.Drawing.Size(193, 186);
            this.gb_player.TabIndex = 4;
            this.gb_player.TabStop = false;
            this.gb_player.Text = "Player";
            // 
            // l_health
            // 
            this.l_health.AutoSize = true;
            this.l_health.Location = new System.Drawing.Point(6, 158);
            this.l_health.Name = "l_health";
            this.l_health.Size = new System.Drawing.Size(35, 13);
            this.l_health.TabIndex = 15;
            this.l_health.Text = "label1";
            this.l_health.Visible = false;
            // 
            // b_create
            // 
            this.b_create.Location = new System.Drawing.Point(7, 132);
            this.b_create.Name = "b_create";
            this.b_create.Size = new System.Drawing.Size(75, 23);
            this.b_create.TabIndex = 13;
            this.b_create.Text = "Create";
            this.b_create.UseVisualStyleBackColor = true;
            this.b_create.Click += new System.EventHandler(this.b_create_Click);
            // 
            // b_apply_selected
            // 
            this.b_apply_selected.Enabled = false;
            this.b_apply_selected.Location = new System.Drawing.Point(89, 132);
            this.b_apply_selected.Name = "b_apply_selected";
            this.b_apply_selected.Size = new System.Drawing.Size(89, 23);
            this.b_apply_selected.TabIndex = 5;
            this.b_apply_selected.Text = "Apply selected";
            this.b_apply_selected.UseVisualStyleBackColor = true;
            this.b_apply_selected.Click += new System.EventHandler(this.b_apply_selected_Click);
            // 
            // l_exp_rem
            // 
            this.l_exp_rem.AutoSize = true;
            this.l_exp_rem.Location = new System.Drawing.Point(117, 108);
            this.l_exp_rem.Name = "l_exp_rem";
            this.l_exp_rem.Size = new System.Drawing.Size(35, 13);
            this.l_exp_rem.TabIndex = 12;
            this.l_exp_rem.Text = "label1";
            this.l_exp_rem.Visible = false;
            // 
            // l_exp_cur
            // 
            this.l_exp_cur.AutoSize = true;
            this.l_exp_cur.Location = new System.Drawing.Point(117, 79);
            this.l_exp_cur.Name = "l_exp_cur";
            this.l_exp_cur.Size = new System.Drawing.Size(35, 13);
            this.l_exp_cur.TabIndex = 11;
            this.l_exp_cur.Text = "label1";
            this.l_exp_cur.Visible = false;
            // 
            // l_level
            // 
            this.l_level.AutoSize = true;
            this.l_level.Location = new System.Drawing.Point(117, 50);
            this.l_level.Name = "l_level";
            this.l_level.Size = new System.Drawing.Size(35, 13);
            this.l_level.TabIndex = 10;
            this.l_level.Text = "label1";
            this.l_level.Visible = false;
            // 
            // b_intup
            // 
            this.b_intup.Location = new System.Drawing.Point(76, 103);
            this.b_intup.Name = "b_intup";
            this.b_intup.Size = new System.Drawing.Size(23, 23);
            this.b_intup.TabIndex = 9;
            this.b_intup.Text = "+";
            this.b_intup.UseVisualStyleBackColor = true;
            this.b_intup.Click += new System.EventHandler(this.b_intup_Click);
            // 
            // b_agiup
            // 
            this.b_agiup.Location = new System.Drawing.Point(76, 74);
            this.b_agiup.Name = "b_agiup";
            this.b_agiup.Size = new System.Drawing.Size(23, 23);
            this.b_agiup.TabIndex = 8;
            this.b_agiup.Text = "+";
            this.b_agiup.UseVisualStyleBackColor = true;
            this.b_agiup.Click += new System.EventHandler(this.b_agiup_Click);
            // 
            // b_strup
            // 
            this.b_strup.Location = new System.Drawing.Point(76, 45);
            this.b_strup.Name = "b_strup";
            this.b_strup.Size = new System.Drawing.Size(23, 23);
            this.b_strup.TabIndex = 7;
            this.b_strup.Text = "+";
            this.b_strup.UseVisualStyleBackColor = true;
            this.b_strup.Click += new System.EventHandler(this.b_strup_Click);
            // 
            // b_intdwn
            // 
            this.b_intdwn.Enabled = false;
            this.b_intdwn.Location = new System.Drawing.Point(6, 103);
            this.b_intdwn.Name = "b_intdwn";
            this.b_intdwn.Size = new System.Drawing.Size(23, 23);
            this.b_intdwn.TabIndex = 6;
            this.b_intdwn.Text = "-";
            this.b_intdwn.UseVisualStyleBackColor = true;
            this.b_intdwn.Click += new System.EventHandler(this.b_intdwn_Click);
            // 
            // b_agidwn
            // 
            this.b_agidwn.Enabled = false;
            this.b_agidwn.Location = new System.Drawing.Point(6, 74);
            this.b_agidwn.Name = "b_agidwn";
            this.b_agidwn.Size = new System.Drawing.Size(23, 23);
            this.b_agidwn.TabIndex = 5;
            this.b_agidwn.Text = "-";
            this.b_agidwn.UseVisualStyleBackColor = true;
            this.b_agidwn.Click += new System.EventHandler(this.b_agidwn_Click);
            // 
            // b_strdwn
            // 
            this.b_strdwn.Enabled = false;
            this.b_strdwn.Location = new System.Drawing.Point(6, 45);
            this.b_strdwn.Name = "b_strdwn";
            this.b_strdwn.Size = new System.Drawing.Size(23, 23);
            this.b_strdwn.TabIndex = 4;
            this.b_strdwn.Text = "-";
            this.b_strdwn.UseVisualStyleBackColor = true;
            this.b_strdwn.Click += new System.EventHandler(this.b_strdwn_Click);
            // 
            // l_int
            // 
            this.l_int.AutoSize = true;
            this.l_int.Location = new System.Drawing.Point(46, 108);
            this.l_int.Name = "l_int";
            this.l_int.Size = new System.Drawing.Size(13, 13);
            this.l_int.TabIndex = 3;
            this.l_int.Text = "5";
            // 
            // l_agi
            // 
            this.l_agi.AutoSize = true;
            this.l_agi.Location = new System.Drawing.Point(46, 79);
            this.l_agi.Name = "l_agi";
            this.l_agi.Size = new System.Drawing.Size(13, 13);
            this.l_agi.TabIndex = 2;
            this.l_agi.Text = "5";
            // 
            // l_str
            // 
            this.l_str.AutoSize = true;
            this.l_str.Location = new System.Drawing.Point(46, 50);
            this.l_str.Name = "l_str";
            this.l_str.Size = new System.Drawing.Size(13, 13);
            this.l_str.TabIndex = 1;
            this.l_str.Text = "5";
            // 
            // tb_playerName
            // 
            this.tb_playerName.Location = new System.Drawing.Point(6, 19);
            this.tb_playerName.Name = "tb_playerName";
            this.tb_playerName.Size = new System.Drawing.Size(165, 20);
            this.tb_playerName.TabIndex = 0;
            this.tb_playerName.Text = "Lama";
            // 
            // b_next_turn
            // 
            this.b_next_turn.Enabled = false;
            this.b_next_turn.Location = new System.Drawing.Point(68, 476);
            this.b_next_turn.Name = "b_next_turn";
            this.b_next_turn.Size = new System.Drawing.Size(89, 27);
            this.b_next_turn.TabIndex = 6;
            this.b_next_turn.Text = "Simulate turn";
            this.b_next_turn.UseVisualStyleBackColor = true;
            this.b_next_turn.Click += new System.EventHandler(this.b_next_turn_Click);
            // 
            // gb_Entity
            // 
            this.gb_Entity.Controls.Add(this.b_entity_LA2);
            this.gb_Entity.Controls.Add(this.b_entity_LA1);
            this.gb_Entity.Controls.Add(this.b_apply_entity2);
            this.gb_Entity.Controls.Add(this.l_entity_health2);
            this.gb_Entity.Controls.Add(this.l_entity_name2);
            this.gb_Entity.Controls.Add(this.b_apply_entity1);
            this.gb_Entity.Controls.Add(this.l_entity_health1);
            this.gb_Entity.Controls.Add(this.l_entity_name1);
            this.gb_Entity.Location = new System.Drawing.Point(19, 339);
            this.gb_Entity.Name = "gb_Entity";
            this.gb_Entity.Size = new System.Drawing.Size(193, 119);
            this.gb_Entity.TabIndex = 7;
            this.gb_Entity.TabStop = false;
            this.gb_Entity.Text = "Entity";
            // 
            // b_entity_LA2
            // 
            this.b_entity_LA2.Location = new System.Drawing.Point(96, 84);
            this.b_entity_LA2.Name = "b_entity_LA2";
            this.b_entity_LA2.Size = new System.Drawing.Size(89, 29);
            this.b_entity_LA2.TabIndex = 10;
            this.b_entity_LA2.Text = "List active";
            this.b_entity_LA2.UseVisualStyleBackColor = true;
            this.b_entity_LA2.Click += new System.EventHandler(this.b_entity_LA2_Click);
            // 
            // b_entity_LA1
            // 
            this.b_entity_LA1.Location = new System.Drawing.Point(6, 84);
            this.b_entity_LA1.Name = "b_entity_LA1";
            this.b_entity_LA1.Size = new System.Drawing.Size(89, 29);
            this.b_entity_LA1.TabIndex = 8;
            this.b_entity_LA1.Text = "List active";
            this.b_entity_LA1.UseVisualStyleBackColor = true;
            this.b_entity_LA1.Click += new System.EventHandler(this.b_entity_LA1_Click);
            // 
            // b_apply_entity2
            // 
            this.b_apply_entity2.Location = new System.Drawing.Point(96, 55);
            this.b_apply_entity2.Name = "b_apply_entity2";
            this.b_apply_entity2.Size = new System.Drawing.Size(89, 23);
            this.b_apply_entity2.TabIndex = 9;
            this.b_apply_entity2.Text = "Apply selected";
            this.b_apply_entity2.UseVisualStyleBackColor = true;
            this.b_apply_entity2.Click += new System.EventHandler(this.b_apply_entity2_Click);
            // 
            // l_entity_health2
            // 
            this.l_entity_health2.AutoSize = true;
            this.l_entity_health2.Location = new System.Drawing.Point(96, 39);
            this.l_entity_health2.Name = "l_entity_health2";
            this.l_entity_health2.Size = new System.Drawing.Size(74, 13);
            this.l_entity_health2.TabIndex = 8;
            this.l_entity_health2.Text = "Entity health 2";
            // 
            // l_entity_name2
            // 
            this.l_entity_name2.AutoSize = true;
            this.l_entity_name2.Location = new System.Drawing.Point(96, 16);
            this.l_entity_name2.Name = "l_entity_name2";
            this.l_entity_name2.Size = new System.Drawing.Size(39, 13);
            this.l_entity_name2.TabIndex = 7;
            this.l_entity_name2.Text = "Entity2";
            // 
            // b_apply_entity1
            // 
            this.b_apply_entity1.Location = new System.Drawing.Point(6, 55);
            this.b_apply_entity1.Name = "b_apply_entity1";
            this.b_apply_entity1.Size = new System.Drawing.Size(89, 23);
            this.b_apply_entity1.TabIndex = 6;
            this.b_apply_entity1.Text = "Apply selected";
            this.b_apply_entity1.UseVisualStyleBackColor = true;
            this.b_apply_entity1.Click += new System.EventHandler(this.b_apply_entity1_Click);
            // 
            // l_entity_health1
            // 
            this.l_entity_health1.AutoSize = true;
            this.l_entity_health1.Location = new System.Drawing.Point(6, 39);
            this.l_entity_health1.Name = "l_entity_health1";
            this.l_entity_health1.Size = new System.Drawing.Size(74, 13);
            this.l_entity_health1.TabIndex = 1;
            this.l_entity_health1.Text = "Entity health 1";
            // 
            // l_entity_name1
            // 
            this.l_entity_name1.AutoSize = true;
            this.l_entity_name1.Location = new System.Drawing.Point(6, 16);
            this.l_entity_name1.Name = "l_entity_name1";
            this.l_entity_name1.Size = new System.Drawing.Size(39, 13);
            this.l_entity_name1.TabIndex = 0;
            this.l_entity_name1.Text = "Entity1";
            // 
            // EffectsList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 515);
            this.Controls.Add(this.gb_Entity);
            this.Controls.Add(this.b_next_turn);
            this.Controls.Add(this.gb_player);
            this.Controls.Add(this.b_list_active);
            this.Controls.Add(this.b_list_all);
            this.Controls.Add(this.gb_effect_selection);
            this.Controls.Add(this.dgw_effect);
            this.Name = "EffectsList";
            this.Text = "EffectsList";
            ((System.ComponentModel.ISupportInitialize)(this.dgw_effect)).EndInit();
            this.gb_effect_selection.ResumeLayout(false);
            this.gb_effect_selection.PerformLayout();
            this.gb_player.ResumeLayout(false);
            this.gb_player.PerformLayout();
            this.gb_Entity.ResumeLayout(false);
            this.gb_Entity.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgw_effect;
        private System.Windows.Forms.GroupBox gb_effect_selection;
        private System.Windows.Forms.ComboBox cb_effect_selection;
        private System.Windows.Forms.Label l_effect_selection;
        private System.Windows.Forms.Button b_list_all;
        private System.Windows.Forms.Button b_list_active;
        private System.Windows.Forms.GroupBox gb_player;
        private System.Windows.Forms.Label l_health;
        private System.Windows.Forms.Button b_create;
        private System.Windows.Forms.Label l_exp_rem;
        private System.Windows.Forms.Label l_exp_cur;
        private System.Windows.Forms.Label l_level;
        private System.Windows.Forms.Button b_intup;
        private System.Windows.Forms.Button b_agiup;
        private System.Windows.Forms.Button b_strup;
        private System.Windows.Forms.Button b_intdwn;
        private System.Windows.Forms.Button b_agidwn;
        private System.Windows.Forms.Button b_strdwn;
        private System.Windows.Forms.Label l_int;
        private System.Windows.Forms.Label l_agi;
        private System.Windows.Forms.Label l_str;
        private System.Windows.Forms.TextBox tb_playerName;
        private System.Windows.Forms.Button b_apply_selected;
        private System.Windows.Forms.Button b_next_turn;
        private System.Windows.Forms.GroupBox gb_Entity;
        private System.Windows.Forms.Label l_entity_health1;
        private System.Windows.Forms.Label l_entity_name1;
        private System.Windows.Forms.Button b_apply_entity1;
        private System.Windows.Forms.Button b_entity_LA2;
        private System.Windows.Forms.Button b_entity_LA1;
        private System.Windows.Forms.Button b_apply_entity2;
        private System.Windows.Forms.Label l_entity_health2;
        private System.Windows.Forms.Label l_entity_name2;
        private System.Windows.Forms.DataGridViewTextBoxColumn effect_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn effect_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn effect_Behaviour;
        private System.Windows.Forms.DataGridViewTextBoxColumn effect_type;
        private System.Windows.Forms.DataGridViewTextBoxColumn effect_Stat;
        private System.Windows.Forms.DataGridViewTextBoxColumn effect_change;
        private System.Windows.Forms.DataGridViewTextBoxColumn effect_Duration;
    }
}