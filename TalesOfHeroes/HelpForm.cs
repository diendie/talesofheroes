﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.IO;
using System.Media;

namespace TalesOfHeroes
{
    public partial class HelpForm : Form
    {
        PrivateFontCollection MyFont = new PrivateFontCollection();
        public HelpForm()
        {
            InitializeComponent();
        }

        private void HelpForm_Load(object sender, EventArgs e)
        {
            InterfaceSettings();
        }
        
        void InterfaceSettings()
        {
            MyFont.AddFontFile("..\\..\\Resources\\EnchantedLand.otf");

            foreach (Control Elements in this.Controls)
            {
                Elements.Font = new Font(MyFont.Families[0], 20);
            }

            foreach (Control Elements in tabPage_links.Controls)
            {
                Elements.Font = new Font(MyFont.Families[0], 20);
            }

            foreach (Control Elements in tabPage_classes.Controls)
            {
                Elements.Font = new Font(MyFont.Families[0], 20);
            }
        }

        private void label_classes_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTab = tabPage_classes;
        }

    }
}
