﻿namespace TalesOfHeroes
{
    partial class DebugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_playerName = new System.Windows.Forms.TextBox();
            this.gb_player = new System.Windows.Forms.GroupBox();
            this.l_health = new System.Windows.Forms.Label();
            this.b_addExp = new System.Windows.Forms.Button();
            this.b_create = new System.Windows.Forms.Button();
            this.l_exp_rem = new System.Windows.Forms.Label();
            this.l_exp_cur = new System.Windows.Forms.Label();
            this.l_level = new System.Windows.Forms.Label();
            this.b_intup = new System.Windows.Forms.Button();
            this.b_agiup = new System.Windows.Forms.Button();
            this.b_strup = new System.Windows.Forms.Button();
            this.b_intdwn = new System.Windows.Forms.Button();
            this.b_agidwn = new System.Windows.Forms.Button();
            this.b_strdwn = new System.Windows.Forms.Button();
            this.l_int = new System.Windows.Forms.Label();
            this.l_agi = new System.Windows.Forms.Label();
            this.l_str = new System.Windows.Forms.Label();
            this.gb_entity_create = new System.Windows.Forms.GroupBox();
            this.b_ent_spawn = new System.Windows.Forms.Button();
            this.tb_ent_amount = new System.Windows.Forms.TextBox();
            this.l_ent_amount = new System.Windows.Forms.Label();
            this.b_ent_type = new System.Windows.Forms.Label();
            this.cb_ent_type = new System.Windows.Forms.ComboBox();
            this.gb_list_alive = new System.Windows.Forms.GroupBox();
            this.dgw_alive = new System.Windows.Forms.DataGridView();
            this.ent_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ent_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alive_health = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ent_behaviour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ent_Str = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ent_Agi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ent_Int = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_Kill = new System.Windows.Forms.GroupBox();
            this.b_ent_kill = new System.Windows.Forms.Button();
            this.tb_kill_id = new System.Windows.Forms.TextBox();
            this.l_kill_id = new System.Windows.Forms.Label();
            this.gb_list_dead = new System.Windows.Forms.GroupBox();
            this.dgw_dead = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dead_health = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_damage = new System.Windows.Forms.GroupBox();
            this.b_damage_inflict = new System.Windows.Forms.Button();
            this.rb_heal = new System.Windows.Forms.RadioButton();
            this.rb_damage = new System.Windows.Forms.RadioButton();
            this.tb_damage_amount = new System.Windows.Forms.TextBox();
            this.label_hp_amount = new System.Windows.Forms.Label();
            this.tb_damage_id = new System.Windows.Forms.TextBox();
            this.l_damage_id = new System.Windows.Forms.Label();
            this.gb_player_damage = new System.Windows.Forms.GroupBox();
            this.b_kill_player = new System.Windows.Forms.Button();
            this.b_inflict_player = new System.Windows.Forms.Button();
            this.rb_player_heal = new System.Windows.Forms.RadioButton();
            this.rb_player_damage = new System.Windows.Forms.RadioButton();
            this.tb_damage_player = new System.Windows.Forms.TextBox();
            this.l_damage_player = new System.Windows.Forms.Label();
            this.gb_grouping = new System.Windows.Forms.GroupBox();
            this.b_grouping_kill = new System.Windows.Forms.Button();
            this.l_grouping_selected = new System.Windows.Forms.Label();
            this.b_grouping4 = new System.Windows.Forms.Button();
            this.b_grouping3 = new System.Windows.Forms.Button();
            this.b_grouping2 = new System.Windows.Forms.Button();
            this.b_grouping5 = new System.Windows.Forms.Button();
            this.b_grouping1 = new System.Windows.Forms.Button();
            this.gb_effects = new System.Windows.Forms.GroupBox();
            this.b_effects_panel = new System.Windows.Forms.Button();
            this.gb_player.SuspendLayout();
            this.gb_entity_create.SuspendLayout();
            this.gb_list_alive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgw_alive)).BeginInit();
            this.gb_Kill.SuspendLayout();
            this.gb_list_dead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgw_dead)).BeginInit();
            this.gb_damage.SuspendLayout();
            this.gb_player_damage.SuspendLayout();
            this.gb_grouping.SuspendLayout();
            this.gb_effects.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_playerName
            // 
            this.tb_playerName.Location = new System.Drawing.Point(6, 19);
            this.tb_playerName.Name = "tb_playerName";
            this.tb_playerName.Size = new System.Drawing.Size(165, 20);
            this.tb_playerName.TabIndex = 0;
            // 
            // gb_player
            // 
            this.gb_player.Controls.Add(this.l_health);
            this.gb_player.Controls.Add(this.b_addExp);
            this.gb_player.Controls.Add(this.b_create);
            this.gb_player.Controls.Add(this.l_exp_rem);
            this.gb_player.Controls.Add(this.l_exp_cur);
            this.gb_player.Controls.Add(this.l_level);
            this.gb_player.Controls.Add(this.b_intup);
            this.gb_player.Controls.Add(this.b_agiup);
            this.gb_player.Controls.Add(this.b_strup);
            this.gb_player.Controls.Add(this.b_intdwn);
            this.gb_player.Controls.Add(this.b_agidwn);
            this.gb_player.Controls.Add(this.b_strdwn);
            this.gb_player.Controls.Add(this.l_int);
            this.gb_player.Controls.Add(this.l_agi);
            this.gb_player.Controls.Add(this.l_str);
            this.gb_player.Controls.Add(this.tb_playerName);
            this.gb_player.Location = new System.Drawing.Point(12, 12);
            this.gb_player.Name = "gb_player";
            this.gb_player.Size = new System.Drawing.Size(178, 186);
            this.gb_player.TabIndex = 1;
            this.gb_player.TabStop = false;
            this.gb_player.Text = "Player";
            // 
            // l_health
            // 
            this.l_health.AutoSize = true;
            this.l_health.Location = new System.Drawing.Point(6, 158);
            this.l_health.Name = "l_health";
            this.l_health.Size = new System.Drawing.Size(35, 13);
            this.l_health.TabIndex = 15;
            this.l_health.Text = "label1";
            this.l_health.Visible = false;
            // 
            // b_addExp
            // 
            this.b_addExp.Location = new System.Drawing.Point(96, 132);
            this.b_addExp.Name = "b_addExp";
            this.b_addExp.Size = new System.Drawing.Size(75, 23);
            this.b_addExp.TabIndex = 14;
            this.b_addExp.Text = "Add exp";
            this.b_addExp.UseVisualStyleBackColor = true;
            this.b_addExp.Click += new System.EventHandler(this.b_addExp_Click);
            // 
            // b_create
            // 
            this.b_create.Location = new System.Drawing.Point(7, 132);
            this.b_create.Name = "b_create";
            this.b_create.Size = new System.Drawing.Size(75, 23);
            this.b_create.TabIndex = 13;
            this.b_create.Text = "Create";
            this.b_create.UseVisualStyleBackColor = true;
            this.b_create.Click += new System.EventHandler(this.b_create_Click);
            // 
            // l_exp_rem
            // 
            this.l_exp_rem.AutoSize = true;
            this.l_exp_rem.Location = new System.Drawing.Point(117, 108);
            this.l_exp_rem.Name = "l_exp_rem";
            this.l_exp_rem.Size = new System.Drawing.Size(35, 13);
            this.l_exp_rem.TabIndex = 12;
            this.l_exp_rem.Text = "label1";
            this.l_exp_rem.Visible = false;
            // 
            // l_exp_cur
            // 
            this.l_exp_cur.AutoSize = true;
            this.l_exp_cur.Location = new System.Drawing.Point(117, 79);
            this.l_exp_cur.Name = "l_exp_cur";
            this.l_exp_cur.Size = new System.Drawing.Size(35, 13);
            this.l_exp_cur.TabIndex = 11;
            this.l_exp_cur.Text = "label1";
            this.l_exp_cur.Visible = false;
            // 
            // l_level
            // 
            this.l_level.AutoSize = true;
            this.l_level.Location = new System.Drawing.Point(117, 50);
            this.l_level.Name = "l_level";
            this.l_level.Size = new System.Drawing.Size(35, 13);
            this.l_level.TabIndex = 10;
            this.l_level.Text = "label1";
            this.l_level.Visible = false;
            // 
            // b_intup
            // 
            this.b_intup.Location = new System.Drawing.Point(76, 103);
            this.b_intup.Name = "b_intup";
            this.b_intup.Size = new System.Drawing.Size(23, 23);
            this.b_intup.TabIndex = 9;
            this.b_intup.Text = "+";
            this.b_intup.UseVisualStyleBackColor = true;
            this.b_intup.Click += new System.EventHandler(this.b_intup_Click);
            // 
            // b_agiup
            // 
            this.b_agiup.Location = new System.Drawing.Point(76, 74);
            this.b_agiup.Name = "b_agiup";
            this.b_agiup.Size = new System.Drawing.Size(23, 23);
            this.b_agiup.TabIndex = 8;
            this.b_agiup.Text = "+";
            this.b_agiup.UseVisualStyleBackColor = true;
            this.b_agiup.Click += new System.EventHandler(this.b_agiup_Click);
            // 
            // b_strup
            // 
            this.b_strup.Location = new System.Drawing.Point(76, 45);
            this.b_strup.Name = "b_strup";
            this.b_strup.Size = new System.Drawing.Size(23, 23);
            this.b_strup.TabIndex = 7;
            this.b_strup.Text = "+";
            this.b_strup.UseVisualStyleBackColor = true;
            this.b_strup.Click += new System.EventHandler(this.b_strup_Click);
            // 
            // b_intdwn
            // 
            this.b_intdwn.Enabled = false;
            this.b_intdwn.Location = new System.Drawing.Point(6, 103);
            this.b_intdwn.Name = "b_intdwn";
            this.b_intdwn.Size = new System.Drawing.Size(23, 23);
            this.b_intdwn.TabIndex = 6;
            this.b_intdwn.Text = "-";
            this.b_intdwn.UseVisualStyleBackColor = true;
            this.b_intdwn.Click += new System.EventHandler(this.b_intdwn_Click);
            // 
            // b_agidwn
            // 
            this.b_agidwn.Enabled = false;
            this.b_agidwn.Location = new System.Drawing.Point(6, 74);
            this.b_agidwn.Name = "b_agidwn";
            this.b_agidwn.Size = new System.Drawing.Size(23, 23);
            this.b_agidwn.TabIndex = 5;
            this.b_agidwn.Text = "-";
            this.b_agidwn.UseVisualStyleBackColor = true;
            this.b_agidwn.Click += new System.EventHandler(this.b_agidwn_Click);
            // 
            // b_strdwn
            // 
            this.b_strdwn.Enabled = false;
            this.b_strdwn.Location = new System.Drawing.Point(6, 45);
            this.b_strdwn.Name = "b_strdwn";
            this.b_strdwn.Size = new System.Drawing.Size(23, 23);
            this.b_strdwn.TabIndex = 4;
            this.b_strdwn.Text = "-";
            this.b_strdwn.UseVisualStyleBackColor = true;
            this.b_strdwn.Click += new System.EventHandler(this.b_strdwn_Click);
            // 
            // l_int
            // 
            this.l_int.AutoSize = true;
            this.l_int.Location = new System.Drawing.Point(46, 108);
            this.l_int.Name = "l_int";
            this.l_int.Size = new System.Drawing.Size(13, 13);
            this.l_int.TabIndex = 3;
            this.l_int.Text = "0";
            // 
            // l_agi
            // 
            this.l_agi.AutoSize = true;
            this.l_agi.Location = new System.Drawing.Point(46, 79);
            this.l_agi.Name = "l_agi";
            this.l_agi.Size = new System.Drawing.Size(13, 13);
            this.l_agi.TabIndex = 2;
            this.l_agi.Text = "0";
            // 
            // l_str
            // 
            this.l_str.AutoSize = true;
            this.l_str.Location = new System.Drawing.Point(46, 50);
            this.l_str.Name = "l_str";
            this.l_str.Size = new System.Drawing.Size(13, 13);
            this.l_str.TabIndex = 1;
            this.l_str.Text = "0";
            // 
            // gb_entity_create
            // 
            this.gb_entity_create.Controls.Add(this.b_ent_spawn);
            this.gb_entity_create.Controls.Add(this.tb_ent_amount);
            this.gb_entity_create.Controls.Add(this.l_ent_amount);
            this.gb_entity_create.Controls.Add(this.b_ent_type);
            this.gb_entity_create.Controls.Add(this.cb_ent_type);
            this.gb_entity_create.Location = new System.Drawing.Point(12, 204);
            this.gb_entity_create.Name = "gb_entity_create";
            this.gb_entity_create.Size = new System.Drawing.Size(178, 127);
            this.gb_entity_create.TabIndex = 2;
            this.gb_entity_create.TabStop = false;
            this.gb_entity_create.Text = "Entity creation";
            // 
            // b_ent_spawn
            // 
            this.b_ent_spawn.Location = new System.Drawing.Point(49, 98);
            this.b_ent_spawn.Name = "b_ent_spawn";
            this.b_ent_spawn.Size = new System.Drawing.Size(75, 23);
            this.b_ent_spawn.TabIndex = 14;
            this.b_ent_spawn.Text = "Spawn";
            this.b_ent_spawn.UseVisualStyleBackColor = true;
            this.b_ent_spawn.Click += new System.EventHandler(this.b_ent_spawn_Click);
            // 
            // tb_ent_amount
            // 
            this.tb_ent_amount.Location = new System.Drawing.Point(5, 72);
            this.tb_ent_amount.Name = "tb_ent_amount";
            this.tb_ent_amount.Size = new System.Drawing.Size(165, 20);
            this.tb_ent_amount.TabIndex = 14;
            // 
            // l_ent_amount
            // 
            this.l_ent_amount.AutoSize = true;
            this.l_ent_amount.Location = new System.Drawing.Point(6, 56);
            this.l_ent_amount.Name = "l_ent_amount";
            this.l_ent_amount.Size = new System.Drawing.Size(75, 13);
            this.l_ent_amount.TabIndex = 14;
            this.l_ent_amount.Text = "Select amount";
            // 
            // b_ent_type
            // 
            this.b_ent_type.AutoSize = true;
            this.b_ent_type.Location = new System.Drawing.Point(6, 16);
            this.b_ent_type.Name = "b_ent_type";
            this.b_ent_type.Size = new System.Drawing.Size(60, 13);
            this.b_ent_type.TabIndex = 14;
            this.b_ent_type.Text = "Select type";
            // 
            // cb_ent_type
            // 
            this.cb_ent_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_ent_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_ent_type.FormattingEnabled = true;
            this.cb_ent_type.Location = new System.Drawing.Point(5, 32);
            this.cb_ent_type.Name = "cb_ent_type";
            this.cb_ent_type.Size = new System.Drawing.Size(166, 21);
            this.cb_ent_type.TabIndex = 0;
            // 
            // gb_list_alive
            // 
            this.gb_list_alive.Controls.Add(this.dgw_alive);
            this.gb_list_alive.Location = new System.Drawing.Point(196, 12);
            this.gb_list_alive.Name = "gb_list_alive";
            this.gb_list_alive.Size = new System.Drawing.Size(708, 227);
            this.gb_list_alive.TabIndex = 3;
            this.gb_list_alive.TabStop = false;
            this.gb_list_alive.Text = "Alive";
            // 
            // dgw_alive
            // 
            this.dgw_alive.AllowUserToAddRows = false;
            this.dgw_alive.AllowUserToDeleteRows = false;
            this.dgw_alive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgw_alive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ent_ID,
            this.ent_Name,
            this.alive_health,
            this.ent_behaviour,
            this.ent_Str,
            this.ent_Agi,
            this.ent_Int});
            this.dgw_alive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgw_alive.Location = new System.Drawing.Point(3, 16);
            this.dgw_alive.Name = "dgw_alive";
            this.dgw_alive.ReadOnly = true;
            this.dgw_alive.Size = new System.Drawing.Size(702, 208);
            this.dgw_alive.TabIndex = 0;
            // 
            // ent_ID
            // 
            this.ent_ID.HeaderText = "ID";
            this.ent_ID.Name = "ent_ID";
            this.ent_ID.ReadOnly = true;
            this.ent_ID.Width = 50;
            // 
            // ent_Name
            // 
            this.ent_Name.HeaderText = "Name";
            this.ent_Name.Name = "ent_Name";
            this.ent_Name.ReadOnly = true;
            // 
            // alive_health
            // 
            this.alive_health.HeaderText = "Health";
            this.alive_health.Name = "alive_health";
            this.alive_health.ReadOnly = true;
            // 
            // ent_behaviour
            // 
            this.ent_behaviour.HeaderText = "Behaviour";
            this.ent_behaviour.Name = "ent_behaviour";
            this.ent_behaviour.ReadOnly = true;
            // 
            // ent_Str
            // 
            this.ent_Str.HeaderText = "Strength";
            this.ent_Str.Name = "ent_Str";
            this.ent_Str.ReadOnly = true;
            // 
            // ent_Agi
            // 
            this.ent_Agi.HeaderText = "Agility";
            this.ent_Agi.Name = "ent_Agi";
            this.ent_Agi.ReadOnly = true;
            // 
            // ent_Int
            // 
            this.ent_Int.HeaderText = "Intellect";
            this.ent_Int.Name = "ent_Int";
            this.ent_Int.ReadOnly = true;
            // 
            // gb_Kill
            // 
            this.gb_Kill.Controls.Add(this.b_ent_kill);
            this.gb_Kill.Controls.Add(this.tb_kill_id);
            this.gb_Kill.Controls.Add(this.l_kill_id);
            this.gb_Kill.Location = new System.Drawing.Point(12, 337);
            this.gb_Kill.Name = "gb_Kill";
            this.gb_Kill.Size = new System.Drawing.Size(178, 87);
            this.gb_Kill.TabIndex = 4;
            this.gb_Kill.TabStop = false;
            this.gb_Kill.Text = "Kill by ID";
            // 
            // b_ent_kill
            // 
            this.b_ent_kill.Location = new System.Drawing.Point(49, 58);
            this.b_ent_kill.Name = "b_ent_kill";
            this.b_ent_kill.Size = new System.Drawing.Size(75, 23);
            this.b_ent_kill.TabIndex = 15;
            this.b_ent_kill.Text = "Kill";
            this.b_ent_kill.UseVisualStyleBackColor = true;
            this.b_ent_kill.Click += new System.EventHandler(this.b_ent_kill_Click);
            // 
            // tb_kill_id
            // 
            this.tb_kill_id.Location = new System.Drawing.Point(7, 32);
            this.tb_kill_id.Name = "tb_kill_id";
            this.tb_kill_id.Size = new System.Drawing.Size(165, 20);
            this.tb_kill_id.TabIndex = 15;
            // 
            // l_kill_id
            // 
            this.l_kill_id.AutoSize = true;
            this.l_kill_id.Location = new System.Drawing.Point(6, 16);
            this.l_kill_id.Name = "l_kill_id";
            this.l_kill_id.Size = new System.Drawing.Size(46, 13);
            this.l_kill_id.TabIndex = 15;
            this.l_kill_id.Text = "Enter ID";
            // 
            // gb_list_dead
            // 
            this.gb_list_dead.Controls.Add(this.dgw_dead);
            this.gb_list_dead.Location = new System.Drawing.Point(196, 245);
            this.gb_list_dead.Name = "gb_list_dead";
            this.gb_list_dead.Size = new System.Drawing.Size(708, 227);
            this.gb_list_dead.TabIndex = 4;
            this.gb_list_dead.TabStop = false;
            this.gb_list_dead.Text = "Dead";
            // 
            // dgw_dead
            // 
            this.dgw_dead.AllowUserToAddRows = false;
            this.dgw_dead.AllowUserToDeleteRows = false;
            this.dgw_dead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgw_dead.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dead_health,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.dgw_dead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgw_dead.Location = new System.Drawing.Point(3, 16);
            this.dgw_dead.Name = "dgw_dead";
            this.dgw_dead.ReadOnly = true;
            this.dgw_dead.Size = new System.Drawing.Size(702, 208);
            this.dgw_dead.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dead_health
            // 
            this.dead_health.HeaderText = "Health";
            this.dead_health.Name = "dead_health";
            this.dead_health.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Behaviour";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Strength";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Agility";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Intellect";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // gb_damage
            // 
            this.gb_damage.Controls.Add(this.b_damage_inflict);
            this.gb_damage.Controls.Add(this.rb_heal);
            this.gb_damage.Controls.Add(this.rb_damage);
            this.gb_damage.Controls.Add(this.tb_damage_amount);
            this.gb_damage.Controls.Add(this.label_hp_amount);
            this.gb_damage.Controls.Add(this.tb_damage_id);
            this.gb_damage.Controls.Add(this.l_damage_id);
            this.gb_damage.Location = new System.Drawing.Point(12, 430);
            this.gb_damage.Name = "gb_damage";
            this.gb_damage.Size = new System.Drawing.Size(178, 157);
            this.gb_damage.TabIndex = 5;
            this.gb_damage.TabStop = false;
            this.gb_damage.Text = "Damage and Heal";
            // 
            // b_damage_inflict
            // 
            this.b_damage_inflict.Location = new System.Drawing.Point(49, 122);
            this.b_damage_inflict.Name = "b_damage_inflict";
            this.b_damage_inflict.Size = new System.Drawing.Size(75, 23);
            this.b_damage_inflict.TabIndex = 16;
            this.b_damage_inflict.Text = "Inflict";
            this.b_damage_inflict.UseVisualStyleBackColor = true;
            this.b_damage_inflict.Click += new System.EventHandler(this.b_damage_inflict_Click);
            // 
            // rb_heal
            // 
            this.rb_heal.AutoSize = true;
            this.rb_heal.Location = new System.Drawing.Point(75, 100);
            this.rb_heal.Name = "rb_heal";
            this.rb_heal.Size = new System.Drawing.Size(47, 17);
            this.rb_heal.TabIndex = 21;
            this.rb_heal.Text = "Heal";
            this.rb_heal.UseVisualStyleBackColor = true;
            // 
            // rb_damage
            // 
            this.rb_damage.AutoSize = true;
            this.rb_damage.Checked = true;
            this.rb_damage.Location = new System.Drawing.Point(4, 100);
            this.rb_damage.Name = "rb_damage";
            this.rb_damage.Size = new System.Drawing.Size(65, 17);
            this.rb_damage.TabIndex = 20;
            this.rb_damage.TabStop = true;
            this.rb_damage.Text = "Damage";
            this.rb_damage.UseVisualStyleBackColor = true;
            // 
            // tb_damage_amount
            // 
            this.tb_damage_amount.Location = new System.Drawing.Point(4, 71);
            this.tb_damage_amount.Name = "tb_damage_amount";
            this.tb_damage_amount.Size = new System.Drawing.Size(168, 20);
            this.tb_damage_amount.TabIndex = 18;
            // 
            // label_hp_amount
            // 
            this.label_hp_amount.AutoSize = true;
            this.label_hp_amount.Location = new System.Drawing.Point(3, 55);
            this.label_hp_amount.Name = "label_hp_amount";
            this.label_hp_amount.Size = new System.Drawing.Size(70, 13);
            this.label_hp_amount.TabIndex = 19;
            this.label_hp_amount.Text = "Enter amount";
            // 
            // tb_damage_id
            // 
            this.tb_damage_id.Location = new System.Drawing.Point(4, 32);
            this.tb_damage_id.Name = "tb_damage_id";
            this.tb_damage_id.Size = new System.Drawing.Size(168, 20);
            this.tb_damage_id.TabIndex = 16;
            // 
            // l_damage_id
            // 
            this.l_damage_id.AutoSize = true;
            this.l_damage_id.Location = new System.Drawing.Point(3, 16);
            this.l_damage_id.Name = "l_damage_id";
            this.l_damage_id.Size = new System.Drawing.Size(46, 13);
            this.l_damage_id.TabIndex = 17;
            this.l_damage_id.Text = "Enter ID";
            // 
            // gb_player_damage
            // 
            this.gb_player_damage.Controls.Add(this.b_kill_player);
            this.gb_player_damage.Controls.Add(this.b_inflict_player);
            this.gb_player_damage.Controls.Add(this.rb_player_heal);
            this.gb_player_damage.Controls.Add(this.rb_player_damage);
            this.gb_player_damage.Controls.Add(this.tb_damage_player);
            this.gb_player_damage.Controls.Add(this.l_damage_player);
            this.gb_player_damage.Location = new System.Drawing.Point(199, 478);
            this.gb_player_damage.Name = "gb_player_damage";
            this.gb_player_damage.Size = new System.Drawing.Size(182, 109);
            this.gb_player_damage.TabIndex = 6;
            this.gb_player_damage.TabStop = false;
            this.gb_player_damage.Text = "Damage Player";
            // 
            // b_kill_player
            // 
            this.b_kill_player.Location = new System.Drawing.Point(87, 80);
            this.b_kill_player.Name = "b_kill_player";
            this.b_kill_player.Size = new System.Drawing.Size(87, 23);
            this.b_kill_player.TabIndex = 27;
            this.b_kill_player.Text = "Kill Player";
            this.b_kill_player.UseVisualStyleBackColor = true;
            // 
            // b_inflict_player
            // 
            this.b_inflict_player.Location = new System.Drawing.Point(6, 80);
            this.b_inflict_player.Name = "b_inflict_player";
            this.b_inflict_player.Size = new System.Drawing.Size(75, 23);
            this.b_inflict_player.TabIndex = 24;
            this.b_inflict_player.Text = "Inflict";
            this.b_inflict_player.UseVisualStyleBackColor = true;
            this.b_inflict_player.Click += new System.EventHandler(this.b_inflict_player_Click);
            // 
            // rb_player_heal
            // 
            this.rb_player_heal.AutoSize = true;
            this.rb_player_heal.Location = new System.Drawing.Point(77, 58);
            this.rb_player_heal.Name = "rb_player_heal";
            this.rb_player_heal.Size = new System.Drawing.Size(47, 17);
            this.rb_player_heal.TabIndex = 26;
            this.rb_player_heal.Text = "Heal";
            this.rb_player_heal.UseVisualStyleBackColor = true;
            // 
            // rb_player_damage
            // 
            this.rb_player_damage.AutoSize = true;
            this.rb_player_damage.Checked = true;
            this.rb_player_damage.Location = new System.Drawing.Point(6, 58);
            this.rb_player_damage.Name = "rb_player_damage";
            this.rb_player_damage.Size = new System.Drawing.Size(65, 17);
            this.rb_player_damage.TabIndex = 25;
            this.rb_player_damage.TabStop = true;
            this.rb_player_damage.Text = "Damage";
            this.rb_player_damage.UseVisualStyleBackColor = true;
            // 
            // tb_damage_player
            // 
            this.tb_damage_player.Location = new System.Drawing.Point(6, 32);
            this.tb_damage_player.Name = "tb_damage_player";
            this.tb_damage_player.Size = new System.Drawing.Size(168, 20);
            this.tb_damage_player.TabIndex = 22;
            // 
            // l_damage_player
            // 
            this.l_damage_player.AutoSize = true;
            this.l_damage_player.Location = new System.Drawing.Point(5, 16);
            this.l_damage_player.Name = "l_damage_player";
            this.l_damage_player.Size = new System.Drawing.Size(70, 13);
            this.l_damage_player.TabIndex = 23;
            this.l_damage_player.Text = "Enter amount";
            // 
            // gb_grouping
            // 
            this.gb_grouping.Controls.Add(this.b_grouping_kill);
            this.gb_grouping.Controls.Add(this.l_grouping_selected);
            this.gb_grouping.Controls.Add(this.b_grouping4);
            this.gb_grouping.Controls.Add(this.b_grouping3);
            this.gb_grouping.Controls.Add(this.b_grouping2);
            this.gb_grouping.Controls.Add(this.b_grouping5);
            this.gb_grouping.Controls.Add(this.b_grouping1);
            this.gb_grouping.Location = new System.Drawing.Point(387, 478);
            this.gb_grouping.Name = "gb_grouping";
            this.gb_grouping.Size = new System.Drawing.Size(303, 109);
            this.gb_grouping.TabIndex = 28;
            this.gb_grouping.TabStop = false;
            this.gb_grouping.Text = "Grouping Test";
            // 
            // b_grouping_kill
            // 
            this.b_grouping_kill.Location = new System.Drawing.Point(195, 52);
            this.b_grouping_kill.Name = "b_grouping_kill";
            this.b_grouping_kill.Size = new System.Drawing.Size(75, 23);
            this.b_grouping_kill.TabIndex = 33;
            this.b_grouping_kill.Text = "Kill selected";
            this.b_grouping_kill.UseVisualStyleBackColor = true;
            this.b_grouping_kill.Click += new System.EventHandler(this.b_grouping_kill_Click);
            // 
            // l_grouping_selected
            // 
            this.l_grouping_selected.AutoSize = true;
            this.l_grouping_selected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.l_grouping_selected.Location = new System.Drawing.Point(183, 28);
            this.l_grouping_selected.Name = "l_grouping_selected";
            this.l_grouping_selected.Size = new System.Drawing.Size(97, 15);
            this.l_grouping_selected.TabIndex = 28;
            this.l_grouping_selected.Text = "Selected ID: None";
            // 
            // b_grouping4
            // 
            this.b_grouping4.Location = new System.Drawing.Point(6, 80);
            this.b_grouping4.Name = "b_grouping4";
            this.b_grouping4.Size = new System.Drawing.Size(75, 23);
            this.b_grouping4.TabIndex = 32;
            this.b_grouping4.UseVisualStyleBackColor = true;
            this.b_grouping4.Click += new System.EventHandler(this.b_grouping4_Click);
            // 
            // b_grouping3
            // 
            this.b_grouping3.Location = new System.Drawing.Point(56, 52);
            this.b_grouping3.Name = "b_grouping3";
            this.b_grouping3.Size = new System.Drawing.Size(75, 23);
            this.b_grouping3.TabIndex = 31;
            this.b_grouping3.UseVisualStyleBackColor = true;
            this.b_grouping3.Click += new System.EventHandler(this.b_grouping3_Click);
            // 
            // b_grouping2
            // 
            this.b_grouping2.Location = new System.Drawing.Point(102, 23);
            this.b_grouping2.Name = "b_grouping2";
            this.b_grouping2.Size = new System.Drawing.Size(75, 23);
            this.b_grouping2.TabIndex = 30;
            this.b_grouping2.UseVisualStyleBackColor = true;
            this.b_grouping2.Click += new System.EventHandler(this.b_grouping2_Click);
            // 
            // b_grouping5
            // 
            this.b_grouping5.Location = new System.Drawing.Point(102, 80);
            this.b_grouping5.Name = "b_grouping5";
            this.b_grouping5.Size = new System.Drawing.Size(75, 23);
            this.b_grouping5.TabIndex = 29;
            this.b_grouping5.UseVisualStyleBackColor = true;
            this.b_grouping5.Click += new System.EventHandler(this.b_grouping5_Click);
            // 
            // b_grouping1
            // 
            this.b_grouping1.Location = new System.Drawing.Point(6, 23);
            this.b_grouping1.Name = "b_grouping1";
            this.b_grouping1.Size = new System.Drawing.Size(75, 23);
            this.b_grouping1.TabIndex = 28;
            this.b_grouping1.UseVisualStyleBackColor = true;
            this.b_grouping1.Click += new System.EventHandler(this.b_grouping1_Click);
            // 
            // gb_effects
            // 
            this.gb_effects.Controls.Add(this.b_effects_panel);
            this.gb_effects.Location = new System.Drawing.Point(696, 478);
            this.gb_effects.Name = "gb_effects";
            this.gb_effects.Size = new System.Drawing.Size(208, 109);
            this.gb_effects.TabIndex = 28;
            this.gb_effects.TabStop = false;
            this.gb_effects.Text = "Effects";
            // 
            // b_effects_panel
            // 
            this.b_effects_panel.Location = new System.Drawing.Point(47, 49);
            this.b_effects_panel.Name = "b_effects_panel";
            this.b_effects_panel.Size = new System.Drawing.Size(122, 23);
            this.b_effects_panel.TabIndex = 24;
            this.b_effects_panel.Text = "Open Effects panel";
            this.b_effects_panel.UseVisualStyleBackColor = true;
            this.b_effects_panel.Click += new System.EventHandler(this.b_effects_panel_Click);
            // 
            // DebugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 594);
            this.Controls.Add(this.gb_effects);
            this.Controls.Add(this.gb_grouping);
            this.Controls.Add(this.gb_player_damage);
            this.Controls.Add(this.gb_damage);
            this.Controls.Add(this.gb_list_dead);
            this.Controls.Add(this.gb_Kill);
            this.Controls.Add(this.gb_list_alive);
            this.Controls.Add(this.gb_entity_create);
            this.Controls.Add(this.gb_player);
            this.Name = "DebugForm";
            this.Text = "DebugForm";
            this.gb_player.ResumeLayout(false);
            this.gb_player.PerformLayout();
            this.gb_entity_create.ResumeLayout(false);
            this.gb_entity_create.PerformLayout();
            this.gb_list_alive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgw_alive)).EndInit();
            this.gb_Kill.ResumeLayout(false);
            this.gb_Kill.PerformLayout();
            this.gb_list_dead.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgw_dead)).EndInit();
            this.gb_damage.ResumeLayout(false);
            this.gb_damage.PerformLayout();
            this.gb_player_damage.ResumeLayout(false);
            this.gb_player_damage.PerformLayout();
            this.gb_grouping.ResumeLayout(false);
            this.gb_grouping.PerformLayout();
            this.gb_effects.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tb_playerName;
        private System.Windows.Forms.GroupBox gb_player;
        private System.Windows.Forms.Label l_str;
        private System.Windows.Forms.Label l_exp_rem;
        private System.Windows.Forms.Label l_exp_cur;
        private System.Windows.Forms.Label l_level;
        private System.Windows.Forms.Button b_intup;
        private System.Windows.Forms.Button b_agiup;
        private System.Windows.Forms.Button b_strup;
        private System.Windows.Forms.Button b_intdwn;
        private System.Windows.Forms.Button b_agidwn;
        private System.Windows.Forms.Button b_strdwn;
        private System.Windows.Forms.Label l_int;
        private System.Windows.Forms.Label l_agi;
        private System.Windows.Forms.Button b_create;
        private System.Windows.Forms.GroupBox gb_entity_create;
        private System.Windows.Forms.Button b_ent_spawn;
        private System.Windows.Forms.TextBox tb_ent_amount;
        private System.Windows.Forms.Label l_ent_amount;
        private System.Windows.Forms.Label b_ent_type;
        private System.Windows.Forms.ComboBox cb_ent_type;
        private System.Windows.Forms.GroupBox gb_list_alive;
        private System.Windows.Forms.DataGridView dgw_alive;
        private System.Windows.Forms.GroupBox gb_Kill;
        private System.Windows.Forms.TextBox tb_kill_id;
        private System.Windows.Forms.Label l_kill_id;
        private System.Windows.Forms.Button b_ent_kill;
        private System.Windows.Forms.GroupBox gb_list_dead;
        private System.Windows.Forms.DataGridView dgw_dead;
        private System.Windows.Forms.Button b_addExp;
        private System.Windows.Forms.Label l_health;
        private System.Windows.Forms.Button b_damage_inflict;
        private System.Windows.Forms.GroupBox gb_damage;
        private System.Windows.Forms.RadioButton rb_heal;
        private System.Windows.Forms.RadioButton rb_damage;
        private System.Windows.Forms.TextBox tb_damage_amount;
        private System.Windows.Forms.Label label_hp_amount;
        private System.Windows.Forms.TextBox tb_damage_id;
        private System.Windows.Forms.Label l_damage_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ent_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ent_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn alive_health;
        private System.Windows.Forms.DataGridViewTextBoxColumn ent_behaviour;
        private System.Windows.Forms.DataGridViewTextBoxColumn ent_Str;
        private System.Windows.Forms.DataGridViewTextBoxColumn ent_Agi;
        private System.Windows.Forms.DataGridViewTextBoxColumn ent_Int;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dead_health;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.GroupBox gb_player_damage;
        private System.Windows.Forms.Button b_kill_player;
        private System.Windows.Forms.Button b_inflict_player;
        private System.Windows.Forms.RadioButton rb_player_heal;
        private System.Windows.Forms.RadioButton rb_player_damage;
        private System.Windows.Forms.TextBox tb_damage_player;
        private System.Windows.Forms.Label l_damage_player;
        private System.Windows.Forms.GroupBox gb_grouping;
        private System.Windows.Forms.Label l_grouping_selected;
        private System.Windows.Forms.Button b_grouping4;
        private System.Windows.Forms.Button b_grouping3;
        private System.Windows.Forms.Button b_grouping2;
        private System.Windows.Forms.Button b_grouping5;
        private System.Windows.Forms.Button b_grouping1;
        private System.Windows.Forms.Button b_grouping_kill;
        private System.Windows.Forms.GroupBox gb_effects;
        private System.Windows.Forms.Button b_effects_panel;
    }
}