﻿namespace TalesOfHeroes
{
    partial class CombatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_enemy0 = new System.Windows.Forms.Button();
            this.button_enemy1 = new System.Windows.Forms.Button();
            this.button_enemy2 = new System.Windows.Forms.Button();
            this.button_enemy3 = new System.Windows.Forms.Button();
            this.button_enemy4 = new System.Windows.Forms.Button();
            this.button_enemy5 = new System.Windows.Forms.Button();
            this.button_enemy6 = new System.Windows.Forms.Button();
            this.button_enemy7 = new System.Windows.Forms.Button();
            this.button_enemy8 = new System.Windows.Forms.Button();
            this.button_enemy9 = new System.Windows.Forms.Button();
            this.button_enemy10 = new System.Windows.Forms.Button();
            this.button_endturn = new System.Windows.Forms.Button();
            this.comboBox_bonusaction = new System.Windows.Forms.ComboBox();
            this.comboBox_action = new System.Windows.Forms.ComboBox();
            this.textBox_combatlog = new System.Windows.Forms.TextBox();
            this.button_friend10 = new System.Windows.Forms.Button();
            this.button_friend9 = new System.Windows.Forms.Button();
            this.button_friend8 = new System.Windows.Forms.Button();
            this.button_friend7 = new System.Windows.Forms.Button();
            this.button_friend6 = new System.Windows.Forms.Button();
            this.button_friend5 = new System.Windows.Forms.Button();
            this.button_friend4 = new System.Windows.Forms.Button();
            this.button_friend3 = new System.Windows.Forms.Button();
            this.button_friend2 = new System.Windows.Forms.Button();
            this.button_friend1 = new System.Windows.Forms.Button();
            this.button_friend0 = new System.Windows.Forms.Button();
            this.textBox_target_info = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button_enemy0
            // 
            this.button_enemy0.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy0.Location = new System.Drawing.Point(310, 12);
            this.button_enemy0.Name = "button_enemy0";
            this.button_enemy0.Size = new System.Drawing.Size(116, 35);
            this.button_enemy0.TabIndex = 0;
            this.button_enemy0.Text = "Sample";
            this.button_enemy0.UseVisualStyleBackColor = true;
            this.button_enemy0.Click += new System.EventHandler(this.Button_enemy0_Click);
            // 
            // button_enemy1
            // 
            this.button_enemy1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy1.Location = new System.Drawing.Point(310, 53);
            this.button_enemy1.Name = "button_enemy1";
            this.button_enemy1.Size = new System.Drawing.Size(116, 35);
            this.button_enemy1.TabIndex = 1;
            this.button_enemy1.Text = "Sample";
            this.button_enemy1.UseVisualStyleBackColor = true;
            this.button_enemy1.Click += new System.EventHandler(this.Button_enemy1_Click);
            // 
            // button_enemy2
            // 
            this.button_enemy2.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy2.Location = new System.Drawing.Point(310, 94);
            this.button_enemy2.Name = "button_enemy2";
            this.button_enemy2.Size = new System.Drawing.Size(116, 35);
            this.button_enemy2.TabIndex = 2;
            this.button_enemy2.Text = "Sample";
            this.button_enemy2.UseVisualStyleBackColor = true;
            this.button_enemy2.Click += new System.EventHandler(this.Button_enemy2_Click);
            // 
            // button_enemy3
            // 
            this.button_enemy3.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy3.Location = new System.Drawing.Point(310, 135);
            this.button_enemy3.Name = "button_enemy3";
            this.button_enemy3.Size = new System.Drawing.Size(116, 35);
            this.button_enemy3.TabIndex = 3;
            this.button_enemy3.Text = "Sample";
            this.button_enemy3.UseVisualStyleBackColor = true;
            this.button_enemy3.Click += new System.EventHandler(this.Button_enemy3_Click);
            // 
            // button_enemy4
            // 
            this.button_enemy4.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy4.Location = new System.Drawing.Point(310, 176);
            this.button_enemy4.Name = "button_enemy4";
            this.button_enemy4.Size = new System.Drawing.Size(116, 35);
            this.button_enemy4.TabIndex = 4;
            this.button_enemy4.Text = "Sample";
            this.button_enemy4.UseVisualStyleBackColor = true;
            this.button_enemy4.Click += new System.EventHandler(this.Button_enemy4_Click);
            // 
            // button_enemy5
            // 
            this.button_enemy5.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy5.Location = new System.Drawing.Point(310, 217);
            this.button_enemy5.Name = "button_enemy5";
            this.button_enemy5.Size = new System.Drawing.Size(116, 35);
            this.button_enemy5.TabIndex = 5;
            this.button_enemy5.Text = "Sample";
            this.button_enemy5.UseVisualStyleBackColor = true;
            this.button_enemy5.Click += new System.EventHandler(this.Button_enemy5_Click);
            // 
            // button_enemy6
            // 
            this.button_enemy6.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy6.Location = new System.Drawing.Point(310, 258);
            this.button_enemy6.Name = "button_enemy6";
            this.button_enemy6.Size = new System.Drawing.Size(116, 35);
            this.button_enemy6.TabIndex = 6;
            this.button_enemy6.Text = "Sample";
            this.button_enemy6.UseVisualStyleBackColor = true;
            this.button_enemy6.Click += new System.EventHandler(this.Button_enemy6_Click);
            // 
            // button_enemy7
            // 
            this.button_enemy7.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy7.Location = new System.Drawing.Point(310, 295);
            this.button_enemy7.Name = "button_enemy7";
            this.button_enemy7.Size = new System.Drawing.Size(116, 35);
            this.button_enemy7.TabIndex = 7;
            this.button_enemy7.Text = "Sample";
            this.button_enemy7.UseVisualStyleBackColor = true;
            this.button_enemy7.Click += new System.EventHandler(this.Button_enemy7_Click);
            // 
            // button_enemy8
            // 
            this.button_enemy8.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy8.Location = new System.Drawing.Point(310, 336);
            this.button_enemy8.Name = "button_enemy8";
            this.button_enemy8.Size = new System.Drawing.Size(116, 35);
            this.button_enemy8.TabIndex = 8;
            this.button_enemy8.Text = "Sample";
            this.button_enemy8.UseVisualStyleBackColor = true;
            this.button_enemy8.Click += new System.EventHandler(this.Button_enemy8_Click);
            // 
            // button_enemy9
            // 
            this.button_enemy9.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy9.Location = new System.Drawing.Point(310, 377);
            this.button_enemy9.Name = "button_enemy9";
            this.button_enemy9.Size = new System.Drawing.Size(116, 35);
            this.button_enemy9.TabIndex = 9;
            this.button_enemy9.Text = "Sample";
            this.button_enemy9.UseVisualStyleBackColor = true;
            this.button_enemy9.Click += new System.EventHandler(this.Button_enemy9_Click);
            // 
            // button_enemy10
            // 
            this.button_enemy10.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_enemy10.Location = new System.Drawing.Point(310, 418);
            this.button_enemy10.Name = "button_enemy10";
            this.button_enemy10.Size = new System.Drawing.Size(116, 35);
            this.button_enemy10.TabIndex = 10;
            this.button_enemy10.Text = "Sample";
            this.button_enemy10.UseVisualStyleBackColor = true;
            this.button_enemy10.Click += new System.EventHandler(this.Button_enemy10_Click);
            // 
            // button_endturn
            // 
            this.button_endturn.Enabled = false;
            this.button_endturn.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_endturn.Location = new System.Drawing.Point(157, 310);
            this.button_endturn.Name = "button_endturn";
            this.button_endturn.Size = new System.Drawing.Size(121, 36);
            this.button_endturn.TabIndex = 29;
            this.button_endturn.Text = "End turn";
            this.button_endturn.UseVisualStyleBackColor = true;
            this.button_endturn.Visible = false;
            this.button_endturn.Click += new System.EventHandler(this.Button_endturn_Click);
            // 

            // comboBox_bonusaction
            // 
            this.comboBox_bonusaction.Enabled = false;
            this.comboBox_bonusaction.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_bonusaction.FormattingEnabled = true;
            this.comboBox_bonusaction.Items.AddRange(new object[] {
            "Nothing",
            "Swap weapons"});
            this.comboBox_bonusaction.Location = new System.Drawing.Point(146, 257);
            this.comboBox_bonusaction.Name = "comboBox_bonusaction";
            this.comboBox_bonusaction.Size = new System.Drawing.Size(144, 34);
            this.comboBox_bonusaction.TabIndex = 28;
            this.comboBox_bonusaction.Visible = false;
            // 
            // comboBox_action
            // 
            this.comboBox_action.Enabled = false;
            this.comboBox_action.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_action.FormattingEnabled = true;
            this.comboBox_action.Items.AddRange(new object[] {
            "Attack",
            "Cast a spell",
            "Dodge"});
            this.comboBox_action.Location = new System.Drawing.Point(146, 217);
            this.comboBox_action.Name = "comboBox_action";
            this.comboBox_action.Size = new System.Drawing.Size(144, 34);
            this.comboBox_action.TabIndex = 27;
            this.comboBox_action.Visible = false;

            

            // 
            // textBox_combatlog
            // 
            this.textBox_combatlog.Location = new System.Drawing.Point(458, 12);
            this.textBox_combatlog.Multiline = true;
            this.textBox_combatlog.Name = "textBox_combatlog";
            this.textBox_combatlog.Size = new System.Drawing.Size(430, 441);
            this.textBox_combatlog.TabIndex = 30;
            // 
            // button_friend10
            // 
            this.button_friend10.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend10.Location = new System.Drawing.Point(12, 418);
            this.button_friend10.Name = "button_friend10";
            this.button_friend10.Size = new System.Drawing.Size(116, 35);
            this.button_friend10.TabIndex = 41;
            this.button_friend10.Text = "Sample";
            this.button_friend10.UseVisualStyleBackColor = true;
            this.button_friend1.Click += new System.EventHandler(this.Button_friend10_Click);
            // 
            // button_friend9
            // 
            this.button_friend9.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend9.Location = new System.Drawing.Point(12, 377);
            this.button_friend9.Name = "button_friend9";
            this.button_friend9.Size = new System.Drawing.Size(116, 35);
            this.button_friend9.TabIndex = 40;
            this.button_friend9.Text = "Sample";
            this.button_friend9.UseVisualStyleBackColor = true;
            this.button_friend1.Click += new System.EventHandler(this.Button_friend9_Click);

            // 
            // button_friend8
            // 
            this.button_friend8.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend8.Location = new System.Drawing.Point(12, 336);
            this.button_friend8.Name = "button_friend8";
            this.button_friend8.Size = new System.Drawing.Size(116, 35);
            this.button_friend8.TabIndex = 39;
            this.button_friend8.Text = "Sample";
            this.button_friend8.UseVisualStyleBackColor = true;
            this.button_friend1.Click += new System.EventHandler(this.Button_friend8_Click);
            // 
            // button_friend7
            // 
            this.button_friend7.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend7.Location = new System.Drawing.Point(12, 295);
            this.button_friend7.Name = "button_friend7";
            this.button_friend7.Size = new System.Drawing.Size(116, 35);
            this.button_friend7.TabIndex = 38;
            this.button_friend7.Text = "Sample";
            this.button_friend7.UseVisualStyleBackColor = true;
            this.button_friend1.Click += new System.EventHandler(this.Button_friend7_Click);
            // 
            // button_friend6
            // 
            this.button_friend6.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend6.Location = new System.Drawing.Point(12, 258);
            this.button_friend6.Name = "button_friend6";
            this.button_friend6.Size = new System.Drawing.Size(116, 35);
            this.button_friend6.TabIndex = 37;
            this.button_friend6.Text = "Sample";
            this.button_friend6.UseVisualStyleBackColor = true;
            this.button_friend1.Click += new System.EventHandler(this.Button_friend6_Click);
            // 
            // button_friend5
            // 
            this.button_friend5.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend5.Location = new System.Drawing.Point(12, 217);
            this.button_friend5.Name = "button_friend5";
            this.button_friend5.Size = new System.Drawing.Size(116, 35);
            this.button_friend5.TabIndex = 36;
            this.button_friend5.Text = "Sample";
            this.button_friend5.UseVisualStyleBackColor = true;
            this.button_friend1.Click += new System.EventHandler(this.Button_friend5_Click);
            // 
            // button_friend4
            // 
            this.button_friend4.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend4.Location = new System.Drawing.Point(12, 176);
            this.button_friend4.Name = "button_friend4";
            this.button_friend4.Size = new System.Drawing.Size(116, 35);
            this.button_friend4.TabIndex = 35;
            this.button_friend4.Text = "Sample";
            this.button_friend4.UseVisualStyleBackColor = true;
            this.button_friend4.Click += new System.EventHandler(this.Button_friend4_Click);
            // 
            // button_friend3
            // 
            this.button_friend3.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend3.Location = new System.Drawing.Point(12, 135);
            this.button_friend3.Name = "button_friend3";
            this.button_friend3.Size = new System.Drawing.Size(116, 35);
            this.button_friend3.TabIndex = 34;
            this.button_friend3.Text = "Sample";
            this.button_friend3.UseVisualStyleBackColor = true;
            this.button_friend3.Click += new System.EventHandler(this.Button_friend3_Click);
            // 
            // button_friend2
            // 
            this.button_friend2.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend2.Location = new System.Drawing.Point(12, 94);
            this.button_friend2.Name = "button_friend2";
            this.button_friend2.Size = new System.Drawing.Size(116, 35);
            this.button_friend2.TabIndex = 33;
            this.button_friend2.Text = "Sample";
            this.button_friend2.UseVisualStyleBackColor = true;
            this.button_friend2.Click += new System.EventHandler(this.Button_friend2_Click);
            // 
            // button_friend1
            // 
            this.button_friend1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend1.Location = new System.Drawing.Point(12, 53);
            this.button_friend1.Name = "button_friend1";
            this.button_friend1.Size = new System.Drawing.Size(116, 35);
            this.button_friend1.TabIndex = 32;
            this.button_friend1.Text = "Sample";
            this.button_friend1.UseVisualStyleBackColor = true;
            this.button_friend1.Click += new System.EventHandler(this.Button_friend1_Click);
            // 
            // button_friend0
            // 
            this.button_friend0.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_friend0.Location = new System.Drawing.Point(12, 12);
            this.button_friend0.Name = "button_friend0";
            this.button_friend0.Size = new System.Drawing.Size(116, 35);
            this.button_friend0.TabIndex = 31;
            this.button_friend0.Text = "Sample";
            this.button_friend0.UseVisualStyleBackColor = true;
            this.button_friend0.Click += new System.EventHandler(this.Button_friend0_Click);
            // 
            // textBox_target_info
            // 
            this.textBox_target_info.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_target_info.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_target_info.Location = new System.Drawing.Point(134, 377);
            this.textBox_target_info.Multiline = true;
            this.textBox_target_info.Name = "textBox_target_info";
            this.textBox_target_info.ReadOnly = true;
            this.textBox_target_info.Size = new System.Drawing.Size(170, 76);
            this.textBox_target_info.TabIndex = 42;
            // 
            // textBox_target_info
            // 
            this.textBox_target_info.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_target_info.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_target_info.Location = new System.Drawing.Point(168, 377);
            this.textBox_target_info.Multiline = true;
            this.textBox_target_info.Name = "textBox_target_info";
            this.textBox_target_info.ReadOnly = true;
            this.textBox_target_info.Size = new System.Drawing.Size(100, 76);
            this.textBox_target_info.TabIndex = 42;
            // 
            // CombatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 538);
            this.Controls.Add(this.textBox_target_info);
            this.Controls.Add(this.button_friend10);
            this.Controls.Add(this.button_friend9);
            this.Controls.Add(this.button_friend8);
            this.Controls.Add(this.button_friend7);
            this.Controls.Add(this.button_friend6);
            this.Controls.Add(this.button_friend5);
            this.Controls.Add(this.button_friend4);
            this.Controls.Add(this.button_friend3);
            this.Controls.Add(this.button_friend2);
            this.Controls.Add(this.button_friend1);
            this.Controls.Add(this.button_friend0);
            this.Controls.Add(this.textBox_combatlog);
            this.Controls.Add(this.button_endturn);
            this.Controls.Add(this.comboBox_bonusaction);
            this.Controls.Add(this.comboBox_action);
            this.Controls.Add(this.button_enemy10);
            this.Controls.Add(this.button_enemy9);
            this.Controls.Add(this.button_enemy8);
            this.Controls.Add(this.button_enemy7);
            this.Controls.Add(this.button_enemy6);
            this.Controls.Add(this.button_enemy5);
            this.Controls.Add(this.button_enemy4);
            this.Controls.Add(this.button_enemy3);
            this.Controls.Add(this.button_enemy2);
            this.Controls.Add(this.button_enemy1);
            this.Controls.Add(this.button_enemy0);
            this.Name = "CombatForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.CombatForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_enemy0;
        private System.Windows.Forms.Button button_enemy1;
        private System.Windows.Forms.Button button_enemy2;
        private System.Windows.Forms.Button button_enemy3;
        private System.Windows.Forms.Button button_enemy4;
        private System.Windows.Forms.Button button_enemy5;
        private System.Windows.Forms.Button button_enemy6;
        private System.Windows.Forms.Button button_enemy7;
        private System.Windows.Forms.Button button_enemy8;
        private System.Windows.Forms.Button button_enemy9;
        private System.Windows.Forms.Button button_enemy10;
        private System.Windows.Forms.Button button_endturn;
        private System.Windows.Forms.ComboBox comboBox_bonusaction;
        private System.Windows.Forms.ComboBox comboBox_action;
        private System.Windows.Forms.TextBox textBox_combatlog;
        private System.Windows.Forms.Button button_friend10;
        private System.Windows.Forms.Button button_friend9;
        private System.Windows.Forms.Button button_friend8;
        private System.Windows.Forms.Button button_friend7;
        private System.Windows.Forms.Button button_friend6;
        private System.Windows.Forms.Button button_friend5;
        private System.Windows.Forms.Button button_friend4;
        private System.Windows.Forms.Button button_friend3;
        private System.Windows.Forms.Button button_friend2;
        private System.Windows.Forms.Button button_friend1;
        private System.Windows.Forms.Button button_friend0;
        private System.Windows.Forms.TextBox textBox_target_info;
    }
}