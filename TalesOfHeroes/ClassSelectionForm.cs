﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.IO;
using System.Media;
using System.Configuration;
using System.Collections.Specialized;

namespace TalesOfHeroes
{
    public partial class ClassSelectionForm : Form
    {
        internal static Configuration config;
        internal static ConfigurationSectionGroup resources;
        internal static ConfigurationSection imagesSection;
        internal static ConfigurationSection soundsSection;
        internal static ConfigurationSection fontsSection;
        internal static NameValueCollection soundsPaths;
        internal static NameValueCollection imagesPaths;
        internal static NameValueCollection fontsPaths;

        PrivateFontCollection MyFont = new PrivateFontCollection();
       
        public static string SelectedClass = "";

        public static string SelectedBackground = "";

        int[] StatsArray = new int[6];

        public ClassSelectionForm()
        {
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            resources = config.GetSectionGroup("resources");
            imagesSection = resources.Sections["images"];
            soundsSection = resources.Sections["sounds"];
            fontsSection = resources.Sections["fonts"];

            imagesPaths = ConfigurationManager.GetSection(imagesSection.SectionInformation.SectionName) as NameValueCollection;
            soundsPaths = ConfigurationManager.GetSection(soundsSection.SectionInformation.SectionName) as NameValueCollection;
            fontsPaths = ConfigurationManager.GetSection(fontsSection.SectionInformation.SectionName) as NameValueCollection;

            InitializeComponent();
        }
        private void ClassSelectionForm_Load(object sender, EventArgs e)
        {
            InterfaceSettings();
        }

        /// <summary>
        /// Sets new parametres for every interface element
        /// </summary>
        void InterfaceSettings()
        {
            //MyFont.AddFontFile("..\\..\\Resources\\Fonts\\EnchantedLand.otf");
            MyFont.AddFontFile(fontsPaths["EnchantedLand"]);

            foreach (Control Elements in this.Controls)
            {
                Elements.Font = new Font(MyFont.Families[0], 20);
            }

            label_stat.Font = new Font(MyFont.Families[0], 16);
            label_modifier.Font = new Font(MyFont.Families[0], 16);

            comboBox_strength.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_agility.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_constitution.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_intellegence.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_wisdom.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_charisma.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_chosenenemy.DropDownStyle = ComboBoxStyle.DropDownList;

            comboBox_chosenenemy.SelectedIndex = 0;
            comboBox_chosenenemy.Visible = false;
            comboBox_chosenenemy.Enabled = false;

            label_chosenenemy.Visible = false;
            label_chosenenemy.Enabled = false;

            checkedListBox_cantrips.Visible = false;
            checkedListBox_cantrips.Enabled = false;

            label_cantrips.Visible = false;
            label_cantrips.Enabled = false;

            pictureBox_barbarian.Visible = false;
            pictureBox_barbarian.Enabled = false;

            pictureBox_fighter.Visible = false;
            pictureBox_fighter.Enabled = false;

            pictureBox_paladin.Visible = false;
            pictureBox_paladin.Enabled = false;

            pictureBox_ranger.Visible = false;
            pictureBox_ranger.Enabled = false;

            pictureBox_rogue.Visible = false;
            pictureBox_rogue.Enabled = false;

            button_bard.BackgroundImage = null;
            button_cleric.BackgroundImage = null;
            button_druid.BackgroundImage = null;
            button_monk.BackgroundImage = null;
            button_paladin.BackgroundImage = null;
            button_warlock.BackgroundImage = null;
            button_sorcerer.BackgroundImage = null;
            button_wizard.BackgroundImage = null;

        }

        void OutlanderStatReset()
        {
            for (int i = 0; i < 6; i++)
            {
                StatsArray[i] = 0;
            }
        }
        void Combobox_reset()
        {
            comboBox_strength.Items[1] = 8;
            comboBox_strength.Items[2] = 10;
            comboBox_strength.Items[3] = 12;
            comboBox_strength.Items[4] = 13;
            comboBox_strength.Items[5] = 14;
            comboBox_strength.Items[6] = 15;

            comboBox_strength.SelectedIndex = 0;

            comboBox_agility.Items[1] = 8;
            comboBox_agility.Items[2] = 10;
            comboBox_agility.Items[3] = 12;
            comboBox_agility.Items[4] = 13;
            comboBox_agility.Items[5] = 14;
            comboBox_agility.Items[6] = 15;

            comboBox_agility.SelectedIndex = 0;

            comboBox_constitution.Items[1] = 8;
            comboBox_constitution.Items[2] = 10;
            comboBox_constitution.Items[3] = 12;
            comboBox_constitution.Items[4] = 13;
            comboBox_constitution.Items[5] = 14;
            comboBox_constitution.Items[6] = 15;

            comboBox_constitution.SelectedIndex = 0;

            comboBox_intellegence.Items[1] = 8;
            comboBox_intellegence.Items[2] = 10;
            comboBox_intellegence.Items[3] = 12;
            comboBox_intellegence.Items[4] = 13;
            comboBox_intellegence.Items[5] = 14;
            comboBox_intellegence.Items[6] = 15;

            comboBox_intellegence.SelectedIndex = 0;

            comboBox_wisdom.Items[1] = 8;
            comboBox_wisdom.Items[2] = 10;
            comboBox_wisdom.Items[3] = 12;
            comboBox_wisdom.Items[4] = 13;
            comboBox_wisdom.Items[5] = 14;
            comboBox_wisdom.Items[6] = 15;

            comboBox_wisdom.SelectedIndex = 0;

            comboBox_charisma.Items[1] = 8;
            comboBox_charisma.Items[2] = 10;
            comboBox_charisma.Items[3] = 12;
            comboBox_charisma.Items[4] = 13;
            comboBox_charisma.Items[5] = 14;
            comboBox_charisma.Items[6] = 15;

            comboBox_charisma.SelectedIndex = 0;
        }

        /// <summary>
        /// Removes class button selection
        /// </summary>
        void RemoveClassSelection()
        {
            button_barbarian.BackColor = System.Drawing.Color.Transparent;
            button_bard.BackColor = System.Drawing.Color.Transparent;
            button_cleric.BackColor = System.Drawing.Color.Transparent;
            button_druid.BackColor = System.Drawing.Color.Transparent;
            button_monk.BackColor = System.Drawing.Color.Transparent;
            button_paladin.BackColor = System.Drawing.Color.Transparent;
            button_ranger.BackColor = System.Drawing.Color.Transparent;
            button_rogue.BackColor = System.Drawing.Color.Transparent;
            button_sorcerer.BackColor = System.Drawing.Color.Transparent;
            button_fighter.BackColor = System.Drawing.Color.Transparent;
            button_warlock.BackColor = System.Drawing.Color.Transparent;
            button_wizard.BackColor = System.Drawing.Color.Transparent;

            comboBox_chosenenemy.SelectedIndex = 0;
            comboBox_chosenenemy.Visible = false;
            comboBox_chosenenemy.Enabled = false;

            label_chosenenemy.Visible = false;
            label_chosenenemy.Enabled = false;

            checkedListBox_cantrips.Items.Clear();
            checkedListBox_cantrips.Visible = false;
            checkedListBox_cantrips.Enabled = false;

            label_cantrips.Visible = false;
            label_cantrips.Enabled = false;

            pictureBox_barbarian.Visible = false;
            pictureBox_barbarian.Enabled = false;

            pictureBox_fighter.Visible = false;
            pictureBox_fighter.Enabled = false;

            pictureBox_paladin.Visible = false;
            pictureBox_paladin.Enabled = false;

            pictureBox_ranger.Visible = false;
            pictureBox_ranger.Enabled = false;

            pictureBox_rogue.Visible = false;
            pictureBox_rogue.Enabled = false;

            button_bard.BackgroundImage = null;
            button_cleric.BackgroundImage = null;
            button_druid.BackgroundImage = null;
            button_monk.BackgroundImage = null;
            button_paladin.BackgroundImage = null;
            button_warlock.BackgroundImage = null;
            button_sorcerer.BackgroundImage = null;
            button_wizard.BackgroundImage = null;

            return;
        }
        /// <summary>
        /// Removes background button selection
        /// </summary>
        void RemoveBackgroundSelection()
        {

            //button_acolyte.BackColor = System.Drawing.Color.Transparent;
            //button_charlatan.BackColor = System.Drawing.Color.Transparent;
            //button_criminal.BackColor = System.Drawing.Color.Transparent;
            //button_entertainer.BackColor = System.Drawing.Color.Transparent;
            //button_folkhero.BackColor = System.Drawing.Color.Transparent;
            //button_guildartisan.BackColor = System.Drawing.Color.Transparent;
            //button_hermit.BackColor = System.Drawing.Color.Transparent;
            //button_outlander.BackColor = System.Drawing.Color.Transparent;
            //button_noble.BackColor = System.Drawing.Color.Transparent;
            //button_sage.BackColor = System.Drawing.Color.Transparent;
            //button_sailor.BackColor = System.Drawing.Color.Transparent;
            //button_soldier.BackColor = System.Drawing.Color.Transparent;
            //button_urchin.BackColor = System.Drawing.Color.Transparent;

            button_acolyte.BackgroundImage = null;
            button_charlatan.BackgroundImage = null;
            button_criminal.BackgroundImage = null;
            button_entertainer.BackgroundImage = null;
            button_folkhero.BackgroundImage = null;
            button_guildartisan.BackgroundImage = null;
            button_hermit.BackgroundImage = null;
            button_outlander.BackgroundImage = null;
            button_noble.BackgroundImage = null;
            button_sage.BackgroundImage = null;
            button_sailor.BackgroundImage = null;
            button_soldier.BackgroundImage = null;
            button_urchin.BackgroundImage = null;

            return;
        }

        private void Button_barbarian_Click(object sender, EventArgs e)
        {
            if (SelectedClass=="barbarian")
            {
                return;
            }    

            RemoveClassSelection();

            textBox_description.Text = "Barbarians, different as they might be, are defined by their rage: unbridled, unquenchable, and "+
    "unthinking fury. More than a mere emotion, their anger is the ferocity of a cornered predator, the unrelenting assault of a storm, the churning turmoil of the sea.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use your fury and Strength to crush your enemies.";
            textBox_description.Text += "\r\n" + "Starting HP = 12 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 7 + Constitution modifier per barbarian level after 1st.";
            textBox_description.Text += "\r\n" + "You can enter Rage and gain the following benefits if you are not wearing any heavy armour:";
            textBox_description.Text += "\r\n" + "- Bonus to your melee attacks (+3).";
            textBox_description.Text += "\r\n" + "- Resistance to  bludgeoning, piercing, and slashing damage until your next turn.";
            textBox_description.Text += "\r\n" + "You can enter Rage 2 times per combat.";
            textBox_description.Text += "\r\n" + "While you are not wearing any armour, your Armour Class equals 10 + your Dexterity modifier + your Constitution " +
                "modifier. You can use a shield and still gain this benefit.";
            textBox_description.Text += "\r\n"+ "Proficiencies:" +"\r\n"+ "Armour: Light Armour, Medium Armour, Shields."+"\r\n"+"Weapons: Simple Weapons, Martial Weapons.";

            if (textBox_constitution_mod.Text!="-")
            {
                label_hp.Text = "Starting Hit Points: " + (12 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Greataxe (1-12 of slashing damage)" + "\r\n" + "Two handaxes (1-6 of slashing damage)" + "\r\n"+"Four javelins (1-6 of piercing damage)";

            SelectedClass = "barbarian";
            pictureBox_barbarian.Visible = true;
            pictureBox_barbarian.Enabled = true;
            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["AxeSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Axe"]);

            simpleSound.Play();
            //button_barbarian.BackColor = System.Drawing.Color.Red;

        }

        private void Button_bard_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "bard")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Whether scholar, skald, or scoundrel, a bard weaves magic through words and music "+
                "to inspire allies, demoralize foes, manipulate minds, create illusions, and even heal wounds.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Charisma to cast spells.";
            textBox_description.Text += "\r\n" + "Starting HP = 8 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 5 + Constitution modifier per bard level after 1st.";
            textBox_description.Text += "\r\n" + "At 1st level you know 2 bard cantrips and 4 1st-level bard spells of your choice.";
            textBox_description.Text += "\r\n" + "You start with 2 1st-level spell slots and gain more as you level up.";
            textBox_description.Text += "\r\n" + "You can cast Bardic Inspiration on an allied creature of yourself as a bonus action to gain (+3) to all stats.";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: Light Armour." + "\r\n" + "Weapons: Simple Weapons, hand crossbows, longswords, rapiers, shortswords.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Rapier (1-8 of piercing damage)"+"\r\n"+"Dagger (1-4 of piercing damage)"+"\r\n"+"Leather armour (11 AC)";

            checkedListBox_cantrips.Visible = true;
            checkedListBox_cantrips.Enabled = true;

            label_cantrips.Visible = true;
            label_cantrips.Enabled = true;

            checkedListBox_cantrips.Items.Add("blade ward", false);
            checkedListBox_cantrips.Items.Add("true strike", false);
            checkedListBox_cantrips.Items.Add("vicious mockery", false);

            SelectedClass = "bard";
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Harp"]);
            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["HarpSound"]);
            
            button_bard.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["MusicNotes"]);
            //button_bard.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\MusicNotes.png");

simpleSound.Play();

            //button_bard.BackColor = System.Drawing.Color.Red;
        }

        private void Button_cleric_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "cleric")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Clerics are intermediaries between the mortal world and the distant planes of the gods." +
    " Wielding the divine power, clerics heal wounded and turn away undead.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Wisdom to cast spells.";
            textBox_description.Text += "\r\n" + "Starting HP = 8 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 5 + Constitution modifier per cleric level after 1st.";
            textBox_description.Text += "\r\n" + "At 1st level you know 3 cleric cantrips of your choice and Bless and Cure Wounds cantrip.";
            textBox_description.Text += "\r\n" + "You start with 2 1st-level spell slots and gain more as you level up.";
            textBox_description.Text += "\r\n" + "Although you know every cleric spell, you have to prepare a list of spells after every rest. The number of " +
                "prepared spells equals your Wisdom modifier + your level.";
            textBox_description.Text += "\r\n" + "During combat you can use Turn Undead to make all undead creatures flee, making them unable to attack.";
            textBox_description.Text += "\r\n" + "Every healing spell you cast heals (+4) more.";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: Light Armour, Medium Armour, Shields." + "\r\n" + "Weapons: Simple Weapons.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Mace (1-6 of bludgeoning damage)"+"\r\n"+"Light crossbow and 20 bolts (1-8 of piercing damage)"+"\r\n"+"Chain mail (16 AC)"+"\r\n"+"Shield (2 AC)";

            checkedListBox_cantrips.Visible = true;
            checkedListBox_cantrips.Enabled = true;

            label_cantrips.Visible = true;
            label_cantrips.Enabled = true;

            checkedListBox_cantrips.Items.Add("Sacred Flame", false);
            checkedListBox_cantrips.Items.Add("Resistance", false);
            checkedListBox_cantrips.Items.Add("Guidance", false);

            SelectedClass = "cleric";

            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["ChoirSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Choir"]);
            simpleSound.Play();
            button_cleric.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Candles"]);
            //.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\Candles.png");
            //button_cleric.BackColor = System.Drawing.Color.Red;
        }

        private void Button_druid_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "druid")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Whether calling on the elemental forces of nature or emulating the creatures of the animal world, druids are"+
                " an embodiment of nature’s resilience, cunning, and fury. They claim no mastery over nature. Instead, they see themselves as extensions of nature’s indomitable will.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Wisdom to cast spells.";
            textBox_description.Text += "\r\n" + "Starting HP = 8 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 5 + Constitution modifier per druid level after 1st.";
            textBox_description.Text += "\r\n" + "At 1st level you know 2 druid cantrips of your choice.";
            textBox_description.Text += "\r\n" + "Although you know every druid spell, you have to prepare a list of spells after every rest. The number of " +
                "prepared spells equals your Wisdom modifier + your level.";
            textBox_description.Text += "\r\n" + "You also know Druidic, the secret language of druids.";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: Light Armour, Medium Armour, Shields (druids will not wear armour or use Shields made of metal)." + "\r\n" + "Weapons: Simple Weapons.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Scimitar (1-6 of slashing damage)"+"\r\n"+"Leather armour (11 AC)"+"\r\n"+"Shield (2 AC)";

            checkedListBox_cantrips.Visible = true;
            checkedListBox_cantrips.Enabled = true;

            label_cantrips.Visible = true;
            label_cantrips.Enabled = true;

            checkedListBox_cantrips.Items.Add("Shillelagh", false);
            checkedListBox_cantrips.Items.Add("Resistance", false);
            checkedListBox_cantrips.Items.Add("Guidance", false);
            checkedListBox_cantrips.Items.Add("Produce Flame", false);
            checkedListBox_cantrips.Items.Add("Poison Spray", false);
            checkedListBox_cantrips.Items.Add("Thorn Whip", false);

            SelectedClass = "druid";

            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["ForestSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Forest"]);
            simpleSound.Play();
            button_druid.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Leaves"]);
            //.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\Leaves.png");
            //button_druid.BackColor = System.Drawing.Color.Red;
        }

        private void Button_fighter_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "fighter")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Fighters share anunparalleled mastery with weapons and armour, and athorough knowledge of the " +
                "skills of combat. And they are well acquainted with death, both meting it out and staring it defiantly in the face.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Strength and Agility to strike down enemies.";
            textBox_description.Text += "\r\n" + "Starting HP = 10 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 6 + Constitution modifier per fighter level after 1st.";
            textBox_description.Text += "\r\n" + "When you are wielding a melee weapon in one hand and no other weapons, you gain a (+2) bonus to damage rolls with that weapon.";
            textBox_description.Text += "\r\n" + "Once per combat as a bonus action you can heal yourself for (+6) HP.";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: Light Armour, Medium Armour, Heavy Armour, Shields." + "\r\n" + "Weapons: Simple Weapons, Martial Weapons.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (10 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Longsword (1-8 of slashing damage)"+"\r\n"+"Light crossbow and 20 bolts (1-8 of piercing damage)"+"\r\n"+"Chain mail (16 AC)"+"\r\n"+"Shield (2 AC)";

            SelectedClass = "fighter";

            pictureBox_fighter.Visible = true;
            pictureBox_fighter.Enabled = true;
            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["SwordSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Sword"]);
            simpleSound.Play();
            //button_fighter.BackColor = System.Drawing.Color.Red;
        }

        private void Button_monk_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "monk")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Whatever their discipline, monks are united in their ability to magically harness the energy that flows intheir bodies. " +
                "Whether channeled as a striking display of combat prowess or a subtler focus of defensive ability and speed, this energy infuses all that a monk does.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use martial arts to inflict fatal damage to enemies.";
            textBox_description.Text += "\r\n" + "Starting HP = 8 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 5 + Constitution modifier per monk level after 1st.";
            textBox_description.Text += "\r\n" + "Beginning at 1st level, while you are wearing no armour and not wielding a shield, your Armour Class equals 10 + your"+
                " Agility modifier + your Wisdom modifier.";
            textBox_description.Text += "\r\n" + "Martial arts knowledge lets you attack with your unarmed stike which gains (+4) bonus damage as a bonus action.";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: none." + "\r\n" + "Weapons: Simple Weapons, shortswords.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Shortsword (1-6 of piercing damage)"+"\r\n"+"10 darts (1-4 of piercing damage)";

            SelectedClass = "monk";

            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["PunchSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Punch"]);
            simpleSound.Play();
            button_monk.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Cracks"]);
            //.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\Cracks.png");
            //button_monk.BackColor = System.Drawing.Color.Red;
        }

        private void Button_paladin_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "paladin")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Whatever their origin and their mission, paladins are united by their oaths to stand against the forces of evil.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Strength to fight and Charisma to wield divine powers.";
            textBox_description.Text += "\r\n" + "Starting HP = 10 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 6 + Constitution modifier per paladin level after 1st.";
            textBox_description.Text += "\r\n" + "You deal (+3) more damage to undead and demons.";
            textBox_description.Text += "\r\n" + "Divine power gives you the ability to restore (+5) HP to any allied creature. With each paladin level the bonus increases by (+5).";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: Light Armour, Medium Armour, Heavy Armour, Shields." + "\r\n" + "Weapons: Simple Weapons, Martial Weapons.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (10 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Warhammer (1-8 of bludgeoning damage)"+"\r\n"+"Five javelins (1-6 of piercing damage)"+"\r\n"+"Chain mail (16 AC)"+"\r\n"+"Shield (2 AC)";

            SelectedClass = "paladin";

            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["HammerSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Hammer"]);
            simpleSound.Play();
            pictureBox_paladin.Visible = true;
            pictureBox_paladin.Enabled = true;
            button_paladin.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["HammerCracks"]);
            //.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\HammerCracks.png");
            //button_paladin.BackColor = System.Drawing.Color.Red;
        }

        private void Button_ranger_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "ranger")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Far from the bustle of cities and towns, past the hedges that shelter the most distant farms from the"+
        " terrors of the wild, amid the dense-packed trees of trackless forests and across wide and empty plains, rangers keep their unending watch.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Agility to bring down foes and Wisdom to use the power of Nature.";
            textBox_description.Text += "\r\n" + "Starting HP = 10 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 6 + Constitution modifier per ranger level after 1st.";
            textBox_description.Text += "\r\n" + "You gain (+5) damage bonus against an enemy type of your choice.";
            textBox_description.Text += "\r\n" + "Your proficiency with bows give you (+2) damage bonus if you use a bow during combat.";
            textBox_description.Text += "\r\n" + "You use Agility instead of Strength to deal damage.";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: Light Armour, Medium Armour, Shields." + "\r\n" + "Weapons: Simple Weapons, Martial Weapons.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (10 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Two shortswords (1-6 of piercing damage)"+"\r\n"+"Longbow and 20 arrows (1-8 of piercing damage)"+"\r\n"+"Leather armour (11 AC)";

            comboBox_chosenenemy.Visible = true;
            comboBox_chosenenemy.Enabled = true;

            label_chosenenemy.Visible = true;
            label_chosenenemy.Enabled = true;

            SelectedClass = "ranger";

            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["ArrowSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Arrow"]);
            simpleSound.Play();
            pictureBox_ranger.Visible = true;
            pictureBox_ranger.Enabled = true;
            //button_ranger.BackColor = System.Drawing.Color.Red;
        }

        private void Button_rogue_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "rogue")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Rogues rely on skill, stealth, and their foes’ vulnerabilities to get the upper hand in any situation.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Agility to stealthy eliminate your opponents.";
            textBox_description.Text += "\r\n" + "Starting HP = 8 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 5 + Constitution modifier per rogue level after 1st.";
            textBox_description.Text += "\r\n" + "You deal additional (+4) damage to an enemy if you use a simple weapon.";
            textBox_description.Text += "\r\n" + "You use Agility instead of Strength to deal damage.";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: Light Armour." + "\r\n" + "Weapons: Simple Weapons, hand crossbows, longswords, rapiers, shortswords.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Rapier (1-8 of piercing damage)"+"\r\n"+"Two daggers (1-4 of piercing damage)"+"\r\n"+"Shortbow and 20 arrows (1-6 of piercing damage)"+"\r\n"+"Leather armour (11 AC)";

            SelectedClass = "rogue";

            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["DaggerSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Dagger"]);
            simpleSound.Play();
            pictureBox_rogue.Visible = true;
            pictureBox_rogue.Enabled = true;
            //button_rogue.BackColor = System.Drawing.Color.Red;
        }

        private void Button_sorcerer_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "sorcerer")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Sorcerers have no use for the spellbooks and ancient tomes of magic lore that wizards rely on, nor do they" +
        " rely on a patron to grant their spells as warlocks do. By learning to harness and channel their own inborn dragon magic, they can discover new and staggering ways" +
        " to unleash that power.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Charisma to cast spells.";
            textBox_description.Text += "\r\n" + "Starting HP = 7 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 5 + Constitution modifier per sorcerer level after 1st.";
            textBox_description.Text += "\r\n" + "At 1st level, you have 4 sorcerer cantrips and 2 1st-level sorcerer spells of your choice.";
            textBox_description.Text += "\r\n" + "You start with 2 1st-level spell slots and gain more as you level up.";
            textBox_description.Text += "\r\n" + "When you aren’t wearing armour, your Armour Class equals 13 + your Agility modifier.";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: none." + "\r\n" + "Weapons: daggers, darts, slings, quarterstaffs, light crossbows.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (7 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Two daggers (1-4 of piercing damage)"+"\r\n"+"Light crossbow and 20 bolts (1-8 of piercing damage)";

            label_cantrips.Visible = true;
            label_cantrips.Enabled = true;

            checkedListBox_cantrips.Visible = true;
            checkedListBox_cantrips.Enabled = true;

            checkedListBox_cantrips.Items.Add("Acid Splash", false);
            checkedListBox_cantrips.Items.Add("Blade Ward", false);
            checkedListBox_cantrips.Items.Add("Chill Touch", false);
            checkedListBox_cantrips.Items.Add("Ray of Frost", false);
            checkedListBox_cantrips.Items.Add("True Strike", false);
            checkedListBox_cantrips.Items.Add("Fire Bolt", false);
            checkedListBox_cantrips.Items.Add("Shocking Grasp", false);
            checkedListBox_cantrips.Items.Add("Poison Spray", false);

            SelectedClass = "sorcerer";

            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["MagicSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Magic"]);
            simpleSound.Play();
            button_sorcerer.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Star"]);
            //.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\Star.png");
            //button_sorcerer.BackColor = System.Drawing.Color.Red;
        }

        private void Button_warlock_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "warlock")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Warlocks are seekers of the knowledge that lies hidden in the fabric of the multiverse. Through pacts"+
" made with mysterious beings of supernatural power, warlocks unlock magical effects both subtle and spectacular.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Charisma to cast spells.";
            textBox_description.Text += "\r\n" + "Starting HP = 8 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 5 + Constitution modifier per warlock level after 1st.";
            textBox_description.Text += "\r\n" + "At 1st level, you have 2 warlock cantrips of your choice and 2 1st-level warlock spells (Burning Hands, Command).";
            textBox_description.Text += "\r\n" + "You start with 1 1st-level spell slots and gain more as you level up.";
            textBox_description.Text += "\r\n" + "Starting at 1st level, when you reduce a hostile creature to 0 hit points, you gain temporary hit points equal "+
                "to your Charisma modifier + your warlock level (minimum of 1).";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: Light Armour." + "\r\n" + "Weapons: Simple Weapons.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }
            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Two daggers (1-4 of piercing damage)"+"\r\n"+"Sickle (1-4 of slashing damage)"+"\r\n"+"Light crossbow and 20 bolts (1-8 of piercing damage)"+"\r\n"+"Leather armour (11 AC)";

            checkedListBox_cantrips.Visible = true;
            checkedListBox_cantrips.Enabled = true;

            label_cantrips.Visible = true;
            label_cantrips.Enabled = true;

            checkedListBox_cantrips.Items.Add("Blade Ward", false);
            checkedListBox_cantrips.Items.Add("Chill Touch", false);
            checkedListBox_cantrips.Items.Add("True Strike", false);
            checkedListBox_cantrips.Items.Add("Eldritch Blast", false);
            checkedListBox_cantrips.Items.Add("Poison Spray", false);

            SelectedClass = "warlock";

            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["DarkMagicSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["DarkMagic"]);
            simpleSound.Play();
            button_warlock.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Tentacles"]);
            //.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\Tentacles.png");
            //button_warlock.BackColor = System.Drawing.Color.Red;
        }

        private void Button_wizard_Click(object sender, EventArgs e)
        {
            if (SelectedClass == "wizard")
            {
                return;
            }

            RemoveClassSelection();

            textBox_description.Text = "Wizards are supreme magic-users, defined and united as a class by the spells they cast. Drawing on the subtle " +
    "weave of magic that permeates the cosmos, wizards cast spells of explosive fire, arcing lightning, subtle deception, and brute-force mind control.";

            textBox_description.Text += "\r\n" + "\r\n" + "You use Intellegence to cast spells.";
            textBox_description.Text += "\r\n" + "Starting HP = 6 + Constitution modifier.";
            textBox_description.Text += "\r\n" + "HP after 1st level = 4 + Constitution modifier per wizard level after 1st.";
            textBox_description.Text += "\r\n" + "At 1st level, you have a spellbook containing six 1st-level wizard spells and 3 wizard cantrips of your choice.";
            textBox_description.Text += "\r\n" + "You start with 2 1st-level spell slots and gain more as you level up.";
            textBox_description.Text += "\r\n" + "Proficiencies:" + "\r\n" + "Armour: none." + "\r\n" + "Weapons: daggers, darts, slings, quarterstaffs, light crossbows.";

            if (textBox_constitution_mod.Text != "-")
            {
                label_hp.Text = "Starting Hit Points: " + (6 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
            }

            label_startingequipment.Text = "Starting equipment:" + "\r\n" + "Quarterstaff (1-6 of bludgeoning damage)";

            checkedListBox_cantrips.Visible = true;
            checkedListBox_cantrips.Enabled = true;

            label_cantrips.Visible = true;
            label_cantrips.Enabled = true;

            checkedListBox_cantrips.Items.Add("Acid Splash", false);
            checkedListBox_cantrips.Items.Add("Blade Ward", false);
            checkedListBox_cantrips.Items.Add("Chill Touch", false);
            checkedListBox_cantrips.Items.Add("Ray of Frost", false);
            checkedListBox_cantrips.Items.Add("True Strike", false);
            checkedListBox_cantrips.Items.Add("Fire Bolt", false);
            checkedListBox_cantrips.Items.Add("Shocking Grasp", false);
            checkedListBox_cantrips.Items.Add("Poison Spray", false);

            SelectedClass = "wizard";

            //SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["LightningSound"]);
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Lightning"]);
            simpleSound.Play();
            button_wizard.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Lightning"]);
            //.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\Lightning_image.png");
            //button_wizard.BackColor = System.Drawing.Color.Red;
        }

        private void Button_acolyte_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text ="You came from a temple, surving a diety that possibly grants you powers.";
            textBox_description.Text +="\r\n"+"\r\n"+ "You gain (+1) bonus to Charisma and (+2) bonus to Wisdom.";

            Combobox_reset();

            comboBox_charisma.Items[1] = 9;
            comboBox_charisma.Items[2] = 11;
            comboBox_charisma.Items[3] = 12;
            comboBox_charisma.Items[4] = 14;
            comboBox_charisma.Items[5] = 15;
            comboBox_charisma.Items[6] = 16;

            comboBox_wisdom.Items[1] = 10;
            comboBox_wisdom.Items[2] = 12;
            comboBox_wisdom.Items[3] = 14;
            comboBox_wisdom.Items[4] = 15;
            comboBox_wisdom.Items[5] = 16;
            comboBox_wisdom.Items[6] = 17;

            SelectedBackground = "acolyte";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_acolyte.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\Frame.PNG");
            //button_acolyte.BackColor = System.Drawing.Color.Red;
        }

        private void Button_charlatan_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You came from the streets, where you deceived citizens and made money of it.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+1) bonus to Agility and (+2) bonus to Charisma.";

            Combobox_reset();

            comboBox_agility.Items[1] = 9;
            comboBox_agility.Items[2] = 11;
            comboBox_agility.Items[3] = 12;
            comboBox_agility.Items[4] = 14;
            comboBox_agility.Items[5] = 15;
            comboBox_agility.Items[6] = 16;

            comboBox_charisma.Items[1] = 10;
            comboBox_charisma.Items[2] = 12;
            comboBox_charisma.Items[3] = 14;
            comboBox_charisma.Items[4] = 15;
            comboBox_charisma.Items[5] = 16;
            comboBox_charisma.Items[6] = 17;

            SelectedBackground = "charlatan";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_charlatan.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //.BackgroundImage = System.Drawing.Image.FromFile(@"..\\..\\Resources\\Frame.PNG");
            //button_charlatan.BackColor = System.Drawing.Color.Red;
        }

        private void Button_criminal_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You were a part of a notorious gang, which held in fear several cities.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+2) bonus to Agility and (+1) bonus to Strength.";

            Combobox_reset();

            comboBox_strength.Items[1] = 9;
            comboBox_strength.Items[2] = 11;
            comboBox_strength.Items[3] = 12;
            comboBox_strength.Items[4] = 14;
            comboBox_strength.Items[5] = 15;
            comboBox_strength.Items[6] = 16;

            comboBox_agility.Items[1] = 10;
            comboBox_agility.Items[2] = 12;
            comboBox_agility.Items[3] = 14;
            comboBox_agility.Items[4] = 15;
            comboBox_agility.Items[5] = 16;
            comboBox_agility.Items[6] = 17;

            SelectedBackground = "criminal";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_criminal.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_criminal.BackColor = System.Drawing.Color.Red;
        }

        private void Button_entertainer_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You performed on stage and made living by entertaining crowds.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+3) bonus to Charisma.";

            Combobox_reset();

            comboBox_charisma.Items[1] = 11;
            comboBox_charisma.Items[2] = 13;
            comboBox_charisma.Items[3] = 15;
            comboBox_charisma.Items[4] = 16;
            comboBox_charisma.Items[5] = 17;
            comboBox_charisma.Items[6] = 18;

            SelectedBackground = "entertainer";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_entertainer.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_entertainer.BackColor = System.Drawing.Color.Red;
        }

        private void Button_folkhero_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You saved dozens of people when the accident happened, becoming a folk lore legend.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+2) bonus to Constitution and (+1) bonus to Wisdom.";

            Combobox_reset();

            comboBox_constitution.Items[1] = 10;
            comboBox_constitution.Items[2] = 12;
            comboBox_constitution.Items[3] = 14;
            comboBox_constitution.Items[4] = 15;
            comboBox_constitution.Items[5] = 16;
            comboBox_constitution.Items[6] = 17;

            comboBox_wisdom.Items[1] = 9;
            comboBox_wisdom.Items[2] = 11;
            comboBox_wisdom.Items[3] = 13;
            comboBox_wisdom.Items[4] = 14;
            comboBox_wisdom.Items[5] = 15;
            comboBox_wisdom.Items[6] = 16;

            SelectedBackground = "folkhero";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_folkhero.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_folkhero.BackColor = System.Drawing.Color.Red;
        }

        private void Button_guildartisan_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You were a guild master in one of the cities and earned money through business.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+2) bonus to Charisma and (+1) bonus to Intellegence.";

            Combobox_reset();

            comboBox_charisma.Items[1] = 10;
            comboBox_charisma.Items[2] = 12;
            comboBox_charisma.Items[3] = 14;
            comboBox_charisma.Items[4] = 15;
            comboBox_charisma.Items[5] = 16;
            comboBox_charisma.Items[6] = 17;

            comboBox_intellegence.Items[1] = 9;
            comboBox_intellegence.Items[2] = 11;
            comboBox_intellegence.Items[3] = 13;
            comboBox_intellegence.Items[4] = 14;
            comboBox_intellegence.Items[5] = 15;
            comboBox_intellegence.Items[6] = 16;

            SelectedBackground = "guildartisan";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_guildartisan.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_guildartisan.BackColor = System.Drawing.Color.Red;
        }

        private void Button_hermit_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You travelled the lands searching wisdom and seeking knowledge.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+1) bonus to Intellegence and (+2) bonus to Wisdom.";

            Combobox_reset();

            comboBox_wisdom.Items[1] = 10;
            comboBox_wisdom.Items[2] = 12;
            comboBox_wisdom.Items[3] = 14;
            comboBox_wisdom.Items[4] = 15;
            comboBox_wisdom.Items[5] = 16;
            comboBox_wisdom.Items[6] = 17;

            comboBox_intellegence.Items[1] = 9;
            comboBox_intellegence.Items[2] = 11;
            comboBox_intellegence.Items[3] = 13;
            comboBox_intellegence.Items[4] = 14;
            comboBox_intellegence.Items[5] = 15;
            comboBox_intellegence.Items[6] = 16;

            SelectedBackground = "hermit";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_hermit.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_hermit.BackColor = System.Drawing.Color.Red;
        }

        private void Button_outlander_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            OutlanderStatReset();

            textBox_description.Text = "No one knows where you came from and what your story is.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain two random characteristics bonuses.";

            Combobox_reset();

            Random Roll = new Random();

            int path = Roll.Next(0, 2);

            if (path==0)
            {
                int random_stat = Roll.Next(0, 6);
                StatsArray[random_stat] += 1;
                random_stat = Roll.Next(0, 6);
                StatsArray[random_stat] += 2;
            }
            if (path == 1)
            {
                int random_stat = Roll.Next(0, 6);
                StatsArray[random_stat] += 3;
            }

            SelectedBackground = "outlander";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_outlander.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_outlander.BackColor = System.Drawing.Color.Red;
        }

        private void Button_noble_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You came from a noble house ";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+1) bonus to Constitution and (+2) bonus to Intellegence.";

            Combobox_reset();

            comboBox_intellegence.Items[1] = 10;
            comboBox_intellegence.Items[2] = 12;
            comboBox_intellegence.Items[3] = 14;
            comboBox_intellegence.Items[4] = 15;
            comboBox_intellegence.Items[5] = 16;
            comboBox_intellegence.Items[6] = 17;

            comboBox_constitution.Items[1] = 9;
            comboBox_constitution.Items[2] = 11;
            comboBox_constitution.Items[3] = 13;
            comboBox_constitution.Items[4] = 14;
            comboBox_constitution.Items[5] = 15;
            comboBox_constitution.Items[6] = 16;

            SelectedBackground = "noble";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_noble.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_noble.BackColor = System.Drawing.Color.Red;
        }

        private void Button_sage_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You studied hard to learn the mysteries of the Multiverse.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+3) bonus to Intellegence.";

            Combobox_reset();

            comboBox_intellegence.Items[1] = 11;
            comboBox_intellegence.Items[2] = 13;
            comboBox_intellegence.Items[3] = 15;
            comboBox_intellegence.Items[4] = 16;
            comboBox_intellegence.Items[5] = 17;
            comboBox_intellegence.Items[6] = 18;

            SelectedBackground = "sage";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_sage.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_sage.BackColor = System.Drawing.Color.Red;
        }

        private void Button_sailor_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You sailed the seas of the world and got tough by all the challenges you faced.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+3) bonus to Stength.";

            Combobox_reset();

            comboBox_strength.Items[1] = 11;
            comboBox_strength.Items[2] = 13;
            comboBox_strength.Items[3] = 15;
            comboBox_strength.Items[4] = 16;
            comboBox_strength.Items[5] = 17;
            comboBox_strength.Items[6] = 18;

            SelectedBackground = "sailor";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_sailor.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_sailor.BackColor = System.Drawing.Color.Red;
        }

        private void Button_soldier_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You are a trained soldier and your combat skills are outstanding.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+2) bonus to Strength and (+1) bonus to Constitution.";

            Combobox_reset();

            comboBox_strength.Items[1] = 10;
            comboBox_strength.Items[2] = 12;
            comboBox_strength.Items[3] = 14;
            comboBox_strength.Items[4] = 15;
            comboBox_strength.Items[5] = 16;
            comboBox_strength.Items[6] = 17;

            comboBox_constitution.Items[1] = 9;
            comboBox_constitution.Items[2] = 11;
            comboBox_constitution.Items[3] = 13;
            comboBox_constitution.Items[4] = 14;
            comboBox_constitution.Items[5] = 15;
            comboBox_constitution.Items[6] = 16;

            SelectedBackground = "soldier";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_soldier.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_soldier.BackColor = System.Drawing.Color.Red;
        }

        private void Button_urchin_Click(object sender, EventArgs e)
        {
            RemoveBackgroundSelection();

            textBox_description.Text = "You were an urchin from the very childhood, which made you sneaky and cunning.";
            textBox_description.Text += "\r\n" + "\r\n" + "You gain (+3) bonus to Agility.";

            Combobox_reset();

            comboBox_agility.Items[1] = 11;
            comboBox_agility.Items[2] = 13;
            comboBox_agility.Items[3] = 15;
            comboBox_agility.Items[4] = 16;
            comboBox_agility.Items[5] = 17;
            comboBox_agility.Items[6] = 18;

            SelectedBackground = "urchin";

            //SoundPlayer simpleSound = new SoundPlayer(@"..\\..\\Resources\\Click.wav");
            SoundPlayer simpleSound = new SoundPlayer(soundsPaths["Click"]);
            simpleSound.Play();
            button_urchin.BackgroundImage = System.Drawing.Image.FromFile(imagesPaths["Frame"]);
            //(@"..\\..\\Resources\\Frame.PNG");
            //button_urchin.BackColor = System.Drawing.Color.Red;
        }

        private void Button_submit_Click(object sender, EventArgs e)
        {
            if (SelectedClass=="")
            {
                MessageBox.Show("Choose your class!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (SelectedBackground == "")
            {
                MessageBox.Show("Choose your background!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (comboBox_strength.SelectedIndex==0|| comboBox_agility.SelectedIndex == 0|| comboBox_constitution.SelectedIndex == 0|| comboBox_intellegence.SelectedIndex == 0
                || comboBox_wisdom.SelectedIndex == 0|| comboBox_charisma.SelectedIndex == 0)
            {
                MessageBox.Show("Check your stats!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MainForm MainForm_1 = new MainForm();
            MainForm_1.Show();
            this.Visible = false;
            this.Enabled = false;
            return;
        }
        /// <summary>
        /// !OBSOLETE!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void TextBox_strength_Leave(object sender, EventArgs e)
        //{
        //    if (int.Parse(textBox_strength.Text) > 15|| int.Parse(textBox_strength.Text) == 9|| int.Parse(textBox_strength.Text) < 8|| int.Parse(textBox_strength.Text) ==11)
        //    {
        //        textBox_strength.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_strength.Focus();
        //        return;
        //    }

        //    if (textBox_strength.Text == "8" && (textBox_agility.Text=="8"|| textBox_constitution.Text == "8" || textBox_intellegence.Text == "8" || textBox_wisdom.Text == "8" || textBox_charisma.Text == "8"))
        //    {
        //        textBox_strength.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_strength.Focus();
        //        return;
        //    }
        //    if (textBox_strength.Text == "10" && (textBox_agility.Text == "10" || textBox_constitution.Text == "10" || textBox_intellegence.Text == "10" || textBox_wisdom.Text == "10" || textBox_charisma.Text == "10"))
        //    {
        //        textBox_strength.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_strength.Focus();
        //        return;
        //    }
        //    if (textBox_strength.Text == "12" && (textBox_agility.Text == "12" || textBox_constitution.Text == "12" || textBox_intellegence.Text == "12" || textBox_wisdom.Text == "12" || textBox_charisma.Text == "12"))
        //    {
        //        textBox_strength.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_strength.Focus();
        //        return;
        //    }
        //    if (textBox_strength.Text == "13" && (textBox_agility.Text == "13" || textBox_constitution.Text == "13" || textBox_intellegence.Text == "13" || textBox_wisdom.Text == "13" || textBox_charisma.Text == "13"))
        //    {
        //        textBox_strength.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_strength.Focus();
        //        return;
        //    }
        //    if (textBox_strength.Text == "14" && (textBox_agility.Text == "14" || textBox_constitution.Text == "14" || textBox_intellegence.Text == "14" || textBox_wisdom.Text == "14" || textBox_charisma.Text == "14"))
        //    {
        //        textBox_strength.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_strength.Focus();
        //        return;
        //    }
        //    if (textBox_strength.Text == "15" && (textBox_agility.Text == "15" || textBox_constitution.Text == "15" || textBox_intellegence.Text == "15" || textBox_wisdom.Text == "15" || textBox_charisma.Text == "15"))
        //    {
        //        textBox_strength.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_strength.Focus();
        //        return;
        //    }
        //    double value = (Math.Abs(int.Parse(textBox_strength.Text) - 10) / 2);
        //    textBox_strength_mod.Text = Math.Floor(value).ToString();
        //}

        //private void TextBox_agility_Leave(object sender, EventArgs e)
        //{
        //    if (int.Parse(textBox_agility.Text) > 15 || int.Parse(textBox_agility.Text) == 9 || int.Parse(textBox_agility.Text) < 8 || int.Parse(textBox_agility.Text) == 11)
        //    {
        //        textBox_agility.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_agility.Focus();
        //        return;
        //    }

        //    if (textBox_agility.Text == "8" && (textBox_strength.Text == "8" || textBox_constitution.Text == "8" || textBox_intellegence.Text == "8" || textBox_wisdom.Text == "8" || textBox_charisma.Text == "8"))
        //    {
        //        textBox_agility.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_agility.Focus();
        //        return;
        //    }
        //    if (textBox_agility.Text == "10" && (textBox_strength.Text == "10" || textBox_constitution.Text == "10" || textBox_intellegence.Text == "10" || textBox_wisdom.Text == "10" || textBox_charisma.Text == "10"))
        //    {
        //        textBox_agility.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_agility.Focus();
        //        return;
        //    }
        //    if (textBox_agility.Text == "12" && (textBox_strength.Text == "12" || textBox_constitution.Text == "12" || textBox_intellegence.Text == "12" || textBox_wisdom.Text == "12" || textBox_charisma.Text == "12"))
        //    {
        //        textBox_agility.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_agility.Focus();
        //        return;
        //    }
        //    if (textBox_agility.Text == "13" && (textBox_strength.Text == "13" || textBox_constitution.Text == "13" || textBox_intellegence.Text == "13" || textBox_wisdom.Text == "13" || textBox_charisma.Text == "13"))
        //    {
        //        textBox_agility.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_agility.Focus();
        //        return;
        //    }
        //    if (textBox_agility.Text == "14" && (textBox_strength.Text == "14" || textBox_constitution.Text == "14" || textBox_intellegence.Text == "14" || textBox_wisdom.Text == "14" || textBox_charisma.Text == "14"))
        //    {
        //        textBox_agility.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_agility.Focus();
        //        return;
        //    }
        //    if (textBox_agility.Text == "15" && (textBox_strength.Text == "15" || textBox_constitution.Text == "15" || textBox_intellegence.Text == "15" || textBox_wisdom.Text == "15" || textBox_charisma.Text == "15"))
        //    {
        //        textBox_agility.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_agility.Focus();
        //        return;
        //    }
        //}

        //private void TextBox_constitution_Leave(object sender, EventArgs e)
        //{
        //    if (int.Parse(textBox_constitution.Text) > 15 || int.Parse(textBox_constitution.Text) == 9 || int.Parse(textBox_constitution.Text) < 8 || int.Parse(textBox_constitution.Text) == 11)
        //    {
        //        textBox_constitution.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_constitution.Focus();
        //        return;
        //    }

        //    if (textBox_constitution.Text == "8" && (textBox_strength.Text == "8" || textBox_agility.Text == "8" || textBox_intellegence.Text == "8" || textBox_wisdom.Text == "8" || textBox_charisma.Text == "8"))
        //    {
        //        textBox_constitution.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_constitution.Focus();
        //        return;
        //    }
        //    if (textBox_constitution.Text == "10" && (textBox_strength.Text == "10" || textBox_agility.Text == "10" || textBox_intellegence.Text == "10" || textBox_wisdom.Text == "10" || textBox_charisma.Text == "10"))
        //    {
        //        textBox_constitution.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_constitution.Focus();
        //        return;
        //    }
        //    if (textBox_constitution.Text == "12" && (textBox_strength.Text == "12" || textBox_agility.Text == "12" || textBox_intellegence.Text == "12" || textBox_wisdom.Text == "12" || textBox_charisma.Text == "12"))
        //    {
        //        textBox_constitution.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_constitution.Focus();
        //        return;
        //    }
        //    if (textBox_constitution.Text == "13" && (textBox_strength.Text == "13" || textBox_agility.Text == "13" || textBox_intellegence.Text == "13" || textBox_wisdom.Text == "13" || textBox_charisma.Text == "13"))
        //    {
        //        textBox_constitution.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_constitution.Focus();
        //        return;
        //    }
        //    if (textBox_constitution.Text == "14" && (textBox_strength.Text == "14" || textBox_agility.Text == "14" || textBox_intellegence.Text == "14" || textBox_wisdom.Text == "14" || textBox_charisma.Text == "14"))
        //    {
        //        textBox_constitution.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_constitution.Focus();
        //        return;
        //    }
        //    if (textBox_constitution.Text == "15" && (textBox_strength.Text == "15" || textBox_agility.Text == "15" || textBox_intellegence.Text == "15" || textBox_wisdom.Text == "15" || textBox_charisma.Text == "15"))
        //    {
        //        textBox_constitution.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_constitution.Focus();
        //        return;
        //    }
        //}

        //private void TextBox_intellegence_Leave(object sender, EventArgs e)
        //{
        //    if (int.Parse(textBox_intellegence.Text) > 15 || int.Parse(textBox_intellegence.Text) == 9 || int.Parse(textBox_intellegence.Text) < 8 || int.Parse(textBox_intellegence.Text) == 11)
        //    {
        //        textBox_intellegence.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_intellegence.Focus();
        //        return;
        //    }

        //    if (textBox_intellegence.Text == "8" && (textBox_strength.Text == "8" || textBox_agility.Text == "8" || textBox_constitution.Text == "8" || textBox_wisdom.Text == "8" || textBox_charisma.Text == "8"))
        //    {
        //        textBox_intellegence.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_intellegence.Focus();
        //        return;
        //    }
        //    if (textBox_intellegence.Text == "10" && (textBox_strength.Text == "10" || textBox_agility.Text == "10" || textBox_constitution.Text == "10" || textBox_wisdom.Text == "10" || textBox_charisma.Text == "10"))
        //    {
        //        textBox_intellegence.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        return;
        //    }
        //    if (textBox_intellegence.Text == "12" && (textBox_strength.Text == "12" || textBox_agility.Text == "12" || textBox_constitution.Text == "12" || textBox_wisdom.Text == "12" || textBox_charisma.Text == "12"))
        //    {
        //        textBox_intellegence.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_intellegence.Focus();
        //        return;
        //    }
        //    if (textBox_intellegence.Text == "13" && (textBox_strength.Text == "13" || textBox_agility.Text == "13" || textBox_constitution.Text == "13" || textBox_wisdom.Text == "13" || textBox_charisma.Text == "13"))
        //    {
        //        textBox_intellegence.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_intellegence.Focus();
        //        return;
        //    }
        //    if (textBox_intellegence.Text == "14" && (textBox_strength.Text == "14" || textBox_agility.Text == "14" || textBox_constitution.Text == "14" || textBox_wisdom.Text == "14" || textBox_charisma.Text == "14"))
        //    {
        //        textBox_intellegence.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_intellegence.Focus();
        //        return;
        //    }
        //    if (textBox_intellegence.Text == "15" && (textBox_strength.Text == "15" || textBox_agility.Text == "15" || textBox_constitution.Text == "15" || textBox_wisdom.Text == "15" || textBox_charisma.Text == "15"))
        //    {
        //        textBox_intellegence.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_intellegence.Focus();
        //        return;
        //    }
        //}

        //private void TextBox_wisdom_Leave(object sender, EventArgs e)
        //{
        //    if (int.Parse(textBox_wisdom.Text) > 15 || int.Parse(textBox_wisdom.Text) == 9 || int.Parse(textBox_wisdom.Text) < 8 || int.Parse(textBox_wisdom.Text) == 11)
        //    {
        //        textBox_wisdom.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_wisdom.Focus();
        //        return;
        //    }

        //    if (textBox_wisdom.Text == "8" && (textBox_strength.Text == "8" || textBox_agility.Text == "8" || textBox_constitution.Text == "8" || textBox_intellegence.Text == "8" || textBox_charisma.Text == "8"))
        //    {
        //        textBox_wisdom.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_wisdom.Focus();
        //        return;
        //    }
        //    if (textBox_wisdom.Text == "10" && (textBox_strength.Text == "10" || textBox_agility.Text == "10" || textBox_constitution.Text == "10" || textBox_intellegence.Text == "10" || textBox_charisma.Text == "10"))
        //    {
        //        textBox_wisdom.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_wisdom.Focus();
        //        return;
        //    }
        //    if (textBox_wisdom.Text == "12" && (textBox_strength.Text == "12" || textBox_agility.Text == "12" || textBox_constitution.Text == "12" || textBox_intellegence.Text == "12" || textBox_charisma.Text == "12"))
        //    {
        //        textBox_wisdom.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_wisdom.Focus();
        //        return;
        //    }
        //    if (textBox_wisdom.Text == "13" && (textBox_strength.Text == "13" || textBox_agility.Text == "13" || textBox_constitution.Text == "13" || textBox_intellegence.Text == "13" || textBox_charisma.Text == "13"))
        //    {
        //        textBox_wisdom.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_wisdom.Focus();
        //        return;
        //    }
        //    if (textBox_wisdom.Text == "14" && (textBox_strength.Text == "14" || textBox_agility.Text == "14" || textBox_constitution.Text == "14" || textBox_intellegence.Text == "14" || textBox_charisma.Text == "14"))
        //    {
        //        textBox_wisdom.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_wisdom.Focus();
        //        return;
        //    }
        //    if (textBox_wisdom.Text == "15" && (textBox_strength.Text == "15" || textBox_agility.Text == "15" || textBox_constitution.Text == "15" || textBox_intellegence.Text == "15" || textBox_charisma.Text == "15"))
        //    {
        //        textBox_wisdom.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_wisdom.Focus();
        //        return;
        //    }
        //}

        //private void TextBox_charisma_Leave(object sender, EventArgs e)
        //{
        //    if (int.Parse(textBox_charisma.Text) > 15 || int.Parse(textBox_charisma.Text) == 9 || int.Parse(textBox_charisma.Text) < 8 || int.Parse(textBox_charisma.Text) == 11)
        //    {
        //        textBox_charisma.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_charisma.Focus();
        //        return;
        //    }

        //    if (textBox_charisma.Text == "8" && (textBox_strength.Text == "8" || textBox_agility.Text == "8" || textBox_constitution.Text == "8" || textBox_intellegence.Text == "8" || textBox_wisdom.Text == "8"))
        //    {
        //        textBox_charisma.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_charisma.Focus();
        //        return;
        //    }
        //    if (textBox_charisma.Text == "10" && (textBox_strength.Text == "10" || textBox_agility.Text == "10" || textBox_constitution.Text == "10" || textBox_intellegence.Text == "10" || textBox_wisdom.Text == "10"))
        //    {
        //        textBox_charisma.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_charisma.Focus();
        //        return;
        //    }
        //    if (textBox_charisma.Text == "12" && (textBox_strength.Text == "12" || textBox_agility.Text == "12" || textBox_constitution.Text == "12" || textBox_intellegence.Text == "12" || textBox_wisdom.Text == "12"))
        //    {
        //        textBox_charisma.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_charisma.Focus();
        //        return;
        //    }
        //    if (textBox_charisma.Text == "13" && (textBox_strength.Text == "13" || textBox_agility.Text == "13" || textBox_constitution.Text == "13" || textBox_intellegence.Text == "13" || textBox_wisdom.Text == "13"))
        //    {
        //        textBox_charisma.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_charisma.Focus();
        //        return;
        //    }
        //    if (textBox_charisma.Text == "14" && (textBox_strength.Text == "14" || textBox_agility.Text == "14" || textBox_constitution.Text == "14" || textBox_intellegence.Text == "14" || textBox_wisdom.Text == "14"))
        //    {
        //        textBox_charisma.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_charisma.Focus();
        //        return;
        //    }
        //    if (textBox_charisma.Text == "15" && (textBox_strength.Text == "15" || textBox_agility.Text == "15" || textBox_constitution.Text == "15" || textBox_intellegence.Text == "15" || textBox_wisdom.Text == "15"))
        //    {
        //        textBox_charisma.Text = "";
        //        MessageBox.Show("Invalid value! Try one of the following Numberss: (8), (10), (12), (13), (14), (15).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        textBox_charisma.Focus();
        //        return;
        //    }
        //} //OBSOLETE!!

        private void ComboBox_strength_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((comboBox_strength.SelectedIndex==comboBox_agility.SelectedIndex)|| (comboBox_strength.SelectedIndex == comboBox_constitution.SelectedIndex)
                || (comboBox_strength.SelectedIndex == comboBox_intellegence.SelectedIndex)|| (comboBox_strength.SelectedIndex == comboBox_wisdom.SelectedIndex)
                    || (comboBox_strength.SelectedIndex == comboBox_charisma.SelectedIndex))&&comboBox_strength.SelectedIndex!=0)
            {
                MessageBox.Show("You have already selected this position!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                comboBox_strength.SelectedIndex = 0;

                label_hp.Focus();

                return;
            }

            if (comboBox_strength.SelectedIndex != 0)
            {
                decimal value = Decimal.Divide((int.Parse(comboBox_strength.Text) - 10), 2);
                textBox_strength_mod.Text = Math.Floor(value).ToString();
            }
            else
            { 
                textBox_strength_mod.Text = "-";
            }

            label_hp.Focus();
        }

        private void ComboBox_agility_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((comboBox_agility.SelectedIndex == comboBox_strength.SelectedIndex) || (comboBox_agility.SelectedIndex == comboBox_constitution.SelectedIndex)
                || (comboBox_agility.SelectedIndex == comboBox_intellegence.SelectedIndex) || (comboBox_agility.SelectedIndex == comboBox_wisdom.SelectedIndex)
                    || (comboBox_agility.SelectedIndex == comboBox_charisma.SelectedIndex)) && comboBox_agility.SelectedIndex != 0)
            {
                MessageBox.Show("You have already selected this position!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                comboBox_agility.SelectedIndex = 0;

                label_hp.Focus();

                return;
            }

            if (comboBox_agility.SelectedIndex != 0)
            {
                decimal value = Decimal.Divide((int.Parse(comboBox_agility.Text) - 10) , 2);
                textBox_agility_mod.Text = Math.Floor(value).ToString();
            }
            else
            {
                textBox_agility_mod.Text = "-";
            }

            label_hp.Focus();
        }

        private void ComboBox_constitution_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((comboBox_constitution.SelectedIndex == comboBox_strength.SelectedIndex) || (comboBox_constitution.SelectedIndex == comboBox_agility.SelectedIndex)
                || (comboBox_constitution.SelectedIndex == comboBox_intellegence.SelectedIndex) || (comboBox_constitution.SelectedIndex == comboBox_wisdom.SelectedIndex)
                    || (comboBox_constitution.SelectedIndex == comboBox_charisma.SelectedIndex)) && comboBox_constitution.SelectedIndex != 0)
            {
                MessageBox.Show("You have already selected this position!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                comboBox_constitution.SelectedIndex = 0;

                label_hp.Focus();

                return;
            }

            if (comboBox_constitution.SelectedIndex != 0)
            {
                decimal value = Decimal.Divide((int.Parse(comboBox_constitution.Text) - 10) , 2);
                textBox_constitution_mod.Text = Math.Floor(value).ToString();

                if (SelectedClass!="")
                {
                    if (SelectedClass == "barbarian")
                    {
                        label_hp.Text = "Starting Hit Points: " + (12 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "bard")
                    {
                        label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "cleric")
                    {
                        label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "druid")
                    {
                        label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "fighter")
                    {
                        label_hp.Text = "Starting Hit Points: " + (10 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "monk")
                    {
                        label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "paladin")
                    {
                        label_hp.Text = "Starting Hit Points: " + (10 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "ranger")
                    {
                        label_hp.Text = "Starting Hit Points: " + (10 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "rogue")
                    {
                        label_hp.Text = "Starting Hit Points: " + (8 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "sorcerer")
                    {
                        label_hp.Text = "Starting Hit Points: " + (7 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "warlock")
                    {
                        label_hp.Text = "Starting Hit Points: " + (5 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                    if (SelectedClass == "wizard")
                    {
                        label_hp.Text = "Starting Hit Points: " + (6 + int.Parse(textBox_constitution_mod.Text.ToString())).ToString();
                    }
                }
                
            }
            else
            {
                textBox_constitution_mod.Text = "-";
                label_hp.Text = "Starting Hit Points:";
            }

            label_hp.Focus();
        }

        private void ComboBox_intellegence_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((comboBox_intellegence.SelectedIndex == comboBox_strength.SelectedIndex) || (comboBox_intellegence.SelectedIndex == comboBox_agility.SelectedIndex)
                || (comboBox_intellegence.SelectedIndex == comboBox_constitution.SelectedIndex) || (comboBox_intellegence.SelectedIndex == comboBox_wisdom.SelectedIndex)
                    || (comboBox_intellegence.SelectedIndex == comboBox_charisma.SelectedIndex)) && comboBox_intellegence.SelectedIndex != 0)
            {
                MessageBox.Show("You have already selected this position!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                comboBox_intellegence.SelectedIndex = 0;

                label_hp.Focus();

                return;
            }

            if (comboBox_intellegence.SelectedIndex != 0)
            {
                decimal value = Decimal.Divide((int.Parse(comboBox_intellegence.Text) - 10) , 2);
                textBox_intellegence_mod.Text = Math.Floor(value).ToString();
               
            }
            else
            {
                textBox_intellegence_mod.Text = "-";
            }

            label_hp.Focus();
        }

        private void ComboBox_wisdom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((comboBox_wisdom.SelectedIndex == comboBox_strength.SelectedIndex) || (comboBox_wisdom.SelectedIndex == comboBox_agility.SelectedIndex)
                || (comboBox_wisdom.SelectedIndex == comboBox_constitution.SelectedIndex) || (comboBox_wisdom.SelectedIndex == comboBox_intellegence.SelectedIndex)
                    || (comboBox_wisdom.SelectedIndex == comboBox_charisma.SelectedIndex)) && comboBox_wisdom.SelectedIndex != 0)
            {
                MessageBox.Show("You have already selected this position!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                comboBox_wisdom.SelectedIndex = 0;

                label_hp.Focus();
                return;

            }

            if (comboBox_wisdom.SelectedIndex != 0)
            {
                decimal value = Decimal.Divide((int.Parse(comboBox_wisdom.Text) - 10) , 2);
                textBox_wisdom_mod.Text = Math.Floor(value).ToString();
            }
            else
            {
                textBox_wisdom_mod.Text = "-";
            }

            label_hp.Focus();
        }

        private void ComboBox_charisma_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((comboBox_charisma.SelectedIndex == comboBox_strength.SelectedIndex) || (comboBox_charisma.SelectedIndex == comboBox_agility.SelectedIndex)
                || (comboBox_charisma.SelectedIndex == comboBox_constitution.SelectedIndex) || (comboBox_charisma.SelectedIndex == comboBox_intellegence.SelectedIndex)
                    || (comboBox_charisma.SelectedIndex == comboBox_wisdom.SelectedIndex)) && comboBox_charisma.SelectedIndex != 0)
            {
                MessageBox.Show("You have already selected this position!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                comboBox_charisma.SelectedIndex = 0;

                label_hp.Focus();

                return;
            }

            if (comboBox_charisma.SelectedIndex != 0)
            {
                decimal value = Decimal.Divide((int.Parse(comboBox_charisma.Text) - 10) , 2);
                textBox_charisma_mod.Text = Math.Floor(value).ToString();
            }
            else
            {
                textBox_charisma_mod.Text = "-";
            }

            label_hp.Focus();
        }
        private void ComboBox_chosenenemy_SelectedIndexChanged(object sender, EventArgs e)
        {
            label_hp.Focus();
        }
        private void checkedListBox_cantrips_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox_cantrips.Items.Count; i++)
            {
                if (checkedListBox_cantrips.GetItemRectangle(i).Contains(checkedListBox_cantrips.PointToClient(MousePosition)))
                {
                    switch (checkedListBox_cantrips.GetItemCheckState(i))
                    {
                        case CheckState.Checked:
                            checkedListBox_cantrips.SetItemCheckState(i, CheckState.Unchecked);
                            break;
                        case CheckState.Indeterminate:
                        case CheckState.Unchecked:
                            if (checkedListBox_cantrips.CheckedItems.Count == 2&&SelectedClass=="bard")
                            {
                                MessageBox.Show("You can't pick more cantrips!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            if (checkedListBox_cantrips.CheckedItems.Count == 3 && SelectedClass == "cleric")
                            {
                                MessageBox.Show("You can't pick more cantrips!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            if (checkedListBox_cantrips.CheckedItems.Count == 2 && SelectedClass == "druid")
                            {
                                MessageBox.Show("You can't pick more cantrips!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            if (checkedListBox_cantrips.CheckedItems.Count == 4 && SelectedClass == "sorcerer")
                            {
                                MessageBox.Show("You can't pick more cantrips!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            if (checkedListBox_cantrips.CheckedItems.Count == 2 && SelectedClass == "warlock")
                            {
                                MessageBox.Show("You can't pick more cantrips!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            if (checkedListBox_cantrips.CheckedItems.Count == 3 && SelectedClass == "wizard")
                            {
                                MessageBox.Show("You can't pick more cantrips!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            checkedListBox_cantrips.SetItemCheckState(i, CheckState.Checked);
                            break;
                    }

                }
                
            }
        }

        /// <summary>
        /// Removes selection from the textbox but leaves focus on it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_description_MouseEnter(object sender, EventArgs e)
        {
            textBox_description.Focus();
            textBox_description.Select(0, 0);
        }
    }
}
