﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
//using LibNPC_STATS;
//using LibNPC_FACTORY;
//using LibPLAYER_STATS;
using System.Media;

namespace TalesOfHeroes
{
    public partial class MainForm : Form
    {
        Random Roll = new Random();//dice roll

        string last_location = "";//saving the last location in order to enable respawn after losing a battle

        public static string SelectedClass = "";//current player's class

        public static string SelectedBackground = "";//current player's background

        public MainForm()
        {
            InitializeComponent();
            combat();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            SelectedClass = ClassSelectionForm.SelectedClass;//connecting selected class (ClassSelectionForm) and the rest of the program (MainForm)
            SelectedBackground = ClassSelectionForm.SelectedBackground;//connecting selected class (ClassSelectionForm) and the rest of the program (MainForm)

            //SoundPlayer simpleSound = new SoundPlayer(@"C:\Users\Username\Downloads\MOON - Hydrogen.wav");//Turn this shit on. Works while using System.Media and .WAV only;
            //simpleSound.Play();
        }

        void combatmenu_hide()//hides and disables menu used during combat
        {
            comboBox_attacktype.Visible = false;
            comboBox_attacktype.Enabled = false;

            comboBox_attacktarget.Visible = false;
            comboBox_attacktarget.Enabled = false;

            comboBox_itemselection.Visible = false;
            comboBox_itemselection.Enabled = false;

            comboBox_itemtarget.Visible = false;
            comboBox_itemtarget.Enabled = false;

            label_arrow0.Visible = false;
            label_arrow0.Enabled = false;

            label_arrow1.Visible = false;
            label_arrow1.Enabled = false;

            button_endturn.Visible = false;
            button_endturn.Enabled = false;

            textBox_queue.Visible = false;
            textBox_queue.Enabled = false;

            textBox_battle_log.Visible = false;
            textBox_battle_log.Enabled = false;

            textBox_general.Visible = true;
            textBox_general.Enabled = true;

            return;
        }

        void combatmenu_show()//shows and enables menu used during combat
        {
            comboBox_attacktype.Visible = true;
            comboBox_attacktype.Enabled = true;

            comboBox_attacktarget.Visible = true;
            comboBox_attacktarget.Enabled = true;

            comboBox_itemselection.Visible = true;
            comboBox_itemselection.Enabled = true;

            comboBox_itemtarget.Visible = true;
            comboBox_itemtarget.Enabled = true;

            label_arrow0.Visible = true;
            label_arrow0.Enabled = true;

            label_arrow1.Visible = true;
            label_arrow1.Enabled = true;

            button_endturn.Visible = true;
            button_endturn.Enabled = true;

            textBox_queue.Visible = true;
            textBox_queue.Enabled = true;

            textBox_battle_log.Visible = true;
            textBox_battle_log.Enabled = true;

            textBox_general.Visible = false;
            textBox_general.Enabled = false;

            return;
        }

        void trademenu_hide()//hides and disables menu used during trade
        {
            groupBox_trade.Visible = false;
            groupBox_trade.Enabled = false;
        }

        void trademenu_show()//shows and enables menu used during trade
        {
            groupBox_trade.Visible = true;
            groupBox_trade.Enabled = true;
        }

        
        void combat()//combat function
        {

            CombatForm CombatForm_1 = new CombatForm();
            CombatForm_1.Show();

            return;
        }



    }
}
