﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
//using LibNPC_FACTORY;
//using LibPLAYER_STATS;
//using LibNPC_STATS;
//using LibEFFECTS;
//using LibTOHDebug;

namespace TalesOfHeroes
{
    public partial class EffectsList : Form
    {
        /// <summary>
        /// Binds Name and ID
        /// </summary>
        Dictionary<string, int> NameID = new Dictionary<string, int>();


        IPLAYER_STATS Player;
        IEFFECTS Effects;
        INPC_FACTORY Factory;
        INPC_STATS Entities;

        int selected;
        int turn_count = 0;

        public EffectsList()
        {
            InitializeComponent();


            //Console.Clear();

            Player = new PLAYER_STATS();
            Player.onPlayerDeath += OnPlayerDeath;
            Factory = new NPC_FACTORY();
            Entities = new NPC_STATS();


            //Factory.LoadEntity(NPC_Type.Old_Dragon);
            //Entities.AddEntity(Factory.EntityStatPack, Factory.EntityTraitPack, Factory.EntityArmourPack, Player.Level);



            //Factory.LoadEntity(NPC_Type.Drunkard);
            //Entities.AddEntity(Factory.EntityStatPack, Factory.EntityTraitPack, Factory.EntityArmourPack, Player.Level);

            Entities.AddEntity(NPC_Type.Old_Dragon, Player.Level);




            Entities.AddEntity(NPC_Type.Old_Dragon, Player.Level);

            //DEBUG
            Entities.Health[0] = 50;


            l_entity_name1.Text = Entities.Name[0];
            l_entity_health1.Text = $"Entity health: {Entities.Health[0]}";

            l_entity_name2.Text = Entities.Name[1];
            l_entity_health2.Text = $"Entity health: {Entities.Health[1]}";

            cb_effect_selection.Items.Clear();
            NameID.Clear();
        }

        private void b_list_all_Click(object sender, EventArgs e)
        {
            ShowList(Effects.IDs);
        }

        /// <summary>
        /// Adds all Effects from ID List to DGW table
        /// </summary>
        /// <param name="ID_List">List of IDs to add</param>
        void ShowList(List<int> ID_List)
        {
            dgw_effect.Rows.Clear();

            foreach (int effect_ID in ID_List)
            {
                string name = Effects.ID_Name[effect_ID];
                string behaviour;
                string duration;
                if (Effects.LingeringEffects.ContainsKey(effect_ID))
                {
                    behaviour = "Lingering";
                    duration = Effects.LingeringEffects[effect_ID].ToString();
                }
                else
                {
                    behaviour = "Instant";
                    duration = "Instant";
                }
                string type = Effects.Types[effect_ID].ToString();
                string stat = "";
                string change = "";
                switch (Effects.Types[effect_ID])
                {
                    case EffectTypes.Buff:
                        {
                            Stats changed_stat = Effects.Buffers[effect_ID].Keys.Last();
                            stat = changed_stat.ToString();
                            change = Effects.Buffers[effect_ID][changed_stat].ToString();
                            break;
                        }
                    case EffectTypes.Debuff:
                        {
                            Stats changed_stat = Effects.Debuffers[effect_ID].Keys.Last();
                            stat = Effects.Debuffers[effect_ID].Keys.Last().ToString();
                            change = Effects.Debuffers[effect_ID][changed_stat].ToString();
                            break;
                        }
                    case EffectTypes.Heal:
                        {
                            stat = "Health";
                            change = Effects.Healers[effect_ID].ToString();
                            break;
                        }
                    case EffectTypes.Damage:
                        {
                            stat = "Health";
                            change = Effects.Damagers[effect_ID].ToString();
                            break;
                        }
                    default:
                        throw new NotImplementedException($"This Effect Type is not implemented! Effect Type: {Effects.Types[effect_ID]}");
                }

                dgw_effect.Rows.Add(effect_ID.ToString(), name, behaviour, type, stat, change, duration);
            }
            dgw_effect.Sort(effect_ID, System.ComponentModel.ListSortDirection.Ascending);
        }

        /// <summary>
        /// Shows Active Effects with current Duration
        /// </summary>
        void ShowWithDuration(int target_ID)
        {
            dgw_effect.Rows.Clear();

            foreach (int effect_ID in Effects.ActiveEffects[target_ID])
            {
                string name = Effects.Names[Effects.IDs.IndexOf(effect_ID)];
                string behaviour;
                string duration;
                if (Effects.LingeringEffects.ContainsKey(effect_ID))
                {
                    behaviour = "Lingering";
                    duration = Effects.ActiveDurations[target_ID][effect_ID].ToString();
                }
                else
                {
                    behaviour = "Instant";
                    duration = "Instant";
                }
                string type = Effects.Types[effect_ID].ToString();
                string stat = "";
                string change = "";
                switch (Effects.Types[effect_ID])
                {
                    case EffectTypes.Buff:
                        {
                            Stats changed_stat = Effects.Buffers[effect_ID].Keys.Last();
                            stat = changed_stat.ToString();
                            change = Effects.Buffers[effect_ID][changed_stat].ToString();
                            break;
                        }
                    case EffectTypes.Debuff:
                        {
                            Stats changed_stat = Effects.Debuffers[effect_ID].Keys.Last();
                            stat = Effects.Debuffers[effect_ID].Keys.Last().ToString();
                            change = Effects.Debuffers[effect_ID][changed_stat].ToString();
                            break;
                        }
                    case EffectTypes.Heal:
                        {
                            stat = "Health";
                            change = Effects.Healers[effect_ID].ToString();
                            break;
                        }
                    case EffectTypes.Damage:
                        {
                            stat = "Health";
                            change = Effects.Damagers[effect_ID].ToString();
                            break;
                        }
                    default:
                        throw new NotImplementedException($"This Effect Type is not implemented! Effect Type: {Effects.Types[effect_ID]}");
                }

                dgw_effect.Rows.Add(effect_ID.ToString(), name, behaviour, type, stat, change, duration);
            }
            dgw_effect.Sort(effect_ID, System.ComponentModel.ListSortDirection.Ascending);
        }


        /// <summary>
        /// Creates a test Player
        /// </summary>
        void CreatePlayer()
        {
            string player_name = tb_playerName.Text;
            int str = Convert.ToInt32(l_str.Text);
            int agi = Convert.ToInt32(l_agi.Text);
            int intel = Convert.ToInt32(l_int.Text);

            StatPack playerStatPack = new StatPack
            {
                Strength = str,
                Constitution = 11,//placeholder
                Agility = agi,
                Intellect = intel,
                Wisdom = 11,//placeholder
                Charisma = 11//placeholder
            };

            ArmourPack playerArmourPack = new ArmourPack
            {
                ArmourClass = ArmourClass.Light, //placeholder
                ArmourRating = 15//placeholder
            };


            Player.AddPlayer(player_name, playerStatPack, playerArmourPack);

            Effects = new EFFECTS(ref Player, ref Entities, Factory.ListOfStats);

            for (int effect_index = 0; effect_index < Effects.IDs.Last(); effect_index++)
            {
                string name = Effects.Names[effect_index];
                NameID[name] = Effects.IDs[effect_index];
                cb_effect_selection.Items.Add(name);
            }

            ShowList(Effects.IDs);

            Console.WriteLine($"|||||TURN {turn_count}|||||");
        }

        /// <summary>
        /// Updates Health and Stats
        /// </summary>
        void UpdateAll()
        {
            UpdateHealth();
            UpdateStats();
            UpdateEntity();
        }

        void UpdateEntity()
        {
            l_entity_health1.Text = $"Entity health: {Entities.Health[0]}";
            l_entity_health2.Text = $"Entity health: {Entities.Health[1]}";
        }

        /// <summary>
        /// Updates Player Health
        /// </summary>
        void UpdateHealth()
        {
            l_health.Text = $"Health: {Player.Health}/{Player.MaxHealth}";
        }
        /// <summary>
        /// Updates Player Stats
        /// </summary>
        void UpdateStats()
        {
            l_str.Text = Player.Strength.ToString();
            l_agi.Text = Player.Agility.ToString();
            l_int.Text = Player.Intellect.ToString();
        }

        /// <summary>
        /// Player creation
        /// </summary>
        private void b_create_Click(object sender, EventArgs e)
        {
            if (tb_playerName.Text == "")
            {
                MessageBox.Show("Enter Player name!");
            }
            else
            {
                if ((l_str.Text == "0") || (l_agi.Text == "0") || (l_int.Text == "0"))
                {
                    MessageBox.Show("Set all stats!");
                }
                else
                {
                    CreatePlayer();
                    UpdateAll();
                    l_health.Visible = true;

                    tb_playerName.Enabled = false;
                    b_strdwn.Enabled = false;
                    b_strup.Enabled = false;
                    b_agidwn.Enabled = false;
                    b_agiup.Enabled = false;
                    b_intdwn.Enabled = false;
                    b_intup.Enabled = false;
                    b_create.Enabled = false;
                }

                b_list_active.Enabled = true;
                b_apply_selected.Enabled = true;
                b_next_turn.Enabled = true;
            }
        }


        private void b_strdwn_Click(object sender, EventArgs e)
        {
            int temp = Convert.ToInt32(l_str.Text) - 1;
            if (temp <= 0)
            {
                b_strdwn.Enabled = false;
            }

            l_str.Text = temp.ToString();
        }

        private void b_strup_Click(object sender, EventArgs e)
        {
            l_str.Text = (Convert.ToInt32(l_str.Text) + 1).ToString();
            b_strdwn.Enabled = true;
        }

        private void b_agidwn_Click(object sender, EventArgs e)
        {
            int temp = Convert.ToInt32(l_agi.Text) - 1;
            if (temp <= 0)
            {
                b_agidwn.Enabled = false;
            }

            l_agi.Text = temp.ToString();
        }

        private void b_agiup_Click(object sender, EventArgs e)
        {
            l_agi.Text = (Convert.ToInt32(l_agi.Text) + 1).ToString();
            b_agidwn.Enabled = true;
        }

        private void b_intdwn_Click(object sender, EventArgs e)
        {
            int temp = Convert.ToInt32(l_int.Text) - 1;
            if (temp <= 0)
            {
                b_intdwn.Enabled = false;
            }

            l_int.Text = temp.ToString();
        }

        private void b_intup_Click(object sender, EventArgs e)
        {
            l_int.Text = (Convert.ToInt32(l_int.Text) + 1).ToString();
            b_intdwn.Enabled = true;
        }



        private void b_apply_selected_Click(object sender, EventArgs e)
        {
            int effect_ID = NameID[cb_effect_selection.Text];
            Effects.CastEffect(0, 0, effect_ID);
            ShowWithDuration(0);
            UpdateAll();
        }

        /// <summary>
        /// Actions following Player death
        /// </summary>
        private void OnPlayerDeath(int killer_ID)
        {
            UpdateHealth();
            Console.WriteLine("! Player Death");
            Console.WriteLine($"  |Killer ID: {killer_ID}");

            MessageBox.Show("You Died", "Death", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void b_list_active_Click(object sender, EventArgs e)
        {
            selected = 0;
            ShowWithDuration(0);
        }

        private void b_next_turn_Click(object sender, EventArgs e)
        {
            Console.WriteLine($"|||||END OF TURN {turn_count}|||||");
            Console.WriteLine();
            Console.WriteLine();
            turn_count++;
            Console.WriteLine($"|||||TURN {turn_count}|||||");

            Effects.TickEffects();
            UpdateAll();
            switch (selected)
            {
                case 0:
                    {
                        ShowWithDuration(0);
                        break;
                    }
                case 1:
                    {
                        ShowWithDuration(1);
                        break;
                    }
                case 2:
                    {
                        ShowWithDuration(2);
                        break;
                    }
                default: break;
            }
        }

        private void b_apply_entity1_Click(object sender, EventArgs e)
        {
            int effect_ID = NameID[cb_effect_selection.Text];
            Effects.CastEffect(0, 1, effect_ID);
            ShowWithDuration(1);
            UpdateEntity();
        }

        private void b_apply_entity2_Click(object sender, EventArgs e)
        {
            int effect_ID = NameID[cb_effect_selection.Text];
            Effects.CastEffect(0, 2, effect_ID);
            ShowWithDuration(2);
            UpdateEntity();
        }

        private void b_entity_LA1_Click(object sender, EventArgs e)
        {
            selected = 1;
            ShowWithDuration(Entities.ID[0]);
        }

        private void b_entity_LA2_Click(object sender, EventArgs e)
        {
            selected = 2;
            ShowWithDuration(Entities.ID[0]);
        }

        private void cb_effect_selection_TextChanged(object sender, EventArgs e)
        {
            if (!(cb_effect_selection.Text == ""))
            {
                Console.WriteLine($"-Selected Effect: {cb_effect_selection.Text}");

                dgw_effect.Rows.Clear();

                int effect_ID = NameID[cb_effect_selection.Text];
                string name = Effects.ID_Name[effect_ID];
                string behaviour;
                string duration;
                if (Effects.LingeringEffects.ContainsKey(effect_ID))
                {
                    behaviour = "Lingering";
                    duration = Effects.LingeringEffects[effect_ID].ToString();
                }
                else
                {
                    behaviour = "Instant";
                    duration = "Instant";
                }
                string type = Effects.Types[effect_ID].ToString();
                string stat = "";
                string change = "";
                switch (Effects.Types[effect_ID])
                {
                    case EffectTypes.Buff:
                        {
                            Stats changed_stat = Effects.Buffers[effect_ID].Keys.Last();
                            stat = changed_stat.ToString();
                            change = Effects.Buffers[effect_ID][changed_stat].ToString();
                            break;
                        }
                    case EffectTypes.Debuff:
                        {
                            Stats changed_stat = Effects.Debuffers[effect_ID].Keys.Last();
                            stat = Effects.Debuffers[effect_ID].Keys.Last().ToString();
                            change = Effects.Debuffers[effect_ID][changed_stat].ToString();
                            break;
                        }
                    case EffectTypes.Heal:
                        {
                            stat = "Health";
                            change = Effects.Healers[effect_ID].ToString();
                            break;
                        }
                    case EffectTypes.Damage:
                        {
                            stat = "Health";
                            change = Effects.Damagers[effect_ID].ToString();
                            break;
                        }
                    default:
                        throw new NotImplementedException($"This Effect Type is not implemented! Effect Type: {Effects.Types[effect_ID]}");
                }

                dgw_effect.Rows.Add(effect_ID.ToString(), name, behaviour, type, stat, change, duration);
            }
        }
    }
}
