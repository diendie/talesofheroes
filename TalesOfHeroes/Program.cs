﻿using System;
using System.Windows.Forms;
using LoggerLib;
using System.Collections.Specialized;
using System.Configuration;


namespace TalesOfHeroes
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            NameValueCollection filestreamPaths = ConfigurationManager.GetSection("filestreamPaths") as NameValueCollection;
            if (filestreamPaths == null)
            {
                throw new ConfigurationErrorsException("filestreamPaths section not found");
            }

            //Logger startup, needed only here
            Logger.Initialize(args, filestreamPaths["Logs"]);


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new ClassSelectionForm());
            //Application.Run(new DebugForm());
            Application.Run(new CombatForm());
            //Application.Run(new EffectsList());

        }
    }
}
