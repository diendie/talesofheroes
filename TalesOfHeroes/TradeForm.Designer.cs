﻿namespace TalesOfHeroes
{
    partial class TradeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_trade = new System.Windows.Forms.GroupBox();
            this.button_trade_cancel = new System.Windows.Forms.Button();
            this.button_trade_buy = new System.Windows.Forms.Button();
            this.label_trade_sum_value = new System.Windows.Forms.Label();
            this.label_trade_sum = new System.Windows.Forms.Label();
            this.button_trade_item6_more = new System.Windows.Forms.Button();
            this.button_trade_item5_more = new System.Windows.Forms.Button();
            this.button_trade_item4_more = new System.Windows.Forms.Button();
            this.button_trade_item3_more = new System.Windows.Forms.Button();
            this.button_trade_item2_more = new System.Windows.Forms.Button();
            this.button_trade_item1_more = new System.Windows.Forms.Button();
            this.button_trade_item0_more = new System.Windows.Forms.Button();
            this.button_trade_item6_less = new System.Windows.Forms.Button();
            this.button_trade_item5_less = new System.Windows.Forms.Button();
            this.button_trade_item4_less = new System.Windows.Forms.Button();
            this.button_trade_item3_less = new System.Windows.Forms.Button();
            this.button_trade_item2_less = new System.Windows.Forms.Button();
            this.button_trade_item1_less = new System.Windows.Forms.Button();
            this.button_trade_item0_less = new System.Windows.Forms.Button();
            this.label_trade_item6_quantity = new System.Windows.Forms.Label();
            this.label_trade_item5_quantity = new System.Windows.Forms.Label();
            this.label_trade_item4_quantity = new System.Windows.Forms.Label();
            this.label_trade_item3_quantity = new System.Windows.Forms.Label();
            this.label_trade_item2_quantity = new System.Windows.Forms.Label();
            this.label_trade_item1_quantity = new System.Windows.Forms.Label();
            this.label_trade_item0_quantity = new System.Windows.Forms.Label();
            this.label_trade_item6 = new System.Windows.Forms.Label();
            this.label_trade_item5 = new System.Windows.Forms.Label();
            this.label_trade_item4 = new System.Windows.Forms.Label();
            this.label_trade_item3 = new System.Windows.Forms.Label();
            this.label_trade_item2 = new System.Windows.Forms.Label();
            this.label_trade_item1 = new System.Windows.Forms.Label();
            this.label_trade_item0 = new System.Windows.Forms.Label();
            this.groupBox_inventory = new System.Windows.Forms.GroupBox();
            this.label_item9_quantity = new System.Windows.Forms.Label();
            this.label_item8_quantity = new System.Windows.Forms.Label();
            this.label_item7_quantity = new System.Windows.Forms.Label();
            this.label_item6_quantity = new System.Windows.Forms.Label();
            this.label_item5_quantity = new System.Windows.Forms.Label();
            this.label_item4_quantity = new System.Windows.Forms.Label();
            this.label_item3_quantity = new System.Windows.Forms.Label();
            this.label_item2_quantity = new System.Windows.Forms.Label();
            this.label_item1_quantity = new System.Windows.Forms.Label();
            this.label_item0_quantity = new System.Windows.Forms.Label();
            this.label_item9 = new System.Windows.Forms.Label();
            this.label_item8 = new System.Windows.Forms.Label();
            this.label_item7 = new System.Windows.Forms.Label();
            this.label_item6 = new System.Windows.Forms.Label();
            this.label_item5 = new System.Windows.Forms.Label();
            this.label_item4 = new System.Windows.Forms.Label();
            this.label_item3 = new System.Windows.Forms.Label();
            this.label_item2 = new System.Windows.Forms.Label();
            this.label_item1 = new System.Windows.Forms.Label();
            this.label_item0 = new System.Windows.Forms.Label();
            this.groupBox_trade.SuspendLayout();
            this.groupBox_inventory.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_trade
            // 
            this.groupBox_trade.Controls.Add(this.button_trade_cancel);
            this.groupBox_trade.Controls.Add(this.button_trade_buy);
            this.groupBox_trade.Controls.Add(this.label_trade_sum_value);
            this.groupBox_trade.Controls.Add(this.label_trade_sum);
            this.groupBox_trade.Controls.Add(this.button_trade_item6_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item5_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item4_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item3_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item2_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item1_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item0_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item6_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item5_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item4_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item3_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item2_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item1_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item0_less);
            this.groupBox_trade.Controls.Add(this.label_trade_item6_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item5_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item4_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item3_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item2_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item1_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item0_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item6);
            this.groupBox_trade.Controls.Add(this.label_trade_item5);
            this.groupBox_trade.Controls.Add(this.label_trade_item4);
            this.groupBox_trade.Controls.Add(this.label_trade_item3);
            this.groupBox_trade.Controls.Add(this.label_trade_item2);
            this.groupBox_trade.Controls.Add(this.label_trade_item1);
            this.groupBox_trade.Controls.Add(this.label_trade_item0);
            this.groupBox_trade.Enabled = false;
            this.groupBox_trade.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_trade.Location = new System.Drawing.Point(12, 12);
            this.groupBox_trade.Name = "groupBox_trade";
            this.groupBox_trade.Size = new System.Drawing.Size(435, 291);
            this.groupBox_trade.TabIndex = 16;
            this.groupBox_trade.TabStop = false;
            this.groupBox_trade.Text = "Trade";
            this.groupBox_trade.Visible = false;
            // 
            // button_trade_cancel
            // 
            this.button_trade_cancel.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.button_trade_cancel.Location = new System.Drawing.Point(280, 247);
            this.button_trade_cancel.Name = "button_trade_cancel";
            this.button_trade_cancel.Size = new System.Drawing.Size(113, 41);
            this.button_trade_cancel.TabIndex = 31;
            this.button_trade_cancel.Text = "Leave";
            this.button_trade_cancel.UseVisualStyleBackColor = true;
            // 
            // button_trade_buy
            // 
            this.button_trade_buy.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.button_trade_buy.Location = new System.Drawing.Point(161, 247);
            this.button_trade_buy.Name = "button_trade_buy";
            this.button_trade_buy.Size = new System.Drawing.Size(113, 41);
            this.button_trade_buy.TabIndex = 30;
            this.button_trade_buy.Text = "Buy";
            this.button_trade_buy.UseVisualStyleBackColor = true;
            // 
            // label_trade_sum_value
            // 
            this.label_trade_sum_value.AutoSize = true;
            this.label_trade_sum_value.Location = new System.Drawing.Point(97, 262);
            this.label_trade_sum_value.Name = "label_trade_sum_value";
            this.label_trade_sum_value.Size = new System.Drawing.Size(24, 26);
            this.label_trade_sum_value.TabIndex = 29;
            this.label_trade_sum_value.Text = "0";
            // 
            // label_trade_sum
            // 
            this.label_trade_sum.AutoSize = true;
            this.label_trade_sum.Location = new System.Drawing.Point(6, 262);
            this.label_trade_sum.Name = "label_trade_sum";
            this.label_trade_sum.Size = new System.Drawing.Size(58, 26);
            this.label_trade_sum.TabIndex = 28;
            this.label_trade_sum.Text = "Total";
            // 
            // button_trade_item6_more
            // 
            this.button_trade_item6_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item6_more.Location = new System.Drawing.Point(327, 212);
            this.button_trade_item6_more.Name = "button_trade_item6_more";
            this.button_trade_item6_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item6_more.TabIndex = 27;
            this.button_trade_item6_more.Text = "+";
            this.button_trade_item6_more.UseVisualStyleBackColor = true;
            this.button_trade_item6_more.Click += new System.EventHandler(this.Button_trade_item6_more_Click);
            // 
            // button_trade_item5_more
            // 
            this.button_trade_item5_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item5_more.Location = new System.Drawing.Point(327, 182);
            this.button_trade_item5_more.Name = "button_trade_item5_more";
            this.button_trade_item5_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item5_more.TabIndex = 26;
            this.button_trade_item5_more.Text = "+";
            this.button_trade_item5_more.UseVisualStyleBackColor = true;
            this.button_trade_item5_more.Click += new System.EventHandler(this.Button_trade_item5_more_Click);
            // 
            // button_trade_item4_more
            // 
            this.button_trade_item4_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item4_more.Location = new System.Drawing.Point(327, 152);
            this.button_trade_item4_more.Name = "button_trade_item4_more";
            this.button_trade_item4_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item4_more.TabIndex = 25;
            this.button_trade_item4_more.Text = "+";
            this.button_trade_item4_more.UseVisualStyleBackColor = true;
            this.button_trade_item4_more.Click += new System.EventHandler(this.Button_trade_item4_more_Click);
            // 
            // button_trade_item3_more
            // 
            this.button_trade_item3_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item3_more.Location = new System.Drawing.Point(327, 124);
            this.button_trade_item3_more.Name = "button_trade_item3_more";
            this.button_trade_item3_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item3_more.TabIndex = 24;
            this.button_trade_item3_more.Text = "+";
            this.button_trade_item3_more.UseVisualStyleBackColor = true;
            this.button_trade_item3_more.Click += new System.EventHandler(this.Button_trade_item3_more_Click);
            // 
            // button_trade_item2_more
            // 
            this.button_trade_item2_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item2_more.Location = new System.Drawing.Point(327, 96);
            this.button_trade_item2_more.Name = "button_trade_item2_more";
            this.button_trade_item2_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item2_more.TabIndex = 23;
            this.button_trade_item2_more.Text = "+";
            this.button_trade_item2_more.UseVisualStyleBackColor = true;
            this.button_trade_item2_more.Click += new System.EventHandler(this.Button_trade_item2_more_Click);
            // 
            // button_trade_item1_more
            // 
            this.button_trade_item1_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item1_more.Location = new System.Drawing.Point(327, 68);
            this.button_trade_item1_more.Name = "button_trade_item1_more";
            this.button_trade_item1_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item1_more.TabIndex = 22;
            this.button_trade_item1_more.Text = "+";
            this.button_trade_item1_more.UseVisualStyleBackColor = true;
            this.button_trade_item1_more.Click += new System.EventHandler(this.Button_trade_item1_more_Click);
            // 
            // button_trade_item0_more
            // 
            this.button_trade_item0_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item0_more.Location = new System.Drawing.Point(327, 39);
            this.button_trade_item0_more.Name = "button_trade_item0_more";
            this.button_trade_item0_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item0_more.TabIndex = 21;
            this.button_trade_item0_more.Text = "+";
            this.button_trade_item0_more.UseVisualStyleBackColor = true;
            this.button_trade_item0_more.Click += new System.EventHandler(this.Button_trade_item0_more_Click);
            // 
            // button_trade_item6_less
            // 
            this.button_trade_item6_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item6_less.Location = new System.Drawing.Point(222, 212);
            this.button_trade_item6_less.Name = "button_trade_item6_less";
            this.button_trade_item6_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item6_less.TabIndex = 20;
            this.button_trade_item6_less.Text = "-";
            this.button_trade_item6_less.UseVisualStyleBackColor = true;
            this.button_trade_item6_less.Click += new System.EventHandler(this.Button_trade_item6_less_Click);
            // 
            // button_trade_item5_less
            // 
            this.button_trade_item5_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item5_less.Location = new System.Drawing.Point(222, 182);
            this.button_trade_item5_less.Name = "button_trade_item5_less";
            this.button_trade_item5_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item5_less.TabIndex = 19;
            this.button_trade_item5_less.Text = "-";
            this.button_trade_item5_less.UseVisualStyleBackColor = true;
            this.button_trade_item5_less.Click += new System.EventHandler(this.Button_trade_item5_less_Click);
            // 
            // button_trade_item4_less
            // 
            this.button_trade_item4_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item4_less.Location = new System.Drawing.Point(222, 152);
            this.button_trade_item4_less.Name = "button_trade_item4_less";
            this.button_trade_item4_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item4_less.TabIndex = 18;
            this.button_trade_item4_less.Text = "-";
            this.button_trade_item4_less.UseVisualStyleBackColor = true;
            this.button_trade_item4_less.Click += new System.EventHandler(this.Button_trade_item4_less_Click);
            // 
            // button_trade_item3_less
            // 
            this.button_trade_item3_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item3_less.Location = new System.Drawing.Point(222, 124);
            this.button_trade_item3_less.Name = "button_trade_item3_less";
            this.button_trade_item3_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item3_less.TabIndex = 17;
            this.button_trade_item3_less.Text = "-";
            this.button_trade_item3_less.UseVisualStyleBackColor = true;
            this.button_trade_item3_less.Click += new System.EventHandler(this.Button_trade_item3_less_Click);
            // 
            // button_trade_item2_less
            // 
            this.button_trade_item2_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item2_less.Location = new System.Drawing.Point(222, 96);
            this.button_trade_item2_less.Name = "button_trade_item2_less";
            this.button_trade_item2_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item2_less.TabIndex = 16;
            this.button_trade_item2_less.Text = "-";
            this.button_trade_item2_less.UseVisualStyleBackColor = true;
            this.button_trade_item2_less.Click += new System.EventHandler(this.Button_trade_item2_less_Click);
            // 
            // button_trade_item1_less
            // 
            this.button_trade_item1_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item1_less.Location = new System.Drawing.Point(222, 68);
            this.button_trade_item1_less.Name = "button_trade_item1_less";
            this.button_trade_item1_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item1_less.TabIndex = 15;
            this.button_trade_item1_less.Text = "-";
            this.button_trade_item1_less.UseVisualStyleBackColor = true;
            this.button_trade_item1_less.Click += new System.EventHandler(this.Button_trade_item1_less_Click);
            // 
            // button_trade_item0_less
            // 
            this.button_trade_item0_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item0_less.Location = new System.Drawing.Point(222, 39);
            this.button_trade_item0_less.Name = "button_trade_item0_less";
            this.button_trade_item0_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item0_less.TabIndex = 14;
            this.button_trade_item0_less.Text = "-";
            this.button_trade_item0_less.UseVisualStyleBackColor = true;
            this.button_trade_item0_less.Click += new System.EventHandler(this.Button_trade_item0_less_Click);
            // 
            // label_trade_item6_quantity
            // 
            this.label_trade_item6_quantity.AutoSize = true;
            this.label_trade_item6_quantity.Location = new System.Drawing.Point(289, 210);
            this.label_trade_item6_quantity.Name = "label_trade_item6_quantity";
            this.label_trade_item6_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item6_quantity.TabIndex = 13;
            this.label_trade_item6_quantity.Text = "0";
            // 
            // label_trade_item5_quantity
            // 
            this.label_trade_item5_quantity.AutoSize = true;
            this.label_trade_item5_quantity.Location = new System.Drawing.Point(289, 180);
            this.label_trade_item5_quantity.Name = "label_trade_item5_quantity";
            this.label_trade_item5_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item5_quantity.TabIndex = 12;
            this.label_trade_item5_quantity.Text = "0";
            // 
            // label_trade_item4_quantity
            // 
            this.label_trade_item4_quantity.AutoSize = true;
            this.label_trade_item4_quantity.Location = new System.Drawing.Point(289, 150);
            this.label_trade_item4_quantity.Name = "label_trade_item4_quantity";
            this.label_trade_item4_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item4_quantity.TabIndex = 11;
            this.label_trade_item4_quantity.Text = "0";
            // 
            // label_trade_item3_quantity
            // 
            this.label_trade_item3_quantity.AutoSize = true;
            this.label_trade_item3_quantity.Location = new System.Drawing.Point(289, 122);
            this.label_trade_item3_quantity.Name = "label_trade_item3_quantity";
            this.label_trade_item3_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item3_quantity.TabIndex = 10;
            this.label_trade_item3_quantity.Text = "0";
            // 
            // label_trade_item2_quantity
            // 
            this.label_trade_item2_quantity.AutoSize = true;
            this.label_trade_item2_quantity.Location = new System.Drawing.Point(289, 94);
            this.label_trade_item2_quantity.Name = "label_trade_item2_quantity";
            this.label_trade_item2_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item2_quantity.TabIndex = 9;
            this.label_trade_item2_quantity.Text = "0";
            // 
            // label_trade_item1_quantity
            // 
            this.label_trade_item1_quantity.AutoSize = true;
            this.label_trade_item1_quantity.Location = new System.Drawing.Point(289, 66);
            this.label_trade_item1_quantity.Name = "label_trade_item1_quantity";
            this.label_trade_item1_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item1_quantity.TabIndex = 8;
            this.label_trade_item1_quantity.Text = "0";
            // 
            // label_trade_item0_quantity
            // 
            this.label_trade_item0_quantity.AutoSize = true;
            this.label_trade_item0_quantity.Location = new System.Drawing.Point(289, 37);
            this.label_trade_item0_quantity.Name = "label_trade_item0_quantity";
            this.label_trade_item0_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item0_quantity.TabIndex = 7;
            this.label_trade_item0_quantity.Text = "0";
            // 
            // label_trade_item6
            // 
            this.label_trade_item6.AutoSize = true;
            this.label_trade_item6.Location = new System.Drawing.Point(6, 210);
            this.label_trade_item6.Name = "label_trade_item6";
            this.label_trade_item6.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item6.TabIndex = 6;
            this.label_trade_item6.Text = "-";
            // 
            // label_trade_item5
            // 
            this.label_trade_item5.AutoSize = true;
            this.label_trade_item5.Location = new System.Drawing.Point(6, 180);
            this.label_trade_item5.Name = "label_trade_item5";
            this.label_trade_item5.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item5.TabIndex = 5;
            this.label_trade_item5.Text = "-";
            // 
            // label_trade_item4
            // 
            this.label_trade_item4.AutoSize = true;
            this.label_trade_item4.Location = new System.Drawing.Point(6, 150);
            this.label_trade_item4.Name = "label_trade_item4";
            this.label_trade_item4.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item4.TabIndex = 4;
            this.label_trade_item4.Text = "-";
            // 
            // label_trade_item3
            // 
            this.label_trade_item3.AutoSize = true;
            this.label_trade_item3.Location = new System.Drawing.Point(6, 122);
            this.label_trade_item3.Name = "label_trade_item3";
            this.label_trade_item3.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item3.TabIndex = 3;
            this.label_trade_item3.Text = "-";
            // 
            // label_trade_item2
            // 
            this.label_trade_item2.AutoSize = true;
            this.label_trade_item2.Location = new System.Drawing.Point(6, 94);
            this.label_trade_item2.Name = "label_trade_item2";
            this.label_trade_item2.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item2.TabIndex = 2;
            this.label_trade_item2.Text = "-";
            // 
            // label_trade_item1
            // 
            this.label_trade_item1.AutoSize = true;
            this.label_trade_item1.Location = new System.Drawing.Point(6, 66);
            this.label_trade_item1.Name = "label_trade_item1";
            this.label_trade_item1.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item1.TabIndex = 1;
            this.label_trade_item1.Text = "-";
            // 
            // label_trade_item0
            // 
            this.label_trade_item0.AutoSize = true;
            this.label_trade_item0.Location = new System.Drawing.Point(6, 37);
            this.label_trade_item0.Name = "label_trade_item0";
            this.label_trade_item0.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item0.TabIndex = 0;
            this.label_trade_item0.Text = "-";
            // 
            // groupBox_inventory
            // 
            this.groupBox_inventory.Controls.Add(this.label_item9_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item8_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item7_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item6_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item5_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item4_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item3_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item2_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item1_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item0_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item9);
            this.groupBox_inventory.Controls.Add(this.label_item8);
            this.groupBox_inventory.Controls.Add(this.label_item7);
            this.groupBox_inventory.Controls.Add(this.label_item6);
            this.groupBox_inventory.Controls.Add(this.label_item5);
            this.groupBox_inventory.Controls.Add(this.label_item4);
            this.groupBox_inventory.Controls.Add(this.label_item3);
            this.groupBox_inventory.Controls.Add(this.label_item2);
            this.groupBox_inventory.Controls.Add(this.label_item1);
            this.groupBox_inventory.Controls.Add(this.label_item0);
            this.groupBox_inventory.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_inventory.Location = new System.Drawing.Point(570, 12);
            this.groupBox_inventory.Name = "groupBox_inventory";
            this.groupBox_inventory.Size = new System.Drawing.Size(273, 309);
            this.groupBox_inventory.TabIndex = 15;
            this.groupBox_inventory.TabStop = false;
            this.groupBox_inventory.Text = "Inventory";
            // 
            // label_item9_quantity
            // 
            this.label_item9_quantity.AutoSize = true;
            this.label_item9_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item9_quantity.Location = new System.Drawing.Point(237, 261);
            this.label_item9_quantity.Name = "label_item9_quantity";
            this.label_item9_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item9_quantity.TabIndex = 19;
            this.label_item9_quantity.Text = "0";
            // 
            // label_item8_quantity
            // 
            this.label_item8_quantity.AutoSize = true;
            this.label_item8_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item8_quantity.Location = new System.Drawing.Point(237, 235);
            this.label_item8_quantity.Name = "label_item8_quantity";
            this.label_item8_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item8_quantity.TabIndex = 18;
            this.label_item8_quantity.Text = "0";
            // 
            // label_item7_quantity
            // 
            this.label_item7_quantity.AutoSize = true;
            this.label_item7_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item7_quantity.Location = new System.Drawing.Point(237, 209);
            this.label_item7_quantity.Name = "label_item7_quantity";
            this.label_item7_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item7_quantity.TabIndex = 17;
            this.label_item7_quantity.Text = "0";
            // 
            // label_item6_quantity
            // 
            this.label_item6_quantity.AutoSize = true;
            this.label_item6_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item6_quantity.Location = new System.Drawing.Point(237, 183);
            this.label_item6_quantity.Name = "label_item6_quantity";
            this.label_item6_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item6_quantity.TabIndex = 16;
            this.label_item6_quantity.Text = "0";
            // 
            // label_item5_quantity
            // 
            this.label_item5_quantity.AutoSize = true;
            this.label_item5_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item5_quantity.Location = new System.Drawing.Point(237, 157);
            this.label_item5_quantity.Name = "label_item5_quantity";
            this.label_item5_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item5_quantity.TabIndex = 15;
            this.label_item5_quantity.Text = "0";
            // 
            // label_item4_quantity
            // 
            this.label_item4_quantity.AutoSize = true;
            this.label_item4_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item4_quantity.Location = new System.Drawing.Point(237, 131);
            this.label_item4_quantity.Name = "label_item4_quantity";
            this.label_item4_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item4_quantity.TabIndex = 14;
            this.label_item4_quantity.Text = "0";
            // 
            // label_item3_quantity
            // 
            this.label_item3_quantity.AutoSize = true;
            this.label_item3_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item3_quantity.Location = new System.Drawing.Point(237, 105);
            this.label_item3_quantity.Name = "label_item3_quantity";
            this.label_item3_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item3_quantity.TabIndex = 13;
            this.label_item3_quantity.Text = "0";
            // 
            // label_item2_quantity
            // 
            this.label_item2_quantity.AutoSize = true;
            this.label_item2_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item2_quantity.Location = new System.Drawing.Point(237, 79);
            this.label_item2_quantity.Name = "label_item2_quantity";
            this.label_item2_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item2_quantity.TabIndex = 12;
            this.label_item2_quantity.Text = "0";
            // 
            // label_item1_quantity
            // 
            this.label_item1_quantity.AutoSize = true;
            this.label_item1_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item1_quantity.Location = new System.Drawing.Point(237, 53);
            this.label_item1_quantity.Name = "label_item1_quantity";
            this.label_item1_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item1_quantity.TabIndex = 11;
            this.label_item1_quantity.Text = "0";
            // 
            // label_item0_quantity
            // 
            this.label_item0_quantity.AutoSize = true;
            this.label_item0_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item0_quantity.Location = new System.Drawing.Point(237, 27);
            this.label_item0_quantity.Name = "label_item0_quantity";
            this.label_item0_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item0_quantity.TabIndex = 10;
            this.label_item0_quantity.Text = "0";
            // 
            // label_item9
            // 
            this.label_item9.AutoSize = true;
            this.label_item9.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item9.Location = new System.Drawing.Point(6, 261);
            this.label_item9.Name = "label_item9";
            this.label_item9.Size = new System.Drawing.Size(20, 26);
            this.label_item9.TabIndex = 9;
            this.label_item9.Text = "-";
            // 
            // label_item8
            // 
            this.label_item8.AutoSize = true;
            this.label_item8.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item8.Location = new System.Drawing.Point(6, 235);
            this.label_item8.Name = "label_item8";
            this.label_item8.Size = new System.Drawing.Size(20, 26);
            this.label_item8.TabIndex = 8;
            this.label_item8.Text = "-";
            // 
            // label_item7
            // 
            this.label_item7.AutoSize = true;
            this.label_item7.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item7.Location = new System.Drawing.Point(6, 209);
            this.label_item7.Name = "label_item7";
            this.label_item7.Size = new System.Drawing.Size(20, 26);
            this.label_item7.TabIndex = 7;
            this.label_item7.Text = "-";
            // 
            // label_item6
            // 
            this.label_item6.AutoSize = true;
            this.label_item6.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item6.Location = new System.Drawing.Point(6, 183);
            this.label_item6.Name = "label_item6";
            this.label_item6.Size = new System.Drawing.Size(20, 26);
            this.label_item6.TabIndex = 6;
            this.label_item6.Text = "-";
            // 
            // label_item5
            // 
            this.label_item5.AutoSize = true;
            this.label_item5.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item5.Location = new System.Drawing.Point(6, 157);
            this.label_item5.Name = "label_item5";
            this.label_item5.Size = new System.Drawing.Size(20, 26);
            this.label_item5.TabIndex = 5;
            this.label_item5.Text = "-";
            // 
            // label_item4
            // 
            this.label_item4.AutoSize = true;
            this.label_item4.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item4.Location = new System.Drawing.Point(6, 131);
            this.label_item4.Name = "label_item4";
            this.label_item4.Size = new System.Drawing.Size(20, 26);
            this.label_item4.TabIndex = 4;
            this.label_item4.Text = "-";
            // 
            // label_item3
            // 
            this.label_item3.AutoSize = true;
            this.label_item3.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item3.Location = new System.Drawing.Point(6, 105);
            this.label_item3.Name = "label_item3";
            this.label_item3.Size = new System.Drawing.Size(20, 26);
            this.label_item3.TabIndex = 3;
            this.label_item3.Text = "-";
            // 
            // label_item2
            // 
            this.label_item2.AutoSize = true;
            this.label_item2.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item2.Location = new System.Drawing.Point(6, 79);
            this.label_item2.Name = "label_item2";
            this.label_item2.Size = new System.Drawing.Size(20, 26);
            this.label_item2.TabIndex = 2;
            this.label_item2.Text = "-";
            // 
            // label_item1
            // 
            this.label_item1.AutoSize = true;
            this.label_item1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item1.Location = new System.Drawing.Point(6, 53);
            this.label_item1.Name = "label_item1";
            this.label_item1.Size = new System.Drawing.Size(20, 26);
            this.label_item1.TabIndex = 1;
            this.label_item1.Text = "-";
            // 
            // label_item0
            // 
            this.label_item0.AutoSize = true;
            this.label_item0.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item0.Location = new System.Drawing.Point(6, 27);
            this.label_item0.Name = "label_item0";
            this.label_item0.Size = new System.Drawing.Size(20, 26);
            this.label_item0.TabIndex = 0;
            this.label_item0.Text = "-";
            // 
            // TradeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 356);
            this.Controls.Add(this.groupBox_trade);
            this.Controls.Add(this.groupBox_inventory);
            this.Name = "TradeForm";
            this.Text = "TradeForm";
            this.Load += new System.EventHandler(this.TradeForm_Load);
            this.groupBox_trade.ResumeLayout(false);
            this.groupBox_trade.PerformLayout();
            this.groupBox_inventory.ResumeLayout(false);
            this.groupBox_inventory.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_trade;
        private System.Windows.Forms.Button button_trade_cancel;
        private System.Windows.Forms.Button button_trade_buy;
        private System.Windows.Forms.Label label_trade_sum_value;
        private System.Windows.Forms.Label label_trade_sum;
        private System.Windows.Forms.Button button_trade_item6_more;
        private System.Windows.Forms.Button button_trade_item5_more;
        private System.Windows.Forms.Button button_trade_item4_more;
        private System.Windows.Forms.Button button_trade_item3_more;
        private System.Windows.Forms.Button button_trade_item2_more;
        private System.Windows.Forms.Button button_trade_item1_more;
        private System.Windows.Forms.Button button_trade_item0_more;
        private System.Windows.Forms.Button button_trade_item6_less;
        private System.Windows.Forms.Button button_trade_item5_less;
        private System.Windows.Forms.Button button_trade_item4_less;
        private System.Windows.Forms.Button button_trade_item3_less;
        private System.Windows.Forms.Button button_trade_item2_less;
        private System.Windows.Forms.Button button_trade_item1_less;
        private System.Windows.Forms.Button button_trade_item0_less;
        private System.Windows.Forms.Label label_trade_item6_quantity;
        private System.Windows.Forms.Label label_trade_item5_quantity;
        private System.Windows.Forms.Label label_trade_item4_quantity;
        private System.Windows.Forms.Label label_trade_item3_quantity;
        private System.Windows.Forms.Label label_trade_item2_quantity;
        private System.Windows.Forms.Label label_trade_item1_quantity;
        private System.Windows.Forms.Label label_trade_item0_quantity;
        private System.Windows.Forms.Label label_trade_item6;
        private System.Windows.Forms.Label label_trade_item5;
        private System.Windows.Forms.Label label_trade_item4;
        private System.Windows.Forms.Label label_trade_item3;
        private System.Windows.Forms.Label label_trade_item2;
        private System.Windows.Forms.Label label_trade_item1;
        private System.Windows.Forms.Label label_trade_item0;
        private System.Windows.Forms.GroupBox groupBox_inventory;
        private System.Windows.Forms.Label label_item9_quantity;
        private System.Windows.Forms.Label label_item8_quantity;
        private System.Windows.Forms.Label label_item7_quantity;
        private System.Windows.Forms.Label label_item6_quantity;
        private System.Windows.Forms.Label label_item5_quantity;
        private System.Windows.Forms.Label label_item4_quantity;
        private System.Windows.Forms.Label label_item3_quantity;
        private System.Windows.Forms.Label label_item2_quantity;
        private System.Windows.Forms.Label label_item1_quantity;
        private System.Windows.Forms.Label label_item0_quantity;
        private System.Windows.Forms.Label label_item9;
        private System.Windows.Forms.Label label_item8;
        private System.Windows.Forms.Label label_item7;
        private System.Windows.Forms.Label label_item6;
        private System.Windows.Forms.Label label_item5;
        private System.Windows.Forms.Label label_item4;
        private System.Windows.Forms.Label label_item3;
        private System.Windows.Forms.Label label_item2;
        private System.Windows.Forms.Label label_item1;
        private System.Windows.Forms.Label label_item0;
    }
}