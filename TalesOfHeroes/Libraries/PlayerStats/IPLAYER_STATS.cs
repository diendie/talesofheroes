﻿//using LibNPC_FACTORY;
using System.Collections.Generic;

namespace TalesOfHeroes
{
    /// <summary>
    /// Delegate - Player Health change
    /// </summary>
    /// <param name="source_ID"></param>
    /// <param name="value"></param>
    public delegate void PlayerHPChangeDelegate(int source_ID, int target_ID, int value);
    /// <summary>
    /// Delegate - Player death
    /// </summary>
    /// <param name="killer_ID"></param>
    public delegate void PlayerDeathDelegate(int killer_ID);

    /// <summary>
    /// Supported Player Classes
    /// </summary>
    public enum Class
    {
        /// <summary>
        /// Mage
        /// </summary>
        Mage,
        /// <summary>
        /// Warrior
        /// </summary>
        Warrior,
        /// <summary>
        /// Cleric
        /// </summary>
        Cleric,
        /// <summary>
        /// Rogue
        /// </summary>
        Rogue
    }


    /// <summary>
    /// Interface for the PLayer_Stats
    /// </summary>
    public interface IPLAYER_STATS
    {
        /// <summary>
        /// Notifies about Player Health change
        /// </summary>
        event PlayerHPChangeDelegate onPlayerHealthChange;
        /// <summary>
        /// Triggers on Player Death
        /// </summary>
        event PlayerDeathDelegate onPlayerDeath;

        /// <summary>
        /// Player state - dead or alive
        /// </summary>
        bool Alive { get; }

        /// <summary>
        /// Player Agility
        /// </summary>
        int Agility { get; }
        /// <summary>
        /// Player Charisma
        /// </summary>
        int Charisma { get; }
        /// <summary>
        /// Player Constitution
        /// </summary>
        int Constitution { get; }
        /// <summary>
        /// Current level Experience
        /// </summary>
        int CurrentExp { get; }
        /// <summary>
        /// Damage inflicted by Player
        /// </summary>
        int Damage { get; }
        /// <summary>
        /// Player current Health
        /// </summary>
        int Health { get; }
        /// <summary>
        /// Player ID (will most likely be removed)
        /// </summary>
        int ID { get; }
        /// <summary>
        /// Player Intellect
        /// </summary>
        int Intellect { get; }
        /// <summary>
        /// Player Level
        /// </summary>
        int Level { get; }
        /// <summary>
        /// Experience needed to get to the next level
        /// </summary>
        int LVLExp { get; }
        /// <summary>
        /// Player max Health
        /// </summary>
        int MaxHealth { get; }
        /// <summary>
        /// Player Name
        /// </summary>
        string Name { get; }
        /// <summary>
        /// Player Armour Class
        /// </summary>
        ArmourClass PlayerArmourClass { get; }
        /// Player Armour Rating
        /// </summary>
        int PlayerArmourRating { get; }
        /// <summary>
        /// Experience left till the levelup
        /// </summary>
        int RemainingExp { get; }
        /// <summary>
        /// Player Strength
        /// </summary>
        int Strength { get; }
        /// <summary>
        /// Player Wisdom
        /// </summary>
        int Wisdom { get; }



        /// <summary>
        /// Add Player using StatPack and ArmourPack structs
        /// </summary>
        /// <param name="name">Player Name</param>
        /// <param name="playerStatPack">StatPack struct</param>
        /// <param name="playerArmourPack">ArmourPack struct</param>
        void AddPlayer(string name, StatPack playerStatPack, ArmourPack playerArmourPack);
        /// <summary>
        /// !WIP! Add Player using preset
        /// </summary>
        /// <param name="name">Player name</param>
        /// <param name="presetName">Preset name</param>
        void AddPLayerFromPreset(string name, string presetName);


        /// <summary>
        /// Changes Health by the set amount. Can be used as heal (negative amounts).
        /// <para>
        /// <event cref="onPlayerHealthChange">Triggers when Player Health changes</event>
        /// <event cref="onPlayerDeath">Indirect trigger through Die()</event>
        /// </para>
        /// </summary>
        /// <param name="source_ID">Source ID</param>
        /// <param name="amount">Amount of change</param>
        /// <returns>True - Player is Alive, Fals - Player is Dead</returns>
        bool ChangeHealth(int source_ID, int amount);
        /// <summary>
        /// Handles the EXP adding
        /// </summary>
        /// <param name="amount">Amount of Exp to grant</param>
        void AddExp(int amount);


        //Turn during combat
        int Turn { get; }

        /// <summary>
        /// Sets Player Armour Class
        /// </summary>
        /// <param name="newAC">New Armour Class</param>
        void SetArmourClass(ArmourClass newAC);
        /// <summary>
        /// Sets Player Armour Class
        /// </summary>
        /// <param name="newAR">New Armour Rating</param>
        void SetArmourRating(int newAR);
        /// <summary>
        /// Sets Player Damage
        /// </summary>
        /// <param name="value">Damage Value</param>
        void SetDamage(int value);

        /// <summary>
        /// Sets the selected Stat to a Value
        /// </summary>
        /// <param name="stat">Selected Stat</param>
        /// <param name="value">Value</param>
        void SetStat(Stats stat, int value);

        //Sets Turn value in combat queue
        void SetTurn();


        /// <summary>
        /// Sets Health to 0 and Alive to false
        /// <para>
        /// <event cref="onPlayerDeath">Triggers when Player dies</event>
        /// </para>
        /// </summary>
        /// <param name="killer_ID">Killer ID</param>
        void Die(int killer_ID);
    }
}