﻿//using LibNPC_FACTORY;
using System;
using System.Collections.Generic;
using System.Linq;


namespace TalesOfHeroes
{
    /// <summary>
    /// Holds all information about the Player
    /// </summary>
    public class PLAYER_STATS : IPLAYER_STATS
    {
        /// <summary>
        /// Notifies about Player Health change
        /// </summary>
        public event PlayerHPChangeDelegate onPlayerHealthChange;
        /// <summary>
        /// Notifies about Player death
        /// </summary>
        public event PlayerDeathDelegate onPlayerDeath;

        public PLAYER_STATS()
        {
        }

        /// <summary>
        /// Add Player using StatPack and ArmourPack structs
        /// </summary>
        /// <param name="name">Player Name</param>
        /// <param name="playerStatPack">StatPack struct</param>
        /// <param name="playerArmourPack">ArmourPack struct</param>
        public void AddPlayer(string name, StatPack playerStatPack, ArmourPack playerArmourPack)
        {
            Alive = true;

            Name = name;

            //SetStatsFromList(playerStatValues);
            SetStatsFromPack(playerStatPack);
            SetArmourFromPack(playerArmourPack);

            Level = 1;
            CurrentExp = 0;

            
            //MaxHealth = 100;
            MaxHealth = 10000;//DEBUG
            Health = MaxHealth;

            LVLExp = 100;
            //Exp
            RemainingExp = LVLExp - CurrentExp;
        }

        /// <summary>
        /// !WIP! Add Player using preset
        /// </summary>
        /// <param name="name">Player name</param>
        /// <param name="presetName">Preset name</param>
        public void AddPLayerFromPreset(string name, string presetName)
        {
            throw new NotImplementedException("WIP Adding PLayer from preset");
        }


        /// <summary>
        /// Handles the EXP adding
        /// </summary>
        /// <param name="amount">Amount of Exp to grant</param>
        public void AddExp(int amount)
        {
            CurrentExp += amount;

            //lvlup
            if (CurrentExp >= LVLExp)
            {
                int over = CurrentExp - LVLExp;
                Level += 1;
                LVLExp = 100 + Convert.ToInt32(100 * (Level * 0.1));

                Console.WriteLine($"Level Up! Now level {Level}");

                CurrentExp = over;

                MaxHealth += Strength * 2;
                Health = MaxHealth;
            }

            RemainingExp = LVLExp - CurrentExp;
        }


        /// <summary>
        /// Changes Health by the set amount. Can be used as heal (negative amounts).
        /// <para>
        /// <event cref="onPlayerHealthChange">Triggers when Player Health changes</event>
        /// <event cref="onPlayerDeath">Indirect trigger through Die()</event>
        /// </para>
        /// </summary>
        /// <param name="source_ID">Source ID</param>
        /// <param name="amount">Amount of change</param>
        /// <returns>True - Player is Alive, Fals - Player is Dead</returns>
        public bool ChangeHealth(int source_ID, int amount)
        {
            if (Health - Math.Abs(amount) <= 0)
            {
                //Console.WriteLine("Killing Player");
                Die(source_ID);
                return false;
            }
            else
            {
                Health += amount;
                if (Health > MaxHealth)
                {
                    Health = MaxHealth;
                }

                onPlayerHealthChange?.Invoke(source_ID, 0, amount);
                //Console.WriteLine($"Changing Player Health by {amount}. New Health: {Health}");
                return true;
            }
        }

        /// <summary>
        /// Sets Player Armour Class
        /// </summary>
        /// <param name="newAC">New Armour Class</param>
        public void SetArmourClass(ArmourClass newAC)
        {
            PlayerArmourClass = newAC;
        }
        /// <summary>
        /// Sets Player Armour Class
        /// </summary>
        /// <param name="newAR">New Armour Rating</param>
        public void SetArmourRating(int newAR)
        {
            PlayerArmourRating = newAR;
        }
        /// <summary>
        /// Sets Player Armour values using ArmourPack struct
        /// </summary>
        /// <param name="newArmourPack">Armour Pack</param>
        internal void SetArmourFromPack(ArmourPack newArmourPack)
        {
            PlayerArmourClass = newArmourPack.ArmourClass;
            PlayerArmourRating = newArmourPack.ArmourRating;
        }

        /// <summary>
        /// Sets the selected Stat to a Value
        /// </summary>
        /// <param name="stat">Selected Stat</param>
        /// <param name="value">Value</param>
        public void SetStat(Stats stat, int value)
        {
            switch (stat)
            {
                case Stats.Strength:
                    {
                        Strength = value;
                        break;
                    }
                case Stats.Agility:
                    {
                        Agility = value;
                        break;
                    }
                case Stats.Intellect:
                    {
                        Intellect = value;
                        break;
                    }
                case Stats.Charisma:
                    {
                        Charisma = value;
                        break;
                    }
            }
        }
        /// <summary>
        /// Sets Player Stats using 
        /// </summary>
        /// <param name="newStatPack"></param>
        internal void SetStatsFromPack(StatPack newStatPack)
        {
            Strength = newStatPack.Strength;
            Constitution = newStatPack.Constitution;
            Agility = newStatPack.Agility;
            Intellect = newStatPack.Intellect;
            Wisdom = newStatPack.Wisdom;
            Charisma = newStatPack.Charisma;
        }

        /// <summary>
        /// Sets Player Damage
        /// </summary>
        /// <param name="value">Damage Value</param>
        public void SetDamage(int value)
        {
            Damage = value;
        }


        /// <summary>
        /// Sets Health to 0 and Alive to false
        /// <para>
        /// <event cref="onPlayerDeath">Triggers when Player dies</event>
        /// </para>
        /// </summary>
        /// <param name="killer_ID">Killer ID</param>
        public void Die(int killer_ID)
        {
            Console.WriteLine("DEAD");
            Health = 0;
            Alive = false;

            onPlayerDeath?.Invoke(killer_ID);
        }



        //Sets Turn value in combat queue
        public void SetTurn()
        {
            Random Roll = new Random();

            Turn = Roll.Next(1, 100);
        }



        /// <summary>
        /// Player Name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Player ID (will most likely be removed)
        /// </summary>
        public int ID { get; private set; } = 0;

        //public StatsPack PlayerStats;// { get; private set; }

        /// <summary>
        /// Player Strength
        /// </summary>
        public int Strength { get; private set; }
        /// <summary>
        /// Player Agility
        /// </summary>
        public int Agility { get; private set; }
        /// <summary>
        /// Player Intellect
        /// </summary>
        public int Intellect { get; private set; }
        /// <summary>
        /// Player Charisma
        /// </summary>
        public int Charisma { get; private set; }
        /// <summary>
        /// Player Constitution
        /// </summary>
        public int Constitution { get; private set; }
        /// <summary>
        /// Player Wisdom
        /// </summary>
        public int Wisdom { get; private set; }
        /// <summary>
        /// Player Armour Class
        /// </summary>
        public ArmourClass PlayerArmourClass { get; private set; }
        /// <summary>
        /// Player Armour Rating
        /// </summary>
        public int PlayerArmourRating { get; private set; }
        /// <summary>
        /// Damage inflicted by Player
        /// </summary>
        public int Damage { get; private set; }

        /// <summary>
        /// Player current Health
        /// </summary>
        public int Health { get; private set; }
        /// <summary>
        /// Player max Health
        /// </summary>
        public int MaxHealth { get; private set; }

        /// <summary>
        /// Player state - dead or alive
        /// </summary>
        public bool Alive { get; private set; }


        /// <summary>
        /// Player Level
        /// </summary>
        public int Level { get; private set; }
        /// <summary>
        /// Current level Experience
        /// </summary>
        public int CurrentExp { get; private set; }
        /// <summary>
        /// Experience left till the levelup
        /// </summary>
        public int RemainingExp { get; private set; }
        /// <summary>
        /// Experience needed to get to the next level
        /// </summary>
        public int LVLExp { get; private set; }
        //Turn during combat
        public int Turn { get; private set; }
    }
}
