﻿//using LibNPC_FACTORY;
using System.Collections.Generic;

namespace TalesOfHeroes
{
    /// <summary>
    /// Delegate - Entity Death
    /// </summary>
    /// <param name="new_ID">ID of new Entity</param>
    public delegate void EntityCreationDelegate(int new_ID);
    /// <summary>
    /// Delegate - Entity Health Changed
    /// </summary>
    /// <param name="source_ID">Who changed</param>
    /// <param name="target_ID">Target</param>
    /// <param name="amount">Change</param>
    public delegate void EntityHPChangeDelegate(int source_ID, int target_ID, int amount);

    /// <summary>
    /// Delegate - Entity Death
    /// </summary>
    /// <param name="dead_ID">ID of a dead Entity</param>
    /// <param name="killer_ID">ID of a Killer</param>
    public delegate void EntityDeathDelegate(int dead_ID, int killer_ID);

    /// <summary>
    /// Interface for the NPC_Stats
    /// </summary>
    public interface INPC_STATS
    {
        /// <summary>
        /// Event - Entity Creation
        /// </summary>
        event EntityCreationDelegate onEntityCreation;
        /// <summary>
        /// Event - Entity Health changed
        /// </summary>
        event EntityHPChangeDelegate onEntityHealthChange;
        /// <summary>
        /// Event - Entity Death
        /// </summary>
        event EntityDeathDelegate onEntityDeath;

        /// <summary>
        /// List of Agility stats
        /// </summary>
        List<int> Agility { get; }
        /// <summary>
        /// List of Alive Entities
        /// </summary>
        List<int> Alive { get; }
        /// <summary>
        /// List of Attack Damage of all Entities
        /// </summary>
        List<int> AttackDamage { get; }
        /// <summary>
        /// List of Entity Attack types
        /// </summary>
        List<string> AttackRange { get; }
        /// <summary>
        /// List of Entity Behaviour types
        /// </summary>
        List<string> BehaviourType { get; }
        /// <summary>
        /// List of all Charisma stats
        /// </summary>
        List<int> Charisma { get; }
        /// <summary>
        /// List of all Constitution stats
        /// </summary>
        List<int> Constitution { get; }
        /// <summary>
        /// List of Dead Entities
        /// </summary>
        List<int> Dead { get; }
        /// <summary>
        /// List of all Armour Classes
        /// </summary>
        List<ArmourClass> EntityArmourClass { get; }
        /// <summary>
        /// List of all Armour Ratings
        /// </summary>
        List<int> EntityArmourRating { get; }
        /// <summary>
        /// List of Friendly Entities
        /// </summary>
        List<int> Friendlies { get; }
        /// <summary>
        /// List of Health of all Entities
        /// </summary>
        List<int> Health { get; }
        /// <summary>
        /// List of Hostile Entities
        /// </summary>
        List<int> Hostiles { get; }
        /// <summary>
        /// List of all Entity IDs
        /// </summary>
        List<int> ID { get; }
        /// <summary>
        /// List of Int stats
        /// </summary>
        List<int> Intellect { get; }
        /// <summary>
        /// List of Max Health of all Entities
        /// </summary>
        List<int> MaxHealth { get; }
        /// <summary>
        /// List of Entity Names
        /// </summary>
        List<string> Name { get; }
        /// <summary>
        /// List of Neutral Entities
        /// </summary>
        List<int> Neutrals { get; }
        /// <summary>
        /// List of Entity Rarities
        /// </summary>
        List<string> Rarity { get; }
        /// <summary>
        /// List of Str stats
        /// </summary>
        List<int> Strength { get; }
        /// <summary>
        /// List of NPC Turn positions
        /// </summary>
        List<int> Turn { get; }
        /// <summary>
        /// List of all Wisdom stats
        /// </summary>
        List<int> Wisdom { get; }


        /// <summary>
        /// !OBSOLETE!
        /// </summary>
        List<List<string>> Behaviour { get; }


        /// <summary>
        /// Creates Entity of requested type
        /// <para>
        /// <event cref="onEntityCreation">Triggers when Entity is created</event>
        /// </para>
        /// </summary>
        /// <param name="selection">Selected Entity type</param>
        /// <param name="player_level">Player level</param>
        void AddEntity(NPC_Type selection, int player_level);

        /// <summary>
        /// Creates Entity using Packs
        /// <para>
        /// <event cref="onEntityCreation">Triggers when Entity is created</event>
        /// </para>
        /// </summary>
        /// <param name="statPack">Entity stats</param>
        /// <param name="traitPack">Entity traits</param>
        /// <param name="armourPack">Entity armour</param>
        /// <param name="player_level">Player level</param>
        void AddEntity(StatPack statPack, TraitPack traitPack, ArmourPack armourPack, int player_level);

        /// <summary>
        /// Sets the Armour Class of the target Entity
        /// </summary>
        /// <param name="entity_ID">Target Entity</param>
        /// <param name="newAC">New Armour Class</param>
        void SetArmourClass(int entity_ID, ArmourClass newAC);
        /// <summary>
        /// Sets the Armour Rating of the target Entity
        /// </summary>
        /// <param name="entity_ID">Target Entity</param>
        /// <param name="newAR">New Armour Rating</param>
        void SetArmourRating(int entity_ID, int newAR);
        /// <summary>
        /// Sets Entity Damage
        /// </summary>
        /// <param name="entity_ID">Target ID</param>
        /// <param name="value">Damage Value</param>
        void SetDamage(int entity_ID, int value);
        /// <summary>
        /// Sets the health of an entity
        /// </summary>
        /// <param name="entity_ID">Target Entity ID</param>
        /// <param name="value">Initial Health pool of an Entity</param>
        void SetHealth(int entity_ID, int value);
        /// <summary>
        /// Sets the Stat of the Target to the Value
        /// </summary>
        /// <param name="entity_ID">Target ID</param>
        /// <param name="stat">Stat to set</param>
        /// <param name="value">Value</param>
        void SetStat(int entity_ID, Stats stat, int value);
        /// <summary>
        /// Changes Health by the set amount. Can be used as heal (negative amounts)
        /// </summary>
        /// <param name="source_ID">ID of changer</param>
        ///<param name="target_ID">ID of target</param>
        /// <param name="amount">Change</param>
        void ChangeHealth(int source_ID, int target_ID, int amount);

        /// <summary>
        /// Kills an entity, removing it from the Alive and alignment list, moving to the Dead
        /// </summary>
        /// <param name="target_ID">Target Entity ID</param>
        /// <param name="killer_ID">Killer ID</param>
        void KillEntity(int target_ID, int killer_ID);
    }
}