﻿using System;
using System.Collections.Generic;
//using LibNPC_FACTORY;
//using LibPLAYER_STATS;
using System.Linq;

namespace TalesOfHeroes
{
    /// <summary>
    /// Contains information about all NPCs
    /// </summary>
    public class NPC_STATS : INPC_STATS
    {
        /// <summary>
        /// Triggered by Entity Creation
        /// </summary>
        public event EntityCreationDelegate onEntityCreation;
        /// <summary>
        /// Triggered by Entity Health change
        /// </summary>
        public event EntityHPChangeDelegate onEntityHealthChange;
        /// <summary>
        /// Triggered by Entity death
        /// </summary>
        public event EntityDeathDelegate onEntityDeath;

        [NonSerialized]
        readonly INPC_FACTORY Factory;

        /// <summary>
        /// Default constructor, initiates NPC Factory
        /// </summary>
        public NPC_STATS()
        {
            Factory = new NPC_FACTORY();
            Turn.Clear();
        }

        /// <summary>
        /// Creates Entity of requested type
        /// <para>
        /// <event cref="onEntityCreation">Triggers when Entity is created</event>
        /// </para>
        /// </summary>
        /// <param name="selection">Selected Entity type</param>
        /// <param name="player_level">Player level</param>
        public void AddEntity(NPC_Type selection, int player_level)
        {
            Factory.LoadEntity(selection);

            ID.Add(Next_ID);
            Alive.Add(Next_ID);

            int index = ID.IndexOf(Next_ID);
            SetStatsFromPack(index, player_level, Factory.EntityStatPack);
            SetTraitsFromPack(index, Factory.EntityTraitPack);
            SetArmourFromPack(index, Factory.EntityArmourPack);

            onEntityCreation?.Invoke(Next_ID);

            Next_ID++;
        }

        /// <summary>
        /// Creates Entity using Packs
        /// <para>
        /// <event cref="onEntityCreation">Triggers when Entity is created</event>
        /// </para>
        /// </summary>
        /// <param name="statPack">Entity stats</param>
        /// <param name="traitPack">Entity traits</param>
        /// <param name="armourPack">Entity armour</param>
        /// <param name="player_level">Player level</param>
        public void AddEntity(StatPack statPack, TraitPack traitPack, ArmourPack armourPack, int player_level)
        {
            ID.Add(Next_ID);
            Alive.Add(Next_ID);

            int index = ID.IndexOf(Next_ID);
            SetStatsFromPack(index, player_level, statPack);
            SetTraitsFromPack(index, traitPack);
            SetArmourFromPack(index, armourPack);

            onEntityCreation?.Invoke(Next_ID);

            Next_ID++;
        }

        /// <summary>
        /// Sets the Stat of the Target to the Value
        /// </summary>
        /// <param name="entity_ID">Target ID</param>
        /// <param name="stat">Stat to set</param>
        /// <param name="value">Value</param>
        public void SetStat(int entity_ID, Stats stat, int value)
        {
            int index = ID.IndexOf(entity_ID);
            switch (stat)
            {
                case Stats.Strength:
                    {
                        //check if List hold less elements than needed
                        //if true, add
                        if (index >= Strength.Count)
                        {
                            Strength.Add(value);
                        }
                        //if false assign
                        else
                        {
                            Strength[index] = value;
                        }
                        break;
                    }
                case Stats.Agility:
                    {
                        if (index >= Agility.Count)
                        {
                            Agility.Add(value);
                        }
                        else
                            Agility[index] = value;
                        break;
                    }
                case Stats.Intellect:
                    {
                        if (index >= Intellect.Count)
                        {
                            Intellect.Add(value);
                        }
                        else
                            Intellect[index] = value;
                        break;
                    }
                case Stats.Charisma:
                    {
                        if (index >= Charisma.Count)
                        {
                            Charisma.Add(value);
                        }
                        else
                            Charisma[index] = value;
                        break;
                    }
                case Stats.Constitution:
                    {
                        if (index >= Constitution.Count)
                        {
                            Constitution.Add(value);
                        }
                        else
                            Constitution[index] = value;
                        break;
                    }
                case Stats.Wisdom:
                    {
                        if (index >= Wisdom.Count)
                        {
                            Wisdom.Add(value);
                        }
                        else
                            Wisdom[index] = value;
                        break;
                    }
            }
        }
        /// <summary>
        /// Sets Entity Stats using StatPack
        /// </summary>
        /// <param name="index">Target Entity index</param>
        /// <param name="player_lvl">Player level</param>
        /// <param name="entityStatPack">StatPack struct</param>
        internal void SetStatsFromPack(int index, int player_lvl, StatPack entityStatPack)
        {
            Strength.Insert(index, entityStatPack.Strength);
            Constitution.Insert(index, entityStatPack.Constitution);
            Agility.Insert(index, entityStatPack.Agility);
            Intellect.Insert(index, entityStatPack.Intellect);
            Wisdom.Insert(index, entityStatPack.Wisdom);
            Charisma.Insert(index, entityStatPack.Charisma);

            InitHealth(index, entityStatPack.BaseHealth, player_lvl);
        }

        /// <summary>
        /// Sets the Armour Class of the target Entity
        /// </summary>
        /// <param name="entity_ID">Target Entity</param>
        /// <param name="newAC">New Armour Class</param>
        public void SetArmourClass(int entity_ID, ArmourClass newAC)
        {
            EntityArmourClass[ID.IndexOf(entity_ID)] = newAC;
        }
        /// <summary>
        /// Sets the Armour Rating of the target Entity
        /// </summary>
        /// <param name="entity_ID">Target Entity</param>
        /// <param name="newAR">New Armour Rating</param>
        public void SetArmourRating(int entity_ID, int newAR)
        {
            EntityArmourRating[ID.IndexOf(entity_ID)] = newAR;
        }
        /// <summary>
        /// Sets the Armour Class and Armour Rating of an Entity using ArmourPack
        /// </summary>
        /// <param name="index">Target Entity index</param>
        /// <param name="entityArmourPack">Armour Pack</param>
        internal void SetArmourFromPack(int index, ArmourPack entityArmourPack)
        {
            EntityArmourClass.Insert(index, entityArmourPack.ArmourClass);
            EntityArmourRating.Insert(index, entityArmourPack.ArmourRating);
        }
        /// <summary>
        /// Sets Entity Traits using TraitPack. !Must be called after SetStatsFromPack!
        /// </summary>
        /// <param name="index">Target Entity index</param>
        /// <param name="entityTraitPack">TraitPack</param>
        internal void SetTraitsFromPack(int index, TraitPack entityTraitPack)
        {
            string name = entityTraitPack.Name;
            string type = entityTraitPack.BehaviourType;
            string attack = entityTraitPack.AttackRange;
            string rarity = entityTraitPack.Rarity;

            Name.Insert(index, name);
            BehaviourType.Insert(index, type);
            AttackRange.Insert(index, attack);
            Rarity.Insert(index, rarity);

            switch(type)
            {
                case "Friendly":
                    {
                        Friendlies.Add(Next_ID);
                        break;
                    }
                case "Hostile":
                    {
                        Hostiles.Add(Next_ID);
                        break;
                    }
                case "Neutral":
                    {
                        Neutrals.Add(Next_ID);
                        break;
                    }
                default: throw new NotImplementedException("Could not determine Entity Type");
            }

            switch(attack)
            {
                case "Melee":
                    {
                        AttackDamage.Add(-(Strength[index] + Agility[index]));
                        break;
                    }
                case "Ranged":
                    {
                        AttackDamage.Add(-(Agility[index] + Intellect[index]));
                        break;
                    }
            }
        }


        /// <summary>
        /// Sets the max amount of Health of an Entity
        /// </summary>
        /// <param name="index">Target Entity index</param>
        /// <param name="base_health">Base Health</param>
        /// <param name="player_lvl">Player level</param>
        void InitHealth(int index, int base_health, int player_lvl)
        {
            int max_health = base_health + player_lvl * Strength[index];
            MaxHealth.Add(max_health);
            Health.Add(max_health);
        }
        /// <summary>
        /// Sets the health of an entity
        /// </summary>
        /// <param name="entity_ID">Target Entity ID</param>
        /// <param name="value">Health pool of an Entity</param>
        public void SetHealth(int entity_ID, int value)
        {
            if (Alive.IndexOf(entity_ID) >= 0)
            {
                int index = ID.IndexOf(entity_ID);

                Health[index] = value;
            }
        }
        /// <summary>
        /// Changes Health by the set amount. Can be used as heal (negative amounts)
        /// <para>
        /// <event cref="onEntityHealthChange">Triggers when Entity Health changes</event>
        /// <event cref="onEntityDeath">Indirect trigger through KillEntity()</event>
        /// </para>
        /// </summary>
        /// <param name="source_ID">ID of changer</param>
        ///<param name="target_ID">ID of target</param>
        /// <param name="amount">Change</param>
        public void ChangeHealth(int source_ID, int target_ID, int amount)
        {
            int index = ID.IndexOf(target_ID);
            if (Health[index] - Math.Abs(amount) <= 0)
            {
                //Console.WriteLine($"Killing ID{target_ID}");
                KillEntity(target_ID, source_ID);
            }
            else
            {
                Health[index] += amount;
                if (Health[index] > MaxHealth[index])
                {
                    Health[index] = MaxHealth[index];
                }


                //Console.WriteLine($"ID{source_ID} changes ID{target_ID} Health by {amount}");
                onEntityHealthChange?.Invoke(source_ID, target_ID, amount);
            }
        }


        /// <summary>
        /// Kills an entity, removing it from the Alive and alignment list, moving to the Dead
        /// <para>
        /// <event cref="onEntityDeath">Triggers when Entity dies</event>
        /// </para>
        /// </summary>
        /// <param name="target_ID">Target Entity ID</param>
        /// <param name="killer_ID">Killer ID</param>
        public void KillEntity(int target_ID, int killer_ID)
        {
            if (Alive.Contains(target_ID))
            {
                int index = ID.IndexOf(target_ID);

                switch (BehaviourType[index])
                {
                    case "Friendly": Friendlies.Remove(target_ID); Friendlies.TrimExcess(); break;
                    case "Hostile": Hostiles.Remove(target_ID); Hostiles.TrimExcess(); break;
                    case "Neutral": Neutrals.Remove(target_ID); Neutrals.TrimExcess(); break;
                }

                SetHealth(target_ID, 0);

                Alive.Remove(target_ID);
                Dead.Add(target_ID);

                Alive.TrimExcess();

                onEntityDeath?.Invoke(target_ID, killer_ID);
            }
        }

        /// <summary>
        /// Sets Entity Damage
        /// </summary>
        /// <param name="entity_ID">Target ID</param>
        /// <param name="value">Damage Value</param>
        public void SetDamage(int entity_ID, int value)
        {
            int index = ID.IndexOf(entity_ID);

            AttackDamage[index] = value;
        }


        /// <summary>
        /// Next assigned ID
        /// </summary>
        int Next_ID = 1;

        /// <summary>
        /// List of all Entity IDs
        /// </summary>
        public List<int> ID { get; private set; } = new List<int>();

        /// <summary>
        /// List of Entity Names
        /// </summary>
        public List<string> Name { get; private set; } = new List<string>();
        /// <summary>
        /// List of Entity Behaviour Types
        /// </summary>
        public List<string> BehaviourType { get; private set; } = new List<string>();
        /// <summary>
        /// List of Entity Attack ranges
        /// </summary>
        public List<string> AttackRange { get; private set; } = new List<string>();
        /// <summary>
        /// List of Entity Rarities
        /// </summary>
        public List<string> Rarity { get; private set; } = new List<string>();

        /// <summary>
        /// !OBSOLETE!
        /// </summary>
        public List<List<string>> Behaviour { get; private set; } = new List<List<string>>();


        /// <summary>
        /// List of Attack Damage of all Entities
        /// </summary>
        public List<int> AttackDamage { get; private set; } = new List<int>();
        /// <summary>
        /// List of Str stats
        /// </summary>
        public List<int> Strength { get; private set; } = new List<int>();
        /// <summary>
        /// List of Agility stats
        /// </summary>
        public List<int> Agility { get; private set; } = new List<int>();
        /// <summary>
        /// List of Intellect stats
        /// </summary>
        public List<int> Intellect { get; private set; } = new List<int>();
        /// <summary>
        /// List of all Charisma stats
        /// </summary>
        public List<int> Charisma { get; private set; } = new List<int>();
        /// <summary>
        /// List of all Constitution stats
        /// </summary>
        public List<int> Constitution { get; private set; } = new List<int>();
        /// <summary>
        /// List of all Wisdom stats
        /// </summary>
        public List<int> Wisdom { get; private set; } = new List<int>();

        /// <summary>
        /// List of all Armour Classes
        /// </summary>
        public List<ArmourClass> EntityArmourClass { get; private set; } = new List<ArmourClass>();
        /// <summary>
        /// List of all Armour Ratings
        /// </summary>
        public List<int> EntityArmourRating { get; private set; } = new List<int>();



        /// <summary>
        /// List of Health of all Entities
        /// </summary>
        public List<int> Health { get; private set; } = new List<int>();
        /// <summary>
        /// List of Max Health of all Entities
        /// </summary>
        public List<int> MaxHealth { get; private set; } = new List<int>();


        /// <summary>
        /// List of Friendly Entities
        /// </summary>
        public List<int> Friendlies { get; private set; } = new List<int>();
        /// <summary>
        /// List of Hostile Entities
        /// </summary>
        public List<int> Hostiles { get; private set; } = new List<int>();
        /// <summary>
        /// List of Neutral Entities
        /// </summary>
        public List<int> Neutrals { get; private set; } = new List<int>();


        /// <summary>
        /// List of Alive Entities
        /// </summary>
        public List<int> Alive { get; private set; } = new List<int>();
        /// <summary>
        /// List of Dead Entities
        /// </summary>
        public List<int> Dead { get; private set; } = new List<int>();
        /// <summary>
        /// List of NPC Turn positions
        /// </summary>
        public List<int> Turn { get; private set; } = new List<int>();
    }
}