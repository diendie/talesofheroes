﻿using System.Collections.Generic;

namespace TalesOfHeroes
{
    /// <summary>
    /// Packed Stat values (7)
    /// </summary>
    public struct StatPack
    {
        /// <summary>
        /// Base Health
        /// </summary>
        public int BaseHealth;

        /// <summary>
        /// Strength
        /// </summary>
        public int Strength;
        /// <summary>
        /// Constitution
        /// </summary>
        public int Constitution;
        /// <summary>
        /// Agility
        /// </summary>
        public int Agility;
        /// <summary>
        /// Intellect
        /// </summary>
        public int Intellect;
        /// <summary>
        /// Wisdom
        /// </summary>
        public int Wisdom;
        /// <summary>
        /// Charisma
        /// </summary>
        public int Charisma;
    }
    /// <summary>
    /// Packed Traits (4)
    /// </summary>
    public struct TraitPack
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name;
        /// <summary>
        /// Type
        /// </summary>
        public string BehaviourType;
        /// <summary>
        /// Attack type
        /// </summary>
        public string AttackRange;
        /// <summary>
        /// Rarity
        /// </summary>
        public string Rarity;
    }

    /// <summary>
    /// Packed Armour values (2)
    /// </summary>
    public struct ArmourPack
    {
        /// <summary>
        /// Armour Class
        /// </summary>
        public ArmourClass ArmourClass;
        /// <summary>
        /// Armour Rating
        /// </summary>
        public int ArmourRating;
    }

    /// <summary>
    /// All available NPC types
    /// </summary>
    public enum NPC_Type
    {
        /// <summary>
        /// Drunkard
        /// </summary>
        Drunkard = 0,
        /// <summary>
        /// Castle Archer
        /// </summary>
        Guard_Archer = 1,
        /// <summary>
        /// Old Dragon
        /// </summary>
        Old_Dragon = 2
    }

    /// <summary>
    /// Available NPC stats
    /// </summary>
    public enum Stats
    {
        /// <summary>
        /// Strength
        /// </summary>
        Strength = 0,
        /// <summary>
        /// Agility
        /// </summary>
        Agility = 1,
        /// <summary>
        /// Intellect
        /// </summary>
        Intellect = 2,
        /// <summary>
        /// Charisma
        /// </summary>
        Charisma = 3,
        /// <summary>
        /// Constitution
        /// </summary>
        Constitution = 4,
        /// <summary>
        /// Wisdom
        /// </summary>
        Wisdom = 5
    }

    /// <summary>
    /// NPC Attack Type
    /// </summary>
    public enum AttackType
    {
        /// <summary>
        /// Melee
        /// </summary>
        Melee = 0,
        /// <summary>
        /// Ranged
        /// </summary>
        Ranged = 1
    }

    /// <summary>
    /// Armour Class
    /// </summary>
    public enum ArmourClass
    { 
        /// <summary>
        /// Cloth Armour
        /// </summary>
        Cloth = 0,
        /// <summary>
        /// Light Armour
        /// </summary>
        Light = 1,
        /// <summary>
        /// Medium Armour
        /// </summary>
        Medium = 2,
        /// <summary>
        /// Heavy Armour
        /// </summary>
        Heavy = 3
    }

    /// <summary>
    /// Handles the assignment of Stats and Traits
    /// </summary>
    public interface INPC_FACTORY
    {
        /// <summary>
        /// Pack of Armour Class and Rating for the Entity Type
        /// </summary>
        ArmourPack EntityArmourPack { get; }
        /// <summary>
        /// Entity StatPack
        /// </summary>
        StatPack EntityStatPack { get; }
        /// <summary>
        /// Entity TraitPack
        /// </summary>
        TraitPack EntityTraitPack { get; }

        /// <summary>
        /// All Stats from Stats enum (NPC_FACTORY)
        /// </summary>
        List<Stats> ListOfStats { get; }
        /// <summary>
        /// List of Entity Stats
        /// </summary>
        //List<int> Stats { get; }
        /// <summary>
        /// List of Entity Traits
        /// </summary>
        //List<string> Traits { get; }

        /// <summary>
        /// Loads selected Entity Stats and Traits
        /// <para>After loading you need to pass Factory.Stats and Factory.Traits to the NPC_Stats.AddEntity()</para>
        /// </summary>
        /// <param name="Selection">Entity to load</param>
        void LoadEntity(NPC_Type Selection);
    }
}