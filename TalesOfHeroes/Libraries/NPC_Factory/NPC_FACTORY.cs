﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;

namespace TalesOfHeroes
{
    /// <summary>
    /// Handles the assignment of Stats and Traits
    /// </summary>
    public class NPC_FACTORY : INPC_FACTORY
    {
        /// <summary>
        /// DB Connection string
        /// </summary>
        internal string CString = ConfigurationManager.ConnectionStrings["Entities"].ConnectionString;

        /// <summary>
        /// Stores the last loaded type
        /// </summary>
        private NPC_Type LastLoadedType;

        /// <summary>
        /// All Stats from Stats enum (NPC_FACTORY)
        /// </summary>
        public List<Stats> ListOfStats { get; } = Enum.GetValues(typeof(Stats)).Cast<Stats>().ToList();

        /// <summary>
        /// Pack of Armour Class and Rating for the Entity Type
        /// </summary>
        public ArmourPack EntityArmourPack { get; private set; }
        /// <summary>
        /// Entity StatPack
        /// </summary>
        public StatPack EntityStatPack { get; private set; }
        /// <summary>
        /// Entity TraitPack
        /// </summary>
        public TraitPack EntityTraitPack { get; private set; }

        /// <summary>
        /// Loads selected Entity Stats and Traits
        /// </summary>
        /// <param name="Selection">Entity to load</param>
        public void LoadEntity(NPC_Type Selection)
        {
            if (LastLoadedType != Selection)
            {

                //Stats.Clear();
                //Traits.Clear();

                List<Stats> StatsList = Enum.GetValues(typeof(Stats)).Cast<Stats>().ToList<Stats>();
                try
                {
                    using (SQLiteConnection read_connection = new SQLiteConnection(CString))
                    {
                        read_connection.Open();
                        string selected = Selection.ToString();
                        string query = $"SELECT * FROM TraitsAndStats WHERE ID=\'{selected}\'";
                        //Console.WriteLine(query);//DEBUG
                        try
                        {
                            using (SQLiteCommand command = read_connection.CreateCommand())
                            {
                                command.CommandText = query;
                                command.CommandType = System.Data.CommandType.Text;

                                SQLiteDataReader reader = command.ExecuteReader();
                                while (reader.Read())
                                {
                                    Console.WriteLine($"Loading Entity Archetype: {selected}");
                                    Console.WriteLine("_______________________________________________________");
                                    Console.WriteLine("-Loading Traits");

                                    EntityTraitPack = new TraitPack
                                    {
                                        Name = Convert.ToString(reader["Name"]),
                                        BehaviourType = Convert.ToString(reader["BehaviourType"]),
                                        AttackRange = Convert.ToString(reader["AttackRange"]),
                                        Rarity = Convert.ToString(reader["Rarity"])
                                    };
                                    Console.WriteLine($" |Name...........{EntityTraitPack.Name}");
                                    Console.WriteLine($" |BehaviourType..{EntityTraitPack.BehaviourType}");
                                    Console.WriteLine($" |AttackRange....{EntityTraitPack.AttackRange}");
                                    Console.WriteLine($" |Rarity.........{EntityTraitPack.Rarity}");

                                    Console.WriteLine("--Traits loaded");


                                    Console.WriteLine("-Loading Stats");

                                    EntityStatPack = new StatPack
                                    {
                                        BaseHealth = Convert.ToInt32(reader["BaseHealth"]),

                                        Strength = ExtractStat(Stats.Strength, reader),
                                        Constitution = ExtractStat(Stats.Constitution, reader),
                                        Agility = ExtractStat(Stats.Agility, reader),
                                        Intellect = ExtractStat(Stats.Intellect, reader),
                                        Wisdom = ExtractStat(Stats.Wisdom, reader),
                                        Charisma = ExtractStat(Stats.Charisma, reader)
                                    };
                                    Console.WriteLine($" |Base Health....{EntityStatPack.BaseHealth}");
                                    Console.WriteLine($" |Strength.......{EntityStatPack.Strength}");
                                    Console.WriteLine($" |Constitution...{EntityStatPack.Constitution}");
                                    Console.WriteLine($" |Agility........{EntityStatPack.Agility}");
                                    Console.WriteLine($" |Intellect......{EntityStatPack.Intellect}");
                                    Console.WriteLine($" |Wisdom.........{EntityStatPack.Wisdom}");
                                    Console.WriteLine($" |Charisma.......{EntityStatPack.Charisma}");

                                    Console.WriteLine("--Stats loaded");
                                    Console.WriteLine("-Loading Armour");

                                    ArmourClass newAC;
                                    Enum.TryParse(Convert.ToString(reader["ArmourClass"]), out newAC);
                                    int newAR = Convert.ToInt32(reader["ArmourRating"]);

                                    EntityArmourPack = new ArmourPack
                                    {
                                        ArmourClass = newAC,
                                        ArmourRating = newAR,
                                    };
                                    Console.WriteLine($" |Armour Class...{Convert.ToString(reader["ArmourClass"])}");
                                    Console.WriteLine($" |Armour Rating..{EntityArmourPack.ArmourRating}");

                                    Console.WriteLine("--Armour loaded");
                                    Console.WriteLine("_______________________________________________________");
                                    Console.WriteLine($"Archetype Loaded");
                                    Console.WriteLine();


                                    LastLoadedType = Selection;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Failed to read the data!");
                            throw new SQLiteException("Failed to read the data!");
                        }
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Failed to open connection!");
                    throw new SQLiteException("Failed to open connection!");
                }
            }
        }

        int ExtractStat(Stats stat, SQLiteDataReader reader)
        {
            try
            {
                Random random = new Random();

                //Console.WriteLine($" |Extracting {stat.ToString()}");
                string temp = Convert.ToString(reader[stat.ToString()]);
                //Console.WriteLine($"  \\Extracted {temp}");
                int range = temp.IndexOf("-");

                int low = Convert.ToInt32(temp.Substring(0, range));
                int high = Convert.ToInt32(temp.Substring(range + 1));

                int final = random.Next(low, high + 1);
                //Console.WriteLine($"   \\Final value {final}");
                return final;
            }
            catch (Exception)
            {
                //Console.WriteLine("Extraction Exception");
                throw new ArgumentException("Extraction Exception");
            }
        }        
    }
}
