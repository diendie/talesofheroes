﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using LibNPC_FACTORY;
//using LibNPC_STATS;
//using LibPLAYER_STATS;

namespace TalesOfHeroes
{
    /// <summary>
    /// Loads and stores all Effect data
    /// </summary>
    public class EFFECTS : IEFFECTS
    {
        /// <summary>
        /// Triggered when Effect changes HP
        /// </summary>
        public event OnEffectHPChange onEffectHPChange;

        /// <summary>
        /// DB Connection String
        /// </summary>
        private string CString = ConfigurationManager.ConnectionStrings["Effects"].ConnectionString;


        /// <summary>
        /// Adds the Effect into the list of Buffers
        /// </summary>
        /// <param name="e_ID">ID of the Effect</param>
        /// <param name="stat">Buffed Stat </param>
        /// <param name="change">Buff Amount</param>
        void AddBuffing(int e_ID, Stats stat, int change)
        {
            Console.WriteLine($"  |Changes {stat.ToString()} by {change}");
            Dictionary<Stats, int> buff = new Dictionary<Stats, int>();
            buff[stat] = change;

            Buffers[e_ID] = buff;
        }
        /// <summary>
        /// Adds the Effect into the list of Debuffers
        /// </summary>
        /// <param name="e_ID">ID of the Effect</param>
        /// <param name="stat">Debuffed Stat </param>
        /// <param name="change">Debuff Amount</param>
        void AddDebuffing(int e_ID, Stats stat, int change)
        {
            Console.WriteLine($"  |Changes {stat.ToString()} by -{change}");
            Dictionary<Stats, int> debuff = new Dictionary<Stats, int>();
            debuff[stat] = change;

            Debuffers[e_ID] = debuff;
        }
        /// <summary>
        /// Adds the Effect into the list of Healers
        /// </summary>
        /// <param name="e_ID">Effect ID</param>
        /// <param name="heal">Heal Amount</param>
        void AddHealing(int e_ID, int heal)
        {
            Console.WriteLine($"  |Heals {heal}");
            Healers[e_ID] = heal;
        }
        /// <summary>
        /// Adds the Effect into the list of Damagers
        /// </summary>
        /// <param name="e_ID">Effect ID</param>
        /// <param name="damage">Damage Amount</param>
        void AddDamaging(int e_ID, int damage)
        {
            Console.WriteLine($"  |Damages {damage}");
            Damagers[e_ID] = damage;
        }


        /// <summary>
        /// Loads Effects from the DB
        /// </summary>
        public EFFECTS(ref IPLAYER_STATS player, ref INPC_STATS entities, List<Stats> listofstats)
        {
            Console.WriteLine("==Loading Effects==");
            LoadBuffDebuff();
            LoadHealDamage();

            //IDs.Sort();

            Player = player;
            Entities = entities;
            ListOfStats = listofstats;

            for (int i = 0; i < Entities.ID.Count + 1; i++)
            {
                ActiveEffects.Add(new List<int>());
                ActiveDurations.Add(new Dictionary<int, int>());
                ChangedStats.Add(new Dictionary<Stats, int>());
                OriginalStats.Add(new Dictionary<Stats, int>());
                NotApplied.Add(new List<int>());
                BuffDebuffApplied.Add(new List<int>());

                expired_effects.Add(new List<int>());

                foreach (Stats stat in ListOfStats)
                {
                    SaveStat(i, stat);
                }
            }
            Console.WriteLine("==Effects loaded==");
            Console.WriteLine($"Active effects count: {ActiveEffects.Count}");
            Console.WriteLine();
        }

        /// <summary>
        /// Loads Buffs and Debuffs from the DB
        /// </summary>
        void LoadBuffDebuff()
        {
            try
            {
                using (SQLiteConnection read_connection = new SQLiteConnection(CString))
                {
                    read_connection.Open();

                    //Load Buffers
                    Console.WriteLine("Loading Buffers");
                    string query = "SELECT * FROM Effects_data WHERE Type=\'Buff\'";
                    try
                    {
                        using (SQLiteCommand command = new SQLiteCommand(query, read_connection))
                        {
                            SQLiteDataReader reader = command.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    int new_ID = Convert.ToInt32(reader["ID"]);
                                    string name = Convert.ToString(reader["Name"]);
                                    IDs.Add(new_ID);
                                    Names.Add(name);
                                    ID_Name[new_ID] = name;
                                    Console.WriteLine($"-Bound \"{name}\" to e_ID{new_ID}");
                                    string stat = Convert.ToString(reader["Stat"]);
                                    switch (stat)
                                    {
                                        case "Strength":
                                            {
                                                AddBuffing(new_ID, Stats.Strength, Convert.ToInt32(reader["Change"]));
                                                break;
                                            }
                                        case "Agility":
                                            {
                                                AddBuffing(new_ID, Stats.Agility, Convert.ToInt32(reader["Change"]));
                                                break;
                                            }
                                        case "Intellect":
                                            {
                                                AddBuffing(new_ID, Stats.Intellect, Convert.ToInt32(reader["Change"]));
                                                break;
                                            }
                                    }
                                    Types[new_ID] = EffectTypes.Buff;
                                    LingeringEffects[new_ID] = Convert.ToInt32(reader["Duration"]);
                                }
                            }
                        }
                    }
                    catch (SQLiteException)
                    {
                        Console.WriteLine("Could not read Buffers!");
                        throw new SQLiteException("Could not read Buffers!");
                    }

                    //Load Debuffers
                    Console.WriteLine("Loading Debuffers");
                    query = "SELECT * FROM Effects_data WHERE Type=\'Debuff\'";
                    try
                    {
                        using (SQLiteCommand command = new SQLiteCommand(query, read_connection))
                        {
                            SQLiteDataReader reader = command.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    int new_ID = Convert.ToInt32(reader["ID"]);
                                    string name = Convert.ToString(reader["Name"]);
                                    IDs.Add(new_ID);
                                    Names.Add(name);
                                    ID_Name[new_ID] = name;
                                    Console.WriteLine($"-Bound \"{name}\" to e_ID{new_ID}");

                                    string stat = Convert.ToString(reader["Stat"]);
                                    switch (stat)
                                    {
                                        case "Strength":
                                            {
                                                AddDebuffing(new_ID, Stats.Strength, Convert.ToInt32(reader["Change"]));
                                                break;
                                            }
                                        case "Agility":
                                            {
                                                AddDebuffing(new_ID, Stats.Agility, Convert.ToInt32(reader["Change"]));
                                                break;
                                            }
                                        case "Intellect":
                                            {
                                                AddDebuffing(new_ID, Stats.Intellect, Convert.ToInt32(reader["Change"]));
                                                break;
                                            }
                                    }
                                    Types[new_ID] = EffectTypes.Debuff;
                                    LingeringEffects[new_ID] = Convert.ToInt32(reader["Duration"]);
                                    //BuffDebuffDurations[new_ID] = Convert.ToInt32(reader["Duration"]);
                                }
                            }
                        }
                    }
                    catch (SQLiteException)
                    {
                        Console.WriteLine("Could not read Debuffers!");
                        throw new SQLiteException("Could not read Debuffers!");
                    }
                }
            }
            catch (SQLiteException)
            {
                Console.WriteLine("Effects - BuffDebuff - Failed to open connection!");
                throw new SQLiteException("Effects - BuffDebuff - Failed to open connection!");
            }
        }

        /// <summary>
        /// Loads Heals and Damages from the DB
        /// </summary>
        void LoadHealDamage()
        {
            try
            {
                using (SQLiteConnection read_connection = new SQLiteConnection(CString))
                {
                    read_connection.Open();

                    //Load Healers (ADDS +CHANGE TO HEALERS)
                    Console.WriteLine("Loading Healers");
                    string query = "SELECT * FROM Effects_data WHERE Type=\'Heal\'";
                    try
                    {
                        using (SQLiteCommand command = new SQLiteCommand(query, read_connection))
                        {
                            SQLiteDataReader reader = command.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    int new_ID = Convert.ToInt32(reader["ID"]);
                                    string name = Convert.ToString(reader["Name"]);
                                    IDs.Add(new_ID);
                                    Names.Add(name);
                                    ID_Name[new_ID] = name;
                                    Console.WriteLine($"-Bound \"{name}\" to e_ID{new_ID}");

                                    AddHealing(new_ID, Convert.ToInt32(reader["Change"]));
                                    Types[new_ID] = EffectTypes.Heal;

                                    switch(Convert.ToString(reader["Behaviour"]))
                                    {
                                        case "Instant":
                                            {
                                                InstantEffects.Add(new_ID);
                                                break;
                                            }
                                        case "Lingering":
                                            {
                                                LingeringEffects[new_ID] = Convert.ToInt32(reader["Duration"]);
                                                break;
                                            }
                                    }
                                }
                            }
                        }
                    }
                    catch (SQLiteException)
                    {
                        Console.WriteLine("Could not read Healers!");
                        throw new SQLiteException("Could not read Healers!");
                    }

                    //Load Damagers (ADDS +CHANGE TO DAMAGERS)
                    Console.WriteLine("Loading Damagers");
                    query = "SELECT * FROM Effects_data WHERE Type=\'Damage\'";
                    try
                    {
                        using (SQLiteCommand command = new SQLiteCommand(query, read_connection))
                        {
                            SQLiteDataReader reader = command.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    int new_ID = Convert.ToInt32(reader["ID"]);
                                    string name = Convert.ToString(reader["Name"]);
                                    IDs.Add(new_ID);
                                    Names.Add(name);
                                    ID_Name[new_ID] = name;
                                    Console.WriteLine($"-Bound \"{name}\" to e_ID{new_ID}");

                                    AddDamaging(new_ID, Convert.ToInt32(reader["Change"]));
                                    Types[new_ID] = EffectTypes.Damage;

                                    switch (Convert.ToString(reader["Behaviour"]))
                                    {
                                        case "Instant":
                                            {
                                                InstantEffects.Add(new_ID);
                                                break;
                                            }
                                        case "Lingering":
                                            {
                                                LingeringEffects[new_ID] = Convert.ToInt32(reader["Duration"]);
                                                break;
                                            }
                                    }
                                }
                            }
                        }
                    }
                    catch (SQLiteException)
                    {
                        Console.WriteLine("Could not read Damagers!");
                        throw new SQLiteException("Could not read Damagers!");
                    }
                }
            }
            catch (SQLiteException)
            {
                Console.WriteLine("Effects - HealDamage - Failed to open connection!");
                throw new SQLiteException("Effects - HealDamage - Could not connect to the database!");
            }
        }


        /// <summary>
        /// Add Effect to Target lists
        /// </summary>
        /// <param name="source_ID">Effect Source</param>
        /// <param name="target_ID">Effect Target</param>
        /// <param name="effect_ID">Effect ID</param>
        public void CastEffect(int source_ID, int target_ID, int effect_ID)
        {
            Console.WriteLine($"!Cast e_ID{effect_ID} \"{ID_Name[effect_ID]}\" on EntityID{target_ID}");
            switch (Types[effect_ID])
            {
                case EffectTypes.Buff:
                    {
                        foreach (Stats stat in ListOfStats)
                        {
                            if (Buffers[effect_ID].ContainsKey(stat))
                            {
                                int change = Buffers[effect_ID][stat];
                                if (!ActiveDurations[target_ID].ContainsKey(effect_ID))
                                {
                                    ActiveEffects[target_ID].Add(effect_ID);
                                    ActiveDurations[target_ID][effect_ID] = LingeringEffects[effect_ID];
                                    Console.WriteLine($"-ID{target_ID} original {stat.ToString()}: {OriginalStats[target_ID][stat]}");
                                    Console.WriteLine($"--Active duration: {ActiveDurations[target_ID][effect_ID]}");
                                    ChangeStat(target_ID, stat, change);

                                    Console.WriteLine($"--{stat.ToString()} buff: {ChangedStats[target_ID][stat]}");

                                    NotApplied[target_ID].Add(effect_ID);
                                    if (!NeedUpdate.Contains(target_ID))
                                    {
                                        NeedUpdate.Add(target_ID);
                                    }
                                }
                                else
                                {
                                    ActiveDurations[target_ID][effect_ID] += LingeringEffects[effect_ID];
                                }
                            }
                        }
                        break;
                    }
                case EffectTypes.Debuff:
                    {
                        foreach (Stats stat in ListOfStats)
                        {
                            if (Debuffers[effect_ID].ContainsKey(stat))
                            {
                                int change = - Debuffers[effect_ID][stat];
                                if (!ActiveDurations[target_ID].ContainsKey(effect_ID))
                                {
                                    ActiveEffects[target_ID].Add(effect_ID);
                                    ActiveDurations[target_ID][effect_ID] = LingeringEffects[effect_ID];
                                    Console.WriteLine($"-ID{target_ID} original {stat.ToString()}: {OriginalStats[target_ID][stat]}");
                                    Console.WriteLine($"--Active duration: {ActiveDurations[target_ID][effect_ID]}");
                                    ChangeStat(target_ID, stat, change);

                                    Console.WriteLine($"--{stat.ToString()} debuff: {ChangedStats[target_ID][stat]}");

                                    NotApplied[target_ID].Add(effect_ID);
                                    if (!NeedUpdate.Contains(target_ID))
                                    {
                                        NeedUpdate.Add(target_ID);
                                    }
                                }
                                else
                                {
                                    ActiveDurations[target_ID][effect_ID] += LingeringEffects[effect_ID];
                                }
                            }
                        }
                        break;
                    }
                case EffectTypes.Heal:
                    {
                        if (LingeringEffects.ContainsKey(effect_ID))
                        {
                            if (!ActiveDurations[target_ID].ContainsKey(effect_ID))
                            {
                                ActiveEffects[target_ID].Add(effect_ID);
                                ActiveDurations[target_ID][effect_ID] = LingeringEffects[effect_ID];

                                NotApplied[target_ID].Add(effect_ID);

                                if (!NeedUpdate.Contains(target_ID))
                                {
                                    NeedUpdate.Add(target_ID);
                                }
                            }
                            else
                            {
                                ActiveDurations[target_ID][effect_ID] += LingeringEffects[effect_ID];
                            }
                            Console.WriteLine($"--Effect Duration: {ActiveDurations[target_ID][effect_ID]}");

                            if (!ActiveHealthChanges.ContainsKey(target_ID))
                            {
                                ActiveHealthChanges[target_ID] = Healers[effect_ID];
                                BuffDebuffApplied[target_ID].Add(effect_ID);
                            }
                            else
                            {
                                if (!BuffDebuffApplied[target_ID].Contains(effect_ID))
                                {
                                    ActiveHealthChanges[target_ID] += Healers[effect_ID];
                                    BuffDebuffApplied[target_ID].Add(effect_ID);
                                }
                            }
                            Console.WriteLine($"--Health Change on {target_ID} is {ActiveHealthChanges[target_ID]}");
                        }
                        else
                        {
                            ApplyInstant(source_ID, target_ID, effect_ID);
                        }
                        break;
                    }
                case EffectTypes.Damage:
                    {
                        if (LingeringEffects.ContainsKey(effect_ID))
                        {
                            if (!ActiveDurations[target_ID].ContainsKey(effect_ID))
                            {
                                ActiveEffects[target_ID].Add(effect_ID);
                                ActiveDurations[target_ID][effect_ID] = LingeringEffects[effect_ID];

                                NotApplied[target_ID].Add(effect_ID);

                                if (!NeedUpdate.Contains(target_ID))
                                {
                                    NeedUpdate.Add(target_ID);
                                }
                            }
                            else
                            {
                                ActiveDurations[target_ID][effect_ID] += LingeringEffects[effect_ID];
                            }
                            Console.WriteLine($"--Effect Duration: {ActiveDurations[target_ID][effect_ID]}");

                            if (!ActiveHealthChanges.ContainsKey(target_ID))
                            {
                                ActiveHealthChanges[target_ID] = -Damagers[effect_ID];
                                BuffDebuffApplied[target_ID].Add(effect_ID);
                            }
                            else
                            {
                                if (!BuffDebuffApplied[target_ID].Contains(effect_ID))
                                {
                                    ActiveHealthChanges[target_ID] -= Damagers[effect_ID];
                                    BuffDebuffApplied[target_ID].Add(effect_ID);
                                }
                            }
                            Console.WriteLine($"--Health Change on {target_ID} is {ActiveHealthChanges[target_ID]}");
                        }
                        else
                        {
                            ApplyInstant(source_ID, target_ID, effect_ID);
                        }
                        break;
                    }
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Saves the original Stat value of the Target
        /// </summary>
        /// <param name="t_ID">Target ID</param>
        /// <param name="stat">Stat to save</param>
        void SaveStat(int t_ID, Stats stat)
        {
            switch (t_ID)
            {
                case 0:
                    {
                        switch (stat)
                        {
                            case Stats.Strength:
                                {
                                    OriginalStats[t_ID][Stats.Strength] = Player.Strength;
                                    break;
                                }
                            case Stats.Agility:
                                {
                                    OriginalStats[t_ID][Stats.Agility] = Player.Agility;
                                    break;
                                }
                            case Stats.Intellect:
                                {
                                    OriginalStats[t_ID][Stats.Intellect] = Player.Intellect;
                                    break;
                                }
                            case Stats.Charisma:
                                {
                                    OriginalStats[t_ID][Stats.Charisma] = Player.Charisma;
                                    break;
                                }
                        }

                        break;
                    }

                default:
                    {
                        int index = Entities.ID.IndexOf(t_ID);

                        switch (stat)
                        {
                            case Stats.Strength:
                                {
                                    OriginalStats[t_ID][Stats.Strength] = Entities.Strength[index];
                                    break;
                                }
                            case Stats.Agility:
                                {
                                    OriginalStats[t_ID][Stats.Agility] = Entities.Agility[index];
                                    break;
                                }
                            case Stats.Intellect:
                                {
                                    OriginalStats[t_ID][Stats.Intellect] = Entities.Intellect[index];
                                    break;
                                }
                            //case Stats.Charisma:
                            //    {
                            //        OriginalStats[t_ID][Stats.Charisma] = Entities.Charisma[index];
                            //        break;
                            //    }
                        }

                        break;
                    }
            }
        }


        void ChangeHealth(int target_ID, int amount)
        {
            int source_ID = -1;
            if (target_ID == 0)
            {
                Player.ChangeHealth(source_ID, amount);
                Console.WriteLine($"--Player Health: {Player.Health}");
            }
            else
            {
                if (target_ID > 0)
                {
                    Entities.ChangeHealth(source_ID, target_ID, amount);
                    Console.WriteLine($"--Entity {target_ID} Health: {Entities.Health[Entities.ID.IndexOf(target_ID)]}");
                }
                else
                    throw new ArgumentException("Target ID is not valid.");
            }
            Console.WriteLine();
        }
        /// <summary>
        /// Changes the Stat by Amount
        /// </summary>
        /// <param name="t_ID">Target ID</param>
        /// <param name="stat">Stat to change</param>
        /// <param name="amount">Change Amount</param>
        void ChangeStat(int t_ID, Stats stat, int amount)
        {
            Console.WriteLine($"-Changing {stat.ToString()} on ID{t_ID} by {amount}");
            //if Changed Stat does not exist
            if (!ChangedStats[t_ID].ContainsKey(stat))
            {
                //save amount of change
                ChangedStats[t_ID][stat] = amount;
            }
            //of exists
            else
            {
                //add to change
                ChangedStats[t_ID][stat] += amount;
            }

            //if Target is Player
            if (t_ID == 0)
            {
                Player.SetStat(stat, OriginalStats[0][stat] + ChangedStats[0][stat]);
            }
            //if Target is Entity
            else
            {
                //valid ID check
                if (t_ID > 0)
                {
                    Entities.SetStat(t_ID, stat, OriginalStats[t_ID][stat] + ChangedStats[t_ID][stat]);
                }
                else
                    throw new ArgumentException("Target ID is not valid.");
            }
            //Console.WriteLine();
        }

        /// <summary>
        /// Resets the Stat of the Target
        /// </summary>
        /// <param name="target_ID">Target ID</param>
        /// <param name="stat">Stat to reset</param>
        void ResetStat(int target_ID, Stats stat)
        {
            if (target_ID == 0)
            {
                Player.SetStat(stat, OriginalStats[0][stat]);
            }
            else
            {
                if (target_ID > 0)
                {
                    Entities.SetStat(target_ID, stat, OriginalStats[target_ID][stat]);
                }
                else
                    throw new ArgumentException("Target ID is not valid.");
            }
        }

        /// <summary>
        /// Applies all instant effects on Target
        /// <para>
        /// <event cref="onEffectHPChange">Triggered when Effect changes HP</event>
        /// </para>
        /// </summary>
        /// <param name="source_ID">Source ID</param>
        /// <param name="target_ID">Target ID</param>
        /// <param name="effect_ID">Effect ID</param>
        void ApplyInstant(int source_ID, int target_ID, int effect_ID)
        {
            if (Types[effect_ID] == EffectTypes.Heal)
            {
                Console.WriteLine($"-Healing Entity{target_ID} by {Healers[effect_ID]}");
                ChangeHealth(target_ID, Healers[effect_ID]);
                onEffectHPChange?.Invoke(effect_ID, source_ID, target_ID, Healers[effect_ID]);
            }
            if (Types[effect_ID] == EffectTypes.Damage)
            {
                Console.WriteLine($"-Damaging Entity{target_ID} by {Damagers[effect_ID]}");
                ChangeHealth(target_ID, -Damagers[effect_ID]);
                onEffectHPChange?.Invoke(effect_ID, source_ID, target_ID, -Damagers[effect_ID]);
            }
            Console.WriteLine();
        }
        
        /// <summary>
        /// Ticks the Active Effects
        /// </summary>
        public void TickEffects()
        {
            Console.WriteLine($"Entities need update: {NeedUpdate.Count}");

            foreach (int target_ID in NeedUpdate)
            {
                Console.WriteLine($"-Entity {target_ID}");
                Console.WriteLine($"--Effects need update: {NotApplied[target_ID].Count}");

                foreach (int effect_ID in NotApplied[target_ID])
                {
                    ActiveDurations[target_ID][effect_ID] -= 1;

                    Console.WriteLine($"   |{Names[IDs.IndexOf(effect_ID)]}: Duration {ActiveDurations[target_ID][effect_ID]}");
                    //if Effect ends
                    if (ActiveDurations[target_ID][effect_ID] < 0)
                    {
                        Console.WriteLine("---Effect expired, removing");
                        switch (Types[effect_ID])
                        {
                            //remove Buff
                            case EffectTypes.Buff:
                                {
                                    Stats stat = Buffers[effect_ID].Keys.Last();
                                    ChangedStats[target_ID][stat] -= Buffers[effect_ID][stat];
                                    ResetStat(target_ID, stat);
                                    BuffDebuffApplied[target_ID].Remove(effect_ID);
                                    break;
                                }
                            //remove Debuff
                            case EffectTypes.Debuff:
                                {
                                    Stats stat = Debuffers[effect_ID].Keys.Last();
                                    ChangedStats[target_ID][stat] += Debuffers[effect_ID][stat];
                                    ResetStat(target_ID, stat);
                                    BuffDebuffApplied[target_ID].Remove(effect_ID);
                                    break;
                                }
                            //remove Heal
                            case EffectTypes.Heal:
                                {
                                    Console.WriteLine($"---Removing {Healers[effect_ID]} Heal/turn from Entity{target_ID}");
                                    ActiveHealthChanges[target_ID] -= Healers[effect_ID];
                                    Console.WriteLine($"---Active Health change on Entity{target_ID} is {ActiveHealthChanges[target_ID]}");
                                    BuffDebuffApplied[target_ID].Remove(effect_ID);
                                    break;
                                }
                            //remove Damage
                            case EffectTypes.Damage:
                                {
                                    Console.WriteLine($"---Removing {Damagers[effect_ID]} Damage/turn from Entity{target_ID}");
                                    ActiveHealthChanges[target_ID] += Damagers[effect_ID];
                                    Console.WriteLine($"---Active Health change on Entity{target_ID} is {ActiveHealthChanges[target_ID]}");
                                    BuffDebuffApplied[target_ID].Remove(effect_ID);
                                    break;
                                }
                            default:
                                throw new NotImplementedException("Tried to apply non-existent type of Effect.");
                        }
                        if (!expired_entities.Contains(target_ID))
                        {
                            //add Entity Id to the List for cleaning
                            expired_entities.Add(target_ID);
                        }
                        //add Effect to the list for cleaning, parallel to expired_entities
                        expired_effects[target_ID].Add(effect_ID);
                        //remove Effect ID from Active
                        ActiveEffects[target_ID].Remove(effect_ID);
                        //remove from Active Durations
                        ActiveDurations[target_ID].Remove(effect_ID);
                    }
                    //if Effect persisits
                    else
                    {

                        if (BuffDebuffApplied[target_ID].Contains(effect_ID))
                        {
                            switch (Types[effect_ID])
                            {
                                case EffectTypes.Buff:
                                    {
                                        Stats stat = Buffers[effect_ID].Keys.Last();
                                        Console.WriteLine($"--{stat.ToString()} buff: {ChangedStats[target_ID][stat]}");
                                        break;
                                    }
                                case EffectTypes.Debuff:
                                    {
                                        Stats stat = Debuffers[effect_ID].Keys.Last();
                                        Console.WriteLine($"--{stat.ToString()} debuff: {ChangedStats[target_ID][stat]}");
                                        break;
                                    }
                                case EffectTypes.Heal:
                                    {
                                        break;
                                    }
                                case EffectTypes.Damage:
                                    {
                                        break;
                                    }
                                default:
                                    throw new NotImplementedException("Tried to apply non-existent type of Effect.");
                            }
                        }
                    }
                }
                if (ActiveHealthChanges.ContainsKey(target_ID))
                {
                    Console.WriteLine();
                    Console.WriteLine($"-Active Health Change: {ActiveHealthChanges[target_ID]}");
                    ChangeHealth(target_ID, ActiveHealthChanges[target_ID]);
                }
            }

            //cleaning Entities with expired Effects
            foreach (int entity in expired_entities)
            {
                //remove each Effect from NotApplied
                foreach (int effect_ID in expired_effects[entity])
                {
                    Console.WriteLine($"!Effect ID{effect_ID} expired on EntityID{entity}");
                    NotApplied[entity].Remove(effect_ID);
                }
                //wipe list for cleaning (effects)
                expired_effects[entity].Clear();
                //if there are no active effects, remove Entity from NeedUpdate
                if (ActiveDurations[entity].Count == 0)
                {
                    NeedUpdate.Remove(entity);
                }
            }
            //wipe list for cleaning (entities)
            expired_entities.Clear();
            Console.WriteLine();
        }

        /// <summary>
        /// Active Effects on Entities
        /// </summary>
        public List<List<int>> ActiveEffects { get; private set; } = new List<List<int>>();
        /// <summary>
        /// Durations of the Active Effects
        /// </summary>
        public List<Dictionary<int, int>> ActiveDurations { get; private set; } = new List<Dictionary<int, int>>();
        //public List<List<int>> ActiveDurations { get; private set; } = new List<List<int>>();
        /// <summary>
        /// Health Change per turn for Entities
        /// </summary>
        Dictionary<int, int> ActiveHealthChanges { get; set; } = new Dictionary<int, int>();

        /// <summary>
        /// List of changes to Stats
        /// </summary>
        public List<Dictionary<Stats, int>> ChangedStats { get; private set; } = new List<Dictionary<Stats, int>>();
        /// <summary>
        /// Original Stat values of all Entities
        /// </summary>
        public List<Dictionary<Stats, int>> OriginalStats { get; private set; } = new List<Dictionary<Stats, int>>();
        /// <summary>
        /// List of Target IDs in need of update
        /// </summary>
        List<int> NeedUpdate { get; set; } = new List<int>();
        /// <summary>
        /// Effects that were not yet applied for each Target
        /// </summary>
        List<List<int>> NotApplied { get; set; } = new List<List<int>>();

        /// <summary>
        /// List of Instant Effects
        /// </summary>
        public List<int> InstantEffects { get; private set; } = new List<int>();
        /// <summary>
        /// List of Lingering Effects
        /// </summary>
        public Dictionary<int, int> LingeringEffects { get; private set; } = new Dictionary<int, int>();
        /// <summary>
        /// Pairs of ID-StatAndChange for Buff Effects
        /// </summary>
        public Dictionary<int, Dictionary<Stats, int>> Buffers { get; private set; } = new Dictionary<int, Dictionary<Stats, int>>();
        /// <summary>
        /// Pairs of ID-StatAndChange for Debuff Effects
        /// </summary>
        public Dictionary<int, Dictionary<Stats, int>> Debuffers { get; private set; } = new Dictionary<int, Dictionary<Stats, int>>();
        /// <summary>
        /// Pairs of ID-Change for Heal Effects
        /// </summary>
        public Dictionary<int, int> Healers { get; private set; } = new Dictionary<int, int>();
        /// <summary>
        /// Pairs of ID-Change for Damage Effects
        /// </summary>
        public Dictionary<int, int> Damagers { get; private set; } = new Dictionary<int, int>();

        /// <summary>
        /// List of Effect IDs
        /// </summary>
        public List<int> IDs { get; private set; } = new List<int>();
        /// <summary>
        /// List of all Effect Types 
        /// </summary>
        public Dictionary<int, EffectTypes> Types { get; private set; } = new Dictionary<int, EffectTypes>();
        /// <summary>
        /// Pairs of ID-Name for all Effects
        /// </summary>
        public List<string> Names { get; private set; } = new List<string>();
        /// <summary>
        /// Links ID with Name
        /// </summary>
        public Dictionary<int, string> ID_Name { get; private set; } = new Dictionary<int, string>();
        /// <summary>
        /// Pairs of ID-Duration for all Effects
        /// </summary>
        public Dictionary<int, int> BuffDebuffDurations { get; private set; } = new Dictionary<int, int>();

        List<List<int>> BuffDebuffApplied = new List<List<int>>();

        IPLAYER_STATS Player;
        INPC_STATS Entities;

        List<Stats> ListOfStats;

        /// <summary>
        /// List of Entities with expired Effects
        /// </summary>
        List<int> expired_entities = new List<int>();
        /// <summary>
        /// List of expired Effects on Entities
        /// </summary>
        List<List<int>> expired_effects = new List<List<int>>();
    }
}
