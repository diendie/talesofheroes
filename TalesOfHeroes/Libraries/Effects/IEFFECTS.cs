﻿//using LibNPC_FACTORY;
using System.Collections.Generic;

namespace TalesOfHeroes
{
    public delegate void OnEffectHPChange(int effect_ID, int source_ID, int target_ID, int amount);

    /// <summary>
    /// Effect Behaviour 
    /// </summary>
    public enum EffectBehaviour
    { 
        /// <summary>
        /// Instant Effect
        /// </summary>
        Instant = 0,
        /// <summary>
        /// Lingering Effect
        /// </summary>
        Lingering = 1
    }


    /// <summary>
    /// All available Effect types
    /// </summary>
    public enum EffectTypes
    {
        /// <summary>
        /// Buff
        /// </summary>
        Buff = 0,
        /// <summary>
        /// Debuff
        /// </summary>
        Debuff = 1,
        /// <summary>
        /// Heal
        /// </summary>
        Heal = 2,
        /// <summary>
        /// Damage
        /// </summary>
        Damage = 3
    }

    /// <summary>
    /// Loads and stores all Effect data
    /// </summary>
    public interface IEFFECTS
    {
        /// <summary>
        /// Triggers when Effect changes HP
        /// </summary>
        event OnEffectHPChange onEffectHPChange;

        /// <summary>
        /// Durations of the Active Effects
        /// </summary>
        List<Dictionary<int, int>> ActiveDurations { get; }
        /// <summary>
        /// Active Effects on Entities
        /// </summary>
        List<List<int>> ActiveEffects { get; }
        /// <summary>
        /// Pairs of ID-StatAndChange for Buff Effects
        /// </summary>
        Dictionary<int, Dictionary<Stats, int>> Buffers { get; }
        /// <summary>
        /// List of changes to Stats
        /// </summary>
        List<Dictionary<Stats, int>> ChangedStats { get; }
        /// <summary>
        /// Pairs of ID-Change for Damage Effects
        /// </summary>
        Dictionary<int, int> Damagers { get; }
        /// <summary>
        /// Pairs of ID-StatAndChange for Debuff Effects
        /// </summary>
        Dictionary<int, Dictionary<Stats, int>> Debuffers { get; }
        /// <summary>
        /// Pairs of ID-Change for Heal Effects
        /// </summary>
        Dictionary<int, int> Healers { get; }
        /// <summary>
        /// List of Effect IDs
        /// </summary>
        List<int> IDs { get; }
        /// <summary>
        /// Links Name with ID
        /// </summary>
        Dictionary<int, string> ID_Name { get; }
        /// <summary>
        /// List of Instant Effects
        /// </summary>
        List<int> InstantEffects { get; }
        /// <summary>
        /// List of Lingering Effects
        /// </summary>
        Dictionary<int, int> LingeringEffects { get; }
        /// <summary>
        /// Pairs of ID-Name for all Effects
        /// </summary>
        List<string> Names { get; }
        /// <summary>
        /// Original Stat values of all Entities
        /// </summary>
        List<Dictionary<Stats, int>> OriginalStats { get; }
        /// <summary>
        /// List of all Effect Types 
        /// </summary>
        Dictionary<int, EffectTypes> Types { get; }


        /// <summary>
        /// Add Effect to Target lists
        /// </summary>
        /// <param name="source_ID">Effect Source</param>
        /// <param name="target_ID">Effect Target</param>
        /// <param name="effect_ID">Effect ID</param>
        void CastEffect(int source_ID, int target_ID, int effect_ID);
        /// <summary>
        /// Ticks the Active Effects
        /// </summary>
        void TickEffects();
    }
}