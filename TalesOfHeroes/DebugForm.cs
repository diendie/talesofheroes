﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
//using LibNPC_FACTORY;
//using LibNPC_STATS;
//using LibPLAYER_STATS;
//using LibEFFECTS;

namespace TalesOfHeroes
{
    public partial class DebugForm : Form
    {
        readonly IPLAYER_STATS Player;
        readonly INPC_FACTORY Factory;
        readonly INPC_STATS Entities;
        //readonly IEFFECTS Effects;

        /// <summary>
        /// Target ID
        /// </summary>
        int target { get; set; } = -1;
        /// <summary>
        /// Binds an ID to the Button (Button-ID)
        /// </summary>
        Dictionary<int, int> grouping_selections = new Dictionary<int, int>();


        public DebugForm()
        {
            InitializeComponent();
            //Effects = new EFFECTS();
            Player = new PLAYER_STATS();
            Factory = new NPC_FACTORY();
            Entities = new NPC_STATS();

            UpdateEntTypes();
            EntitiesUpdate();
        }


        /// <summary>
        /// Creates a test Player
        /// </summary>
        void CreatePlayer()
        {
            string name = tb_playerName.Text;
            int str = Convert.ToInt32(l_str.Text);
            int agi = Convert.ToInt32(l_agi.Text);
            int intel = Convert.ToInt32(l_int.Text);

            //Player.AddPlayer(name, str, agi, intel);
        }
        /// <summary>
        /// Creates an Entity
        /// </summary>
        /// <param name="entity"></param>
        void CreateEntity(NPC_Type entity)
        {
            Console.WriteLine($"Creating {entity.ToString()}");
            Factory.LoadEntity(entity);

            int player_level = Player.Level;

            //Entities.AddEntity(Factory.Stats, Factory.Traits, Player.Level);
        }


        ///Update functions
        ///=========================================================
        void UpdateHealth()
        {
            l_health.Text = $"Health: {Player.Health}/{Player.MaxHealth}";
        }
        /// <summary>
        /// Updates Player Exp
        /// </summary>
        void UpdateExp()
        {
            l_level.Text = $"Level {Player.Level.ToString()}";
            l_exp_cur.Text = $"Exp: {Player.CurrentExp.ToString()}";
            l_exp_rem.Text = $"To lvl: {Player.RemainingExp.ToString()}";
        }
        /// <summary>
        /// Updates list of entities in cb_ent_type
        /// </summary>
        void UpdateEntTypes()
        {
            cb_ent_type.Items.Clear();

            foreach (string entity in Enum.GetNames(typeof(NPC_Type)))
            {
                cb_ent_type.Items.Add(entity);
            }
        }
        /// <summary>
        /// Updates Alive table
        /// </summary>
        void UpdateAlive()
        {
            dgw_alive.Rows.Clear();

            for (int i = 0; i < Entities.Alive.Count; i++)
            {
                int ID = Entities.Alive[i];
                int index = Entities.ID.IndexOf(ID);

                string[] row = { ID.ToString(), Entities.Behaviour[index][0], Entities.Health[index].ToString(), Entities.Behaviour[index][1], Entities.Strength[index].ToString(), Entities.Agility[index].ToString(), Entities.Intellect[index].ToString() };

                dgw_alive.Rows.Add(row);
            }
        }
        /// <summary>
        /// Updates Dead table
        /// </summary>
        void UpdateDead()
        {
            dgw_dead.Rows.Clear();

            for (int i = 0; i < Entities.Dead.Count; i++)
            {
                int ID = Entities.Dead[i];
                int index = Entities.ID.IndexOf(ID);

                string[] row = { ID.ToString(), Entities.Behaviour[index][0], Entities.Health[index].ToString(), Entities.Behaviour[index][1], Entities.Strength[index].ToString(), Entities.Agility[index].ToString(), Entities.Intellect[index].ToString() };

                dgw_dead.Rows.Add(row);
            }
        }
        /// <summary>
        /// Updates labels and positions for target all buttons
        /// </summary>
        void UpdateButtons()
        {
            //Filling the target buttons with first 3 elements of the Entities.Hostiles[]
            try
            {
                //Resets all positions
                l_grouping_selected.Text = "Selected ID: none";
                b_grouping1.Text = "";
                b_grouping1.Visible = false;
                b_grouping2.Text = "";
                b_grouping2.Visible = false;
                b_grouping3.Text = "";
                b_grouping3.Visible = false;
                b_grouping4.Text = "";
                b_grouping4.Visible = false;
                b_grouping5.Text = "";
                b_grouping5.Visible = false;

                //Resets button-ID bindings
                grouping_selections.Clear();

                //Positions the enemy according to the number of enemies
                switch (Entities.Hostiles.Count)
                {
                    case 1:
                        {
                            int index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                            b_grouping3.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping3.Visible = true;
                            grouping_selections[3] = Entities.ID[index];

                            break;
                        }

                    case 2:
                        {
                            int index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                            b_grouping1.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping1.Visible = true;
                            grouping_selections[1] = Entities.ID[index];
                            

                            index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                            b_grouping4.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping4.Visible = true;

                            grouping_selections[4] = Entities.ID[index];

                            break;
                        }

                    case 3:
                        {
                            int index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                            b_grouping3.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping3.Visible = true;
                            grouping_selections[3] = Entities.ID[index];

                            index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                            b_grouping1.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping1.Visible = true;
                            grouping_selections[1] = Entities.ID[index];

                            index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                            b_grouping4.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping4.Visible = true;
                            grouping_selections[4] = Entities.ID[index];

                            break;
                        }

                    case 4:
                        {
                            int index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                            b_grouping1.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping1.Visible = true;
                            grouping_selections[1] = Entities.ID[index];

                            index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                            b_grouping2.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping2.Visible = true;
                            grouping_selections[2] = Entities.ID[index];

                            index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                            b_grouping4.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping4.Visible = true;
                            grouping_selections[4] = Entities.ID[index];

                            index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                            b_grouping5.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping5.Visible = true;
                            grouping_selections[5] = Entities.ID[index];

                            break;
                        }

                    default:
                        {
                            int index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                            b_grouping1.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping1.Visible = true;
                            grouping_selections[1] = Entities.ID[index];

                            index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                            b_grouping2.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping2.Visible = true;
                            grouping_selections[2] = Entities.ID[index];

                            index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                            b_grouping3.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping3.Visible = true;
                            grouping_selections[3] = Entities.ID[index];

                            index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                            b_grouping4.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping4.Visible = true;
                            grouping_selections[4] = Entities.ID[index];

                            index = Entities.ID.IndexOf(Entities.Hostiles[4]);
                            b_grouping5.Text = Entities.Behaviour[index][0].ToString();
                            b_grouping5.Visible = true;
                            grouping_selections[5] = Entities.ID[index];

                            break;
                        }
                }

            }
            //If none exist
            catch (Exception)
            { }
        }

        /// <summary>
        /// UpdateAlive, Dead, Buttons 
        /// </summary>
        void EntitiesUpdate()
        {
            UpdateAlive();
            UpdateDead();
            UpdateButtons();
        }
        //=========================================================

        //Player creation controls 
        //=========================================================
        /// <summary>
        /// Player creation
        /// </summary>
        private void b_create_Click(object sender, EventArgs e)
        {
            if (tb_playerName.Text == "")
            {
                MessageBox.Show("Enter Player name!");
            }
            else
            {
                if ((l_str.Text == "0")|| (l_agi.Text == "0") || (l_int.Text == "0"))
                {
                    MessageBox.Show("Set all stats!");
                }
                else
                {
                    CreatePlayer();
                    UpdateHealth();
                    l_health.Visible = true;

                    tb_playerName.Enabled = false;
                    b_strdwn.Enabled = false;
                    b_strup.Enabled = false;
                    b_agidwn.Enabled = false;
                    b_agiup.Enabled = false;
                    b_intdwn.Enabled = false;
                    b_intup.Enabled = false;
                    b_create.Enabled = false;

                    UpdateExp();
                    l_level.Visible = true;
                    l_exp_cur.Visible = true;
                    l_exp_rem.Visible = true;
                }
            }
        }
        /// <summary>
        /// Exp addition
        /// </summary>
        private void b_addExp_Click(object sender, EventArgs e)
        {
            Player.AddExp(20);
            UpdateExp();
        }
        //=========================================================


        //Stat buttons
        //=========================================================
        private void b_strdwn_Click(object sender, EventArgs e)
        {
            int temp = Convert.ToInt32(l_str.Text) - 1;
            if (temp <= 0)
            {
                b_strdwn.Enabled = false;
            }
            
            l_str.Text = temp.ToString();
        }

        private void b_strup_Click(object sender, EventArgs e)
        {
            l_str.Text = (Convert.ToInt32(l_str.Text) + 1).ToString();
            b_strdwn.Enabled = true;
        }

        private void b_agidwn_Click(object sender, EventArgs e)
        {
            int temp = Convert.ToInt32(l_agi.Text) - 1;
            if (temp <= 0)
            {
                b_agidwn.Enabled = false;
            }
            
            l_agi.Text = temp.ToString();
        }

        private void b_agiup_Click(object sender, EventArgs e)
        {
            l_agi.Text = (Convert.ToInt32(l_agi.Text) + 1).ToString();
            b_agidwn.Enabled = true;
        }

        private void b_intdwn_Click(object sender, EventArgs e)
        {
            int temp = Convert.ToInt32(l_int.Text) - 1;
            if (temp <= 0)
            {
                b_intdwn.Enabled = false;
            }

            l_int.Text = temp.ToString();
        }

        private void b_intup_Click(object sender, EventArgs e)
        {
            l_int.Text = (Convert.ToInt32(l_int.Text) + 1).ToString();
            b_intdwn.Enabled = true;
        }
        //=========================================================


        //Entities controls
        //=========================================================
        /// <summary>
        /// Spawning entities
        /// </summary>
        private void b_ent_spawn_Click(object sender, EventArgs e)
        {
            if ((cb_ent_type.Text == ""))
            { }
            else
            {
                string selected = cb_ent_type.Text;
                NPC_Type entity = (NPC_Type)Enum.Parse(typeof(NPC_Type), selected);

                int amount;

                if (tb_ent_amount.Text == "")
                {
                    amount = 1;
                }
                else
                {
                    amount = Convert.ToInt32(tb_ent_amount.Text);
                }

                for (int i = 0; i < amount; i++)
                {
                    CreateEntity(entity);
                }

                EntitiesUpdate();
                tb_ent_amount.Text = "";
            }
        }

        /// <summary>
        /// Killing targeted ID
        /// </summary>
        private void b_ent_kill_Click(object sender, EventArgs e)
        {
            if (tb_kill_id.Text == "")
            { }
            else 
            {
                try
                {
                    int id = Convert.ToInt32(tb_kill_id.Text);
                    Entities.KillEntity(id, 0);

                    EntitiesUpdate();

                    tb_kill_id.Text = "";
                }
                catch (Exception)
                {
                    MessageBox.Show($"Could not kill this entity ({Convert.ToInt32((tb_kill_id.Text))})", "Kill Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        //=========================================================


        //Damage
        //=========================================================
        /// <summary>
        /// Inflict damage on Entity
        /// </summary>
        private void b_damage_inflict_Click(object sender, EventArgs e)
        {
            if ((tb_damage_id.Text == "") || (tb_damage_id.Text == ""))
            { }
            else
            {
                try
                {
                    switch (rb_damage.Checked)
                    {
                        case true:
                            {
                                int entity_ID = Convert.ToInt32((tb_damage_id.Text));
                                int amount = Convert.ToInt32(tb_damage_amount.Text);

                                //label1.Text = Entities.Alive.IndexOf(entity_ID).ToString();

                                Entities.ChangeHealth(0, entity_ID, amount);

                                break;
                            }
                        case false:
                            {
                                int entity_ID = Convert.ToInt32((tb_damage_id.Text));
                                int amount = Convert.ToInt32(tb_damage_amount.Text);

                                //label1.Text = Entities.Alive.IndexOf(entity_ID).ToString();

                                Entities.ChangeHealth(0, entity_ID, -amount);

                                break;
                            }
                    }

                    EntitiesUpdate();

                    //tb_damage_id.Text = "";
                    //tb_damage_amount.Text = "";
                    //rb_damage.Checked = true;
                    //rb_heal.Checked = false;
                }
                catch (Exception)
                {
                    MessageBox.Show($"Could not damage this entity ({Convert.ToInt32((tb_damage_id.Text))})", "Damage Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Inflict damage on Player
        /// </summary>
        private void b_inflict_player_Click(object sender, EventArgs e)
        {
            if (tb_damage_player.Text == "")
            { }
            else
            {
                try
                {
                    int amount = Convert.ToInt32(tb_damage_player.Text);

                    switch (rb_player_damage.Checked)
                    {
                        case true:
                            {
                                if (!Player.ChangeHealth(0, amount))
                                {
                                    UpdateHealth();
                                    MessageBox.Show("You Died");
                                    Form.ActiveForm.Close();
                                }
                                else
                                {
                                    UpdateHealth();
                                }

                                break;
                            }
                        case false:
                            {
                                Player.ChangeHealth(0, -amount);
                                UpdateHealth();
                                break;
                            }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Can't touch this", "Player damage Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        //=========================================================


        //Grouping Test
        //=====================================================================
        /// <summary>
        /// Clears the highlights
        /// </summary>
        private void RemoveSelection()
        {
            b_grouping1.BackColor = System.Drawing.Color.Transparent;
            b_grouping2.BackColor = System.Drawing.Color.Transparent;
            b_grouping3.BackColor = System.Drawing.Color.Transparent;
            b_grouping4.BackColor = System.Drawing.Color.Transparent;
            b_grouping5.BackColor = System.Drawing.Color.Transparent;
        }

        /// <summary>
        /// Select target in First button
        /// </summary>
        private void b_grouping1_Click(object sender, EventArgs e)
        {
            //check whether the button is occupied
            if (b_grouping1.Text == "")
            { }
            else
            {
                //clear current selection
                RemoveSelection();

                //select ID bound to this button as target
                target = grouping_selections[1];
                //update Selected ID label
                l_grouping_selected.Text = $"Selected ID: {target.ToString()}";

                //paint the selected button red
                b_grouping1.BackColor = System.Drawing.Color.Red;
            }
        }

        /// <summary>
        /// Select target in Second button
        /// </summary>
        private void b_grouping2_Click(object sender, EventArgs e)
        {
            if (b_grouping2.Text == "")
            { }
            else
            {
                RemoveSelection();

                target = grouping_selections[2];
                l_grouping_selected.Text = $"Selected ID: {target.ToString()}";

                b_grouping2.BackColor = System.Drawing.Color.Red;
            }
        }

        /// <summary>
        /// Select target in Third button
        /// </summary>
        private void b_grouping3_Click(object sender, EventArgs e)
        {
            if (b_grouping3.Text == "")
            { }
            else
            {
                RemoveSelection();

                target = grouping_selections[3];
                l_grouping_selected.Text = $"Selected ID: {target.ToString()}";

                b_grouping3.BackColor = System.Drawing.Color.Red;
            }
        }

        /// <summary>
        /// Select target in Fourth button
        /// </summary>
        private void b_grouping4_Click(object sender, EventArgs e)
        {
            if (b_grouping4.Text == "")
            { }
            else
            {
                RemoveSelection();

                target = grouping_selections[4];
                l_grouping_selected.Text = $"Selected ID: {target.ToString()}";
                
                b_grouping4.BackColor = System.Drawing.Color.Red;
            }
        }

        /// <summary>
        /// Select target in Fifth button
        /// </summary>
        private void b_grouping5_Click(object sender, EventArgs e)
        {
            if (b_grouping5.Text == "")
            { }
            else
            {
                RemoveSelection();

                target = grouping_selections[5];
                l_grouping_selected.Text = $"Selected ID: {target.ToString()}";

                b_grouping5.BackColor = System.Drawing.Color.Red;
            }
        }

        /// <summary>
        /// Kill an Entity targeted by a button
        /// </summary>
        private void b_grouping_kill_Click(object sender, EventArgs e)
        {
            if (target == -1)
            { }
            else
            {
                Entities.KillEntity(0, target);
                RemoveSelection();
                target = -1;

                EntitiesUpdate();
            }
        }
        //=========================================================

        /// <summary>
        /// Open Effects panel
        /// </summary>
        private void b_effects_panel_Click(object sender, EventArgs e)
        {
            EffectsList effectsList = new EffectsList();
            effectsList.Show();
        }
    }
}
