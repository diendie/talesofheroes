﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using LoggerLib;
//using LibNPC_STATS;
//using LibNPC_FACTORY;
//using LibPLAYER_STATS;
//using LibEFFECTS;
//using LibTOHDebug;

namespace TalesOfHeroes//PLAYER'S ENTITY AND OTHER ENTITY SHOULD NOT BE CREATED HERE!!!!
{
    public partial class CombatForm : Form
    {
        Random Roll = new Random();

        Dictionary<int, int> grouping_selections_enemies = new Dictionary<int, int>();

        Dictionary<int, int> grouping_selections_friends = new Dictionary<int, int>();

        INPC_STATS Entities;
        IPLAYER_STATS Player;
        INPC_FACTORY Factory;
        
        List<int> Combat_list = new List<int>();

        /// <summary>
        /// A Target for player's attack
        /// </summary>
        int Target { get; set; } = -1;
        /// <summary>
        /// A flag to mark combat_turns entries
        /// </summary>
        bool combat_flag = false;
        /// <summary>
        /// A flag to mark if any Target selected
        /// </summary>
        bool selection_flag = false;

        public CombatForm()
        {
            //MessageBox.Show("Add code descriptions FFS", "You lazy bastard", MessageBoxButtons.OK, MessageBoxIcon.Warning);


            Entities = new NPC_STATS();
            Player = new PLAYER_STATS();
            Factory = new NPC_FACTORY();


            //Subsribing to the events
            //Define behaviour in functions on the left
            //=====================================================================================
            Entities.onEntityCreation += OnEntityCreation;
            
            Player.onPlayerDeath += OnPlayerDeath;
            Entities.onEntityDeath += OnEntityDeath;

            Player.onPlayerHealthChange += OnHealthChange;
            Entities.onEntityHealthChange += OnHealthChange;
            //=====================================================================================


            InitializeComponent();
            //throw new NotImplementedException("Behaviour[][] is obsolete - need rework.");
            combat();
        }

        private void CombatForm_Load(object sender, EventArgs e)
        {
            if (MainForm.SelectedClass == "barbarian")
            {
                comboBox_action.Items.Add("Rage");
            }

            if (MainForm.SelectedClass == "bard")
            {
                comboBox_action.Items.Add("Bardic Inspiration");
            }

            if (MainForm.SelectedClass == "paladin")
            {
                comboBox_action.Items.Add("Lay on Hands");
            }

            if (MainForm.SelectedClass == "fighter")
            {
                comboBox_bonusaction.Items.Add("Second Wind");
            }

            if (MainForm.SelectedClass == "monk")
            {
                comboBox_bonusaction.Items.Add("Flurry of Fists");
            }
        }



        //Event handlers
        //=====================================================================================
        /// <summary>
        /// Entity creation handler
        /// </summary>
        /// <param name="new_ID">ID of a new Entity</param>
        internal void OnEntityCreation(int new_ID)
        {
            Logger.Log(new MessageType[]
            {
                MessageType.General,
                MessageType.GeneralSub
            },
            new string[]
            {
                "Entity creation",
                $"ID: { new_ID}"
            });
        }
        /// <summary>
        /// Health change handler
        /// </summary>
        /// <param name="source_ID">Change origin</param>
        /// <param name="target_ID">Target</param>
        /// <param name="amount">Change amount</param>
        internal void OnHealthChange(int source_ID, int target_ID, int amount)
        {
            Logger.LogQueueAdd(MessageType.General, "Health change");
            //if Player is a source
            if (source_ID == 0)
            {
                Logger.LogQueueAdd(MessageType.GeneralSub, "Source: Player");
            }
            else
            {
                Logger.LogQueueAdd(MessageType.GeneralSub, $"Source: ID{source_ID}");
            }

            Logger.LogQueueAdd(MessageType.GeneralSub, $"Amount: {amount}");

            //if Player is a target
            if (target_ID == 0)
            {
                Logger.LogQueueAdd(MessageType.GeneralSub, "Target: Player");
                Logger.LogQueueAdd(MessageType.GeneralSub, $"New Health: {Player.Health}");
            }
            else
            {
                Logger.LogQueueAdd(MessageType.GeneralSub, $"Target: ID{target_ID}");
                Logger.LogQueueAdd(MessageType.GeneralSub, $"New Health: {Entities.Health[target_ID]}");
            }

            Logger.LogQueueExecute();
        }
        /// <summary>
        /// Entity death handler
        /// </summary>
        /// <param name="dead_ID">Dead Entity ID</param>
        /// <param name="killer_ID">Killer ID</param>
        internal void OnEntityDeath(int dead_ID, int killer_ID)
        {
            //Console.WriteLine($"! Entity Death");
            //Console.WriteLine($"   |ID: {dead_ID}");
            //Console.WriteLine($"   |Killer ID: {killer_ID}");
            Logger.Log(new MessageType[] 
            {
                MessageType.General,
                MessageType.GeneralSub,
                MessageType.GeneralSub,
            }, new string[]
            {
                "Entity death",
                $"ID: {dead_ID}",
                $"Killer ID: {killer_ID}",
            });
        }

        internal void OnPlayerDeath(int killer_ID)
        {
            //Console.WriteLine($"! Player Death");
            //Console.WriteLine($"   |Killer ID: {killer_ID}");
            Logger.Log(new MessageType[] 
            { 
                MessageType.General, 
                MessageType.GeneralSub 
            }, new string[] 
            { 
                "Player death", 
                $"Killer ID: {killer_ID}" 
            });

            //MessageBox.Show($"You were killed by {Entities.Name[Entities.ID.IndexOf(killer_ID)]}", "Death", MessageBoxButtons.OK, MessageBoxIcon.Error);
            MessageBox.Show("You Died", "Death", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        //=====================================================================================

        int turn_count = 0;

        /// <summary>
        /// Creates a list of entities engaged in combat and sorts it
        /// </summary>
        /// <param name="Combat_list"></param>
        /// <param name="Entities">Entities</param>
        /// <param name="Player">Player</param>
        /// <returns></returns>
        List<int> Combat_list_create(List<int> Combat_list, INPC_STATS Entities, IPLAYER_STATS Player)
        {


            Player.SetTurn();//Setting Player turn value

            for (int i = 0; i < Entities.ID.Count; i++)//Setting turn values and adding entities to Combat_list
            {
                Entities.Turn.Add(Roll.Next(1, 100));
                Combat_list.Add(Entities.Turn[i]);
            }

            Combat_list.Add(Player.Turn);//Adding player to Combat_list

            Combat_list.Sort();//Sorting

            return Combat_list;
        }

        void Combat_buttons_refresh(INPC_STATS Entities, IPLAYER_STATS Player)
        {
            grouping_selections_enemies.Clear();

            button_enemy0.Enabled = false; button_enemy0.Visible = false; button_enemy1.Enabled = false; button_enemy1.Visible = false;
            button_enemy2.Enabled = false; button_enemy2.Visible = false; button_enemy3.Enabled = false; button_enemy3.Visible = false;
            button_enemy4.Enabled = false; button_enemy4.Visible = false; button_enemy5.Enabled = false; button_enemy5.Visible = false;
            button_enemy6.Enabled = false; button_enemy6.Visible = false; button_enemy7.Enabled = false; button_enemy7.Visible = false;
            button_enemy8.Enabled = false; button_enemy8.Visible = false; button_enemy9.Enabled = false; button_enemy9.Visible = false;
            button_enemy10.Enabled = false; button_enemy10.Visible = false;

            switch (Entities.Hostiles.Count)
            {
                case 1:
                    button_enemy5.Enabled = true; button_enemy5.Visible = true;

                    int index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy5.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[5] = Entities.ID[index];

                    break;
                case 2:
                    button_enemy4.Enabled = true; button_enemy4.Visible = true; button_enemy6.Enabled = true; button_enemy6.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    break;
                case 3:
                    button_enemy5.Enabled = true; button_enemy5.Visible = true; button_enemy4.Enabled = true; button_enemy4.Visible = true;
                    button_enemy6.Enabled = true; button_enemy6.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                    button_enemy5.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[5] = Entities.ID[index];

                    break;
                case 4:
                    button_enemy4.Enabled = true; button_enemy4.Visible = true; button_enemy3.Enabled = true; button_enemy3.Visible = true;
                    button_enemy6.Enabled = true; button_enemy6.Visible = true; button_enemy7.Enabled = true; button_enemy7.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                    button_enemy3.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                    button_enemy7.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[7] = Entities.ID[index];

                    break;
                case 5:
                    button_enemy5.Enabled = true; button_enemy5.Visible = true; button_enemy4.Enabled = true; button_enemy4.Visible = true;
                    button_enemy3.Enabled = true; button_enemy3.Visible = true; button_enemy6.Enabled = true; button_enemy6.Visible = true;
                    button_enemy7.Enabled = true; button_enemy7.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                    button_enemy3.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                    button_enemy7.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[4]);
                    button_enemy5.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[5] = Entities.ID[index];

                    break;
                case 6:
                    button_enemy4.Enabled = true; button_enemy4.Visible = true; button_enemy3.Enabled = true; button_enemy3.Visible = true;
                    button_enemy6.Enabled = true; button_enemy6.Visible = true; button_enemy7.Enabled = true; button_enemy7.Visible = true;
                    button_enemy2.Enabled = true; button_enemy2.Visible = true; button_enemy8.Enabled = true; button_enemy8.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                    button_enemy3.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                    button_enemy7.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[4]);
                    button_enemy2.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[5]);
                    button_enemy8.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[8] = Entities.ID[index];

                    break;
                case 7:
                    button_enemy5.Enabled = true; button_enemy5.Visible = true; button_enemy4.Enabled = true; button_enemy4.Visible = true;
                    button_enemy3.Enabled = true; button_enemy3.Visible = true; button_enemy6.Enabled = true; button_enemy6.Visible = true;
                    button_enemy7.Enabled = true; button_enemy7.Visible = true; button_enemy2.Enabled = true; button_enemy2.Visible = true;
                    button_enemy8.Enabled = true; button_enemy8.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                    button_enemy3.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                    button_enemy7.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[4]);
                    button_enemy2.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[5]);
                    button_enemy8.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[8] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[6]);
                    button_enemy5.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[5] = Entities.ID[index];

                    break;
                case 8:
                    button_enemy1.Enabled = true; button_enemy1.Visible = true; button_enemy2.Enabled = true; button_enemy2.Visible = true;
                    button_enemy3.Enabled = true; button_enemy3.Visible = true; button_enemy4.Enabled = true; button_enemy4.Visible = true;
                    button_enemy6.Enabled = true; button_enemy6.Visible = true; button_enemy7.Enabled = true; button_enemy7.Visible = true;
                    button_enemy8.Enabled = true; button_enemy8.Visible = true; button_enemy9.Enabled = true; button_enemy9.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                    button_enemy3.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                    button_enemy7.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[4]);
                    button_enemy2.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[5]);
                    button_enemy8.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[8] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[6]);
                    button_enemy1.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[1] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[7]);
                    button_enemy9.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[9] = Entities.ID[index];

                    break;
                case 9:
                    button_enemy5.Enabled = true; button_enemy5.Visible = true; button_enemy1.Enabled = true; button_enemy1.Visible = true;
                    button_enemy2.Enabled = true; button_enemy2.Visible = true; button_enemy3.Enabled = true; button_enemy3.Visible = true;
                    button_enemy4.Enabled = true; button_enemy4.Visible = true; button_enemy6.Enabled = true; button_enemy6.Visible = true;
                    button_enemy7.Enabled = true; button_enemy7.Visible = true; button_enemy8.Enabled = true; button_enemy8.Visible = true;
                    button_enemy9.Enabled = true; button_enemy9.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                    button_enemy3.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                    button_enemy7.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[4]);
                    button_enemy2.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[5]);
                    button_enemy8.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[8] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[6]);
                    button_enemy1.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[1] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[7]);
                    button_enemy9.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[9] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[8]);
                    button_enemy5.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[5] = Entities.ID[index];

                    break;
                case 10:
                    button_enemy0.Enabled = true; button_enemy0.Visible = true; button_enemy1.Enabled = true; button_enemy1.Visible = true;
                    button_enemy2.Enabled = true; button_enemy2.Visible = true; button_enemy3.Enabled = true; button_enemy3.Visible = true;
                    button_enemy4.Enabled = true; button_enemy4.Visible = true; button_enemy6.Enabled = true; button_enemy6.Visible = true;
                    button_enemy7.Enabled = true; button_enemy7.Visible = true; button_enemy8.Enabled = true; button_enemy8.Visible = true;
                    button_enemy9.Enabled = true; button_enemy9.Visible = true; button_enemy10.Enabled = true; button_enemy10.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                    button_enemy3.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                    button_enemy7.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[4]);
                    button_enemy2.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[5]);
                    button_enemy8.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[8] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[6]);
                    button_enemy1.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[1] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[7]);
                    button_enemy9.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[9] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[8]);
                    button_enemy0.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[0] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[9]);
                    button_enemy10.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[10] = Entities.ID[index];

                    break;
                case 11:
                    button_enemy5.Enabled = true; button_enemy5.Visible = true;
                    button_enemy0.Enabled = true; button_enemy0.Visible = true; button_enemy1.Enabled = true; button_enemy1.Visible = true;
                    button_enemy2.Enabled = true; button_enemy2.Visible = true; button_enemy3.Enabled = true; button_enemy3.Visible = true;
                    button_enemy4.Enabled = true; button_enemy4.Visible = true; button_enemy6.Enabled = true; button_enemy6.Visible = true;
                    button_enemy7.Enabled = true; button_enemy7.Visible = true; button_enemy8.Enabled = true; button_enemy8.Visible = true;
                    button_enemy9.Enabled = true; button_enemy9.Visible = true; button_enemy10.Enabled = true; button_enemy10.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Hostiles[0]);
                    button_enemy4.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[1]);
                    button_enemy6.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[2]);
                    button_enemy3.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[3]);
                    button_enemy7.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[4]);
                    button_enemy2.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[5]);
                    button_enemy8.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[8] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[6]);
                    button_enemy1.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[1] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[7]);
                    button_enemy9.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[9] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[8]);
                    button_enemy0.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[0] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[9]);
                    button_enemy10.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[10] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Hostiles[10]);
                    button_enemy5.Text = Entities.Name[index].ToString();
                    grouping_selections_enemies[5] = Entities.ID[index];

                    break;
            }

            button_friend0.Enabled = false; button_friend0.Visible = false; button_friend1.Enabled = false; button_friend1.Visible = false;
            button_friend2.Enabled = false; button_friend2.Visible = false; button_friend3.Enabled = false; button_friend3.Visible = false;
            button_friend4.Enabled = false; button_friend4.Visible = false; button_friend5.Enabled = false; button_friend5.Visible = false;
            button_friend6.Enabled = false; button_friend6.Visible = false; button_friend7.Enabled = false; button_friend7.Visible = false;
            button_friend8.Enabled = false; button_friend8.Visible = false; button_friend9.Enabled = false; button_friend9.Visible = false;
            button_friend10.Enabled = false; button_friend10.Visible = false;

            switch (Entities.Friendlies.Count)
            {
                case 0:
                    button_friend5.Enabled = true; button_friend5.Visible = true;


                    button_friend5.Text = Player.Name;
                    grouping_selections_friends[5] = 0;

                    break;
                case 1:
                    button_friend4.Enabled = true; button_friend4.Visible = true; button_friend6.Enabled = true; button_friend6.Visible = true;

                    int index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend4.Text = Player.Name;
                    grouping_selections_friends[4] = 0;

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    break;
                case 2:
                    button_friend5.Enabled = true; button_friend5.Visible = true; button_friend4.Enabled = true; button_friend4.Visible = true;
                    button_friend6.Enabled = true; button_friend6.Visible = true;

                    button_friend5.Text = Player.Name;
                    grouping_selections_friends[5] = 0;

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[1]);
                    button_friend4.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[4] = Entities.ID[index];

                    break;
                case 3:
                    button_friend4.Enabled = true; button_friend4.Visible = true; button_friend3.Enabled = true; button_friend3.Visible = true;
                    button_friend6.Enabled = true; button_friend6.Visible = true; button_friend7.Enabled = true; button_friend7.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend7.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[1]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[2]);
                    button_friend3.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[3] = Entities.ID[index];

                    button_friend4.Text = Player.Name;
                    grouping_selections_friends[4] = 0;

                    break;
                case 4:
                    button_friend5.Enabled = true; button_friend5.Visible = true; button_friend4.Enabled = true; button_friend4.Visible = true;
                    button_friend3.Enabled = true; button_friend3.Visible = true; button_friend6.Enabled = true; button_friend6.Visible = true;
                    button_friend7.Enabled = true; button_friend7.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend4.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[1]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[2]);
                    button_friend3.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[3]);
                    button_friend7.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[7] = Entities.ID[index];

                    button_friend5.Text = Player.Name;
                    grouping_selections_friends[5] = 0;

                    break;
                case 5:
                    button_friend4.Enabled = true; button_friend4.Visible = true; button_friend3.Enabled = true; button_friend3.Visible = true;
                    button_friend6.Enabled = true; button_friend6.Visible = true; button_friend7.Enabled = true; button_friend7.Visible = true;
                    button_friend2.Enabled = true; button_friend2.Visible = true; button_friend8.Enabled = true; button_friend8.Visible = true;

                    button_friend4.Text = Player.Name;
                    grouping_selections_friends[4] = 0;

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[1]);
                    button_friend3.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[2]);
                    button_friend7.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[3]);
                    button_friend2.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[4]);
                    button_friend8.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[8] = Entities.ID[index];

                    break;
                case 6:
                    button_friend5.Enabled = true; button_friend5.Visible = true; button_friend4.Enabled = true; button_friend4.Visible = true;
                    button_friend3.Enabled = true; button_friend3.Visible = true; button_friend6.Enabled = true; button_friend6.Visible = true;
                    button_friend7.Enabled = true; button_friend7.Visible = true; button_friend2.Enabled = true; button_friend2.Visible = true;
                    button_friend8.Enabled = true; button_friend8.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend4.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[1]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[2]);
                    button_friend3.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[3]);
                    button_friend7.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[4]);
                    button_friend2.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[5]);
                    button_friend8.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[8] = Entities.ID[index];

                    button_friend5.Text = Player.Name;
                    grouping_selections_friends[5] = 0;

                    break;
                case 7:
                    button_friend1.Enabled = true; button_friend1.Visible = true; button_friend2.Enabled = true; button_friend2.Visible = true;
                    button_friend3.Enabled = true; button_friend3.Visible = true; button_friend4.Enabled = true; button_friend4.Visible = true;
                    button_friend6.Enabled = true; button_friend6.Visible = true; button_friend7.Enabled = true; button_friend7.Visible = true;
                    button_friend8.Enabled = true; button_friend8.Visible = true; button_friend9.Enabled = true; button_friend9.Visible = true;

                    button_friend4.Text = Player.Name;
                    grouping_selections_friends[4] = 0;

                    index = Entities.ID.IndexOf(Entities.Friendlies[1]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[2]);
                    button_friend3.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[3]);
                    button_friend7.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[4]);
                    button_friend2.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[5]);
                    button_friend8.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[8] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[6]);
                    button_friend1.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[1] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend9.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[9] = Entities.ID[index];

                    break;
                case 8:
                    button_friend5.Enabled = true; button_friend5.Visible = true; button_friend1.Enabled = true; button_friend1.Visible = true;
                    button_friend2.Enabled = true; button_friend2.Visible = true; button_friend3.Enabled = true; button_friend3.Visible = true;
                    button_friend4.Enabled = true; button_friend4.Visible = true; button_friend6.Enabled = true; button_friend6.Visible = true;
                    button_friend7.Enabled = true; button_friend7.Visible = true; button_friend8.Enabled = true; button_friend8.Visible = true;
                    button_friend9.Enabled = true; button_friend9.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend4.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[1]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[2]);
                    button_friend3.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[3]);
                    button_friend7.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[4]);
                    button_friend2.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[5]);
                    button_friend8.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[8] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[6]);
                    button_friend1.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[1] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[7]);
                    button_friend9.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[9] = Entities.ID[index];

                    button_friend5.Text = Player.Name;
                    grouping_selections_friends[5] = 0;

                    break;
                case 9:
                    button_friend0.Enabled = true; button_friend0.Visible = true; button_friend1.Enabled = true; button_friend1.Visible = true;
                    button_friend2.Enabled = true; button_friend2.Visible = true; button_friend3.Enabled = true; button_friend3.Visible = true;
                    button_friend4.Enabled = true; button_friend4.Visible = true; button_friend6.Enabled = true; button_friend6.Visible = true;
                    button_friend7.Enabled = true; button_friend7.Visible = true; button_friend8.Enabled = true; button_friend8.Visible = true;
                    button_friend9.Enabled = true; button_friend9.Visible = true; button_friend10.Enabled = true; button_friend10.Visible = true;

                    button_friend4.Text = Player.Name;
                    grouping_selections_friends[4] = 0;

                    index = Entities.ID.IndexOf(Entities.Friendlies[1]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[2]);
                    button_friend3.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[3]);
                    button_friend7.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[4]);
                    button_friend2.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[5]);
                    button_friend8.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[8] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[6]);
                    button_friend1.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[1] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[7]);
                    button_friend9.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[9] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[8]);
                    button_friend0.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[0] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend10.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[10] = Entities.ID[index];

                    break;
                case 10:
                    button_friend5.Enabled = true; button_friend5.Visible = true;
                    button_friend0.Enabled = true; button_friend0.Visible = true; button_friend1.Enabled = true; button_friend1.Visible = true;
                    button_friend2.Enabled = true; button_friend2.Visible = true; button_friend3.Enabled = true; button_friend3.Visible = true;
                    button_friend4.Enabled = true; button_friend4.Visible = true; button_friend6.Enabled = true; button_friend6.Visible = true;
                    button_friend7.Enabled = true; button_friend7.Visible = true; button_friend8.Enabled = true; button_friend8.Visible = true;
                    button_friend9.Enabled = true; button_friend9.Visible = true; button_friend10.Enabled = true; button_friend10.Visible = true;

                    index = Entities.ID.IndexOf(Entities.Friendlies[0]);
                    button_friend4.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[4] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[1]);
                    button_friend6.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[6] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[2]);
                    button_friend3.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[3] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[3]);
                    button_friend7.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[7] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[4]);
                    button_friend2.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[2] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[5]);
                    button_friend8.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[8] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[6]);
                    button_friend1.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[1] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[7]);
                    button_friend9.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[9] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[8]);
                    button_friend0.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[0] = Entities.ID[index];

                    index = Entities.ID.IndexOf(Entities.Friendlies[9]);
                    button_friend10.Text = Entities.Name[index].ToString();
                    grouping_selections_friends[10] = Entities.ID[index];

                    button_friend5.Text = Player.Name;
                    grouping_selections_friends[5] = 0;

                    break;
                
            }
        }
        void combat()
        {
            button_enemy0.Enabled = false; button_enemy0.Visible = false; button_enemy1.Enabled = false; button_enemy1.Visible = false;
            button_enemy2.Enabled = false; button_enemy2.Visible = false; button_enemy3.Enabled = false; button_enemy3.Visible = false;
            button_enemy4.Enabled = false; button_enemy4.Visible = false; button_enemy5.Enabled = false; button_enemy5.Visible = false;
            button_enemy6.Enabled = false; button_enemy6.Visible = false; button_enemy7.Enabled = false; button_enemy7.Visible = false;
            button_enemy8.Enabled = false; button_enemy8.Visible = false; button_enemy9.Enabled = false; button_enemy9.Visible = false;
            button_enemy10.Enabled = false; button_enemy10.Visible = false;

            ArmourPack playerArmourPack = new ArmourPack
            {
                ArmourClass = ArmourClass.Heavy,
                ArmourRating = 15
            };
            StatPack playerStatPack = new StatPack
            {
                Strength = 10,
                Agility = 10,
                BaseHealth = 100,
                Constitution = 10,
                Charisma = 10,
                Intellect = 10,
                Wisdom = 10
            };
            Player.AddPlayer("Ivan", playerStatPack, playerArmourPack);

            for (int i = 0; i < 2; i++)//Creating a few entities  //TEMPORARY!!!!
            {
                //Factory.LoadEntity(NPC_Type.Old_Dragon);
                //Entities.AddEntity(Factory.EntityStatPack, Factory.EntityTraitPack, Factory.EntityArmourPack, Player.Level);

                Entities.AddEntity(NPC_Type.Old_Dragon, Player.Level);
            }

            for (int i = 0; i < 8; i++)//Creating a few entities  //TEMPORARY!!!!
            {
                //Factory.LoadEntity(NPC_Type.Guard_Archer);
                //Entities.AddEntity(Factory.EntityStatPack, Factory.EntityTraitPack, Factory.EntityArmourPack, Player.Level);
                //Entities.AddEntity(Factory.Stats, Factory.Traits, Player.Level);

                Entities.AddEntity(NPC_Type.Guard_Archer, Player.Level);
            }

            Combat_list = Combat_list_create(Combat_list, Entities, Player);

            Combat_buttons_refresh(Entities, Player);

            combat_turns(Combat_list, Entities, Player);

        }
        void combat_turns(List<int> Combat_list, INPC_STATS Entities, IPLAYER_STATS Player)//HEALING DODGING AND GAME OVER SCENARIO REQUIRED
        {
            comboBox_action.Visible = false;
            comboBox_action.Enabled = false;

            comboBox_bonusaction.Visible = false;
            comboBox_bonusaction.Enabled = false;

            button_endturn.Visible = false;
            button_endturn.Enabled = false;

            if (Entities.Hostiles.Count == 0)
            {
                MessageBox.Show("VICTORY", "VICTORY", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            bool flag = false;

            if (combat_flag == false)//The very first function entry; All entities BEFORE player take turns
            {
                if (Player.Turn == Combat_list[0])//If a player is first to take turn then stop this function
                {
                    combat_flag = true;

                    comboBox_action.Visible = true;
                    comboBox_action.Enabled = true;

                    comboBox_bonusaction.Visible = true;
                    comboBox_bonusaction.Enabled = true;

                    button_endturn.Visible = true;
                    button_endturn.Enabled = true;

                    return;
                }
                for (int i = 0; i < Combat_list.IndexOf(Player.Turn); i++)
                {
                    flag = false;

                    int index = Entities.Turn.IndexOf(Combat_list[i]);

                    int random_Target = 0;

                    switch (Entities.BehaviourType[index])
                    {
                        case "Hostile":
                            if (Player.Health <= Entities.AttackDamage[index])
                            {
                                textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to you!";
                                Player.ChangeHealth(Entities.ID[index], Entities.AttackDamage[index]);
                                //GAME OVER
                                continue;
                            }
                            else
                            {
                                for (int j = 0; j < Entities.Friendlies.Count - 1; j++)
                                {
                                    if (Entities.Health[Entities.Friendlies[j]] <= Entities.AttackDamage[index])
                                    {
                                        textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.Friendlies[j]];
                                        //Combat_list.RemoveAt(Combat_list.IndexOf(Entities.Turn[Entities.Friendlies[j]]));

                                        Entities.ChangeHealth(Entities.ID[index], Entities.Friendlies[j], Entities.AttackDamage[index]);
                                        Combat_buttons_refresh(Entities, Player);
                                        flag = true;
                                        break;
                                    }

                                }
                                if (flag == true)
                                {
                                    continue;
                                }

                                random_Target = Roll.Next(0, 2);

                                if (Entities.Friendlies.Count == 0)
                                {
                                    random_Target = 0;
                                }

                                if (random_Target == 0)
                                {
                                    textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to you!";

                                    Player.ChangeHealth(Entities.ID[index], Entities.AttackDamage[index]);
                                    continue;
                                }
                                else
                                {
                                    random_Target = Roll.Next(0, Entities.Friendlies.Count);
                                    textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.Friendlies[random_Target] - 1];

                                    Entities.ChangeHealth(Entities.ID[index], Entities.Friendlies[random_Target], Entities.AttackDamage[index]);
                                    continue;
                                }
                            }


                        case "Friendly":
                            for (int j = 0; j < Entities.Hostiles.Count - 1; j++)
                            {
                                if (Entities.Health[Entities.Hostiles[j]] <= Entities.AttackDamage[index])
                                {
                                    textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.ID.IndexOf(Entities.Hostiles[j])];
                                    //Combat_list.RemoveAt(Combat_list.IndexOf(Entities.Turn[Entities.Hostiles[j]]));
                                    Entities.ChangeHealth(Entities.ID[index], Entities.Hostiles[j], Entities.AttackDamage[index]);
                                    Combat_buttons_refresh(Entities, Player);
                                    flag = true;
                                    break;
                                }

                            }
                            if (flag == true)
                            {
                                continue;
                            }
                            random_Target = Roll.Next(0, Entities.Hostiles.Count);
                            textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.ID.IndexOf(Entities.Hostiles[random_Target])];
                            Entities.ChangeHealth(Entities.ID[index], Entities.Hostiles[random_Target], Entities.AttackDamage[index]);
                            continue;
                    }
                }
                combat_flag = true;

                comboBox_action.Visible = true;
                comboBox_action.Enabled = true;

                comboBox_bonusaction.Visible = true;
                comboBox_bonusaction.Enabled = true;

                button_endturn.Visible = true;
                button_endturn.Enabled = true;

                return;
            }
            else
            {

                if (Player.Turn == Combat_list[Combat_list.Count - 1])//If a player is the last on the list then change FOR iteration order
                {
                    for (int i = 0; i < Combat_list.Count - 1; i++)
                    {
                        flag = false;
                        int index = Entities.Turn.IndexOf(Combat_list[i]);

                        if (Player.Turn == Combat_list[i])
                        {
                            index = Entities.Turn.IndexOf(Combat_list[i - 1]);
                        }
                        else
                        {
                            index = Entities.Turn.IndexOf(Combat_list[i]);
                        }
                        int random_Target = 0;

                        switch (Entities.BehaviourType[index])
                        {
                            case "Hostile":
                                if (Player.Health <= Entities.AttackDamage[index])
                                {
                                    textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to you!";
                                    Player.ChangeHealth(Entities.ID[index], Entities.AttackDamage[index]);
                                    //GAME OVER
                                    continue;
                                }
                                else
                                {
                                    for (int j = 0; j < Entities.Friendlies.Count - 1; j++)
                                    {
                                        if (Entities.Health[Entities.Friendlies[j]] <= Entities.AttackDamage[index])
                                        {
                                            textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.Friendlies[j]];
                                            //Combat_list.RemoveAt(Combat_list.IndexOf(Entities.Turn[Entities.Friendlies[j]]));
                                            Entities.ChangeHealth(Entities.ID[index], Entities.Friendlies[j], Entities.AttackDamage[index]);
                                            Combat_buttons_refresh(Entities, Player);
                                            flag = true;
                                            break;
                                        }

                                    }
                                    if (flag == true)
                                    {
                                        continue;
                                    }

                                    random_Target = Roll.Next(0, 2);
                                    if (Entities.Friendlies.Count == 0)
                                    {
                                        random_Target = 0;
                                    }

                                    if (random_Target == 0)
                                    {
                                        textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to you!";
                                        Player.ChangeHealth(Entities.ID[index], Entities.AttackDamage[index]);
                                        continue;
                                    }
                                    else
                                    {
                                        random_Target = Roll.Next(0, Entities.Friendlies.Count);
                                        textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.Friendlies[random_Target] - 1];
                                        Entities.ChangeHealth(Entities.ID[index], Entities.Friendlies[random_Target], Entities.AttackDamage[index]);
                                        continue;
                                    }
                                }

                            case "Friendly":
                                for (int j = 0; j < Entities.Hostiles.Count - 1; j++)
                                {
                                    if (Entities.Health[Entities.Hostiles[j]] <= Entities.AttackDamage[index])
                                    {
                                        textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.Hostiles[j]];
                                        //Combat_list.RemoveAt(Combat_list.IndexOf(Entities.Turn[Entities.Hostiles[j]]));
                                        Entities.ChangeHealth(Entities.ID[index], Entities.Hostiles[j], Entities.AttackDamage[index]);
                                        Combat_buttons_refresh(Entities, Player);
                                        flag = true;
                                        break;
                                    }

                                }
                                if (flag == true)
                                {
                                    continue;
                                }
                                random_Target = Roll.Next(0, Entities.Hostiles.Count);
                                textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.Hostiles[random_Target]];
                                Entities.ChangeHealth(Entities.ID[index], Entities.Hostiles[random_Target], Entities.AttackDamage[index]);
                                continue;
                        }
                        if (i == Combat_list.Count - 2)//Stopping function because FOR has reached player's turn
                        {
                            break;
                        }
                    }
                    comboBox_action.Visible = true;
                    comboBox_action.Enabled = true;

                    comboBox_bonusaction.Visible = true;
                    comboBox_bonusaction.Enabled = true;

                    button_endturn.Visible = true;
                    button_endturn.Enabled = true;

                    return;

                }
                else//If a player is not the last on the list
                {
                    int i = Combat_list.IndexOf(Player.Turn) + 1;

                    while (i < Combat_list.Count)
                    {
                        if (Combat_list.IndexOf(Player.Turn) == i)
                        {
                            combat_flag = true;

                            comboBox_action.Visible = true;
                            comboBox_action.Enabled = true;

                            comboBox_bonusaction.Visible = true;
                            comboBox_bonusaction.Enabled = true;

                            button_endturn.Visible = true;
                            button_endturn.Enabled = true;

                            return;
                        }

                        flag = false;

                        int index = Entities.Turn.IndexOf(Combat_list[i]);

                        int random_Target = 0;

                        switch (Entities.BehaviourType[index])
                        {
                            case "Hostile":
                                if (Player.Health <= Entities.AttackDamage[index])
                                {
                                    textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to you!";
                                    Player.ChangeHealth(Entities.ID[index], Entities.AttackDamage[index]);
                                    //GAME OVER
                                    MessageBox.Show("FAIL", "FAIL", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    continue;
                                }
                                else
                                {
                                    for (int j = 0; j < Entities.Friendlies.Count - 1; j++)
                                    {
                                        if (Entities.Health[Entities.Friendlies[j]] <= Entities.AttackDamage[index])
                                        {
                                            textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.Friendlies[j]];
                                            //Combat_list.RemoveAt(Combat_list.IndexOf(Entities.Turn[Entities.Friendlies[j]]));
                                            Entities.ChangeHealth(Entities.ID[index], Entities.Friendlies[j], Entities.AttackDamage[index]);
                                            Combat_buttons_refresh(Entities, Player);
                                            flag = true;
                                            break;
                                        }

                                    }
                                    if (flag == true)
                                    {
                                        if (i == Combat_list.Count - 1)
                                        {
                                            i = 0;
                                            continue;
                                        }
                                        i++;
                                        continue;
                                    }

                                    random_Target = Roll.Next(0, 2);
                                    if (Entities.Friendlies.Count == 0)
                                    {
                                        random_Target = 0;
                                    }

                                    if (random_Target == 0)
                                    {
                                        textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to you!";
                                        Player.ChangeHealth(Entities.ID[index], Entities.AttackDamage[index]);
                                        if (i == Combat_list.Count - 1)
                                        {
                                            i = 0;
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        random_Target = Roll.Next(0, Entities.Friendlies.Count);
                                        textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.Friendlies[random_Target] - 1];
                                        Entities.ChangeHealth(Entities.ID[index], Entities.Friendlies[random_Target], Entities.AttackDamage[index]);
                                        if (i == Combat_list.Count - 1)
                                        {
                                            i = 0;
                                            continue;
                                        }
                                        i++;
                                        continue;
                                    }
                                }
                                break;

                            case "Friendly":
                                for (int j = 0; j < Entities.Hostiles.Count - 1; j++)
                                {
                                    if (Entities.Health[Entities.Hostiles[j]] <= Entities.AttackDamage[index])
                                    {
                                        textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.Hostiles[j]];
                                        //Combat_list.RemoveAt(Combat_list.IndexOf(Entities.Turn[Entities.Hostiles[j]]));
                                        Entities.ChangeHealth(Entities.ID[index], Entities.Hostiles[j], Entities.AttackDamage[index]);
                                        Combat_buttons_refresh(Entities, Player);
                                        flag = true;
                                        break;
                                    }

                                }
                                if (flag == true)
                                {
                                    if (i == Combat_list.Count - 1)
                                    {
                                        i = 0;
                                        continue;
                                    }
                                    i++;
                                    continue;
                                }
                                if (Entities.Hostiles.Count != 0)
                                {
                                    random_Target = Roll.Next(0, Entities.Hostiles.Count);
                                    textBox_combatlog.Text += "\r\n" + Entities.Name[index] + " deals " + Entities.AttackDamage[index] + " to " + Entities.Name[Entities.ID.IndexOf(Entities.Hostiles[random_Target])];
                                    Entities.ChangeHealth(Entities.ID[index], Entities.Hostiles[random_Target], Entities.AttackDamage[index]);
                                    if (i == Combat_list.Count - 1)
                                    {
                                        i = 0;
                                        continue;
                                    }
                                    i++;
                                    continue;
                                }
                                break;
                        }
                        i++;
                    }

                }
            }
        }

        /// <summary>
        /// //Player's attack
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_endturn_Click(object sender, EventArgs e)
        {
            Console.WriteLine($"=END OF TURN {turn_count}=");
            Console.WriteLine();
            turn_count++;
            Console.WriteLine($"=TURN {turn_count}=");


            if (selection_flag == false)//if no enemy selected
            {
                return;
            }

            if (comboBox_action.SelectedItem.ToString() == "Attack")
            {
                if (Target == 0)
                {
                    MessageBox.Show("You can't attack yourself", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (Entities.BehaviourType[Entities.ID.IndexOf(Target)] == "Friendly")
                {
                    MessageBox.Show("You can't attack your allies", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                int playerDamage = -(Player.Strength * Player.Level);
                textBox_combatlog.Text += "\r\n" + "You deal " + (-playerDamage).ToString() + " damage to " + Entities.Name[Entities.ID.IndexOf(Target)];
                //Combat_list.RemoveAt(Combat_list.IndexOf(Entities.Turn[Entities.ID.IndexOf(Target)]));

                Entities.ChangeHealth(0, Target, playerDamage);
                Combat_buttons_refresh(Entities, Player);

                comboBox_action.Visible = false;
                comboBox_action.Enabled = false;

                comboBox_bonusaction.Visible = false;
                comboBox_bonusaction.Enabled = false;

                button_endturn.Visible = false;
                button_endturn.Enabled = false;

                RemoveSelection();

                combat_turns(Combat_list, Entities, Player);

                //if (Entities.Health[Entities.ID.IndexOf(Target)] <= Player.Strength * Player.Level)
                //{
                //    textBox_combatlog.Text += "\r\n" + "You deal " + (Player.Strength * Player.Level).ToString() + " damage to " + Entities.Name[Entities.ID.IndexOf(Target)];
                //    Combat_list.RemoveAt(Combat_list.IndexOf(Entities.Turn[Entities.ID.IndexOf(Target)]));
                //    Entities.ChangeHealth(Target, Player.Strength * Player.Level);
                //    Combat_buttons_refresh(Entities, Player);

                //    comboBox_action.Visible = false;
                //    comboBox_action.Enabled = false;

                //    comboBox_bonusaction.Visible = false;
                //    comboBox_bonusaction.Enabled = false;

                //    button_endturn.Visible = false;
                //    button_endturn.Enabled = false;

                //    RemoveSelection();

                //    combat_turns(Combat_list, Entities, Player);
                //}
                //else
                //{
                //    Entities.ChangeHealth(Target, Player.Strength * Player.Level);
                //    textBox_combatlog.Text += "\r\n" + "You deal " + (Player.Strength * Player.Level).ToString() + " damage to " + Entities.Name[Entities.Hostiles.IndexOf(Target)];

                //    comboBox_action.Visible = false;
                //    comboBox_action.Enabled = false;

                //    comboBox_bonusaction.Visible = false;
                //    comboBox_bonusaction.Enabled = false;

                //    button_endturn.Visible = false;
                //    button_endturn.Enabled = false;

                //    RemoveSelection();

                //    combat_turns(Combat_list, Entities, Player);
                //}
            }
            if (comboBox_action.SelectedItem.ToString() == "Cast a spell")
            {

            }
            selection_flag = false;
        }

        private void RemoveSelection()
        {
            button_enemy0.BackColor = System.Drawing.Color.Transparent;
            button_enemy1.BackColor = System.Drawing.Color.Transparent;
            button_enemy2.BackColor = System.Drawing.Color.Transparent;
            button_enemy3.BackColor = System.Drawing.Color.Transparent;
            button_enemy4.BackColor = System.Drawing.Color.Transparent;
            button_enemy5.BackColor = System.Drawing.Color.Transparent;
            button_enemy6.BackColor = System.Drawing.Color.Transparent;
            button_enemy7.BackColor = System.Drawing.Color.Transparent;
            button_enemy8.BackColor = System.Drawing.Color.Transparent;
            button_enemy9.BackColor = System.Drawing.Color.Transparent;
            button_enemy10.BackColor = System.Drawing.Color.Transparent;

            button_friend0.BackColor = System.Drawing.Color.Transparent;
            button_friend1.BackColor = System.Drawing.Color.Transparent;
            button_friend2.BackColor = System.Drawing.Color.Transparent;
            button_friend3.BackColor = System.Drawing.Color.Transparent;
            button_friend4.BackColor = System.Drawing.Color.Transparent;
            button_friend5.BackColor = System.Drawing.Color.Transparent;
            button_friend6.BackColor = System.Drawing.Color.Transparent;
            button_friend7.BackColor = System.Drawing.Color.Transparent;
            button_friend8.BackColor = System.Drawing.Color.Transparent;
            button_friend9.BackColor = System.Drawing.Color.Transparent;
            button_friend10.BackColor = System.Drawing.Color.Transparent;

            textBox_target_info.Clear();
        }

        private void Button_enemy0_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[0];
            button_enemy0.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_enemy1_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[1];
            button_enemy1.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_enemy2_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[2];
            button_enemy2.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_enemy3_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[3];
            button_enemy3.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_enemy4_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[4];
            button_enemy4.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_enemy5_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[5];
            button_enemy5.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_enemy6_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[6];
            button_enemy6.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_enemy7_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[7];
            button_enemy7.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_enemy8_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[8];
            button_enemy8.BackColor = System.Drawing.Color.Red;
            selection_flag = true;
        }

        private void Button_enemy9_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[9];
            button_enemy9.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_enemy10_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_enemies[10];
            button_enemy10.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_friend0_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[0];
            button_friend0.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_friend1_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[1];
            button_friend1.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_friend2_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[2];
            button_friend2.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_friend3_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[3];
            button_friend3.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_friend4_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[4];
            button_friend4.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            if (Target != 0)
            {
                textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
            }
            else
                return;
        }

        private void Button_friend5_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[5];
            button_friend5.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            if (Target != 0)
            {
                textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
            }
            else
                return;
        }

        private void Button_friend6_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[6];
            button_friend6.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            if (Target != 0)
            {
                textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
            }
            else
                return;
        }

        private void Button_friend7_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[7];
            button_friend7.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_friend8_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[8];
            button_friend8.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_friend9_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[9];
            button_friend9.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }

        private void Button_friend10_Click(object sender, EventArgs e)
        {
            RemoveSelection();

            Target = grouping_selections_friends[10];
            button_friend10.BackColor = System.Drawing.Color.Red;
            selection_flag = true;

            textBox_target_info.Text = Entities.Name[Entities.ID.IndexOf(Target)] + " " + Entities.Health[Entities.ID.IndexOf(Target)].ToString();
        }


    }
}
