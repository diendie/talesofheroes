﻿namespace TalesOfHeroes
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_info = new System.Windows.Forms.GroupBox();
            this.label_4thlevelspells_quantity = new System.Windows.Forms.Label();
            this.label_3rdlevelspells_quantity = new System.Windows.Forms.Label();
            this.label_2ndlevelspells_quantity = new System.Windows.Forms.Label();
            this.label_1st = new System.Windows.Forms.Label();
            this.label_money_quantity = new System.Windows.Forms.Label();
            this.label_exptillnextlevel_quantity = new System.Windows.Forms.Label();
            this.label_level_quantity = new System.Windows.Forms.Label();
            this.label_health_quantity = new System.Windows.Forms.Label();
            this.label_4thlevelspells = new System.Windows.Forms.Label();
            this.label_3rdlevelspells = new System.Windows.Forms.Label();
            this.label_2ndlevelspells = new System.Windows.Forms.Label();
            this.label_1stlevelspells = new System.Windows.Forms.Label();
            this.label_money = new System.Windows.Forms.Label();
            this.label_exptillnextlevel = new System.Windows.Forms.Label();
            this.label_level = new System.Windows.Forms.Label();
            this.label_health = new System.Windows.Forms.Label();
            this.groupBox_inventory = new System.Windows.Forms.GroupBox();
            this.label_item9_quantity = new System.Windows.Forms.Label();
            this.label_item8_quantity = new System.Windows.Forms.Label();
            this.label_item7_quantity = new System.Windows.Forms.Label();
            this.label_item6_quantity = new System.Windows.Forms.Label();
            this.label_item5_quantity = new System.Windows.Forms.Label();
            this.label_item4_quantity = new System.Windows.Forms.Label();
            this.label_item3_quantity = new System.Windows.Forms.Label();
            this.label_item2_quantity = new System.Windows.Forms.Label();
            this.label_item1_quantity = new System.Windows.Forms.Label();
            this.label_item0_quantity = new System.Windows.Forms.Label();
            this.label_item9 = new System.Windows.Forms.Label();
            this.label_item8 = new System.Windows.Forms.Label();
            this.label_item7 = new System.Windows.Forms.Label();
            this.label_item6 = new System.Windows.Forms.Label();
            this.label_item5 = new System.Windows.Forms.Label();
            this.label_item4 = new System.Windows.Forms.Label();
            this.label_item3 = new System.Windows.Forms.Label();
            this.label_item2 = new System.Windows.Forms.Label();
            this.label_item1 = new System.Windows.Forms.Label();
            this.label_item0 = new System.Windows.Forms.Label();
            this.textBox_general = new System.Windows.Forms.TextBox();
            this.textBox_queue = new System.Windows.Forms.TextBox();
            this.textBox_battle_log = new System.Windows.Forms.TextBox();
            this.groupBox_trade = new System.Windows.Forms.GroupBox();
            this.button_trade_cancel = new System.Windows.Forms.Button();
            this.button_trade_buy = new System.Windows.Forms.Button();
            this.label_trade_sum_value = new System.Windows.Forms.Label();
            this.label_trade_sum = new System.Windows.Forms.Label();
            this.button_trade_item6_more = new System.Windows.Forms.Button();
            this.button_trade_item5_more = new System.Windows.Forms.Button();
            this.button_trade_item4_more = new System.Windows.Forms.Button();
            this.button_trade_item3_more = new System.Windows.Forms.Button();
            this.button_trade_item2_more = new System.Windows.Forms.Button();
            this.button_trade_item1_more = new System.Windows.Forms.Button();
            this.button_trade_item0_more = new System.Windows.Forms.Button();
            this.button_trade_item6_less = new System.Windows.Forms.Button();
            this.button_trade_item5_less = new System.Windows.Forms.Button();
            this.button_trade_item4_less = new System.Windows.Forms.Button();
            this.button_trade_item3_less = new System.Windows.Forms.Button();
            this.button_trade_item2_less = new System.Windows.Forms.Button();
            this.button_trade_item1_less = new System.Windows.Forms.Button();
            this.button_trade_item0_less = new System.Windows.Forms.Button();
            this.label_trade_item6_quantity = new System.Windows.Forms.Label();
            this.label_trade_item5_quantity = new System.Windows.Forms.Label();
            this.label_trade_item4_quantity = new System.Windows.Forms.Label();
            this.label_trade_item3_quantity = new System.Windows.Forms.Label();
            this.label_trade_item2_quantity = new System.Windows.Forms.Label();
            this.label_trade_item1_quantity = new System.Windows.Forms.Label();
            this.label_trade_item0_quantity = new System.Windows.Forms.Label();
            this.label_trade_item6 = new System.Windows.Forms.Label();
            this.label_trade_item5 = new System.Windows.Forms.Label();
            this.label_trade_item4 = new System.Windows.Forms.Label();
            this.label_trade_item3 = new System.Windows.Forms.Label();
            this.label_trade_item2 = new System.Windows.Forms.Label();
            this.label_trade_item1 = new System.Windows.Forms.Label();
            this.label_trade_item0 = new System.Windows.Forms.Label();
            this.button_action0 = new System.Windows.Forms.Button();
            this.button_action1 = new System.Windows.Forms.Button();
            this.button_action2 = new System.Windows.Forms.Button();
            this.button_action3 = new System.Windows.Forms.Button();
            this.label_arrow1 = new System.Windows.Forms.Label();
            this.label_arrow0 = new System.Windows.Forms.Label();
            this.button_endturn = new System.Windows.Forms.Button();
            this.comboBox_itemtarget = new System.Windows.Forms.ComboBox();
            this.comboBox_itemselection = new System.Windows.Forms.ComboBox();
            this.comboBox_attacktarget = new System.Windows.Forms.ComboBox();
            this.comboBox_attacktype = new System.Windows.Forms.ComboBox();
            this.groupBox_info.SuspendLayout();
            this.groupBox_inventory.SuspendLayout();
            this.groupBox_trade.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_info
            // 
            this.groupBox_info.Controls.Add(this.label_4thlevelspells_quantity);
            this.groupBox_info.Controls.Add(this.label_3rdlevelspells_quantity);
            this.groupBox_info.Controls.Add(this.label_2ndlevelspells_quantity);
            this.groupBox_info.Controls.Add(this.label_1st);
            this.groupBox_info.Controls.Add(this.label_money_quantity);
            this.groupBox_info.Controls.Add(this.label_exptillnextlevel_quantity);
            this.groupBox_info.Controls.Add(this.label_level_quantity);
            this.groupBox_info.Controls.Add(this.label_health_quantity);
            this.groupBox_info.Controls.Add(this.label_4thlevelspells);
            this.groupBox_info.Controls.Add(this.label_3rdlevelspells);
            this.groupBox_info.Controls.Add(this.label_2ndlevelspells);
            this.groupBox_info.Controls.Add(this.label_1stlevelspells);
            this.groupBox_info.Controls.Add(this.label_money);
            this.groupBox_info.Controls.Add(this.label_exptillnextlevel);
            this.groupBox_info.Controls.Add(this.label_level);
            this.groupBox_info.Controls.Add(this.label_health);
            this.groupBox_info.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_info.Location = new System.Drawing.Point(536, 12);
            this.groupBox_info.Name = "groupBox_info";
            this.groupBox_info.Size = new System.Drawing.Size(319, 569);
            this.groupBox_info.TabIndex = 0;
            this.groupBox_info.TabStop = false;
            this.groupBox_info.Text = "Information";
            // 
            // label_4thlevelspells_quantity
            // 
            this.label_4thlevelspells_quantity.AutoSize = true;
            this.label_4thlevelspells_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_4thlevelspells_quantity.Location = new System.Drawing.Point(243, 430);
            this.label_4thlevelspells_quantity.Name = "label_4thlevelspells_quantity";
            this.label_4thlevelspells_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_4thlevelspells_quantity.TabIndex = 22;
            this.label_4thlevelspells_quantity.Text = "0";
            // 
            // label_3rdlevelspells_quantity
            // 
            this.label_3rdlevelspells_quantity.AutoSize = true;
            this.label_3rdlevelspells_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_3rdlevelspells_quantity.Location = new System.Drawing.Point(243, 392);
            this.label_3rdlevelspells_quantity.Name = "label_3rdlevelspells_quantity";
            this.label_3rdlevelspells_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_3rdlevelspells_quantity.TabIndex = 21;
            this.label_3rdlevelspells_quantity.Text = "0";
            // 
            // label_2ndlevelspells_quantity
            // 
            this.label_2ndlevelspells_quantity.AutoSize = true;
            this.label_2ndlevelspells_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_2ndlevelspells_quantity.Location = new System.Drawing.Point(243, 354);
            this.label_2ndlevelspells_quantity.Name = "label_2ndlevelspells_quantity";
            this.label_2ndlevelspells_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_2ndlevelspells_quantity.TabIndex = 20;
            this.label_2ndlevelspells_quantity.Text = "0";
            // 
            // label_1st
            // 
            this.label_1st.AutoSize = true;
            this.label_1st.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_1st.Location = new System.Drawing.Point(243, 315);
            this.label_1st.Name = "label_1st";
            this.label_1st.Size = new System.Drawing.Size(24, 26);
            this.label_1st.TabIndex = 19;
            this.label_1st.Text = "0";
            // 
            // label_money_quantity
            // 
            this.label_money_quantity.AutoSize = true;
            this.label_money_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_money_quantity.Location = new System.Drawing.Point(243, 165);
            this.label_money_quantity.Name = "label_money_quantity";
            this.label_money_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_money_quantity.TabIndex = 11;
            this.label_money_quantity.Text = "0";
            // 
            // label_exptillnextlevel_quantity
            // 
            this.label_exptillnextlevel_quantity.AutoSize = true;
            this.label_exptillnextlevel_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_exptillnextlevel_quantity.Location = new System.Drawing.Point(243, 120);
            this.label_exptillnextlevel_quantity.Name = "label_exptillnextlevel_quantity";
            this.label_exptillnextlevel_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_exptillnextlevel_quantity.TabIndex = 10;
            this.label_exptillnextlevel_quantity.Text = "0";
            // 
            // label_level_quantity
            // 
            this.label_level_quantity.AutoSize = true;
            this.label_level_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_level_quantity.Location = new System.Drawing.Point(243, 79);
            this.label_level_quantity.Name = "label_level_quantity";
            this.label_level_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_level_quantity.TabIndex = 9;
            this.label_level_quantity.Text = "0";
            // 
            // label_health_quantity
            // 
            this.label_health_quantity.AutoSize = true;
            this.label_health_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_health_quantity.Location = new System.Drawing.Point(243, 42);
            this.label_health_quantity.Name = "label_health_quantity";
            this.label_health_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_health_quantity.TabIndex = 8;
            this.label_health_quantity.Text = "0";
            // 
            // label_4thlevelspells
            // 
            this.label_4thlevelspells.AutoSize = true;
            this.label_4thlevelspells.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_4thlevelspells.Location = new System.Drawing.Point(6, 430);
            this.label_4thlevelspells.Name = "label_4thlevelspells";
            this.label_4thlevelspells.Size = new System.Drawing.Size(141, 26);
            this.label_4thlevelspells.TabIndex = 7;
            this.label_4thlevelspells.Text = "4th level spells";
            // 
            // label_3rdlevelspells
            // 
            this.label_3rdlevelspells.AutoSize = true;
            this.label_3rdlevelspells.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_3rdlevelspells.Location = new System.Drawing.Point(6, 392);
            this.label_3rdlevelspells.Name = "label_3rdlevelspells";
            this.label_3rdlevelspells.Size = new System.Drawing.Size(143, 26);
            this.label_3rdlevelspells.TabIndex = 6;
            this.label_3rdlevelspells.Text = "3rd level spells";
            // 
            // label_2ndlevelspells
            // 
            this.label_2ndlevelspells.AutoSize = true;
            this.label_2ndlevelspells.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_2ndlevelspells.Location = new System.Drawing.Point(6, 354);
            this.label_2ndlevelspells.Name = "label_2ndlevelspells";
            this.label_2ndlevelspells.Size = new System.Drawing.Size(144, 26);
            this.label_2ndlevelspells.TabIndex = 5;
            this.label_2ndlevelspells.Text = "2nd level spells";
            // 
            // label_1stlevelspells
            // 
            this.label_1stlevelspells.AutoSize = true;
            this.label_1stlevelspells.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_1stlevelspells.Location = new System.Drawing.Point(6, 315);
            this.label_1stlevelspells.Name = "label_1stlevelspells";
            this.label_1stlevelspells.Size = new System.Drawing.Size(137, 26);
            this.label_1stlevelspells.TabIndex = 4;
            this.label_1stlevelspells.Text = "1st level spells";
            // 
            // label_money
            // 
            this.label_money.AutoSize = true;
            this.label_money.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_money.Location = new System.Drawing.Point(6, 165);
            this.label_money.Name = "label_money";
            this.label_money.Size = new System.Drawing.Size(69, 26);
            this.label_money.TabIndex = 3;
            this.label_money.Text = "Money";
            // 
            // label_exptillnextlevel
            // 
            this.label_exptillnextlevel.AutoSize = true;
            this.label_exptillnextlevel.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_exptillnextlevel.Location = new System.Drawing.Point(6, 120);
            this.label_exptillnextlevel.Name = "label_exptillnextlevel";
            this.label_exptillnextlevel.Size = new System.Drawing.Size(167, 26);
            this.label_exptillnextlevel.TabIndex = 2;
            this.label_exptillnextlevel.Text = "Exp. till next level";
            // 
            // label_level
            // 
            this.label_level.AutoSize = true;
            this.label_level.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_level.Location = new System.Drawing.Point(6, 79);
            this.label_level.Name = "label_level";
            this.label_level.Size = new System.Drawing.Size(56, 26);
            this.label_level.TabIndex = 1;
            this.label_level.Text = "Level";
            // 
            // label_health
            // 
            this.label_health.AutoSize = true;
            this.label_health.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_health.Location = new System.Drawing.Point(6, 42);
            this.label_health.Name = "label_health";
            this.label_health.Size = new System.Drawing.Size(70, 26);
            this.label_health.TabIndex = 0;
            this.label_health.Text = "Health";
            // 
            // groupBox_inventory
            // 
            this.groupBox_inventory.Controls.Add(this.label_item9_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item8_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item7_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item6_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item5_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item4_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item3_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item2_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item1_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item0_quantity);
            this.groupBox_inventory.Controls.Add(this.label_item9);
            this.groupBox_inventory.Controls.Add(this.label_item8);
            this.groupBox_inventory.Controls.Add(this.label_item7);
            this.groupBox_inventory.Controls.Add(this.label_item6);
            this.groupBox_inventory.Controls.Add(this.label_item5);
            this.groupBox_inventory.Controls.Add(this.label_item4);
            this.groupBox_inventory.Controls.Add(this.label_item3);
            this.groupBox_inventory.Controls.Add(this.label_item2);
            this.groupBox_inventory.Controls.Add(this.label_item1);
            this.groupBox_inventory.Controls.Add(this.label_item0);
            this.groupBox_inventory.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_inventory.Location = new System.Drawing.Point(861, 12);
            this.groupBox_inventory.Name = "groupBox_inventory";
            this.groupBox_inventory.Size = new System.Drawing.Size(273, 309);
            this.groupBox_inventory.TabIndex = 1;
            this.groupBox_inventory.TabStop = false;
            this.groupBox_inventory.Text = "Inventory";
            // 
            // label_item9_quantity
            // 
            this.label_item9_quantity.AutoSize = true;
            this.label_item9_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item9_quantity.Location = new System.Drawing.Point(237, 261);
            this.label_item9_quantity.Name = "label_item9_quantity";
            this.label_item9_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item9_quantity.TabIndex = 19;
            this.label_item9_quantity.Text = "0";
            // 
            // label_item8_quantity
            // 
            this.label_item8_quantity.AutoSize = true;
            this.label_item8_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item8_quantity.Location = new System.Drawing.Point(237, 235);
            this.label_item8_quantity.Name = "label_item8_quantity";
            this.label_item8_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item8_quantity.TabIndex = 18;
            this.label_item8_quantity.Text = "0";
            // 
            // label_item7_quantity
            // 
            this.label_item7_quantity.AutoSize = true;
            this.label_item7_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item7_quantity.Location = new System.Drawing.Point(237, 209);
            this.label_item7_quantity.Name = "label_item7_quantity";
            this.label_item7_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item7_quantity.TabIndex = 17;
            this.label_item7_quantity.Text = "0";
            // 
            // label_item6_quantity
            // 
            this.label_item6_quantity.AutoSize = true;
            this.label_item6_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item6_quantity.Location = new System.Drawing.Point(237, 183);
            this.label_item6_quantity.Name = "label_item6_quantity";
            this.label_item6_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item6_quantity.TabIndex = 16;
            this.label_item6_quantity.Text = "0";
            // 
            // label_item5_quantity
            // 
            this.label_item5_quantity.AutoSize = true;
            this.label_item5_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item5_quantity.Location = new System.Drawing.Point(237, 157);
            this.label_item5_quantity.Name = "label_item5_quantity";
            this.label_item5_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item5_quantity.TabIndex = 15;
            this.label_item5_quantity.Text = "0";
            // 
            // label_item4_quantity
            // 
            this.label_item4_quantity.AutoSize = true;
            this.label_item4_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item4_quantity.Location = new System.Drawing.Point(237, 131);
            this.label_item4_quantity.Name = "label_item4_quantity";
            this.label_item4_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item4_quantity.TabIndex = 14;
            this.label_item4_quantity.Text = "0";
            // 
            // label_item3_quantity
            // 
            this.label_item3_quantity.AutoSize = true;
            this.label_item3_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item3_quantity.Location = new System.Drawing.Point(237, 105);
            this.label_item3_quantity.Name = "label_item3_quantity";
            this.label_item3_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item3_quantity.TabIndex = 13;
            this.label_item3_quantity.Text = "0";
            // 
            // label_item2_quantity
            // 
            this.label_item2_quantity.AutoSize = true;
            this.label_item2_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item2_quantity.Location = new System.Drawing.Point(237, 79);
            this.label_item2_quantity.Name = "label_item2_quantity";
            this.label_item2_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item2_quantity.TabIndex = 12;
            this.label_item2_quantity.Text = "0";
            // 
            // label_item1_quantity
            // 
            this.label_item1_quantity.AutoSize = true;
            this.label_item1_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item1_quantity.Location = new System.Drawing.Point(237, 53);
            this.label_item1_quantity.Name = "label_item1_quantity";
            this.label_item1_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item1_quantity.TabIndex = 11;
            this.label_item1_quantity.Text = "0";
            // 
            // label_item0_quantity
            // 
            this.label_item0_quantity.AutoSize = true;
            this.label_item0_quantity.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item0_quantity.Location = new System.Drawing.Point(237, 27);
            this.label_item0_quantity.Name = "label_item0_quantity";
            this.label_item0_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_item0_quantity.TabIndex = 10;
            this.label_item0_quantity.Text = "0";
            // 
            // label_item9
            // 
            this.label_item9.AutoSize = true;
            this.label_item9.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item9.Location = new System.Drawing.Point(6, 261);
            this.label_item9.Name = "label_item9";
            this.label_item9.Size = new System.Drawing.Size(20, 26);
            this.label_item9.TabIndex = 9;
            this.label_item9.Text = "-";
            // 
            // label_item8
            // 
            this.label_item8.AutoSize = true;
            this.label_item8.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item8.Location = new System.Drawing.Point(6, 235);
            this.label_item8.Name = "label_item8";
            this.label_item8.Size = new System.Drawing.Size(20, 26);
            this.label_item8.TabIndex = 8;
            this.label_item8.Text = "-";
            // 
            // label_item7
            // 
            this.label_item7.AutoSize = true;
            this.label_item7.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item7.Location = new System.Drawing.Point(6, 209);
            this.label_item7.Name = "label_item7";
            this.label_item7.Size = new System.Drawing.Size(20, 26);
            this.label_item7.TabIndex = 7;
            this.label_item7.Text = "-";
            // 
            // label_item6
            // 
            this.label_item6.AutoSize = true;
            this.label_item6.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item6.Location = new System.Drawing.Point(6, 183);
            this.label_item6.Name = "label_item6";
            this.label_item6.Size = new System.Drawing.Size(20, 26);
            this.label_item6.TabIndex = 6;
            this.label_item6.Text = "-";
            // 
            // label_item5
            // 
            this.label_item5.AutoSize = true;
            this.label_item5.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item5.Location = new System.Drawing.Point(6, 157);
            this.label_item5.Name = "label_item5";
            this.label_item5.Size = new System.Drawing.Size(20, 26);
            this.label_item5.TabIndex = 5;
            this.label_item5.Text = "-";
            // 
            // label_item4
            // 
            this.label_item4.AutoSize = true;
            this.label_item4.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item4.Location = new System.Drawing.Point(6, 131);
            this.label_item4.Name = "label_item4";
            this.label_item4.Size = new System.Drawing.Size(20, 26);
            this.label_item4.TabIndex = 4;
            this.label_item4.Text = "-";
            // 
            // label_item3
            // 
            this.label_item3.AutoSize = true;
            this.label_item3.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item3.Location = new System.Drawing.Point(6, 105);
            this.label_item3.Name = "label_item3";
            this.label_item3.Size = new System.Drawing.Size(20, 26);
            this.label_item3.TabIndex = 3;
            this.label_item3.Text = "-";
            // 
            // label_item2
            // 
            this.label_item2.AutoSize = true;
            this.label_item2.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item2.Location = new System.Drawing.Point(6, 79);
            this.label_item2.Name = "label_item2";
            this.label_item2.Size = new System.Drawing.Size(20, 26);
            this.label_item2.TabIndex = 2;
            this.label_item2.Text = "-";
            // 
            // label_item1
            // 
            this.label_item1.AutoSize = true;
            this.label_item1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item1.Location = new System.Drawing.Point(6, 53);
            this.label_item1.Name = "label_item1";
            this.label_item1.Size = new System.Drawing.Size(20, 26);
            this.label_item1.TabIndex = 1;
            this.label_item1.Text = "-";
            // 
            // label_item0
            // 
            this.label_item0.AutoSize = true;
            this.label_item0.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_item0.Location = new System.Drawing.Point(6, 27);
            this.label_item0.Name = "label_item0";
            this.label_item0.Size = new System.Drawing.Size(20, 26);
            this.label_item0.TabIndex = 0;
            this.label_item0.Text = "-";
            // 
            // textBox_general
            // 
            this.textBox_general.Location = new System.Drawing.Point(16, 15);
            this.textBox_general.Multiline = true;
            this.textBox_general.Name = "textBox_general";
            this.textBox_general.ReadOnly = true;
            this.textBox_general.Size = new System.Drawing.Size(507, 565);
            this.textBox_general.TabIndex = 3;
            // 
            // textBox_queue
            // 
            this.textBox_queue.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_queue.Location = new System.Drawing.Point(16, 15);
            this.textBox_queue.Multiline = true;
            this.textBox_queue.Name = "textBox_queue";
            this.textBox_queue.ReadOnly = true;
            this.textBox_queue.Size = new System.Drawing.Size(211, 374);
            this.textBox_queue.TabIndex = 4;
            this.textBox_queue.Visible = false;
            // 
            // textBox_battle_log
            // 
            this.textBox_battle_log.Location = new System.Drawing.Point(233, 15);
            this.textBox_battle_log.Multiline = true;
            this.textBox_battle_log.Name = "textBox_battle_log";
            this.textBox_battle_log.ReadOnly = true;
            this.textBox_battle_log.Size = new System.Drawing.Size(290, 374);
            this.textBox_battle_log.TabIndex = 5;
            this.textBox_battle_log.Visible = false;
            // 
            // groupBox_trade
            // 
            this.groupBox_trade.Controls.Add(this.button_trade_cancel);
            this.groupBox_trade.Controls.Add(this.button_trade_buy);
            this.groupBox_trade.Controls.Add(this.label_trade_sum_value);
            this.groupBox_trade.Controls.Add(this.label_trade_sum);
            this.groupBox_trade.Controls.Add(this.button_trade_item6_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item5_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item4_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item3_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item2_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item1_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item0_more);
            this.groupBox_trade.Controls.Add(this.button_trade_item6_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item5_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item4_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item3_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item2_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item1_less);
            this.groupBox_trade.Controls.Add(this.button_trade_item0_less);
            this.groupBox_trade.Controls.Add(this.label_trade_item6_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item5_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item4_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item3_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item2_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item1_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item0_quantity);
            this.groupBox_trade.Controls.Add(this.label_trade_item6);
            this.groupBox_trade.Controls.Add(this.label_trade_item5);
            this.groupBox_trade.Controls.Add(this.label_trade_item4);
            this.groupBox_trade.Controls.Add(this.label_trade_item3);
            this.groupBox_trade.Controls.Add(this.label_trade_item2);
            this.groupBox_trade.Controls.Add(this.label_trade_item1);
            this.groupBox_trade.Controls.Add(this.label_trade_item0);
            this.groupBox_trade.Enabled = false;
            this.groupBox_trade.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_trade.Location = new System.Drawing.Point(861, 327);
            this.groupBox_trade.Name = "groupBox_trade";
            this.groupBox_trade.Size = new System.Drawing.Size(435, 291);
            this.groupBox_trade.TabIndex = 14;
            this.groupBox_trade.TabStop = false;
            this.groupBox_trade.Text = "Trade";
            this.groupBox_trade.Visible = false;
            // 
            // button_trade_cancel
            // 
            this.button_trade_cancel.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.button_trade_cancel.Location = new System.Drawing.Point(280, 247);
            this.button_trade_cancel.Name = "button_trade_cancel";
            this.button_trade_cancel.Size = new System.Drawing.Size(113, 41);
            this.button_trade_cancel.TabIndex = 31;
            this.button_trade_cancel.Text = "Cancel";
            this.button_trade_cancel.UseVisualStyleBackColor = true;
            // 
            // button_trade_buy
            // 
            this.button_trade_buy.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.button_trade_buy.Location = new System.Drawing.Point(161, 247);
            this.button_trade_buy.Name = "button_trade_buy";
            this.button_trade_buy.Size = new System.Drawing.Size(113, 41);
            this.button_trade_buy.TabIndex = 30;
            this.button_trade_buy.Text = "Buy";
            this.button_trade_buy.UseVisualStyleBackColor = true;
            // 
            // label_trade_sum_value
            // 
            this.label_trade_sum_value.AutoSize = true;
            this.label_trade_sum_value.Location = new System.Drawing.Point(97, 262);
            this.label_trade_sum_value.Name = "label_trade_sum_value";
            this.label_trade_sum_value.Size = new System.Drawing.Size(24, 26);
            this.label_trade_sum_value.TabIndex = 29;
            this.label_trade_sum_value.Text = "0";
            // 
            // label_trade_sum
            // 
            this.label_trade_sum.AutoSize = true;
            this.label_trade_sum.Location = new System.Drawing.Point(6, 262);
            this.label_trade_sum.Name = "label_trade_sum";
            this.label_trade_sum.Size = new System.Drawing.Size(58, 26);
            this.label_trade_sum.TabIndex = 28;
            this.label_trade_sum.Text = "Total";
            // 
            // button_trade_item6_more
            // 
            this.button_trade_item6_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item6_more.Location = new System.Drawing.Point(327, 212);
            this.button_trade_item6_more.Name = "button_trade_item6_more";
            this.button_trade_item6_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item6_more.TabIndex = 27;
            this.button_trade_item6_more.Text = "+";
            this.button_trade_item6_more.UseVisualStyleBackColor = true;
            // 
            // button_trade_item5_more
            // 
            this.button_trade_item5_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item5_more.Location = new System.Drawing.Point(327, 182);
            this.button_trade_item5_more.Name = "button_trade_item5_more";
            this.button_trade_item5_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item5_more.TabIndex = 26;
            this.button_trade_item5_more.Text = "+";
            this.button_trade_item5_more.UseVisualStyleBackColor = true;
            // 
            // button_trade_item4_more
            // 
            this.button_trade_item4_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item4_more.Location = new System.Drawing.Point(327, 152);
            this.button_trade_item4_more.Name = "button_trade_item4_more";
            this.button_trade_item4_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item4_more.TabIndex = 25;
            this.button_trade_item4_more.Text = "+";
            this.button_trade_item4_more.UseVisualStyleBackColor = true;
            // 
            // button_trade_item3_more
            // 
            this.button_trade_item3_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item3_more.Location = new System.Drawing.Point(327, 124);
            this.button_trade_item3_more.Name = "button_trade_item3_more";
            this.button_trade_item3_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item3_more.TabIndex = 24;
            this.button_trade_item3_more.Text = "+";
            this.button_trade_item3_more.UseVisualStyleBackColor = true;
            // 
            // button_trade_item2_more
            // 
            this.button_trade_item2_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item2_more.Location = new System.Drawing.Point(327, 96);
            this.button_trade_item2_more.Name = "button_trade_item2_more";
            this.button_trade_item2_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item2_more.TabIndex = 23;
            this.button_trade_item2_more.Text = "+";
            this.button_trade_item2_more.UseVisualStyleBackColor = true;
            // 
            // button_trade_item1_more
            // 
            this.button_trade_item1_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item1_more.Location = new System.Drawing.Point(327, 68);
            this.button_trade_item1_more.Name = "button_trade_item1_more";
            this.button_trade_item1_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item1_more.TabIndex = 22;
            this.button_trade_item1_more.Text = "+";
            this.button_trade_item1_more.UseVisualStyleBackColor = true;
            // 
            // button_trade_item0_more
            // 
            this.button_trade_item0_more.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item0_more.Location = new System.Drawing.Point(327, 39);
            this.button_trade_item0_more.Name = "button_trade_item0_more";
            this.button_trade_item0_more.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item0_more.TabIndex = 21;
            this.button_trade_item0_more.Text = "+";
            this.button_trade_item0_more.UseVisualStyleBackColor = true;
            // 
            // button_trade_item6_less
            // 
            this.button_trade_item6_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item6_less.Location = new System.Drawing.Point(222, 212);
            this.button_trade_item6_less.Name = "button_trade_item6_less";
            this.button_trade_item6_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item6_less.TabIndex = 20;
            this.button_trade_item6_less.Text = "-";
            this.button_trade_item6_less.UseVisualStyleBackColor = true;
            // 
            // button_trade_item5_less
            // 
            this.button_trade_item5_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item5_less.Location = new System.Drawing.Point(222, 182);
            this.button_trade_item5_less.Name = "button_trade_item5_less";
            this.button_trade_item5_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item5_less.TabIndex = 19;
            this.button_trade_item5_less.Text = "-";
            this.button_trade_item5_less.UseVisualStyleBackColor = true;
            // 
            // button_trade_item4_less
            // 
            this.button_trade_item4_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item4_less.Location = new System.Drawing.Point(222, 152);
            this.button_trade_item4_less.Name = "button_trade_item4_less";
            this.button_trade_item4_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item4_less.TabIndex = 18;
            this.button_trade_item4_less.Text = "-";
            this.button_trade_item4_less.UseVisualStyleBackColor = true;
            // 
            // button_trade_item3_less
            // 
            this.button_trade_item3_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item3_less.Location = new System.Drawing.Point(222, 124);
            this.button_trade_item3_less.Name = "button_trade_item3_less";
            this.button_trade_item3_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item3_less.TabIndex = 17;
            this.button_trade_item3_less.Text = "-";
            this.button_trade_item3_less.UseVisualStyleBackColor = true;
            // 
            // button_trade_item2_less
            // 
            this.button_trade_item2_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item2_less.Location = new System.Drawing.Point(222, 96);
            this.button_trade_item2_less.Name = "button_trade_item2_less";
            this.button_trade_item2_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item2_less.TabIndex = 16;
            this.button_trade_item2_less.Text = "-";
            this.button_trade_item2_less.UseVisualStyleBackColor = true;
            // 
            // button_trade_item1_less
            // 
            this.button_trade_item1_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item1_less.Location = new System.Drawing.Point(222, 68);
            this.button_trade_item1_less.Name = "button_trade_item1_less";
            this.button_trade_item1_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item1_less.TabIndex = 15;
            this.button_trade_item1_less.Text = "-";
            this.button_trade_item1_less.UseVisualStyleBackColor = true;
            // 
            // button_trade_item0_less
            // 
            this.button_trade_item0_less.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_trade_item0_less.Location = new System.Drawing.Point(222, 39);
            this.button_trade_item0_less.Name = "button_trade_item0_less";
            this.button_trade_item0_less.Size = new System.Drawing.Size(52, 26);
            this.button_trade_item0_less.TabIndex = 14;
            this.button_trade_item0_less.Text = "-";
            this.button_trade_item0_less.UseVisualStyleBackColor = true;
            // 
            // label_trade_item6_quantity
            // 
            this.label_trade_item6_quantity.AutoSize = true;
            this.label_trade_item6_quantity.Location = new System.Drawing.Point(289, 210);
            this.label_trade_item6_quantity.Name = "label_trade_item6_quantity";
            this.label_trade_item6_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item6_quantity.TabIndex = 13;
            this.label_trade_item6_quantity.Text = "0";
            // 
            // label_trade_item5_quantity
            // 
            this.label_trade_item5_quantity.AutoSize = true;
            this.label_trade_item5_quantity.Location = new System.Drawing.Point(289, 180);
            this.label_trade_item5_quantity.Name = "label_trade_item5_quantity";
            this.label_trade_item5_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item5_quantity.TabIndex = 12;
            this.label_trade_item5_quantity.Text = "0";
            // 
            // label_trade_item4_quantity
            // 
            this.label_trade_item4_quantity.AutoSize = true;
            this.label_trade_item4_quantity.Location = new System.Drawing.Point(289, 150);
            this.label_trade_item4_quantity.Name = "label_trade_item4_quantity";
            this.label_trade_item4_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item4_quantity.TabIndex = 11;
            this.label_trade_item4_quantity.Text = "0";
            // 
            // label_trade_item3_quantity
            // 
            this.label_trade_item3_quantity.AutoSize = true;
            this.label_trade_item3_quantity.Location = new System.Drawing.Point(289, 122);
            this.label_trade_item3_quantity.Name = "label_trade_item3_quantity";
            this.label_trade_item3_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item3_quantity.TabIndex = 10;
            this.label_trade_item3_quantity.Text = "0";
            // 
            // label_trade_item2_quantity
            // 
            this.label_trade_item2_quantity.AutoSize = true;
            this.label_trade_item2_quantity.Location = new System.Drawing.Point(289, 94);
            this.label_trade_item2_quantity.Name = "label_trade_item2_quantity";
            this.label_trade_item2_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item2_quantity.TabIndex = 9;
            this.label_trade_item2_quantity.Text = "0";
            // 
            // label_trade_item1_quantity
            // 
            this.label_trade_item1_quantity.AutoSize = true;
            this.label_trade_item1_quantity.Location = new System.Drawing.Point(289, 66);
            this.label_trade_item1_quantity.Name = "label_trade_item1_quantity";
            this.label_trade_item1_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item1_quantity.TabIndex = 8;
            this.label_trade_item1_quantity.Text = "0";
            // 
            // label_trade_item0_quantity
            // 
            this.label_trade_item0_quantity.AutoSize = true;
            this.label_trade_item0_quantity.Location = new System.Drawing.Point(289, 37);
            this.label_trade_item0_quantity.Name = "label_trade_item0_quantity";
            this.label_trade_item0_quantity.Size = new System.Drawing.Size(24, 26);
            this.label_trade_item0_quantity.TabIndex = 7;
            this.label_trade_item0_quantity.Text = "0";
            // 
            // label_trade_item6
            // 
            this.label_trade_item6.AutoSize = true;
            this.label_trade_item6.Location = new System.Drawing.Point(6, 210);
            this.label_trade_item6.Name = "label_trade_item6";
            this.label_trade_item6.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item6.TabIndex = 6;
            this.label_trade_item6.Text = "-";
            // 
            // label_trade_item5
            // 
            this.label_trade_item5.AutoSize = true;
            this.label_trade_item5.Location = new System.Drawing.Point(6, 180);
            this.label_trade_item5.Name = "label_trade_item5";
            this.label_trade_item5.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item5.TabIndex = 5;
            this.label_trade_item5.Text = "-";
            // 
            // label_trade_item4
            // 
            this.label_trade_item4.AutoSize = true;
            this.label_trade_item4.Location = new System.Drawing.Point(6, 150);
            this.label_trade_item4.Name = "label_trade_item4";
            this.label_trade_item4.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item4.TabIndex = 4;
            this.label_trade_item4.Text = "-";
            // 
            // label_trade_item3
            // 
            this.label_trade_item3.AutoSize = true;
            this.label_trade_item3.Location = new System.Drawing.Point(6, 122);
            this.label_trade_item3.Name = "label_trade_item3";
            this.label_trade_item3.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item3.TabIndex = 3;
            this.label_trade_item3.Text = "-";
            // 
            // label_trade_item2
            // 
            this.label_trade_item2.AutoSize = true;
            this.label_trade_item2.Location = new System.Drawing.Point(6, 94);
            this.label_trade_item2.Name = "label_trade_item2";
            this.label_trade_item2.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item2.TabIndex = 2;
            this.label_trade_item2.Text = "-";
            // 
            // label_trade_item1
            // 
            this.label_trade_item1.AutoSize = true;
            this.label_trade_item1.Location = new System.Drawing.Point(6, 66);
            this.label_trade_item1.Name = "label_trade_item1";
            this.label_trade_item1.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item1.TabIndex = 1;
            this.label_trade_item1.Text = "-";
            // 
            // label_trade_item0
            // 
            this.label_trade_item0.AutoSize = true;
            this.label_trade_item0.Location = new System.Drawing.Point(6, 37);
            this.label_trade_item0.Name = "label_trade_item0";
            this.label_trade_item0.Size = new System.Drawing.Size(20, 26);
            this.label_trade_item0.TabIndex = 0;
            this.label_trade_item0.Text = "-";
            // 
            // button_action0
            // 
            this.button_action0.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.button_action0.Location = new System.Drawing.Point(16, 595);
            this.button_action0.Name = "button_action0";
            this.button_action0.Size = new System.Drawing.Size(507, 34);
            this.button_action0.TabIndex = 15;
            this.button_action0.Text = "button1";
            this.button_action0.UseVisualStyleBackColor = true;
            // 
            // button_action1
            // 
            this.button_action1.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.button_action1.Location = new System.Drawing.Point(16, 635);
            this.button_action1.Name = "button_action1";
            this.button_action1.Size = new System.Drawing.Size(507, 34);
            this.button_action1.TabIndex = 16;
            this.button_action1.Text = "button2";
            this.button_action1.UseVisualStyleBackColor = true;
            // 
            // button_action2
            // 
            this.button_action2.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.button_action2.Location = new System.Drawing.Point(16, 675);
            this.button_action2.Name = "button_action2";
            this.button_action2.Size = new System.Drawing.Size(507, 34);
            this.button_action2.TabIndex = 17;
            this.button_action2.Text = "button3";
            this.button_action2.UseVisualStyleBackColor = true;
            // 
            // button_action3
            // 
            this.button_action3.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.button_action3.Location = new System.Drawing.Point(16, 715);
            this.button_action3.Name = "button_action3";
            this.button_action3.Size = new System.Drawing.Size(507, 34);
            this.button_action3.TabIndex = 18;
            this.button_action3.Text = "button4";
            this.button_action3.UseVisualStyleBackColor = true;
            // 
            // label_arrow1
            // 
            this.label_arrow1.AutoSize = true;
            this.label_arrow1.BackColor = System.Drawing.Color.White;
            this.label_arrow1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_arrow1.Location = new System.Drawing.Point(189, 500);
            this.label_arrow1.Name = "label_arrow1";
            this.label_arrow1.Size = new System.Drawing.Size(19, 26);
            this.label_arrow1.TabIndex = 28;
            this.label_arrow1.Text = ">";
            this.label_arrow1.Visible = false;
            // 
            // label_arrow0
            // 
            this.label_arrow0.AutoSize = true;
            this.label_arrow0.BackColor = System.Drawing.Color.White;
            this.label_arrow0.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_arrow0.Location = new System.Drawing.Point(189, 444);
            this.label_arrow0.Name = "label_arrow0";
            this.label_arrow0.Size = new System.Drawing.Size(19, 26);
            this.label_arrow0.TabIndex = 27;
            this.label_arrow0.Text = ">";
            this.label_arrow0.Visible = false;
            // 
            // button_endturn
            // 
            this.button_endturn.Enabled = false;
            this.button_endturn.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_endturn.Location = new System.Drawing.Point(386, 466);
            this.button_endturn.Name = "button_endturn";
            this.button_endturn.Size = new System.Drawing.Size(121, 36);
            this.button_endturn.TabIndex = 26;
            this.button_endturn.Text = "End turn";
            this.button_endturn.UseVisualStyleBackColor = true;
            this.button_endturn.Visible = false;
            // 
            // comboBox_itemtarget
            // 
            this.comboBox_itemtarget.Enabled = false;
            this.comboBox_itemtarget.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_itemtarget.FormattingEnabled = true;
            this.comboBox_itemtarget.Location = new System.Drawing.Point(220, 496);
            this.comboBox_itemtarget.Name = "comboBox_itemtarget";
            this.comboBox_itemtarget.Size = new System.Drawing.Size(144, 34);
            this.comboBox_itemtarget.TabIndex = 25;
            this.comboBox_itemtarget.Visible = false;
            // 
            // comboBox_itemselection
            // 
            this.comboBox_itemselection.Enabled = false;
            this.comboBox_itemselection.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_itemselection.FormattingEnabled = true;
            this.comboBox_itemselection.Items.AddRange(new object[] {
            "Ничего"});
            this.comboBox_itemselection.Location = new System.Drawing.Point(31, 496);
            this.comboBox_itemselection.Name = "comboBox_itemselection";
            this.comboBox_itemselection.Size = new System.Drawing.Size(144, 34);
            this.comboBox_itemselection.TabIndex = 24;
            this.comboBox_itemselection.Visible = false;
            // 
            // comboBox_attacktarget
            // 
            this.comboBox_attacktarget.Enabled = false;
            this.comboBox_attacktarget.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_attacktarget.FormattingEnabled = true;
            this.comboBox_attacktarget.Location = new System.Drawing.Point(220, 440);
            this.comboBox_attacktarget.Name = "comboBox_attacktarget";
            this.comboBox_attacktarget.Size = new System.Drawing.Size(144, 34);
            this.comboBox_attacktarget.TabIndex = 23;
            this.comboBox_attacktarget.Visible = false;
            // 
            // comboBox_attacktype
            // 
            this.comboBox_attacktype.Enabled = false;
            this.comboBox_attacktype.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_attacktype.FormattingEnabled = true;
            this.comboBox_attacktype.Items.AddRange(new object[] {
            "Атака",
            "Заклинание",
            "Отступить"});
            this.comboBox_attacktype.Location = new System.Drawing.Point(31, 440);
            this.comboBox_attacktype.Name = "comboBox_attacktype";
            this.comboBox_attacktype.Size = new System.Drawing.Size(144, 34);
            this.comboBox_attacktype.TabIndex = 22;
            this.comboBox_attacktype.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1337, 761);
            this.Controls.Add(this.textBox_battle_log);
            this.Controls.Add(this.textBox_queue);
            this.Controls.Add(this.label_arrow1);
            this.Controls.Add(this.label_arrow0);
            this.Controls.Add(this.button_endturn);
            this.Controls.Add(this.comboBox_itemtarget);
            this.Controls.Add(this.comboBox_itemselection);
            this.Controls.Add(this.comboBox_attacktarget);
            this.Controls.Add(this.comboBox_attacktype);
            this.Controls.Add(this.button_action3);
            this.Controls.Add(this.button_action2);
            this.Controls.Add(this.button_action1);
            this.Controls.Add(this.button_action0);
            this.Controls.Add(this.groupBox_trade);
            this.Controls.Add(this.textBox_general);
            this.Controls.Add(this.groupBox_inventory);
            this.Controls.Add(this.groupBox_info);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.groupBox_info.ResumeLayout(false);
            this.groupBox_info.PerformLayout();
            this.groupBox_inventory.ResumeLayout(false);
            this.groupBox_inventory.PerformLayout();
            this.groupBox_trade.ResumeLayout(false);
            this.groupBox_trade.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_info;
        private System.Windows.Forms.GroupBox groupBox_inventory;
        private System.Windows.Forms.TextBox textBox_general;
        private System.Windows.Forms.TextBox textBox_queue;
        private System.Windows.Forms.TextBox textBox_battle_log;
        private System.Windows.Forms.Label label_item9;
        private System.Windows.Forms.Label label_item8;
        private System.Windows.Forms.Label label_item7;
        private System.Windows.Forms.Label label_item6;
        private System.Windows.Forms.Label label_item5;
        private System.Windows.Forms.Label label_item4;
        private System.Windows.Forms.Label label_item3;
        private System.Windows.Forms.Label label_item2;
        private System.Windows.Forms.Label label_item1;
        private System.Windows.Forms.Label label_item0;
        private System.Windows.Forms.Label label_item9_quantity;
        private System.Windows.Forms.Label label_item8_quantity;
        private System.Windows.Forms.Label label_item7_quantity;
        private System.Windows.Forms.Label label_item6_quantity;
        private System.Windows.Forms.Label label_item5_quantity;
        private System.Windows.Forms.Label label_item4_quantity;
        private System.Windows.Forms.Label label_item3_quantity;
        private System.Windows.Forms.Label label_item2_quantity;
        private System.Windows.Forms.Label label_item1_quantity;
        private System.Windows.Forms.Label label_item0_quantity;
        private System.Windows.Forms.GroupBox groupBox_trade;
        private System.Windows.Forms.Button button_trade_cancel;
        private System.Windows.Forms.Button button_trade_buy;
        private System.Windows.Forms.Label label_trade_sum_value;
        private System.Windows.Forms.Label label_trade_sum;
        private System.Windows.Forms.Button button_trade_item6_more;
        private System.Windows.Forms.Button button_trade_item5_more;
        private System.Windows.Forms.Button button_trade_item4_more;
        private System.Windows.Forms.Button button_trade_item3_more;
        private System.Windows.Forms.Button button_trade_item2_more;
        private System.Windows.Forms.Button button_trade_item1_more;
        private System.Windows.Forms.Button button_trade_item0_more;
        private System.Windows.Forms.Button button_trade_item6_less;
        private System.Windows.Forms.Button button_trade_item5_less;
        private System.Windows.Forms.Button button_trade_item4_less;
        private System.Windows.Forms.Button button_trade_item3_less;
        private System.Windows.Forms.Button button_trade_item2_less;
        private System.Windows.Forms.Button button_trade_item1_less;
        private System.Windows.Forms.Button button_trade_item0_less;
        private System.Windows.Forms.Label label_trade_item6_quantity;
        private System.Windows.Forms.Label label_trade_item5_quantity;
        private System.Windows.Forms.Label label_trade_item4_quantity;
        private System.Windows.Forms.Label label_trade_item3_quantity;
        private System.Windows.Forms.Label label_trade_item2_quantity;
        private System.Windows.Forms.Label label_trade_item1_quantity;
        private System.Windows.Forms.Label label_trade_item0_quantity;
        private System.Windows.Forms.Label label_trade_item6;
        private System.Windows.Forms.Label label_trade_item5;
        private System.Windows.Forms.Label label_trade_item4;
        private System.Windows.Forms.Label label_trade_item3;
        private System.Windows.Forms.Label label_trade_item2;
        private System.Windows.Forms.Label label_trade_item1;
        private System.Windows.Forms.Label label_trade_item0;
        private System.Windows.Forms.Button button_action0;
        private System.Windows.Forms.Button button_action1;
        private System.Windows.Forms.Button button_action2;
        private System.Windows.Forms.Button button_action3;
        private System.Windows.Forms.Label label_level;
        private System.Windows.Forms.Label label_health;
        private System.Windows.Forms.Label label_4thlevelspells_quantity;
        private System.Windows.Forms.Label label_3rdlevelspells_quantity;
        private System.Windows.Forms.Label label_2ndlevelspells_quantity;
        private System.Windows.Forms.Label label_1st;
        private System.Windows.Forms.Label label_money_quantity;
        private System.Windows.Forms.Label label_exptillnextlevel_quantity;
        private System.Windows.Forms.Label label_level_quantity;
        private System.Windows.Forms.Label label_health_quantity;
        private System.Windows.Forms.Label label_4thlevelspells;
        private System.Windows.Forms.Label label_3rdlevelspells;
        private System.Windows.Forms.Label label_2ndlevelspells;
        private System.Windows.Forms.Label label_1stlevelspells;
        private System.Windows.Forms.Label label_money;
        private System.Windows.Forms.Label label_exptillnextlevel;
        private System.Windows.Forms.Label label_arrow1;
        private System.Windows.Forms.Label label_arrow0;
        private System.Windows.Forms.Button button_endturn;
        private System.Windows.Forms.ComboBox comboBox_itemtarget;
        private System.Windows.Forms.ComboBox comboBox_itemselection;
        private System.Windows.Forms.ComboBox comboBox_attacktarget;
        private System.Windows.Forms.ComboBox comboBox_attacktype;
    }
}