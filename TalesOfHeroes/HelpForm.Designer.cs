﻿namespace TalesOfHeroes
{
    partial class HelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage_links = new System.Windows.Forms.TabPage();
            this.label_armour = new System.Windows.Forms.Label();
            this.label_weapons = new System.Windows.Forms.Label();
            this.label_spells = new System.Windows.Forms.Label();
            this.label_backgrounds = new System.Windows.Forms.Label();
            this.label_classes = new System.Windows.Forms.Label();
            this.tabPage_classes = new System.Windows.Forms.TabPage();
            this.label_bard = new System.Windows.Forms.Label();
            this.label_cleric = new System.Windows.Forms.Label();
            this.label_druid = new System.Windows.Forms.Label();
            this.label_fighter = new System.Windows.Forms.Label();
            this.label_monk = new System.Windows.Forms.Label();
            this.label_paladin = new System.Windows.Forms.Label();
            this.label_ranger = new System.Windows.Forms.Label();
            this.label_rogue = new System.Windows.Forms.Label();
            this.label_sorcerer = new System.Windows.Forms.Label();
            this.label_warlock = new System.Windows.Forms.Label();
            this.label_wizard = new System.Windows.Forms.Label();
            this.label_barbarian = new System.Windows.Forms.Label();
            this.textBox_classes = new System.Windows.Forms.TextBox();
            this.tabPage_backgrounds = new System.Windows.Forms.TabPage();
            this.textBox_backgrounds = new System.Windows.Forms.TextBox();
            this.label_criminal = new System.Windows.Forms.Label();
            this.label_entertainer = new System.Windows.Forms.Label();
            this.label_guildartisan = new System.Windows.Forms.Label();
            this.label_folkhero = new System.Windows.Forms.Label();
            this.label_hermit = new System.Windows.Forms.Label();
            this.label_outlander = new System.Windows.Forms.Label();
            this.label_sage = new System.Windows.Forms.Label();
            this.label_sailor = new System.Windows.Forms.Label();
            this.label_soldier = new System.Windows.Forms.Label();
            this.label_noble = new System.Windows.Forms.Label();
            this.label_charlatan = new System.Windows.Forms.Label();
            this.label_urchin = new System.Windows.Forms.Label();
            this.label_acolyte = new System.Windows.Forms.Label();
            this.tabPage_spells = new System.Windows.Forms.TabPage();
            this.tabPage_weapons = new System.Windows.Forms.TabPage();
            this.tabPage_armour = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabControl.SuspendLayout();
            this.tabPage_links.SuspendLayout();
            this.tabPage_classes.SuspendLayout();
            this.tabPage_backgrounds.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage_links);
            this.tabControl.Controls.Add(this.tabPage_classes);
            this.tabControl.Controls.Add(this.tabPage_backgrounds);
            this.tabControl.Controls.Add(this.tabPage_spells);
            this.tabControl.Controls.Add(this.tabPage_weapons);
            this.tabControl.Controls.Add(this.tabPage_armour);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.Location = new System.Drawing.Point(3, 1);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(951, 374);
            this.tabControl.TabIndex = 0;
            // 
            // tabPage_links
            // 
            this.tabPage_links.Controls.Add(this.label_armour);
            this.tabPage_links.Controls.Add(this.label_weapons);
            this.tabPage_links.Controls.Add(this.label_spells);
            this.tabPage_links.Controls.Add(this.label_backgrounds);
            this.tabPage_links.Controls.Add(this.label_classes);
            this.tabPage_links.Location = new System.Drawing.Point(4, 31);
            this.tabPage_links.Name = "tabPage_links";
            this.tabPage_links.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_links.Size = new System.Drawing.Size(943, 339);
            this.tabPage_links.TabIndex = 0;
            this.tabPage_links.Text = "Links";
            this.tabPage_links.UseVisualStyleBackColor = true;
            // 
            // label_armour
            // 
            this.label_armour.AutoSize = true;
            this.label_armour.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_armour.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_armour.Location = new System.Drawing.Point(6, 210);
            this.label_armour.Name = "label_armour";
            this.label_armour.Size = new System.Drawing.Size(72, 22);
            this.label_armour.TabIndex = 4;
            this.label_armour.Text = "Armour";
            // 
            // label_weapons
            // 
            this.label_weapons.AutoSize = true;
            this.label_weapons.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_weapons.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weapons.Location = new System.Drawing.Point(6, 169);
            this.label_weapons.Name = "label_weapons";
            this.label_weapons.Size = new System.Drawing.Size(84, 22);
            this.label_weapons.TabIndex = 3;
            this.label_weapons.Text = "Weapons";
            // 
            // label_spells
            // 
            this.label_spells.AutoSize = true;
            this.label_spells.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_spells.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_spells.Location = new System.Drawing.Point(6, 129);
            this.label_spells.Name = "label_spells";
            this.label_spells.Size = new System.Drawing.Size(56, 22);
            this.label_spells.TabIndex = 2;
            this.label_spells.Text = "Spells";
            // 
            // label_backgrounds
            // 
            this.label_backgrounds.AutoSize = true;
            this.label_backgrounds.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_backgrounds.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_backgrounds.Location = new System.Drawing.Point(6, 87);
            this.label_backgrounds.Name = "label_backgrounds";
            this.label_backgrounds.Size = new System.Drawing.Size(110, 22);
            this.label_backgrounds.TabIndex = 1;
            this.label_backgrounds.Text = "Backgrounds";
            // 
            // label_classes
            // 
            this.label_classes.AutoSize = true;
            this.label_classes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_classes.Font = new System.Drawing.Font("Baskerville Old Face", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_classes.Location = new System.Drawing.Point(6, 47);
            this.label_classes.Name = "label_classes";
            this.label_classes.Size = new System.Drawing.Size(66, 22);
            this.label_classes.TabIndex = 0;
            this.label_classes.Text = "Classes";
            this.label_classes.Click += new System.EventHandler(this.label_classes_Click);
            // 
            // tabPage_classes
            // 
            this.tabPage_classes.Controls.Add(this.label_bard);
            this.tabPage_classes.Controls.Add(this.label_cleric);
            this.tabPage_classes.Controls.Add(this.label_druid);
            this.tabPage_classes.Controls.Add(this.label_fighter);
            this.tabPage_classes.Controls.Add(this.label_monk);
            this.tabPage_classes.Controls.Add(this.label_paladin);
            this.tabPage_classes.Controls.Add(this.label_ranger);
            this.tabPage_classes.Controls.Add(this.label_rogue);
            this.tabPage_classes.Controls.Add(this.label_sorcerer);
            this.tabPage_classes.Controls.Add(this.label_warlock);
            this.tabPage_classes.Controls.Add(this.label_wizard);
            this.tabPage_classes.Controls.Add(this.label_barbarian);
            this.tabPage_classes.Controls.Add(this.textBox_classes);
            this.tabPage_classes.Location = new System.Drawing.Point(4, 31);
            this.tabPage_classes.Name = "tabPage_classes";
            this.tabPage_classes.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_classes.Size = new System.Drawing.Size(943, 339);
            this.tabPage_classes.TabIndex = 1;
            this.tabPage_classes.Text = "Classes";
            this.tabPage_classes.UseVisualStyleBackColor = true;
            // 
            // label_bard
            // 
            this.label_bard.AutoSize = true;
            this.label_bard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_bard.Location = new System.Drawing.Point(45, 78);
            this.label_bard.Name = "label_bard";
            this.label_bard.Size = new System.Drawing.Size(48, 22);
            this.label_bard.TabIndex = 12;
            this.label_bard.Text = "Bard";
            // 
            // label_cleric
            // 
            this.label_cleric.AutoSize = true;
            this.label_cleric.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_cleric.Location = new System.Drawing.Point(45, 116);
            this.label_cleric.Name = "label_cleric";
            this.label_cleric.Size = new System.Drawing.Size(57, 22);
            this.label_cleric.TabIndex = 11;
            this.label_cleric.Text = "Cleric";
            // 
            // label_druid
            // 
            this.label_druid.AutoSize = true;
            this.label_druid.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_druid.Location = new System.Drawing.Point(45, 154);
            this.label_druid.Name = "label_druid";
            this.label_druid.Size = new System.Drawing.Size(57, 22);
            this.label_druid.TabIndex = 10;
            this.label_druid.Text = "Druid";
            // 
            // label_fighter
            // 
            this.label_fighter.AutoSize = true;
            this.label_fighter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_fighter.Location = new System.Drawing.Point(45, 192);
            this.label_fighter.Name = "label_fighter";
            this.label_fighter.Size = new System.Drawing.Size(64, 22);
            this.label_fighter.TabIndex = 9;
            this.label_fighter.Text = "Fighter";
            // 
            // label_monk
            // 
            this.label_monk.AutoSize = true;
            this.label_monk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_monk.Location = new System.Drawing.Point(45, 230);
            this.label_monk.Name = "label_monk";
            this.label_monk.Size = new System.Drawing.Size(56, 22);
            this.label_monk.TabIndex = 8;
            this.label_monk.Text = "Monk";
            // 
            // label_paladin
            // 
            this.label_paladin.AutoSize = true;
            this.label_paladin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_paladin.Location = new System.Drawing.Point(152, 40);
            this.label_paladin.Name = "label_paladin";
            this.label_paladin.Size = new System.Drawing.Size(67, 22);
            this.label_paladin.TabIndex = 7;
            this.label_paladin.Text = "Paladin";
            // 
            // label_ranger
            // 
            this.label_ranger.AutoSize = true;
            this.label_ranger.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_ranger.Location = new System.Drawing.Point(152, 78);
            this.label_ranger.Name = "label_ranger";
            this.label_ranger.Size = new System.Drawing.Size(64, 22);
            this.label_ranger.TabIndex = 6;
            this.label_ranger.Text = "Ranger";
            // 
            // label_rogue
            // 
            this.label_rogue.AutoSize = true;
            this.label_rogue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_rogue.Location = new System.Drawing.Point(152, 115);
            this.label_rogue.Name = "label_rogue";
            this.label_rogue.Size = new System.Drawing.Size(59, 22);
            this.label_rogue.TabIndex = 5;
            this.label_rogue.Text = "Rogue";
            // 
            // label_sorcerer
            // 
            this.label_sorcerer.AutoSize = true;
            this.label_sorcerer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_sorcerer.Location = new System.Drawing.Point(152, 153);
            this.label_sorcerer.Name = "label_sorcerer";
            this.label_sorcerer.Size = new System.Drawing.Size(77, 22);
            this.label_sorcerer.TabIndex = 4;
            this.label_sorcerer.Text = "Sorcerer";
            // 
            // label_warlock
            // 
            this.label_warlock.AutoSize = true;
            this.label_warlock.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_warlock.Location = new System.Drawing.Point(152, 191);
            this.label_warlock.Name = "label_warlock";
            this.label_warlock.Size = new System.Drawing.Size(77, 22);
            this.label_warlock.TabIndex = 3;
            this.label_warlock.Text = "Warlock";
            // 
            // label_wizard
            // 
            this.label_wizard.AutoSize = true;
            this.label_wizard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_wizard.Location = new System.Drawing.Point(152, 229);
            this.label_wizard.Name = "label_wizard";
            this.label_wizard.Size = new System.Drawing.Size(68, 22);
            this.label_wizard.TabIndex = 2;
            this.label_wizard.Text = "Wizard";
            // 
            // label_barbarian
            // 
            this.label_barbarian.AutoSize = true;
            this.label_barbarian.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_barbarian.Location = new System.Drawing.Point(45, 40);
            this.label_barbarian.Name = "label_barbarian";
            this.label_barbarian.Size = new System.Drawing.Size(86, 22);
            this.label_barbarian.TabIndex = 1;
            this.label_barbarian.Text = "Barbarian";
            // 
            // textBox_classes
            // 
            this.textBox_classes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_classes.Location = new System.Drawing.Point(316, 6);
            this.textBox_classes.Multiline = true;
            this.textBox_classes.Name = "textBox_classes";
            this.textBox_classes.Size = new System.Drawing.Size(605, 311);
            this.textBox_classes.TabIndex = 0;
            // 
            // tabPage_backgrounds
            // 
            this.tabPage_backgrounds.Controls.Add(this.textBox_backgrounds);
            this.tabPage_backgrounds.Controls.Add(this.label_criminal);
            this.tabPage_backgrounds.Controls.Add(this.label_entertainer);
            this.tabPage_backgrounds.Controls.Add(this.label_guildartisan);
            this.tabPage_backgrounds.Controls.Add(this.label_folkhero);
            this.tabPage_backgrounds.Controls.Add(this.label_hermit);
            this.tabPage_backgrounds.Controls.Add(this.label_outlander);
            this.tabPage_backgrounds.Controls.Add(this.label_sage);
            this.tabPage_backgrounds.Controls.Add(this.label_sailor);
            this.tabPage_backgrounds.Controls.Add(this.label_soldier);
            this.tabPage_backgrounds.Controls.Add(this.label_noble);
            this.tabPage_backgrounds.Controls.Add(this.label_charlatan);
            this.tabPage_backgrounds.Controls.Add(this.label_urchin);
            this.tabPage_backgrounds.Controls.Add(this.label_acolyte);
            this.tabPage_backgrounds.Location = new System.Drawing.Point(4, 31);
            this.tabPage_backgrounds.Name = "tabPage_backgrounds";
            this.tabPage_backgrounds.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_backgrounds.Size = new System.Drawing.Size(943, 339);
            this.tabPage_backgrounds.TabIndex = 2;
            this.tabPage_backgrounds.Text = "Backgrounds";
            this.tabPage_backgrounds.UseVisualStyleBackColor = true;
            // 
            // textBox_backgrounds
            // 
            this.textBox_backgrounds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_backgrounds.Location = new System.Drawing.Point(338, 21);
            this.textBox_backgrounds.Multiline = true;
            this.textBox_backgrounds.Name = "textBox_backgrounds";
            this.textBox_backgrounds.Size = new System.Drawing.Size(420, 279);
            this.textBox_backgrounds.TabIndex = 16;
            // 
            // label_criminal
            // 
            this.label_criminal.AutoSize = true;
            this.label_criminal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_criminal.Location = new System.Drawing.Point(18, 88);
            this.label_criminal.Name = "label_criminal";
            this.label_criminal.Size = new System.Drawing.Size(78, 22);
            this.label_criminal.TabIndex = 15;
            this.label_criminal.Text = "Criminal";
            // 
            // label_entertainer
            // 
            this.label_entertainer.AutoSize = true;
            this.label_entertainer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_entertainer.Location = new System.Drawing.Point(18, 121);
            this.label_entertainer.Name = "label_entertainer";
            this.label_entertainer.Size = new System.Drawing.Size(97, 22);
            this.label_entertainer.TabIndex = 14;
            this.label_entertainer.Text = "Entertainer";
            // 
            // label_guildartisan
            // 
            this.label_guildartisan.AutoSize = true;
            this.label_guildartisan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_guildartisan.Location = new System.Drawing.Point(138, 23);
            this.label_guildartisan.Name = "label_guildartisan";
            this.label_guildartisan.Size = new System.Drawing.Size(114, 22);
            this.label_guildartisan.TabIndex = 13;
            this.label_guildartisan.Text = "Guild Artisan";
            // 
            // label_folkhero
            // 
            this.label_folkhero.AutoSize = true;
            this.label_folkhero.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_folkhero.Location = new System.Drawing.Point(18, 159);
            this.label_folkhero.Name = "label_folkhero";
            this.label_folkhero.Size = new System.Drawing.Size(90, 22);
            this.label_folkhero.TabIndex = 12;
            this.label_folkhero.Text = "Folk Hero";
            // 
            // label_hermit
            // 
            this.label_hermit.AutoSize = true;
            this.label_hermit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_hermit.Location = new System.Drawing.Point(18, 200);
            this.label_hermit.Name = "label_hermit";
            this.label_hermit.Size = new System.Drawing.Size(66, 22);
            this.label_hermit.TabIndex = 11;
            this.label_hermit.Text = "Hermit";
            // 
            // label_outlander
            // 
            this.label_outlander.AutoSize = true;
            this.label_outlander.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_outlander.Location = new System.Drawing.Point(138, 85);
            this.label_outlander.Name = "label_outlander";
            this.label_outlander.Size = new System.Drawing.Size(89, 22);
            this.label_outlander.TabIndex = 10;
            this.label_outlander.Text = "Outlander";
            // 
            // label_sage
            // 
            this.label_sage.AutoSize = true;
            this.label_sage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_sage.Location = new System.Drawing.Point(138, 119);
            this.label_sage.Name = "label_sage";
            this.label_sage.Size = new System.Drawing.Size(45, 22);
            this.label_sage.TabIndex = 9;
            this.label_sage.Text = "Sage";
            // 
            // label_sailor
            // 
            this.label_sailor.AutoSize = true;
            this.label_sailor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_sailor.Location = new System.Drawing.Point(138, 157);
            this.label_sailor.Name = "label_sailor";
            this.label_sailor.Size = new System.Drawing.Size(55, 22);
            this.label_sailor.TabIndex = 8;
            this.label_sailor.Text = "Sailor";
            // 
            // label_soldier
            // 
            this.label_soldier.AutoSize = true;
            this.label_soldier.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_soldier.Location = new System.Drawing.Point(138, 192);
            this.label_soldier.Name = "label_soldier";
            this.label_soldier.Size = new System.Drawing.Size(66, 22);
            this.label_soldier.TabIndex = 7;
            this.label_soldier.Text = "Soldier";
            // 
            // label_noble
            // 
            this.label_noble.AutoSize = true;
            this.label_noble.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_noble.Location = new System.Drawing.Point(138, 53);
            this.label_noble.Name = "label_noble";
            this.label_noble.Size = new System.Drawing.Size(58, 22);
            this.label_noble.TabIndex = 6;
            this.label_noble.Text = "Noble";
            // 
            // label_charlatan
            // 
            this.label_charlatan.AutoSize = true;
            this.label_charlatan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_charlatan.Location = new System.Drawing.Point(18, 53);
            this.label_charlatan.Name = "label_charlatan";
            this.label_charlatan.Size = new System.Drawing.Size(84, 22);
            this.label_charlatan.TabIndex = 5;
            this.label_charlatan.Text = "Charlatan";
            // 
            // label_urchin
            // 
            this.label_urchin.AutoSize = true;
            this.label_urchin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_urchin.Location = new System.Drawing.Point(138, 229);
            this.label_urchin.Name = "label_urchin";
            this.label_urchin.Size = new System.Drawing.Size(64, 22);
            this.label_urchin.TabIndex = 4;
            this.label_urchin.Text = "Urchin";
            // 
            // label_acolyte
            // 
            this.label_acolyte.AutoSize = true;
            this.label_acolyte.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_acolyte.Location = new System.Drawing.Point(18, 21);
            this.label_acolyte.Name = "label_acolyte";
            this.label_acolyte.Size = new System.Drawing.Size(68, 22);
            this.label_acolyte.TabIndex = 2;
            this.label_acolyte.Text = "Acolyte";
            // 
            // tabPage_spells
            // 
            this.tabPage_spells.Location = new System.Drawing.Point(4, 31);
            this.tabPage_spells.Name = "tabPage_spells";
            this.tabPage_spells.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_spells.Size = new System.Drawing.Size(943, 339);
            this.tabPage_spells.TabIndex = 3;
            this.tabPage_spells.Text = "Spells";
            this.tabPage_weapons.UseVisualStyleBackColor = true;
            // 
            // tabPage_weapons
            // 
            this.tabPage_weapons.Location = new System.Drawing.Point(4, 31);
            this.tabPage_weapons.Name = "tabPage_weapons";
            this.tabPage_weapons.Size = new System.Drawing.Size(943, 339);
            this.tabPage_weapons.TabIndex = 4;
            this.tabPage_weapons.Text = "Weapons";
            this.tabPage_weapons.UseVisualStyleBackColor = true;
            // 
            // tabPage_armour
            // 
            this.tabPage_armour.Location = new System.Drawing.Point(4, 31);
            this.tabPage_armour.Name = "tabPage_armour";
            this.tabPage_armour.Size = new System.Drawing.Size(943, 339);
            this.tabPage_armour.TabIndex = 5;
            this.tabPage_armour.Text = "Armour";
            this.tabPage_armour.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 31);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(943, 339);
            this.tabPage3.TabIndex = 6;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 31);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(943, 339);
            this.tabPage5.TabIndex = 7;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // HelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 381);
            this.Controls.Add(this.tabControl);
            this.Name = "HelpForm";
            this.Text = "HelpForm";
            this.Load += new System.EventHandler(this.HelpForm_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPage_links.ResumeLayout(false);
            this.tabPage_links.PerformLayout();
            this.tabPage_classes.ResumeLayout(false);
            this.tabPage_classes.PerformLayout();
            this.tabPage_backgrounds.ResumeLayout(false);
            this.tabPage_backgrounds.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage_links;
        private System.Windows.Forms.TabPage tabPage_classes;
        private System.Windows.Forms.TabPage tabPage_backgrounds;
        private System.Windows.Forms.TabPage tabPage_spells;
        private System.Windows.Forms.Label label_classes;
        private System.Windows.Forms.Label label_armour;
        private System.Windows.Forms.Label label_weapons;
        private System.Windows.Forms.Label label_spells;
        private System.Windows.Forms.Label label_backgrounds;
        private System.Windows.Forms.Label label_barbarian;
        private System.Windows.Forms.TextBox textBox_classes;
        private System.Windows.Forms.Label label_bard;
        private System.Windows.Forms.Label label_cleric;
        private System.Windows.Forms.Label label_druid;
        private System.Windows.Forms.Label label_fighter;
        private System.Windows.Forms.Label label_monk;
        private System.Windows.Forms.Label label_paladin;
        private System.Windows.Forms.Label label_ranger;
        private System.Windows.Forms.Label label_rogue;
        private System.Windows.Forms.Label label_sorcerer;
        private System.Windows.Forms.Label label_warlock;
        private System.Windows.Forms.Label label_wizard;
        private System.Windows.Forms.Label label_criminal;
        private System.Windows.Forms.Label label_entertainer;
        private System.Windows.Forms.Label label_guildartisan;
        private System.Windows.Forms.Label label_folkhero;
        private System.Windows.Forms.Label label_hermit;
        private System.Windows.Forms.Label label_outlander;
        private System.Windows.Forms.Label label_sage;
        private System.Windows.Forms.Label label_sailor;
        private System.Windows.Forms.Label label_soldier;
        private System.Windows.Forms.Label label_noble;
        private System.Windows.Forms.Label label_charlatan;
        private System.Windows.Forms.Label label_urchin;
        private System.Windows.Forms.Label label_acolyte;
        private System.Windows.Forms.TextBox textBox_backgrounds;
        private System.Windows.Forms.TabPage tabPage_weapons;
        private System.Windows.Forms.TabPage tabPage_armour;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage5;
    }
}